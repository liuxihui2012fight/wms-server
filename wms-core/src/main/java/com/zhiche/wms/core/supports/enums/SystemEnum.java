package com.zhiche.wms.core.supports.enums;

public enum SystemEnum {
    OTM("01", "OTM", "otm"),
    INTEGRATION("02", "INTEGRATION", "integration"),
    WMS("03", "WMS", "wms"),
    OMS("04", "OMS", "oms"),
    UNDERLINE("01", "_", "下滑横线");

    private final String index;
    private final String code;
    private final String desc;

    SystemEnum(String index, String code, String desc) {
        this.index = index;
        this.code = code;
        this.desc = desc;
    }

    public String getIndex() {
        return index;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
