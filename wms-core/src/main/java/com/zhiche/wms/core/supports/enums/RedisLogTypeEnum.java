package com.zhiche.wms.core.supports.enums;

public enum RedisLogTypeEnum {
    PUBLIC("00", "PUBLIC", "公共"),
    SHIPMENT("01", "SHIPMENT", "指令"),
    EVENT_EXPORT("02", "EVENT_EXPORT", "事件导出");

    private final String index;
    private final String code;
    private final String desc;

    RedisLogTypeEnum(String index, String code, String desc) {
        this.index = index;
        this.code = code;
        this.desc = desc;
    }

    public String getIndex() {
        return index;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
