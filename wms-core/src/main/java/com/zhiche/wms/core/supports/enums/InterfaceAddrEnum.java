package com.zhiche.wms.core.supports.enums;

public enum InterfaceAddrEnum {

    INBOUND_SCHECDULE_TMS("10", "/mSearchVINOrder.jspx", "TMS定时任务获取入库通知单数据接口地址"),
    INBOUND_ERP("11", "/mInware.jspx", "重庆前置库入库信息传递ERP"),
    SHIPMENT_SCHEDULE_TMS("20", "/mShipSearch.jspx", "TMS定时任务获取出库通知单数据接口地址"),
    UAA_SIGNUP_NORMAL("30", "/uaa/tenant/account/create", "uaa 注册用户地址"),
    UAA_UPDATE_PASSWORD("31", "/uaa/account/updatePassword", "uaa 更新用户登录密码接口地址"),
    EVENT_URI("50","/event/export","interguration提车/寻车/移车/入库/出库任务回传OTM接口地址"),
    PROCESS_RESULT_URI("51","/event/getProcessResult","interguration得到异步处理结果"),
    REGISTRATION_URL("60","/interface/adapter/abnormal_registration_sync","车辆异常信息同步推送otm是否可发"),
    VEHICLE_URL("70","/vehicleStandardMode/queryVehicleModelInfo","查询车型信息"),
    VEHICLE_SYNS_TOOTM_URL("71","/vehicleStandardMode/modifiedVehicleToOtm","修改车型同步OTM"),
    EXCEPTION_RTF_OTM("73","/handler/zlzc/wmsExceptionData","异常信息同步OTM"),
    UPDATE_SHIP_DATE_OTM("74","/handler/zlzc/getDispatchConfirmByRs","获取临牌发运时间"),
    SHIPDATE_TO_TMS("75","/huiyuncheInterface/shipConfirm","同步OTM人送发运时间给TMS"),
    QUERY_STORE_HOUSE("76","/origination/queryPageOrigination","获取仓库地址"),
    QUERY_WATERIRON_SHIP_DATE_OTM("77","/handler/zlzc/waterIronShipDate","获取水铁发运时间"),
    QUERYISSHIPMENT_OTM("78","/handler/zlzc/queryIsShipment","获取指令状态");

    private final String code;
    private final String address;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    public String getAddress() {
        return address;
    }

    InterfaceAddrEnum(String code, String address, String detail) {
        this.code = code;
        this.address = address;
        this.detail = detail;
    }

}
