package com.zhiche.wms.core.supports.enums;

public enum UomEnum {

    TAI("10", "台", "计量单位:台"),
    TON("20", "吨", "计量单位:吨"),
    KG("30", "千克", "计量单位:千克");

    private final String code;
    private final String name;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    public String getName() {
        return name;
    }

    UomEnum(String code, String name, String detail) {
        this.code = code;
        this.name = name;
        this.detail = detail;
    }
}
