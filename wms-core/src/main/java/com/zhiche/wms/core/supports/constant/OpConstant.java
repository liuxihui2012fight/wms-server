package com.zhiche.wms.core.supports.constant;

/**
 * 常量类
 */
public class OpConstant {

    public static final int VIN_LENGTH = 17;
    public static final String QR_BIND_ID_PREFIX = "QR";
    public static final String QR_BIND_DOC_PREFIX = "DOC_";
    public static final String EXCEPTION_DOC_PREFIX = "EXCEP_";
    public static final String STORAGE_SERVER = "QINIU";
    public static final String OUT_TYPE = "O";
    public static final String IN_TYPE = "I";

    private OpConstant() {

    }
}
