package com.zhiche.wms.core.utils.redislog;

import com.lisa.json.JSONUtil;
import com.lisa.syslog.SysLogUtil;
import com.lisa.syslog.model.SysLogDataPo;
import com.lisa.syslog.model.publics.PublicLog;
import com.zhiche.wms.core.supports.enums.RedisLogTypeEnum;
import com.zhiche.wms.core.supports.enums.SystemEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;


public class RedisLogUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisLogUtil.class);

    /**
     * 添加日志记录到日志系统
     */
    public static void addSysLogs(String sourceSystem, String targetSystem, String busEventKey, String reqParam, String uri, Integer dataStatus, Integer isSuc, String errCont, Boolean isTest) {
        try {
            if (busEventKey != null && reqParam != null) {
                String key = sourceSystem + "_" + targetSystem;
                String fKey = uri + SystemEnum.UNDERLINE.getCode() + busEventKey + SystemEnum.UNDERLINE.getCode() + dataStatus;
                // 业务相关实体类
                PublicLog publicLog = new PublicLog(sourceSystem, targetSystem, uri, busEventKey, reqParam, dataStatus, isSuc, 1, errCont);
                SysLogDataPo sysLogDataPo = new SysLogDataPo(SysLogUtil.PUBLIC_, JSONUtil.toJsonStr(publicLog));
                SysLogUtil.addSysLogs(key, fKey, sysLogDataPo, isTest);
            }
        } catch (Exception e) {
            LOGGER.error(busEventKey + "添加日志出现异常:", e.getMessage());
        }
    }

    /**
     * 添加日志记录到日志系统
     */
    public static void addExportLogs(String sourceSystem, String targetSystem, String eventType, String uri, String sourceKey, String lineSourceKey,
                              String vin, String busEventKey, String busEventSequence, String dtoJson, Integer isSuc, Integer dataStatus, String errCont, Boolean isTest) {
        try {
            if (busEventKey != null && busEventSequence != null) {
                String tableIdx = RedisLogTypeEnum.EVENT_EXPORT.getCode();
                String key = sourceSystem + "_" + targetSystem;
                String fKey = uri + SystemEnum.UNDERLINE.getCode() + busEventSequence + SystemEnum.UNDERLINE.getCode() + eventType + SystemEnum.UNDERLINE.getCode() + dataStatus;
                // 业务相关实体类
                LogExportPo logExportPo = new LogExportPo(sourceSystem, targetSystem, eventType, uri, sourceKey,
                        lineSourceKey, vin, busEventKey, busEventSequence, dtoJson, isSuc, errCont, 0, 0, dataStatus);
                SysLogDataPo sysLogDataPo = new SysLogDataPo(tableIdx, JSONUtil.toJsonStr(logExportPo));
                SysLogUtil.addSysLogs(key, fKey, sysLogDataPo, isTest);
            }
        } catch (Exception e) {
            LOGGER.error(busEventKey + "-" + busEventSequence + "事件导出至otm，添加日志异常" + e.getMessage());
        }
    }
}
