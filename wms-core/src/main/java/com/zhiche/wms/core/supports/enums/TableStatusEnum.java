package com.zhiche.wms.core.supports.enums;

public enum TableStatusEnum {

    STATUS_0("0", "状态0的标识"),
    STATUS_1("1", "状态1的标识"),
    STATUS_2("2", "状态2的标识"),
    STATUS_10("10", "状态10的标识"),
    STATUS_20("20", "状态20的标识"),
    STATUS_30("30", "状态30的标识"),
    STATUS_40("40", "状态40的标识"),
    STATUS_42("42", "状态42的标识"),
    STATUS_50("50", "状态50的标识"),
    STATUS_51("51", "状态51的标识"),
    STATUS_60("60", "状态60的标识"),
    STATUS_OR_CREATED("OR_CREATED","RELEASE-已下发"),
    STATUS_BS_CREATED("BS_CREATED","RELEASE-已调度"),
    STATUS_OR_FIND("OR_FIND","RELEASE-已寻车"),
    STATUS_WMS_MOVE("WMS_MOVE","RELEASE-已移车"),
    STATUS_WMS_PICKUP("WMS_PICKUP","RELEASE-开始提车"),
    STATUS_BS_INBOUND("BS_INBOUND","RELEASE-已入库"),
    STATUS_WMS_OUTBOUND("WMS_OUTBOUND","RELEASE-已出库"),
    STATUS_WMS_HANDOVER("WMS_HANDOVER","RELEASE-已交验"),
    STATUS_BS_DISPATCH("BS_DISPATCH","RELEASE-已发运"),
    STATUS_BS_PORTION("BS_PORTION","RELEASE-部分发运"),
    STATUS_BS_ARRIVAL("BS_ARRIVAL","RELEASE-已运抵"),
    STATUS_BS_POD("BS_POD","RELEASE-已回单"),
    STATUS_OR_CLOSED("OR_CLOSED","RELEASE-已结算"),
    STATUS_CANCLE_DISPATCH("CANCLE_DISPATCH","OMT取消发运"),
    STATUS_IS_SEEK("is_seek", "需寻车"),
    STATUS_IS_MOVE("is_move", "需移车"),
    STATUS_IS_PICK("is_pick", "需提车"),
    STATUS_IS_SHIP("is_ship", "需发运"),
    STATUS_Y("Y", "可发运"),
    STATUS_N("N", "不可发运"),
    STATUS_LT("<", "小于"),
    STATUS_LE("<=", "小于等于"),
    STATUS_GT(">", "大于"),
    STATUS_GE(">=", "大于等于"),
    CHANNEL_APP("APP", "App指令发运"),
    CHANNEL_PC("PC", "pc单台车发运（批量发运）"),
    CANCLE_INBOUND_SOURCE("WMSPC", "取消入库来源");

    private final String code;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    TableStatusEnum(String code, String detail) {
        this.code = code;
        this.detail = detail;
    }

}
