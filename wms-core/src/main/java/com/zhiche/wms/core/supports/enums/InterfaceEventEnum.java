package com.zhiche.wms.core.supports.enums;

public enum InterfaceEventEnum {
    BS_WMS_IN("01", "BS_WMS_IN", "事件类型:已入库"),
    BS_OP_DELIVERY("02", "BS_OP_DELIVERY", "事件类型:已发运"),
    BS_TRANS_ARRIVED("03", "BS_TRANS_ARRIVED", "事件类型:已运抵"),
    OR_FIND("04", "OR_FIND", "事件类型:已寻车"),
    BS_ENROUTED("05", "BS_ENROUTED", "事件类型:在途"),
    BINDING_CODE("06", "BINDING_CODE", "事件类型:绑码"),
    ABNORMAL("07", "ABNORMAL", "事件类型:异常"),
    BS_CANCLE_WMS_IN("08", "BS_CANCLE_WMS_IN", "取消入库");

    private final String index;
    private final String code;
    private final String desc;

    InterfaceEventEnum(String index, String code, String desc) {
        this.index = index;
        this.code = code;
        this.desc = desc;
    }

    public String getIndex() {
        return index;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
