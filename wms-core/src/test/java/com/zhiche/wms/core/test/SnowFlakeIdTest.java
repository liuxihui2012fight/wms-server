package com.zhiche.wms.core.test;

import com.zhiche.wms.core.utils.SnowFlakeId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author qichao
 * @create 2018-05-23
 **/
@RunWith(SpringJUnit4ClassRunner.class)
public class SnowFlakeIdTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SnowFlakeIdTest.class);

    @Test
    public void genIdTest () {
        SnowFlakeId snowFlakeId = new SnowFlakeId(0, 0);
        for (int i = 0; i < 10; i++) {
            LOGGER.info("generated id is {}:", snowFlakeId.nextId());
        }
    }
}
