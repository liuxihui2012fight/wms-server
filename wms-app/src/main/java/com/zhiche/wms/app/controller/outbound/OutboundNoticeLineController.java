package com.zhiche.wms.app.controller.outbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Maps;
import com.zhiche.wms.app.vo.AppQueryVo;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceVisitTypeEnum;
import com.zhiche.wms.domain.model.outbound.OutboundShipLine;
import com.zhiche.wms.dto.inbound.InboundDTO;
import com.zhiche.wms.dto.outbound.OutNoticeListResultDTO;
import com.zhiche.wms.dto.outbound.OutboundNoticeDTO;
import com.zhiche.wms.dto.outbound.OutboundShipDTO;
import com.zhiche.wms.service.constant.PutAwayType;
import com.zhiche.wms.service.constant.SourceSystem;
import com.zhiche.wms.service.outbound.IOutboundNoticeHeaderService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import com.zhiche.wms.service.outbound.IOutboundShipHeaderService;
import com.zhiche.wms.service.outbound.IOutboundShipLineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Api(value = "outboundNoticeLine-API", description = "出库分配接口")
@Controller
@RequestMapping(value = "/outboundNoticeLine")
public class OutboundNoticeLineController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutboundNoticeLineController.class);
    @Autowired
    private IOutboundNoticeLineService iOutboundNoticeLineService;
    @Autowired
    private IOutboundShipHeaderService iOutboundPutawayHeaderService;
    @Autowired
    private IOutboundShipLineService iOutboundPutawayLineService;
    @Autowired
    private IOutboundNoticeHeaderService noticeHeaderService;
    @Autowired
    private IOutboundShipLineService shipLineService;

    /**
     * 出库确认--模糊匹配
     */
    @PostMapping(value = "/selectByPage")
    @ResponseBody
    @ApiOperation(value = "模糊搜索出库通知单", notes = "模糊搜索出库通知单", response = OutboundNoticeDTO.class)
    public RestfulResponse<List<OutboundNoticeDTO>> selectByPage(AppQueryVo page) {
        LOGGER.info("selectByPage,Params：{}", page);
        RestfulResponse<List<OutboundNoticeDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            restfulResponse.setData(iOutboundNoticeLineService.selectOutboundsPage(page.getKey(), page.getHouseId(), page.getSize(), page.getCurrent()).getRecords());
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 查询单条出库信息
     */
    @PostMapping(value = "/selectById")
    @ResponseBody
    @ApiOperation(value = "查询单条出库信息", notes = "查询单条出库信息", response = OutboundNoticeDTO.class)
    public RestfulResponse<OutboundNoticeDTO> selectById(AppQueryVo page) {
        LOGGER.info("selectById,Params：{}", page);
        RestfulResponse<OutboundNoticeDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            //判断查询的方式
            if (InterfaceVisitTypeEnum.CLICK_TYPE.getCode().equals(page.getVisitType())) {
                //单击单条信息转入
                restfulResponse.setData(iOutboundNoticeLineService.selectOutbound(new Long(page.getKey()), page.getHouseId()));
            } else if (InterfaceVisitTypeEnum.SCAN_TYPE.getCode().equals(page.getVisitType())) {
                //扫码转入
                restfulResponse.setData(iOutboundNoticeLineService.selectInboundByQrCode(page.getKey(), page.getHouseId()));
            }
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 分配出库
     */
    @PostMapping(value = "/outbound")
    @ResponseBody
    @ApiOperation(value = "分配出库", notes = "分配出库", response = OutboundNoticeDTO.class)
    public RestfulResponse<OutboundShipDTO> updateValueNoticeLineInBound(AppQueryVo page) {
        LOGGER.info("updateValueNoticeLineInBound,Params：{}", page);
        RestfulResponse<OutboundShipDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            //进行出库分配
            List<OutboundShipLine> ss = shipLineService.shipByNoticeLineId(new Long(page.getKey()), PutAwayType.NOTICE_PUTAWAY,
                    SourceSystem.APP).getOutboundShipLineList();
            Long putwayLineId = ss.get(0).getId();
            //查询分配的出库单
            OutboundShipDTO putwaydto = iOutboundPutawayLineService.getPutWaylineById(putwayLineId);
            restfulResponse.setData(putwaydto);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 出库通知查询
     */
    @PostMapping("/getOutboundNoticeList")
    @ResponseBody
    public RestfulResponse<Page<OutNoticeListResultDTO>> getOutboundNoticeList(AppQueryVo appQueryVo) {
        RestfulResponse<Page<OutNoticeListResultDTO>> resultDTO = new RestfulResponse<>(0, "success");
        Page<OutNoticeListResultDTO> page = new Page<>();
        HashMap<String, Object> ct = Maps.newHashMap();
        page.setCondition(ct);
        page.getCondition().put("houseId",appQueryVo.getHouseId());
        page.getCondition().put("status",appQueryVo.getStatus());
        page.setCurrent(appQueryVo.getCurrent());
        page.setSize(appQueryVo.getSize());
        Page<OutNoticeListResultDTO> resultPage = noticeHeaderService.queryOutboundNotice(page);
        resultDTO.setData(resultPage);
        return resultDTO;
    }
}
