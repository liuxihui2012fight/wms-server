package com.zhiche.wms.app.controller.opbaas;


import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.zhiche.wms.app.controller.common.CommonExceptionController;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.base.ResultDTO;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;
import com.zhiche.wms.dto.opbaas.resultdto.TaskDetailResultDTO;
import com.zhiche.wms.service.opbaas.ExceptionToOTMService;
import com.zhiche.wms.service.opbaas.IExceptionRegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 异常登记 前端控制器
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@Api(value = "exceptionRegister-API", description = "异常登记操作接口")
@RestController
@RequestMapping("/exceptionRegister")
public class ExceptionRegisterController {

    @Autowired
    private IExceptionRegisterService registerService;
    @Autowired
    ExceptionToOTMService exceptionToOTMService;

    /**
     * 点击/扫描获取异常信息
     * <p>
     * 异常车辆信息和九宫格信息分离  此接口作废!
     * 获取车辆信息接口:
     * task/getOrderInfo输入  {@link TaskController#getTaskInfo}
     * 扫码task/getTaskDetailByQrCode {@link TaskController#getTaskDetailByQrCode}
     * 获取九宫格异常信息接口:commonException/getExceptionInfo {@link CommonExceptionController#getExceptionInfo}
     * </p>
     */
    @ApiOperation(value = "获取异常登记界面信息", notes = "获取异常登记界面信息")
    @PostMapping("getExceptionInfo")
    @Deprecated
    public ResultDTO<List<ExResultDTO>> getExceptionList(ExceptionRegisterParamDTO dto) {
        List<ExResultDTO> resultDTOList = registerService.getExCount(dto);
        return new ResultDTO<>(true, resultDTOList, "查询成功");
    }


    /**
     * <p>
     * 获取菜单下异常信息
     * </p>
     */
    @ApiOperation(value = "获取标记异常内容图片信息", notes = "获取标记异常内容图片信息")
    @PostMapping("exceptionPicUrl")
    public ResultDTO<List<ExResultDTO>> getExceptionPicUrl(ExceptionRegisterParamDTO dto) {
        List<ExResultDTO> dtoList = registerService.getExceptionPicUrl(dto);
        return new ResultDTO<>(true, dtoList, "操作成功");
    }


    /**
     * 异常标记
     */
    @ApiOperation(value = "标记异常", notes = "获取标记异常内容图片信息")
    @PostMapping("noteException")
    public ResultDTO<TaskDetailResultDTO> noteException(ExceptionRegisterParamDTO dto) {
        registerService.updateNoteException(dto);
        return new ResultDTO<>(true, null, "操作成功");
    }


    @ApiOperation(value = "异常登记该车是否可发", notes = "异常登记该车是否可发")
    @PostMapping("/exceptionFinish")
    public RestfulResponse<Object> getComponentInfo(CommonConditionParamDTO conditionParamDTO) {
        RestfulResponse<Object> restfulResponse = new RestfulResponse<>(0, "success", null);
        //获取异常信息类表
        List<CommonConditionParamDTO> conditions = exceptionToOTMService.setToOtmParam(conditionParamDTO);
        if(CollectionUtils.isNotEmpty(conditions)){
            for (CommonConditionParamDTO conditionParam : conditions){
                exceptionToOTMService.isCanSend(conditionParam);
            }
        }
        return restfulResponse;
    }
}

