package com.zhiche.wms.app.controller.inbound;

import com.zhiche.wms.app.controller.outbound.OutboundNoticeLineController;
import com.zhiche.wms.app.vo.AppQueryVo;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.inbound.InboundNoticeHeaderDTO;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.service.inbound.IInboundNoticeHeaderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 入库计划
 */
@Api(value = "inboundPlan-API", description = "入库计划接口")
@Controller
@RequestMapping(value = "/inboundPlan")
public class InboundPlanController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InboundPlanController.class);
    @Autowired
    private IInboundNoticeHeaderService inboundNoticeHeaderService;

    /**
     * 根据不同状态查询入库计划头信息
     */
    @PostMapping("/inboundPlanList")
    @ResponseBody
    @ApiOperation(value = "搜索不同状态的入库计划", notes = "搜索不同状态的入库计划", response = InboundNoticeHeaderDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status", value = "状态值"),
            @ApiImplicitParam(name = "houseId", value = "仓库号"),
            @ApiImplicitParam(name = "size", value = "每页数量"),
            @ApiImplicitParam(name = "current", value = "页数")
    })
    public RestfulResponse<List<InboundNoticeHeaderDTO>> selectByPage(AppQueryVo page) {
        RestfulResponse<List<InboundNoticeHeaderDTO>> restfulResponse = new RestfulResponse(0, "success", null);
        try {
            List<InboundNoticeHeaderDTO> list = inboundNoticeHeaderService.selectHeaderList(page.getHouseId(), page.getStatus().toString(), page.getSize(), page.getCurrent());
            restfulResponse.setData(list);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 查询入库计划匹配详明
     *
     */
    @PostMapping("/headDetail")
    @ResponseBody
    @ApiOperation(value = "查询入库计划的详明", notes = "查询入库计划的详明", response = InboundNoticeHeaderDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "单头id号"),
            @ApiImplicitParam(name = "houseId", value = "仓库号")
    })
    public RestfulResponse<InboundNoticeHeaderDTO> selectDetailByHeaderId(AppQueryVo page) {
        RestfulResponse<InboundNoticeHeaderDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            InboundNoticeHeaderDTO inboundNoticeHeader = inboundNoticeHeaderService.selectHeaderDeatil(page.getHouseId(), new Long(page.getKey()));
            restfulResponse.setData(inboundNoticeHeader);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }
}
