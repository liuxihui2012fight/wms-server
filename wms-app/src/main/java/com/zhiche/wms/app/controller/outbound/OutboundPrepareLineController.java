package com.zhiche.wms.app.controller.outbound;


import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.app.vo.AppQueryVo;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceVisitTypeEnum;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.dto.outbound.OutboundPrepareDTO;
import com.zhiche.wms.dto.outbound.OutboundPrepareVehicleDTO;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import com.zhiche.wms.service.outbound.IOutboundPrepareLineService;
import com.zhiche.wms.service.outbound.impl.OutboundNoticeHeaderServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 出库备料单明细 前端控制器
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
@Api(value = "outboundPrepareLine-API", description = "出库备料接口")
@Controller
@RequestMapping("/outboundPrepare")
public class OutboundPrepareLineController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutboundPrepareLineController.class);
    @Autowired
    private IOutboundPrepareLineService prepareLineService;
    @Autowired
    private IOutboundPrepareHeaderService prepareHeaderService;


    /**
     * 出库备料-模糊匹配
     */
    @PostMapping("/queryList")
    @ResponseBody
    @ApiOperation(value = "查询搜索未备料的出库通知", notes = "查询搜索未备料的出库通知", response = OutboundPrepareDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "模糊搜索条件"),
            @ApiImplicitParam(name = "houseId", value = "仓库号"),
            @ApiImplicitParam(name = "current", value = "页数"),
            @ApiImplicitParam(name = "size", value = "每页条数")
    })
    public RestfulResponse<List<OutboundPrepareDTO>> queryPageByLotId(AppQueryVo page) {
        RestfulResponse<List<OutboundPrepareDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            List<OutboundPrepareDTO> outboundPrepareDTOS = prepareLineService.queryPageByLotId(
                    page.getKey(),
                    page.getHouseId().toString(),
                    page.getCurrent(),
                    page.getSize());
            restfulResponse.setData(outboundPrepareDTOS);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    @PostMapping("/getPrepare")
    @ResponseBody
    @ApiOperation(value = "查询搜索未备料的出库通知", notes = "查询搜索未备料的出库通知", response = OutboundPrepareDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "模糊搜索条件"),
            @ApiImplicitParam(name = "houseId", value = "仓库号"),
            @ApiImplicitParam(name = "visitType", value = "查询的方式（单击进入，扫码进入）")
    })
    public RestfulResponse<OutboundPrepareDTO> getResultByNoticeId(AppQueryVo page) {
        RestfulResponse<OutboundPrepareDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            if (InterfaceVisitTypeEnum.CLICK_TYPE.getCode().equals(page.getVisitType())) {
                //如果是点击
                OutboundPrepareDTO prepare = prepareLineService.getPrepare(page.getKey(), page.getHouseId());
                restfulResponse.setData(prepare);
            } else if (InterfaceVisitTypeEnum.SCAN_TYPE.getCode().equals(page.getVisitType())) {
                //如果是扫码
                OutboundPrepareDTO prepare = prepareLineService.getPrepareBykey(page.getKey(), page.getHouseId());
                restfulResponse.setData(prepare);
            }
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    @PostMapping("/prepareConfirm")
    @ResponseBody
    public RestfulResponse<OutboundPrepareDTO> Inspect(AppQueryVo page) {
        LOGGER.info("InboundInspectLineController:inboundInspect:page-->{}", page.toString());
        RestfulResponse<OutboundPrepareDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            OutboundPrepareDTO outboundPrepareDTO = prepareLineService.updateStatus(page.getKey(), page.getHouseId());
            restfulResponse.setData(outboundPrepareDTO);
            restfulResponse.setMessage("备料开始");
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 实车校验
     */
    @PostMapping(value = "/realVehicleCheck")
    @ApiOperation(value = "实车校验")
    @ResponseBody
    public RestfulResponse<List<Map<String, String>>> realVehicleCheck (CommonConditionParamDTO conditions) {
        LOGGER.info("/realVehicleCheck （实车校验） params : {} ", conditions);
        RestfulResponse<List<Map<String, String>>> result = new RestfulResponse<>(0, "success", null);

        List<Map<String, String>> resultList = prepareLineService.realVehicleCheck(conditions);
        result.setData(resultList);
        return result;
    }

    /**
     * 一键领取任务
     */
    @PostMapping(value = "/getTheTask")
    @ApiOperation(value = "一键领取任务")
    @ResponseBody
    public RestfulResponse getTheTask (CommonConditionParamDTO conditions) {
        LOGGER.info("/getTheTask （一键领取任务） id : {} ", conditions.getCondition());
        RestfulResponse result = new RestfulResponse<>(0, "success", null);

        Map<String, String> condition = conditions.getCondition();
        if (Objects.isNull(condition)) {
            throw new BaseException("参数不能为空");
        }
        if (StringUtils.isEmpty(condition.get("headerId"))) {
            throw new BaseException("入参为空");
        }
        prepareLineService.batchUpdateStatus(condition.get("headerId"));
        return result;
    }


    /**
     * <p>
     * 出库备料调整,列表展示基于板车纬度 -hs
     * </p>
     */
    @PostMapping("/getPrepareVehicle")
    @ResponseBody
    public RestfulResponse<Page<OutboundPrepareVehicleDTO>> getPrepareVehicle(Page<OutboundPrepareVehicleDTO> page) {
        RestfulResponse<Page<OutboundPrepareVehicleDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            Page<OutboundPrepareVehicleDTO> data = prepareHeaderService.getPrepareVehicle(page);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OutboundPrepareHeaderServiceImpl.getPrepareVehicle BaseException:{}", be.getCode());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OutboundPrepareHeaderServiceImpl.getPrepareVehicle Exception:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    /**
     * <p>
     * 备料列表调整--根据板车号查询明细信息
     * </p>
     */
    @PostMapping("/getVehicleDetail")
    @ResponseBody
    public RestfulResponse<List<OutboundPrepareDTO>> getVehicleDetail(CommonConditionParamDTO dto) {
        RestfulResponse<List<OutboundPrepareDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            List<OutboundPrepareDTO> detail = prepareLineService.getVehicleDetail(dto);
            response.setData(detail);
        } catch (BaseException be) {
            LOGGER.error("OutboundPrepareHeaderServiceImpl.getVehicleDetail BaseException:{}", be.getCode());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OutboundPrepareHeaderServiceImpl.getVehicleDetail Exception:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

}


