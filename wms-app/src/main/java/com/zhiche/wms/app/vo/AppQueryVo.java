package com.zhiche.wms.app.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ser.std.ToStringSerializer;

import java.io.Serializable;

public class AppQueryVo implements Serializable {

    //主信息
    private String key;
    //仓库ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long houseId;
    //确定类型
    private String visitType;
    //分页大小
    private Integer size;
    //每页条数
    private Integer current;

    private Integer status;

    //是否异常发运
    private String isCanSend;

    //发车点名称
    private String originName;

    //系统运单号OR
    private String orderReleaseGid;

    private String estimatedProcessingTime;

    public String getEstimatedProcessingTime () {
        return estimatedProcessingTime;
    }

    public void setEstimatedProcessingTime (String estimatedProcessingTime) {
        this.estimatedProcessingTime = estimatedProcessingTime;
    }

    public String getOrderReleaseGid () {
        return orderReleaseGid;
    }

    public void setOrderReleaseGid (String orderReleaseGid) {
        this.orderReleaseGid = orderReleaseGid;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getIsCanSend() {
        return isCanSend;
    }

    public void setIsCanSend(String isCanSend) {
        this.isCanSend = isCanSend;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public AppQueryVo() {
    }

    public AppQueryVo(String key, Long houseId, String visitType, Integer size, Integer current) {
        this.key = key;
        this.houseId = houseId;
        this.visitType = visitType;
        this.size = size;
        this.current = current;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    @Override
    public String toString() {
        return "AppQueryVo{" +
                "key=" + key +
                ", houseId=" + houseId +
                ", visitType=" + visitType +
                ", size=" + size +
                ", current=" + current +
                "}";
    }
}
