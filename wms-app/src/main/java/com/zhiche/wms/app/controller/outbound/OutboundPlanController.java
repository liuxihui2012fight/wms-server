package com.zhiche.wms.app.controller.outbound;

import com.zhiche.wms.app.vo.AppQueryVo;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.inbound.InboundNoticeHeaderDTO;
import com.zhiche.wms.dto.outbound.OutboundNoticeHeaderDTO;
import com.zhiche.wms.service.outbound.IOutboundNoticeHeaderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 出库计划
 */
@Api(value = "outboundPlan-API", description = "出库计划接口")
@Controller
@RequestMapping(value = "/outboundPlan")
public class OutboundPlanController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutboundPlanController.class);

    @Autowired
    private IOutboundNoticeHeaderService outboundNoticeHeaderService;

    @PostMapping("/outboundPlanList")
    @ResponseBody
    @ApiOperation(value = "搜索不同状态的出库计划", notes = "搜索不同状态的出库计划", response = InboundNoticeHeaderDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status", value = "状态值"),
            @ApiImplicitParam(name = "houseId", value = "仓库号"),
            @ApiImplicitParam(name = "size", value = "每页数量"),
            @ApiImplicitParam(name = "current", value = "页数")}
    )
    public RestfulResponse<List<OutboundNoticeHeaderDTO>> selInboundPlanList(AppQueryVo page) {
        RestfulResponse<List<OutboundNoticeHeaderDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            List<OutboundNoticeHeaderDTO> list = outboundNoticeHeaderService.selectHeaderList(page.getHouseId(), page.getStatus().toString(), page.getSize(), page.getCurrent());
            restfulResponse.setData(list);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    @PostMapping("/headDetail")
    @ResponseBody
    @ApiOperation(value = "查询出库计划的详明", notes = "查询出库计划的详明", response = InboundNoticeHeaderDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "单头id号"),
            @ApiImplicitParam(name = "houseId", value = "仓库号")
    })
    public RestfulResponse<OutboundNoticeHeaderDTO> selInboundDeail(AppQueryVo page) {
        RestfulResponse<OutboundNoticeHeaderDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            OutboundNoticeHeaderDTO outboundNoticeHeader = outboundNoticeHeaderService.selectHeaderDetail(page.getHouseId(), new Long(page.getKey()));
            restfulResponse.setData(outboundNoticeHeader);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }
}
