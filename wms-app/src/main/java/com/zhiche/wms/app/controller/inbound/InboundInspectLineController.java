package com.zhiche.wms.app.controller.inbound;

import com.zhiche.wms.app.vo.AppQueryVo;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceVisitTypeEnum;
import com.zhiche.wms.dto.inbound.InboundInspectDTO;
import com.zhiche.wms.service.inbound.IInboundInspectLineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 入库审批
 */
@Api(value = "inboundInspectLine-API", description = "入库审批/质检接口")
@Controller
@RequestMapping(value = "/inboundInspectLine")
public class InboundInspectLineController {

    @Autowired
    private IInboundInspectLineService iInboundInspectLineService;

    /**
     * 入库质检--模糊查询
     *
     * @param page 自定义传参对象
     */
    @PostMapping(value = "/selectList")
    @ResponseBody
    @ApiOperation(value = "模糊查询未质检的入库通知信息", notes = "模糊查询未质检的入库通知信息", response = InboundInspectDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "模糊搜索关键字"),
            @ApiImplicitParam(name = "houseId", value = "仓库号"),
            @ApiImplicitParam(name = "size", value = "每页数量"),
            @ApiImplicitParam(name = "current", value = "页数")
    })
    public RestfulResponse<List<InboundInspectDTO>> selectInspectByPage(AppQueryVo page) {
        RestfulResponse<List<InboundInspectDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        List<InboundInspectDTO> list = iInboundInspectLineService.queryList(page.getKey(), page.getHouseId(), page.getSize(), page.getCurrent()).getRecords();
        restfulResponse.setData(list);

        return restfulResponse;
    }

    /**
     * 入库质检--获取详情
     */
    @PostMapping(value = "/selectDetail")
    @ResponseBody
    @ApiOperation(value = "根据id查询详情", notes = "根据id查询详情", response = InboundInspectDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "关键id号"),
            @ApiImplicitParam(name = "houseId", value = "仓库号"),
            @ApiImplicitParam(name = "visitType", value = "进入方式（SCAN扫码/CLICK单击）")
    })
    public RestfulResponse<InboundInspectDTO> selectInspectItem(AppQueryVo page) {
        RestfulResponse<InboundInspectDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        //判断查询的方式
        if (InterfaceVisitTypeEnum.CLICK_TYPE.getCode().equals(page.getVisitType())) {
            //如果是点击
            InboundInspectDTO inspect = iInboundInspectLineService.updateInspectInfo(page.getKey(), page.getHouseId());
            inspect.setTaskType("40");
            restfulResponse.setData(inspect);
        } else if (InterfaceVisitTypeEnum.SCAN_TYPE.getCode().equals(page.getVisitType())) {
            //如果是扫码
            InboundInspectDTO inspect = iInboundInspectLineService.updateInspectBykey(page.getKey(), page.getHouseId());
            inspect.setTaskType("40");
            restfulResponse.setData(inspect);
        }
        return restfulResponse;
    }

    /**
     * 入库质检-质检完成
     */
    @PostMapping(value = "/inboundInspect")
    @ResponseBody
    @ApiOperation(value = "质检结果确认", notes = "质检结果确认", response = InboundInspectDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "关键id号"),
            @ApiImplicitParam(name = "houseId", value = "仓库号"),
            @ApiImplicitParam(name = "status", value = "修改方式"),
            @ApiImplicitParam(name = "isCanSend", value = "是否异常发运")
    })
    public RestfulResponse<Integer> Inspect(AppQueryVo page) {
        RestfulResponse<Integer> restfulResponse = new RestfulResponse<>(0, "success", null);
        iInboundInspectLineService.updateInspectStatus(page.getKey(), page.getHouseId(), page.getStatus(),page.getIsCanSend(),
                page.getOrderReleaseGid(),page.getEstimatedProcessingTime());
        restfulResponse.setMessage("质检完成");
        return restfulResponse;
    }
}
