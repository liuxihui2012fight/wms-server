package com.zhiche.wms.app.controller.sys;

import com.zhiche.wms.app.vo.UserVo;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.service.opbaas.IDeliveryPointService;
import com.zhiche.wms.service.sys.IPermissionService;
import com.zhiche.wms.service.sys.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zhaoguixin on 18/06/24.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private IUserService userService;
    @Autowired
    private IPermissionService permissionService;

    @Autowired
    private IDeliveryPointService deliveryPointService;

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ResponseBody
    public RestfulResponse<User> userInfo(@RequestHeader("Authorization") String token) {
        RestfulResponse<User> result = new RestfulResponse<>(0, "success", null);
        try {
            User user = userService.getLoginUser();
            LOGGER.info("查询到登录用户返回用户信息user：{}",user.toString());

            UserVo userVo = new UserVo();
            BeanUtils.copyProperties(user, userVo);
            userVo.setPermissions(permissionService.listAppPermission(user.getId()));
            userVo.setStorehouses(userService.listUserStorehouse(user.getId()));
            userVo.setOpDeliveryPoints(deliveryPointService.userPoint(user.getId()));
            result.setData(userVo);
        } catch (Exception e) {
            LOGGER.error("UserController.accountInfo ERROR Message : {} ", e.getMessage());
            result.setCode(10003);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public RestfulResponse<Boolean> accountLogout() {
        RestfulResponse<Boolean> result = new RestfulResponse<>(0, "success", null);
        try {
            result.setData(true);
        } catch (Exception e) {
            LOGGER.error("AccountController.accountLogout ERROR Message : {} ", e.getMessage());
            result.setMessage("用户名或口令错误");
            result.setCode(10003);
        }
        return result;
    }


}
