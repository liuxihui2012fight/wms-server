package com.zhiche.wms.app.test;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.wms.AppApplication;
import com.zhiche.wms.configuration.MyConfigurationProperties;
import com.zhiche.wms.core.supports.enums.InterfaceAddrEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.HttpClientUtil;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.mapper.base.StorehouseMapper;
import com.zhiche.wms.domain.mapper.opbaas.ExceptionRegisterMapper;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionIsCanSendDTO;
import com.zhiche.wms.service.opbaas.ExceptionToOTMService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @Author: caiHua 标记异常不可发运
 * @Description:
 * @Date: Create in 21:00 2019/1/8
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppApplication.class)
public class ExceptionRegisterTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionRegisterTest.class);
    public static final String USER = "admin";
    @Autowired
    ExceptionToOTMService exceptionToOTMService;
    @Autowired
    OtmOrderReleaseMapper otmOrderReleaseMapper;
    @Autowired
    StorehouseMapper storehouseMapper;
    @Autowired
    ExceptionRegisterMapper exceptionRegisterMapper;
    @Autowired
    private MyConfigurationProperties properties;
    @Autowired
    private SnowFlakeId flakeId;

    @Test
    public void test () {
        //车架号
        String vin = this.readFile();
        String[] split = vin.split(",");
        List<String> vins = Arrays.asList(split);
        for (String vinVal : vins) {
            EntityWrapper<OtmOrderRelease> otmEW = new EntityWrapper<>();
            otmEW.eq("vin", vinVal);
            otmEW.ne("status", TableStatusEnum.STATUS_50.getCode());
            otmEW.orderBy("gmt_modified", false);
            List<OtmOrderRelease> otmOrderReleases = otmOrderReleaseMapper.selectList(otmEW);
            LOGGER.info("【" + vinVal + " 】查询otmOrderReleases结果{}", otmOrderReleases.size());
            if(CollectionUtils.isEmpty(otmOrderReleases)){
                continue;
            }
            if (CollectionUtils.isNotEmpty(otmOrderReleases)) {
                OtmOrderRelease otmOrderRelease = otmOrderReleases.get(0);
                ExceptionRegister exceptionRegister = new ExceptionRegister();

                //设置对象
                exceptionRegister.setId(String.valueOf(flakeId.nextId()));
                if (StringUtils.isEmpty(otmOrderRelease.getCusOrderNo())) {
                    LOGGER.info("【" + vinVal + "】客户订单号为空");
                    continue;
                }
                exceptionRegister.setOmsOrderNo(otmOrderRelease.getCusOrderNo());
                exceptionRegister.setVin(otmOrderRelease.getVin());
                exceptionRegister.setTaskNode(TableStatusEnum.STATUS_40.getCode());
                exceptionRegister.setStatus(TableStatusEnum.STATUS_10.getCode());
                exceptionRegister.setOtmStatus(TableStatusEnum.STATUS_N.getCode());

                EntityWrapper<Storehouse> ew = new EntityWrapper<>();
                ew.eq("code", otmOrderRelease.getDestLocationGid());
                List<Storehouse> storehouse = storehouseMapper.selectList(ew);
                Long storeHoseId = null;
                if (CollectionUtils.isNotEmpty(storehouse)) {
                    storeHoseId = storehouse.get(0).getId();
                }
                if (null == storeHoseId) {
                    LOGGER.info("【" + vinVal + "】release表查询storeHouseId为空！");
                    continue;
                }
                exceptionRegister.setHouseId(String.valueOf(storeHoseId));
                exceptionRegister.setUserCreate(USER);
                exceptionRegister.setUserModified(USER);
                exceptionRegister.setGmtCreate(new Date());
                exceptionRegister.setGmtModified(new Date());
                EntityWrapper<ExceptionRegister> exEW = new EntityWrapper<>();
                exEW.eq("vin", vinVal);
                List<ExceptionRegister> exceptionRegisters = exceptionRegisterMapper.selectList(exEW);
                if (CollectionUtils.isNotEmpty(exceptionRegisters)) {
                    LOGGER.info("车架号" + "【" + vinVal + "】已经存在，无需重复操作");
                    continue;
                }
                //存入异常信息
                exceptionRegisterMapper.insert(exceptionRegister);

                //异常处理推送otm
                this.sendOtm(exceptionRegister);
            }
        }
    }

    private String readFile () {
        StringBuffer vins = new StringBuffer();
        String pathname = "D://vin.txt";
        try {
            FileReader reader = new FileReader(pathname);
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
                // 一次读入一行数据
                System.out.println(line);
                vins.append(line);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return vins.toString();
    }

    /**
     * 异常处理推送otm
     */
    private void sendOtm (ExceptionRegister exceptionRegister) {
        try {
            ExceptionIsCanSendDTO exceptionIsCanSendDTO = new ExceptionIsCanSendDTO();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            exceptionIsCanSendDTO.setRegister_time(simpleDateFormat.format(new Date()));
            exceptionIsCanSendDTO.setVin(exceptionRegister.getVin());
            exceptionIsCanSendDTO.setIs_send(TableStatusEnum.STATUS_N.getCode());
            exceptionRegister.setOtmStatus(TableStatusEnum.STATUS_N.getCode());
            LOGGER.info("异常处理推送otm   params:{},url:{}",  JSONObject.toJSONString(exceptionIsCanSendDTO),
                    properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress());
            String result = HttpClientUtil.postJson(properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress(), null, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getSocketTimeOut());
            LOGGER.info("异常处理推送otm result:{},params:{},url:{}", result, JSONObject.toJSONString(exceptionIsCanSendDTO), properties.getOtmhost() + InterfaceAddrEnum.REGISTRATION_URL.getAddress());
        } catch (Exception e) {
            LOGGER.info(e.getMessage(), e);
        }
    }


}
