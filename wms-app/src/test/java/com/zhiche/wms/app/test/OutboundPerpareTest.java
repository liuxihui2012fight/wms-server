package com.zhiche.wms.app.test;

import com.zhiche.wms.AppApplication;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.base.StoreLocation;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareHeader;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareLine;
import com.zhiche.wms.domain.model.stock.Stock;
import com.zhiche.wms.domain.model.stock.StockProperty;
import com.zhiche.wms.service.base.IBusinessDocNumberService;
import com.zhiche.wms.service.base.IStoreLocationService;
import com.zhiche.wms.service.outbound.IOutboundNoticeHeaderService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import com.zhiche.wms.service.outbound.IOutboundPrepareLineService;
import com.zhiche.wms.service.stock.IStockService;
import com.zhiche.wms.service.sys.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppApplication.class)
public class OutboundPerpareTest {

    @Autowired
    private SnowFlakeId snowFlakeId;
    @Autowired
    private IUserService userService;
    @Autowired
    private IBusinessDocNumberService businessDocNumberService;
    @Autowired
    private IOutboundNoticeLineService outboundNoticeLineService;
    @Autowired
    private IOutboundNoticeHeaderService outboundNoticeHeaderService;
    @Autowired
    private IOutboundPrepareHeaderService outboundPrepareHeaderService;
    @Autowired
    private IOutboundPrepareLineService outboundPrepareLineService;
    @Autowired
    private IStockService stockService;
    @Autowired
    private IStoreLocationService storeLocationService;
    @Test
    public void addPrepareByNoticeId() {
        Long NoticeLineId = 51067720367599617L;
        Long houseId = new Long("100030");
        OutboundNoticeLine noticeLine = outboundNoticeLineService.selectById(NoticeLineId);
        OutboundNoticeHeader noticeHeader = outboundNoticeHeaderService.selectById(noticeLine.getHeaderId());
        StockProperty stockProperty = new StockProperty();
        BeanUtils.copyProperties(noticeLine,stockProperty);
        stockProperty.setStoreHouseId(noticeHeader.getStoreHouseId());
//        stockProperty.setOwnerId();

        //得到可用储位
        List<Stock> stockList = stockService.queryStockList(stockProperty);
        if(Objects.isNull(stockList)) throw new BaseException("未找到匹配库位！");
        Stock stock = stockList.get(0);
        StoreLocation storeLocation= storeLocationService.selectLocationByLocationId(stock.getLocationId());
        if(Objects.isNull(storeLocation)) throw new BaseException("无可用库位！");



        OutboundPrepareHeader prepareHeader = new OutboundPrepareHeader();

        //质检单头信息匹配开始
        //将noticeHeader对象对应值存入inspectHeader
        BeanUtils.copyProperties(noticeHeader, prepareHeader);
        prepareHeader.setId(snowFlakeId.nextId());
        prepareHeader.setNoticeId(noticeHeader.getId());
        prepareHeader.setPrepareNo(businessDocNumberService.getOutboundPrepareNo());
        prepareHeader.setOrderTime(new Date());
        prepareHeader.setPlanSumQty(noticeHeader.getExpectSumQty());
        prepareHeader.setActualSumQty(new BigDecimal(0));

        prepareHeader.setStatus("10");
        prepareHeader.setUserCreate("LAS");
        prepareHeader.setGmtCreate(null);
        prepareHeader.setGmtModified(null);
        outboundPrepareHeaderService.insert(prepareHeader);

            OutboundPrepareLine outPrepareLine = new OutboundPrepareLine();
        BeanUtils.copyProperties(noticeLine,outPrepareLine);
        outPrepareLine.setId(snowFlakeId.nextId());
        outPrepareLine.setHeaderId(prepareHeader.getId());
        outPrepareLine.setNoticeLineId(noticeLine.getId());
        outPrepareLine.setLocationId(storeLocation.getId());
        outPrepareLine.setLocationNo(storeLocation.getStoreAreaName()+" "+storeLocation.getName());
        outPrepareLine.setPlanQty(noticeLine.getExpectQty());
        outPrepareLine.setPlanNetWeight(noticeLine.getExpectNetWeight());
        outPrepareLine.setPlanGrossWeight(noticeLine.getExpectGrossWeight());
        outPrepareLine.setPlanGrossCubage(noticeLine.getExpectGrossCubage());
        outPrepareLine.setPlanPackedCount(noticeLine.getExpectPackedCount());

        outPrepareLine.setActualQty(new BigDecimal(0));
        outPrepareLine.setActualNetWeight(new BigDecimal(0));
        outPrepareLine.setActualGrossWeight(new BigDecimal(0));
        outPrepareLine.setActualGrossCubage(new BigDecimal(0));
        outPrepareLine.setActualPackedCount(new BigDecimal(0));
        outPrepareLine.setStatus("10");
        outPrepareLine.setGmtCreate(null);
        outPrepareLine.setGmtModified(null);
        outboundPrepareLineService.insert(outPrepareLine);
//    BaseException("质检单明细创建失败");  //若新增失败报出异常，否则继续执行
}
}