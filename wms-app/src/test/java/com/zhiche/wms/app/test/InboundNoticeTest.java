package com.zhiche.wms.app.test;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.common.collect.Maps;
import com.zhiche.wms.AppApplication;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.base.BusinessDocNumber;
import com.zhiche.wms.domain.model.base.StoreLocation;
import com.zhiche.wms.domain.model.inbound.InboundNoticeHeader;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.domain.model.stock.Sku;
import com.zhiche.wms.domain.model.stock.Stock;
import com.zhiche.wms.service.base.IBusinessDocNumberService;
import com.zhiche.wms.service.base.IStoreLocationService;
import com.zhiche.wms.service.constant.DocPrefix;
import com.zhiche.wms.service.constant.Status;
import com.zhiche.wms.service.inbound.IInboundNoticeHeaderService;
import com.zhiche.wms.service.inbound.IInboundNoticeLineService;
import com.zhiche.wms.service.outbound.IOutboundNoticeHeaderService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import com.zhiche.wms.service.stock.ISkuService;
import com.zhiche.wms.service.stock.IStockService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppApplication.class)
public class InboundNoticeTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(InboundNoticeTest.class);

    @Autowired
    private IInboundNoticeHeaderService inboundNoticeHeaderService;
    @Autowired
    private IInboundNoticeLineService inboundNoticeLineService;
    @Autowired
    private IOutboundNoticeHeaderService outboundNoticeHeaderService;
    @Autowired
    private IOutboundNoticeLineService outboundNoticeLineService;
    @Autowired
    private SnowFlakeId snowFlakeId;
    @Autowired
    private IBusinessDocNumberService businessDocNumberService;
    @Autowired
    private ISkuService skuService;
    @Autowired
    private IStockService stockService;
    @Autowired
    private IStoreLocationService locationService;

    /**
     * 库存批量调整
     */
    @Test
    public void stockAdjust() {
        String string = jsonString();
        List<Map> maps = JSONObject.parseArray(string, Map.class);
        for (Map map : maps) {
            HashMap<String, String> params = Maps.newHashMap();
            EntityWrapper<Sku> skuEW = new EntityWrapper<>();
            skuEW.eq("lot_no1", map.get("vin"))
                    .orderBy("id");
            Sku sku = skuService.selectOne(skuEW);
            if (sku != null) {
                EntityWrapper<Stock> stockEW = new EntityWrapper<>();
                stockEW.eq("sku_id", sku.getId())
                        .orderBy("id");
                Stock stock = stockService.selectOne(stockEW);
                EntityWrapper<StoreLocation> locationEW = new EntityWrapper<>();
                locationEW.eq("code", map.get("code"))
                        .eq("store_house_id", "10001")
                        .orderBy("id", false);
                StoreLocation storeLocation = locationService.selectOne(locationEW);
                if (stock != null && storeLocation != null) {
                    params.put("houseId", "10001");
                    params.put("stockId", String.valueOf(stock.getId()));
                        params.put("newLocationId", String.valueOf(storeLocation.getId()));
                        stockService.updateStockLocation(params);
                } else {
                    LOGGER.error("未更新车辆:{}", map.get("vin"));
                }
            } else {
                LOGGER.error("未更新车辆:{}", map.get("vin"));
            }
        }
    }


    public String jsonString() {
        return "[{\"vin\":\"L6T7824S6JN272671\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742Z7JB127319\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z5JN277180\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN295525\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z7JN285426\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN278453\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN286728\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S8JN272672\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z2JN277377\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z0JN286238\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN278949\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN286700\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824ZXJN276493\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN278484\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724Z9JX098118\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824ZXJN304261\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z2JN303573\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN302619\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742Z7JB129393\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742Z7JB129409\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN278498\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN311435\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN311433\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN305813\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S7JN306276\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z9JN277179\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S9JN306313\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN254991\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN253440\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z7JN286236\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN266140\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN306198\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z3JN300763\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S2JN298541\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824SXJN311469\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z3JN311438\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S6JN305636\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN305746\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN277187\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824ZXJN305751\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z7JN263152\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z3JN253492\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN264956\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824ZXJN264943\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S7JN306343\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z9JN304252\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824ZXJN295500\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824ZXJN311436\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z2JN311432\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN305781\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN306247\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z0JN277149\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z2JN305758\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN254864\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN254300\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z0JN278429\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z9JN286240\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z5JN306211\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S0JN306359\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S3JN306274\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN277401\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S9JN298519\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z9JN311427\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S7JN311476\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z7JN306257\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN305800\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z9JN304915\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824ZXJN279880\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z3JN251208\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z3JN254738\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN284834\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN300788\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S2JN306315\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S6JN306270\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN284896\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z2JN299816\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824ZXJN268085\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37522Z5JL118641\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z0JN306214\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S0JN306264\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z8JC095267\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN308694\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z0JC095361\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z8JC099660\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z5JC099700\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z8JC099724\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z8JC095396\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z4JC099672\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z0JC099720\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724Z7JX130497\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724Z7JX131200\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z5JC095257\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722ZXJC099739\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742ZXJB128030\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S0JN311481\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z3JC098206\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724S9JX100517\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z3JC099355\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN305776\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z3JN306210\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z5JC099308\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z1JC099290\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z6JC098331\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z2JC099301\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z6JC099298\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S8JN311454\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37522S2JL131625\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z9JN308706\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z0JN302759\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742Z8JB126583\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37522S8JL131371\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742Z8JB126017\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724Z3JX088183\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S5JN306261\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37522Z0JL118580\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37522Z5JL136282\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S9JN306330\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724S1JX131860\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S6JN309279\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S3JN305691\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S1JN305690\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S1JN305687\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z3JN306224\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S9JN306280\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S6JN306284\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z1JC099340\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724Z1JX131337\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37522ZXJL118554\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724Z7JX088218\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742Z9JB129301\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z5JN306175\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z7JN305772\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S4JN312214\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824SXJN306286\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN303266\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z1JC099287\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742Z1JB129339\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN306251\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z0JN306164\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724Z7JX116552\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN306233\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742Z2JB129401\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z5JN306239\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z7JN294983\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z0JN305810\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S8JN306268\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z1JN306173\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824ZXJN294301\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S2JN304984\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z2JC099914\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z0JN295005\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z4JC099865\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z1JN298706\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S6JN305667\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S5JN299344\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z9JC100038\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z5JC099728\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z7JC099889\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z3JC099257\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z6JC099320\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722ZXJC099420\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z7JC099861\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z3JN306160\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S5JN305675\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z3JN306174\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37742Z3JB129388\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z2JN305727\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z5JN303289\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S1JN306290\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z5JN270228\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN306183\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN305729\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S8JN306352\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S3JN306288\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z1JN306139\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z7JN302760\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z6JN306217\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z5JN306225\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z0JN309341\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37722Z7JC094949\",\"code\":\"X\"},\n" +
                "{\"vin\":\"LB37724Z4JX088242\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S6JN311498\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z8JN309362\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z2JN306229\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z5JN268169\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824Z4JN303557\",\"code\":\"X\"},\n" +
                "{\"vin\":\"L6T7824S7JN301417\",\"code\":\"X\"}\n" +
                "]";
    }

    @Test
    public void createInboundNotice() throws InterruptedException {

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                LOGGER.info("thread : {}  ----->start", Thread.currentThread().getName());
                InboundNoticeHeader inboundNoticeHeader = new InboundNoticeHeader();
                inboundNoticeHeader.setId(snowFlakeId.nextId());
                inboundNoticeHeader.setStoreHouseId(100030L);
                inboundNoticeHeader.setNoticeNo(businessDocNumberService.getInboundNoticeNo());
                businessDocNumberService.getInboundInspectNo();
                businessDocNumberService.getInboundPutAwayNo();
                businessDocNumberService.getMovementNo();
                businessDocNumberService.getOutboundNoticeNo();
                businessDocNumberService.getOutboundPrepareNo();
                businessDocNumberService.getOutboundShipNo();
                inboundNoticeHeader.setOrderDate(new Date());
                inboundNoticeHeader.setExpectSumQty(new BigDecimal("2"));
                inboundNoticeHeader.setLineCount(2);
                inboundNoticeHeader.setStatus(Status.Inbound.NOT);
                inboundNoticeHeader.setOwnerId(null);
                inboundNoticeHeader.setOwnerOrderNo("hanxiaohui");
                inboundNoticeHeader.setCarrierName("魏军水铁队自力运队");
                inboundNoticeHeader.setTransportMethod("平板四位板");
                inboundNoticeHeader.setPlateNumber("赣1234");
                inboundNoticeHeader.setDriverName("王二");
                inboundNoticeHeader.setDriverPhone("13333903333");
                inboundNoticeHeader.setExpectRecvDateLower(new Date());
                inboundNoticeHeader.setUom("台");


                InboundNoticeLine inboundNoticeLine = new InboundNoticeLine();
                inboundNoticeLine.setHeaderId(inboundNoticeHeader.getId());
                inboundNoticeLine.setOwnerId("1");
                inboundNoticeLine.setExpectQty(BigDecimal.ONE);
                inboundNoticeLine.setInboundQty(BigDecimal.ZERO);
                inboundNoticeLine.setMaterielId("10");
                inboundNoticeLine.setLotNo0("hanxiaohui1");
                inboundNoticeLine.setLotNo1("hanxiaohui1");
                inboundNoticeLine.setStatus(Status.Inbound.NOT);
                inboundNoticeLine.setId(snowFlakeId.nextId());
                inboundNoticeLine.setUom("台");

                InboundNoticeLine inboundNoticeLine1 = new InboundNoticeLine();
                inboundNoticeLine1.setHeaderId(inboundNoticeHeader.getId());
                inboundNoticeLine1.setOwnerId("1");
                inboundNoticeLine1.setExpectQty(BigDecimal.ONE);
                inboundNoticeLine1.setInboundQty(BigDecimal.ZERO);
                inboundNoticeLine1.setMaterielId("10");
                inboundNoticeLine1.setLotNo0("hanxiaohui2");
                inboundNoticeLine1.setLotNo1("hanxiaohui2");
                inboundNoticeLine1.setStatus(Status.Inbound.NOT);
                inboundNoticeLine1.setId(snowFlakeId.nextId());
                inboundNoticeLine1.setUom("台");
                inboundNoticeHeaderService.insert(inboundNoticeHeader);
                inboundNoticeLineService.insert(inboundNoticeLine);
                inboundNoticeLineService.insert(inboundNoticeLine1);
                LOGGER.info("thread : {}  ----->end", Thread.currentThread().getName());
            }).start();

        }
        Thread.sleep(10000);
/*

        InboundNoticeLine inboundNoticeLine1 = new InboundNoticeLine();
        inboundNoticeLine1.setHeaderId(inboundNoticeHeader.getId());
        inboundNoticeLine1.setOwnerId("1");
        inboundNoticeLine1.setExpectQty(BigDecimal.ONE);
        inboundNoticeLine1.setMaterielId("20");
        inboundNoticeLine1.setLotNo0("1001");
        inboundNoticeLine1.setLotNo1("1002");
        inboundNoticeLine1.setStatus(Status.inbound.NOT);
        inboundNoticeLine1.setId(snowFlakeId.nextId());*/

    }

    @Test
    public void testDocNew() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                LOGGER.info("thread : {}  ----->start", Thread.currentThread().getName());
                LOGGER.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.INBOUND_NOTICE));
                LOGGER.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.INBOUND_PUTAWAY));
                LOGGER.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.INBOUND_INSPECT));
                LOGGER.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.MOVEMENT));
                LOGGER.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.OUTBOUND_NOTICE));
                LOGGER.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.OUTBOUND_PREPARE));
                LOGGER.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.OUTBOUND_SHIP));
                LOGGER.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.STOCK_INIT));
                LOGGER.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.STOCK_ADJUST));
            }).start();
        }
        Thread.sleep(1000L);
    }


    @Test
    public void createOutboundNotice() {
        OutboundNoticeHeader outboundNoticeHeader = new OutboundNoticeHeader();
        outboundNoticeHeader.setId(snowFlakeId.nextId());
        outboundNoticeHeader.setStoreHouseId(100030L);
        outboundNoticeHeader.setNoticeNo(businessDocNumberService.getInboundNoticeNo());
        outboundNoticeHeader.setOrderDate(new Date());
        outboundNoticeHeader.setExpectSumQty(new BigDecimal("2"));
        outboundNoticeHeader.setLineCount(2);
        outboundNoticeHeader.setStatus(Status.Inbound.NOT);
        outboundNoticeHeader.setOwnerId(null);
        outboundNoticeHeader.setOwnerOrderNo("hanxiaohui");
        outboundNoticeHeader.setCarrierName("魏军水铁队自力运队");
        outboundNoticeHeader.setTransportMethod("平板四位板");
        outboundNoticeHeader.setPlateNumber("赣1234");
        outboundNoticeHeader.setDriverName("王二");
        outboundNoticeHeader.setDriverPhone("13333903333");
        outboundNoticeHeader.setExpectShipDateLower(new Date());
        outboundNoticeHeader.setUom("台");


        OutboundNoticeLine outboundNoticeLine = new OutboundNoticeLine();
        outboundNoticeLine.setHeaderId(outboundNoticeHeader.getId());
        outboundNoticeLine.setOwnerId("1");
        outboundNoticeLine.setExpectQty(BigDecimal.ONE);
        outboundNoticeLine.setOutboundQty(BigDecimal.ZERO);
        outboundNoticeLine.setMaterielId("10");
        outboundNoticeLine.setLotNo0("hanxiaohui1");
        outboundNoticeLine.setLotNo1("hanxiaohui1");
        outboundNoticeLine.setStatus(Status.Inbound.NOT);
        outboundNoticeLine.setId(snowFlakeId.nextId());
        outboundNoticeLine.setUom("台");
        OutboundNoticeLine outboundNoticeLine1 = new OutboundNoticeLine();
        outboundNoticeLine1.setHeaderId(outboundNoticeHeader.getId());
        outboundNoticeLine1.setOwnerId("1");
        outboundNoticeLine1.setExpectQty(BigDecimal.ONE);
        outboundNoticeLine1.setOutboundQty(BigDecimal.ZERO);
        outboundNoticeLine1.setMaterielId("10");
        outboundNoticeLine1.setLotNo0("hanxiaohui2");
        outboundNoticeLine1.setLotNo1("hanxiaohui2");
        outboundNoticeLine1.setStatus(Status.Inbound.NOT);
        outboundNoticeLine1.setId(snowFlakeId.nextId());
        outboundNoticeLine1.setUom("台");
/*

        InboundNoticeLine inboundNoticeLine1 = new InboundNoticeLine();
        inboundNoticeLine1.setHeaderId(inboundNoticeHeader.getId());
        inboundNoticeLine1.setOwnerId("1");
        inboundNoticeLine1.setExpectQty(BigDecimal.ONE);
        inboundNoticeLine1.setMaterielId("20");
        inboundNoticeLine1.setLotNo0("1001");
        inboundNoticeLine1.setLotNo1("1002");
        inboundNoticeLine1.setStatus(Status.inbound.NOT);
        inboundNoticeLine1.setId(snowFlakeId.nextId());*/

        outboundNoticeHeaderService.insert(outboundNoticeHeader);
        outboundNoticeLineService.insert(outboundNoticeLine);
        outboundNoticeLineService.insert(outboundNoticeLine1);
    }


}
