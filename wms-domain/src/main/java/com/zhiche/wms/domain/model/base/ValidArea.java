package com.zhiche.wms.domain.model.base;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.math.BigDecimal;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 16:57 2019/11/12
 */
public class ValidArea {

    /**
     * 库区id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long areaId;
    /**
     * 仓库id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 库区编码
     */
    private String areaCode;
    /**
     * 库区名称
     */
    private String areaName;
    /**
     * 库区最大存储量
     */
    private BigDecimal maxStorage;
    /**
     * 使用库储量
     */
    private BigDecimal storedQty;
    /**
     * 剩余库存量
     */
    private BigDecimal usableQty;

    public Long getAreaId () {
        return areaId;
    }

    public void setAreaId (Long areaId) {
        this.areaId = areaId;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getAreaCode () {
        return areaCode;
    }

    public void setAreaCode (String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName () {
        return areaName;
    }

    public void setAreaName (String areaName) {
        this.areaName = areaName;
    }

    public BigDecimal getMaxStorage () {
        return maxStorage;
    }

    public void setMaxStorage (BigDecimal maxStorage) {
        this.maxStorage = maxStorage;
    }

    public BigDecimal getStoredQty () {
        return storedQty;
    }

    public void setStoredQty (BigDecimal storedQty) {
        this.storedQty = storedQty;
    }

    public BigDecimal getUsableQty () {
        return usableQty;
    }

    public void setUsableQty (BigDecimal usableQty) {
        this.usableQty = usableQty;
    }
}
