package com.zhiche.wms.domain.mapper.stock;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.stock.Stock;
import com.zhiche.wms.domain.model.stock.StockProperty;
import com.zhiche.wms.dto.outbound.StockSkuToPrepareDTO;
import com.zhiche.wms.dto.outbound.StockSkuToPrepareParamDTO;
import com.zhiche.wms.dto.stock.FactoryPrepareStockDTO;
import com.zhiche.wms.dto.stock.StockDTO;
import com.zhiche.wms.dto.stock.StockQty;
import com.zhiche.wms.dto.stock.StockWithSkuDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库存 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface StockMapper extends BaseMapper<Stock> {

    /**
     * 更新库存
     */
    Integer updateStockQtyById (StockProperty stockProperty);

    /**
     * 根据仓库属性得到已有库存
     */
    List<Stock> queryStockList (StockProperty stockProperty);

    /**
     * 分页查询库存
     *
     * @param page 分页对象
     * @param ew   动态条件
     */
    List<StockDTO> selectPageStockDTO (Page<StockDTO> page, @Param("ew") Wrapper<StockDTO> ew);

    List<StockDTO> selectPageStockDTOByQrCode (Page<StockDTO> page, @Param("ew") Wrapper<StockDTO> ew);

    List<StockSkuToPrepareDTO> selectStockToPrepareByParams (StockSkuToPrepareParamDTO paramDTO);

    /**
     * 更新库存状态
     */
    Integer updateStockStatus (@Param("stockId") Long stockId, @Param("status") String status, @Param("reason") String reason,
                               @Param("flag") String flag);

    List<StockWithSkuDTO> selectStockWithSkuByParam (HashMap<String, Object> params);

    List<StockDTO> selectExportStockData (@Param("ew") EntityWrapper<StockDTO> ew);

    void dealEmptySku (Map<String, Object> sqlParam);

    List<StockQty> queryStockQty (Page<StockQty> page,@Param("ew") EntityWrapper<StockQty> stockQtyEw);

    List<StockQty> queryStockQtyDetail (@Param("ew")EntityWrapper<StockQty> ew);

    Long queryMaxStockId (@Param("vin") String vin);

    Integer queryStockPrepare (@Param("vin") String vin, @Param("storeHouseId") Long storeHouseId);

    List<FactoryPrepareStockDTO> queryFactoryStock (@Param("ew") EntityWrapper<FactoryPrepareStockDTO> fpEw, Page<FactoryPrepareStockDTO> page);

    List<FactoryPrepareStockDTO> queryFactoryStock (@Param("ew") EntityWrapper<FactoryPrepareStockDTO> fpEw);
}
