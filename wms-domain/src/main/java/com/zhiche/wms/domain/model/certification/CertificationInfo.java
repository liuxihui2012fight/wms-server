package com.zhiche.wms.domain.model.certification;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 9:59 2019/9/5
 */
@TableName("w_certificat_info")
public class CertificationInfo extends Model<CertificationInfo> {
    @Override
    protected Serializable pkVal () {
        return id;
    }

    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 车架号
     */
    private String vin;

    /**
     * 发放时间
     */
    @TableField("giveout_date")
    private Date giveoutDate;

    /**
     * 发放人
     */
    @TableField("giveout_user")
    private String giveoutUser;

    /**
     * 领取人
     */
    @TableField("receive_user")
    private String receiveUser;

    /**
     * 领取时间
     */
    @TableField("receive_date")
    private Date receiveDate;

    /**
     * 合格证数量
     */
    @TableField("certificat_num")
    private Integer certificatNum;

    /**
     * 合格证状态
     */
    @TableField("certificat_status")
    private String certificatStatus;

    /**
     * 库存id
     */
    @TableField("stock_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stockId;

    /**
     * 创建时间
     */
    @TableField("gmt_created")
    private Date gmtCreated;

    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    /**
     * 备注
     */
    private String remarks;

    @TableField(exist = false)
    private String storeHouseName;

    @TableField(exist = false)
    private Date inboundDate;

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public Date getInboundDate () {
        return inboundDate;
    }

    public void setInboundDate (Date inboundDate) {
        this.inboundDate = inboundDate;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public Date getGiveoutDate () {
        return giveoutDate;
    }

    public void setGiveoutDate (Date giveoutDate) {
        this.giveoutDate = giveoutDate;
    }

    public String getGiveoutUser () {
        return giveoutUser;
    }

    public void setGiveoutUser (String giveoutUser) {
        this.giveoutUser = giveoutUser;
    }

    public String getReceiveUser () {
        return receiveUser;
    }

    public void setReceiveUser (String receiveUser) {
        this.receiveUser = receiveUser;
    }

    public Date getReceiveDate () {
        return receiveDate;
    }

    public void setReceiveDate (Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public Integer getCertificatNum () {
        return certificatNum;
    }

    public void setCertificatNum (Integer certificatNum) {
        this.certificatNum = certificatNum;
    }

    public String getCertificatStatus () {
        return certificatStatus;
    }

    public void setCertificatStatus (String certificatStatus) {
        this.certificatStatus = certificatStatus;
    }

    public Long getStockId () {
        return stockId;
    }

    public void setStockId (Long stockId) {
        this.stockId = stockId;
    }

    public Date getGmtCreated () {
        return gmtCreated;
    }

    public void setGmtCreated (Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }
}
