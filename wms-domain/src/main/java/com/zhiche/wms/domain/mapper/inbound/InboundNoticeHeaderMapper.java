package com.zhiche.wms.domain.mapper.inbound;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.inbound.InboundNoticeHeader;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.dto.inbound.InboundNoticeHeaderDTO;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.math.BigDecimal;

/**
 * <p>
 * 入库通知单头 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface InboundNoticeHeaderMapper extends BaseMapper<InboundNoticeHeader> {

    Integer updateStatus(@Param("noticeId") Long noticeId,@Param("status") String status);

    List<InboundNoticeHeaderDTO> queryNoticePage(Page<InboundNoticeHeaderDTO> page, @Param("ew") Wrapper<InboundNoticeHeaderDTO> ew);

    InboundNoticeHeaderDTO getNotice(@Param("houseId") Long houseId,@Param("id")Long id);

    Integer updateStatus(@Param("noticeId") Long noticeId,
                         @Param("status") String status,@Param("inboundQty") BigDecimal inboundQty);

    String selectSourceKeyByLineId (@Param("noticeLineId") Long id, @Param("lineSourceKey") String lineSourceKey);
}
