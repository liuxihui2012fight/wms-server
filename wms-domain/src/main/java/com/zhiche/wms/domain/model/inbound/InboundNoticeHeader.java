package com.zhiche.wms.domain.model.inbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 入库通知单头
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
@TableName("w_inbound_notice_header")
public class InboundNoticeHeader extends Model<InboundNoticeHeader> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<InboundNoticeLine> inboundNoticeLineList;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 收货仓库
     */
    @TableField("store_house_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 入库通知单号
     */
    @TableField("notice_no")
    private String noticeNo;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 货主单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;
    /**
     * 下单日期
     */
    @TableField("order_date")
    private Date orderDate;
    /**
     * 供应商
     */
    @TableField("carrier_id")
    private String carrierId;
    /**
     * 供应商名称
     */
    @TableField("carrier_name")
    private String carrierName;
    /**
     * 运输方式
     */
    @TableField("transport_method")
    private String transportMethod;
    /**
     * 车船号
     */
    @TableField("plate_number")
    private String plateNumber;
    /**
     * 司机姓名
     */
    @TableField("driver_name")
    private String driverName;
    /**
     * 司机联系方式
     */
    @TableField("driver_phone")
    private String driverPhone;
    /**
     * 预计到货日期
     */
    @TableField("expect_recv_date_lower")
    private Date expectRecvDateLower;
    /**
     * 预计收货时间上限
     */
    @TableField("expect_recv_date_upper")
    private Date expectRecvDateUpper;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 预计入库数量
     */
    @TableField("expect_sum_qty")
    private BigDecimal expectSumQty;
    /**
     * 已入库数量
     */
    @TableField("inbound_sum_qty")
    private BigDecimal inboundSumQty;
    /**
     * 明细行数
     */
    @TableField("line_count")
    private Integer lineCount;
    /**
     * 日志头ID
     */
    @TableField("log_header_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long logHeaderId;
    /**
     * 来源唯一键
     */
    @TableField("source_key")
    private String sourceKey;
    /**
     * 来源单据号
     */
    @TableField("source_no")
    private String sourceNo;
    /**
     * 状态(10:未入库,20:部分入库,30:全部入库,40:关闭入库,50:取消)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Date getExpectRecvDateLower() {
        return expectRecvDateLower;
    }

    public void setExpectRecvDateLower(Date expectRecvDateLower) {
        this.expectRecvDateLower = expectRecvDateLower;
    }

    public Date getExpectRecvDateUpper() {
        return expectRecvDateUpper;
    }

    public void setExpectRecvDateUpper(Date expectRecvDateUpper) {
        this.expectRecvDateUpper = expectRecvDateUpper;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getExpectSumQty() {
        return expectSumQty;
    }

    public void setExpectSumQty(BigDecimal expectSumQty) {
        this.expectSumQty = expectSumQty;
    }

    public BigDecimal getInboundSumQty() {
        return inboundSumQty;
    }

    public void setInboundSumQty(BigDecimal inboundSumQty) {
        this.inboundSumQty = inboundSumQty;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public Long getLogHeaderId() {
        return logHeaderId;
    }

    public void setLogHeaderId(Long logHeaderId) {
        this.logHeaderId = logHeaderId;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getSourceNo() {
        return sourceNo;
    }

    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "InboundNoticeHeader{" +
                ", id=" + id +
                ", storeHouseId=" + storeHouseId +
                ", noticeNo=" + noticeNo +
                ", ownerId=" + ownerId +
                ", ownerOrderNo=" + ownerOrderNo +
                ", orderDate=" + orderDate +
                ", carrierId=" + carrierId +
                ", carrierName=" + carrierName +
                ", transportMethod=" + transportMethod +
                ", plateNumber=" + plateNumber +
                ", driverName=" + driverName +
                ", driverPhone=" + driverPhone +
                ", expectRecvDateLower=" + expectRecvDateLower +
                ", expectRecvDateUpper=" + expectRecvDateUpper +
                ", uom=" + uom +
                ", expectSumQty=" + expectSumQty +
                ", inboundSumQty=" + inboundSumQty +
                ", lineCount=" + lineCount +
                ", logHeaderId=" + logHeaderId +
                ", sourceKey=" + sourceKey +
                ", sourceNo=" + sourceNo +
                ", status=" + status +
                ", remarks=" + remarks +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }

    public List<InboundNoticeLine> getInboundNoticeLineList() {
        return inboundNoticeLineList;
    }

    public void setInboundNoticeLineList(List<InboundNoticeLine> inboundNoticeLineList) {
        this.inboundNoticeLineList = inboundNoticeLineList;
    }

    public void addInboundNoticeLine(InboundNoticeLine inboundNoticeLine) {
        if (Objects.isNull(inboundNoticeLineList)) {
            inboundNoticeLineList = new ArrayList<>();
        }
        inboundNoticeLineList.add(inboundNoticeLine);
    }

}
