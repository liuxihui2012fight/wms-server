package com.zhiche.wms.domain.model.otm;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: otm下发指令发运时间
 * @Date: Create in 9:46 2019/6/25
 */
public class OtmUpdateShipWmsDate implements Serializable {

    /**
     * 指令号
     */
    private String shipmengGid;
    /**
     * 运单号
     */
    private String releaseGid;

    /**
     * 发运时间
     */
    private long shipDate;

    public String getShipmengGid () {
        return shipmengGid;
    }

    public void setShipmengGid (String shipmengGid) {
        this.shipmengGid = shipmengGid;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public long getShipDate () {
        return shipDate;
    }

    public void setShipDate (long shipDate) {
        this.shipDate = shipDate;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("OtmUpdateShipWmsDate{");
        sb.append("shipmengGid='").append(shipmengGid).append('\'');
        sb.append(", releaseGid='").append(releaseGid).append('\'');
        sb.append(", shipDate=").append(shipDate);
        sb.append('}');
        return sb.toString();
    }
}
