package com.zhiche.wms.domain.mapper.opbaas;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.opbaas.Driver;

/**
 * <p>
 * 司机信息 Mapper 接口
 * </p>
 *
 * @author user
 * @since 2018-06-06
 */
public interface DriverMapper extends BaseMapper<Driver> {

}
