package com.zhiche.wms.domain.model.inbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: 工厂入库备料
 * @Date: Create in 11:06 2019/11/20
 */
@TableName("w_factory_prepare_inbound")
public class FactoryPrepareInbound extends Model<FactoryPrepareInbound> {

    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 车架号
     */
    private String vin;
    /**
     * 仓库id
     */
    @TableField("store_house_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 库区id
     */
    @TableField("area_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long areaId;
    /**
     * 库位
     */
    @TableField("location_no")
    private String locationNo;
    /**
     * 库位id
     */
    @TableField("location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long locationId;
    /**
     * 库存id
     */
    @TableField("stock_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stockId;

    /**
     * 车辆信息id
     */
    @TableField("sku_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skuId;
    /**
     * 提车指令
     */
    @TableField("shipment_gid")
    private String shipmentGid;
    /**
     * 入库用户
     */
    @TableField("inbound_user")
    private String inboundUser;
    /**
     * 入库状态(10:已入库，20:移实顺,30：已出库(提车指令出库) ，40：返工厂，50：删除)
     */
    @TableField("prepare_status")
    private String prepareStatus;
    /**
     * 入库时间
     */
    @TableField("inbound_time")
    private Date inboundTime;
    /**
     * 出库操作用户
     */
    @TableField("outbound_user")
    private String outboundUser;
    /**
     * 出库时间
     */
    @TableField("outbound_time")
    private Date outboundTime;
    /**
     * 创建时间
     */
    @TableField("gmt_created")
    private Date gmtCreated;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    /**
     * 目标仓库
     */
    @TableField("target_store_house")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long targetStoreHouse;
    /**
     * 入库记录id
     */
    @TableField("putwayId")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long putwayId;

    /**
     * 备注
     */
    private String remarks;

    public Long getPutwayId () {
        return putwayId;
    }

    public void setPutwayId (Long putwayId) {
        this.putwayId = putwayId;
    }

    public Long getTargetStoreHouse () {
        return targetStoreHouse;
    }

    public void setTargetStoreHouse (Long targetStoreHouse) {
        this.targetStoreHouse = targetStoreHouse;
    }

    @Override
    protected Serializable pkVal () {
        return id;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public Long getAreaId () {
        return areaId;
    }

    public void setAreaId (Long areaId) {
        this.areaId = areaId;
    }

    public String getLocationNo () {
        return locationNo;
    }

    public void setLocationNo (String locationNo) {
        this.locationNo = locationNo;
    }

    public Long getLocationId () {
        return locationId;
    }

    public void setLocationId (Long locationId) {
        this.locationId = locationId;
    }

    public Long getStockId () {
        return stockId;
    }

    public void setStockId (Long stockId) {
        this.stockId = stockId;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getInboundUser () {
        return inboundUser;
    }

    public void setInboundUser (String inboundUser) {
        this.inboundUser = inboundUser;
    }


    public Date getInboundTime () {
        return inboundTime;
    }

    public void setInboundTime (Date inboundTime) {
        this.inboundTime = inboundTime;
    }

    public String getOutboundUser () {
        return outboundUser;
    }

    public void setOutboundUser (String outboundUser) {
        this.outboundUser = outboundUser;
    }

    public String getPrepareStatus () {
        return prepareStatus;
    }

    public void setPrepareStatus (String prepareStatus) {
        this.prepareStatus = prepareStatus;
    }

    public Date getOutboundTime () {
        return outboundTime;
    }

    public void setOutboundTime (Date outboundTime) {
        this.outboundTime = outboundTime;
    }

    public Date getGmtCreated () {
        return gmtCreated;
    }

    public void setGmtCreated (Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Long getSkuId () {
        return skuId;
    }

    public void setSkuId (Long skuId) {
        this.skuId = skuId;
    }
}
