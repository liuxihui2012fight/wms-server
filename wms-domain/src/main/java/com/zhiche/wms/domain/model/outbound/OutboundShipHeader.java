package com.zhiche.wms.domain.model.outbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 出库单头
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
@TableName("w_outbound_ship_header")
public class OutboundShipHeader extends Model<OutboundShipHeader> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<OutboundShipLine> outboundShipLineList;

    /**
     * id
     */

    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 出库通知单头键
     */
    @TableField("notice_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long noticeId;
    /**
     * 发货仓库
     */
    @TableField("store_house_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 出库单号
     */
    @TableField("outbound_no")
    private String outboundNo;
    /**
     * 出库时间
     */
    @TableField("outbound_time")
    private Date outboundTime;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 货主单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;
    /**
     * 供应商
     */
    @TableField("carrier_id")
    private String carrierId;
    /**
     * 供应商名称
     */
    @TableField("carrier_name")
    private String carrierName;
    /**
     * 运输方式
     */
    @TableField("transport_method")
    private String transportMethod;
    /**
     * 车船号
     */
    @TableField("plate_number")
    private String plateNumber;
    /**
     * 司机姓名
     */
    @TableField("driver_name")
    private String driverName;
    /**
     * 司机联系方式
     */
    @TableField("driver_phone")
    private String driverPhone;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 出库数量
     */
    @TableField("outbound_sum_qty")
    private BigDecimal outboundSumQty;
    /**
     * 明细行数
     */
    @TableField("line_count")
    private Integer lineCount;
    /**
     * 资料生成方式(10:手工创建,20:道闸触发,30:APP触发)
     */
    @TableField("gen_method")
    private String genMethod;
    /**
     * 出库类型(10:通知出库,20:盲出出库,30:维修出库,40:借车出库)
     */
    private String type;
    /**
     * 状态(10:保存,20:审核,50:撤销)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Long noticeId) {
        this.noticeId = noticeId;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getOutboundNo() {
        return outboundNo;
    }

    public void setOutboundNo(String outboundNo) {
        this.outboundNo = outboundNo;
    }

    public Date getOutboundTime() {
        return outboundTime;
    }

    public void setOutboundTime(Date outboundTime) {
        this.outboundTime = outboundTime;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getOutboundSumQty() {
        return outboundSumQty;
    }

    public void setOutboundSumQty(BigDecimal outboundSumQty) {
        this.outboundSumQty = outboundSumQty;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getGenMethod() {
        return genMethod;
    }

    public void setGenMethod(String genMethod) {
        this.genMethod = genMethod;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OutboundShipHeader{" +
                ", id=" + id +
                ", noticeId=" + noticeId +
                ", storeHouseId=" + storeHouseId +
                ", outboundNo=" + outboundNo +
                ", outboundTime=" + outboundTime +
                ", ownerId=" + ownerId +
                ", ownerOrderNo=" + ownerOrderNo +
                ", carrierId=" + carrierId +
                ", carrierName=" + carrierName +
                ", transportMethod=" + transportMethod +
                ", plateNumber=" + plateNumber +
                ", driverName=" + driverName +
                ", driverPhone=" + driverPhone +
                ", uom=" + uom +
                ", outboundSumQty=" + outboundSumQty +
                ", lineCount=" + lineCount +
                ", genMethod=" + genMethod +
                ", type=" + type +
                ", status=" + status +
                ", remarks=" + remarks +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }

    public List<OutboundShipLine> getOutboundShipLineList() {
        return outboundShipLineList;
    }

    public void setOutboundShipLineList(List<OutboundShipLine> outboundShipLineList) {
        this.outboundShipLineList = outboundShipLineList;
    }

    public void addOutboundShipLine(OutboundShipLine outboundShipLine) {
        if (Objects.isNull(outboundShipLineList)) {
            outboundShipLineList = new ArrayList<>();
        }
        outboundShipLineList.add(outboundShipLine);
    }
}
