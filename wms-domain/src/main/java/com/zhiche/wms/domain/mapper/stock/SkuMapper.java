package com.zhiche.wms.domain.mapper.stock;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.stock.Sku;

/**
 * <p>
 * 库存量单位(Stock Keeping Unit) Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface SkuMapper extends BaseMapper<Sku> {

}
