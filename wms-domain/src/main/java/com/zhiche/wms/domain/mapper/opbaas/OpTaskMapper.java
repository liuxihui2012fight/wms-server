package com.zhiche.wms.domain.mapper.opbaas;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.opbaas.OpTask;
import com.zhiche.wms.dto.opbaas.resultdto.TaskDetailResultDTO;
import com.zhiche.wms.dto.opbaas.resultdto.TaskResultDTO;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 作业任务 Mapper 接口
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface OpTaskMapper extends BaseMapper<OpTask> {

    List<TaskDetailResultDTO> getTaskDetailScan(HashMap<String, Object> queryParam);

    List<TaskDetailResultDTO> getTaskDetail(HashMap<String, Object> queryParam);

    List<TaskResultDTO> queryTaskList(HashMap<String, Object> queryParam);

    List<TaskResultDTO> selectTaskStartOrFinishByParam(HashMap<String, Object> params);

    int countTaskStartOrFinishByParam(HashMap<String, Object> params);

    int countTaskList(HashMap<String, Object> queryParam);

    void insertTaskBatch(ArrayList<OpTask> taskBatch);

    List<TaskResultDTO> queryTaskInfo(HashMap<String, Object> params);

    int countTaskInfoByParamNew(HashMap<String, Object> params);

    List<TaskResultDTO> selectTaskInfoByPage(Page<TaskResultDTO> page, @Param("ew") Wrapper<TaskResultDTO> ew);

    List<TaskResultDTO> queryTaskPage(Page<TaskResultDTO> page, @Param("ew") EntityWrapper<TaskResultDTO> ew);

    OpTask queryNewTask(HashMap<String, Object> params);

    List<TaskResultDTO> selectTaskInfo(@Param("ew") Wrapper<TaskResultDTO> ew);

    List<TaskResultDTO> queryDriverTask();

}
