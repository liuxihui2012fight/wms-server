package com.zhiche.wms.domain.model.otm;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:41 2019/3/20
 */
public class OtmReleaseOtmDTO implements Serializable {

    @JsonSerialize(using = ToStringSerializer.class)
    private long id;
    private String releaseGid;
    private String shipmentGid;
    private String customerId;
    private String cusOrderNo;
    private String cusWaybillNo;
    private String cusTransMode;

    private String orderAtt;
    private Date expectInboundDate;
    private Date expcetShipDate;
    private Date expectArriveDate;
    private String logLineId;
    private String originLocationGid;
    private String originLocationName;
    private String originLocationAddress;
    private String destLocationGid;
    private String destLocationName;
    private String destLocationAddress;
    private String stanVehicleType;
    private String vin;
    private String remarks;
    private Date gmtCreate;
    private Date gmtModified;
    private String vehicleDescribe;
    private String outboundHouseId;
    private String inboundHouseId;
    private String oorStatus;
    private Date shipDate;

    public Date getShipDate () {
        return shipDate;
    }

    public void setShipDate (Date shipDate) {
        this.shipDate = shipDate;
    }

    public long getId () {
        return id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getCustomerId () {
        return customerId;
    }

    public void setCustomerId (String customerId) {
        this.customerId = customerId;
    }

    public String getCusOrderNo () {
        return cusOrderNo;
    }

    public void setCusOrderNo (String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusWaybillNo () {
        return cusWaybillNo;
    }

    public void setCusWaybillNo (String cusWaybillNo) {
        this.cusWaybillNo = cusWaybillNo;
    }

    public String getCusTransMode () {
        return cusTransMode;
    }

    public void setCusTransMode (String cusTransMode) {
        this.cusTransMode = cusTransMode;
    }


    public String getOrderAtt () {
        return orderAtt;
    }

    public void setOrderAtt (String orderAtt) {
        this.orderAtt = orderAtt;
    }

    public Date getExpectInboundDate () {
        return expectInboundDate;
    }

    public void setExpectInboundDate (Date expectInboundDate) {
        this.expectInboundDate = expectInboundDate;
    }

    public Date getExpcetShipDate () {
        return expcetShipDate;
    }

    public void setExpcetShipDate (Date expcetShipDate) {
        this.expcetShipDate = expcetShipDate;
    }

    public Date getExpectArriveDate () {
        return expectArriveDate;
    }

    public void setExpectArriveDate (Date expectArriveDate) {
        this.expectArriveDate = expectArriveDate;
    }

    public String getLogLineId () {
        return logLineId;
    }

    public void setLogLineId (String logLineId) {
        this.logLineId = logLineId;
    }

    public String getOriginLocationGid () {
        return originLocationGid;
    }

    public void setOriginLocationGid (String originLocationGid) {
        this.originLocationGid = originLocationGid;
    }

    public String getOriginLocationName () {
        return originLocationName;
    }

    public void setOriginLocationName (String originLocationName) {
        this.originLocationName = originLocationName;
    }

    public String getOriginLocationAddress () {
        return originLocationAddress;
    }

    public void setOriginLocationAddress (String originLocationAddress) {
        this.originLocationAddress = originLocationAddress;
    }

    public String getDestLocationGid () {
        return destLocationGid;
    }

    public void setDestLocationGid (String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    public String getDestLocationName () {
        return destLocationName;
    }

    public void setDestLocationName (String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestLocationAddress () {
        return destLocationAddress;
    }

    public void setDestLocationAddress (String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

    public String getStanVehicleType () {
        return stanVehicleType;
    }

    public void setStanVehicleType (String stanVehicleType) {
        this.stanVehicleType = stanVehicleType;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getVehicleDescribe () {
        return vehicleDescribe;
    }

    public void setVehicleDescribe (String vehicleDescribe) {
        this.vehicleDescribe = vehicleDescribe;
    }

    public String getOutboundHouseId () {
        return outboundHouseId;
    }

    public void setOutboundHouseId (String outboundHouseId) {
        this.outboundHouseId = outboundHouseId;
    }

    public String getInboundHouseId () {
        return inboundHouseId;
    }

    public void setInboundHouseId (String inboundHouseId) {
        this.inboundHouseId = inboundHouseId;
    }

    public String getOorStatus () {
        return oorStatus;
    }

    public void setOorStatus (String oorStatus) {
        this.oorStatus = oorStatus;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("OtmReleaseOtmDTO{");
        sb.append("id=").append(id);
        sb.append(", releaseGid='").append(releaseGid).append('\'');
        sb.append(", shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", customerId='").append(customerId).append('\'');
        sb.append(", cusOrderNo='").append(cusOrderNo).append('\'');
        sb.append(", cusWaybillNo='").append(cusWaybillNo).append('\'');
        sb.append(", cusTransMode='").append(cusTransMode).append('\'');
        sb.append(", orderAtt='").append(orderAtt).append('\'');
        sb.append(", expectInboundDate=").append(expectInboundDate);
        sb.append(", expcetShipDate=").append(expcetShipDate);
        sb.append(", expectArriveDate=").append(expectArriveDate);
        sb.append(", logLineId='").append(logLineId).append('\'');
        sb.append(", originLocationGid='").append(originLocationGid).append('\'');
        sb.append(", originLocationName='").append(originLocationName).append('\'');
        sb.append(", originLocationAddress='").append(originLocationAddress).append('\'');
        sb.append(", destLocationGid='").append(destLocationGid).append('\'');
        sb.append(", destLocationName='").append(destLocationName).append('\'');
        sb.append(", destLocationAddress='").append(destLocationAddress).append('\'');
        sb.append(", stanVehicleType='").append(stanVehicleType).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", remarks='").append(remarks).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", vehicleDescribe='").append(vehicleDescribe).append('\'');
        sb.append(", outboundHouseId='").append(outboundHouseId).append('\'');
        sb.append(", inboundHouseId='").append(inboundHouseId).append('\'');
        sb.append(", oorStatus='").append(oorStatus).append('\'');
        sb.append(", shipDate='").append(shipDate).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
