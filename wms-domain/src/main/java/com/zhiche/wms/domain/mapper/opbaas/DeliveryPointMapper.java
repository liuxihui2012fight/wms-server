package com.zhiche.wms.domain.mapper.opbaas;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;
import com.zhiche.wms.dto.opbaas.resultdto.OpDeliveryPointResultDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 发车点配置 Mapper 接口
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface DeliveryPointMapper extends BaseMapper<OpDeliveryPoint> {

    List<OpDeliveryPoint> userPoint(Integer userId);

    List<String> queryOpdeliveryPoint (@Param("userid") Integer oPointWrapper);
}
