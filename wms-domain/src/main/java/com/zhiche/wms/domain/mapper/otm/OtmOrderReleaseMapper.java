package com.zhiche.wms.domain.mapper.otm;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.otm.OtmCancleShipment;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.domain.model.otm.OtmReleaseOtmDTO;
import com.zhiche.wms.domain.model.otm.ShipmentInfo;
import com.zhiche.wms.dto.opbaas.paramdto.OrderReleaseParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ReleaseDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ReleaseWithShipmentDTO;
import com.zhiche.wms.dto.opbaas.resultdto.NodeProcessInfoDTO;
import com.zhiche.wms.dto.opbaas.resultdto.TaskReleaseResultDTO;
import com.zhiche.wms.dto.shipmentDto.PushShipmentToOtmDto;
import com.zhiche.wms.dto.shipmentDto.PushShipmentToTmsDto;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 运单 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
public interface OtmOrderReleaseMapper extends BaseMapper<OtmOrderRelease> {

    List<OrderReleaseParamDTO> queryOrderReleaseList(Page<OrderReleaseParamDTO> page, @Param("ew") Wrapper<OrderReleaseParamDTO> ew);

    List<TaskReleaseResultDTO> selectReleaseInfo(HashMap<String, Object> params);

//    NodeProcessInfoDTO queryNodeInfo (@Param("ew") Wrapper<NodeProcessInfoDTO> ew, String taskNode);

    List<ReleaseWithShipmentDTO> queryReleaseShipList(Page<ReleaseWithShipmentDTO> page, @Param("ew") EntityWrapper<ReleaseWithShipmentDTO> ew);

    List<ReleaseWithShipmentDTO> getReleaseShipDetail(@Param("ew") EntityWrapper<ReleaseWithShipmentDTO> ew);

    void updateSQLMode();

    List<OtmReleaseOtmDTO> queryPushShipmentOtm (Page<OtmReleaseOtmDTO> page, @Param("ew") EntityWrapper<OtmReleaseOtmDTO> oorEW);

    NodeProcessInfoDTO queryNodeInfo (Map<String, Object> param);

    List<OtmCancleShipment> queryCancleShipment (@Param("ew") EntityWrapper<OtmCancleShipment> releaseEw);

    void cancleReleaseStatus (@Param("releaseStatus") String releaseStatus,@Param("id") long id);

    List<PushShipmentToOtmDto> queryShipmentLogToOtm ();

    List<PushShipmentToOtmDto> pushInboundToOtm ();

    void updateShipDate (@Param("id") Long id, @Param("shipDate") Date shipDate);

    void updateReleaseInfo (ReleaseDTO releaseDTO);

   List<PushShipmentToTmsDto> queryToBmsShipDate (@Param("ew") EntityWrapper<PushShipmentToTmsDto> asList);

    List<OtmCancleShipment> pullOtmShipDate ();

    void updateShipStatus (@Param("shipmentGid") String shipmentGid, @Param("shipDate") Date dtshipdate, @Param("oorId") long oorId);

    void updateRelease (OtmOrderRelease otmOrderRelease);

    List<String> queryCancleOutboundCount (@Param("ew")EntityWrapper<OtmOrderRelease> releaseEntityWrapper);

    List<OtmCancleShipment> queryWaterIronData ();
    List<OtmOrderRelease> selectHaveDispatch (@Param("ew")EntityWrapper<OtmOrderRelease> releaseEw);
}
