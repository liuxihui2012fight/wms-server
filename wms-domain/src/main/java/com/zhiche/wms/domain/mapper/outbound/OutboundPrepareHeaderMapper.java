package com.zhiche.wms.domain.mapper.outbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareHeader;
import com.zhiche.wms.dto.outbound.OutboundPrepareDTO;
import com.zhiche.wms.dto.outbound.OutboundPrepareVehicleDTO;
import com.zhiche.wms.dto.outbound.PreparationListHeadDTO;
import com.zhiche.wms.dto.outbound.PreparationListDetailDTO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 出库备料单头 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
public interface OutboundPrepareHeaderMapper extends BaseMapper<OutboundPrepareHeader> {

    Integer updateStatus(@Param("id") Long id, @Param("houseId") Long houseId, @Param("status") Integer status, @Param("damagedSum") BigDecimal damagedSum);

    List<PreparationListDetailDTO> selectPreparationDetailListByParams(HashMap<String, Object> params);

    List<PreparationListHeadDTO> queryOutPreparePage(Page<PreparationListHeadDTO> page, @Param("ew") Wrapper<PreparationListHeadDTO> wp);

    List<PreparationListHeadDTO> queryNotOutPreparePage(Page<PreparationListHeadDTO> page, @Param("ew") Wrapper<PreparationListHeadDTO> wp);

    List<OutboundPrepareVehicleDTO> queryPrepareVehiclePage(Page<OutboundPrepareVehicleDTO> page, @Param("ew") EntityWrapper<OutboundPrepareHeader> phEW);

    List<PreparationListHeadDTO> queryOutKeyPreparePage (Page<PreparationListHeadDTO> page, @Param("ew") Wrapper<PreparationListHeadDTO> wp,@Param(
            "storeHouseId") String storeHouseId);

    void updateSQLMode();
}
