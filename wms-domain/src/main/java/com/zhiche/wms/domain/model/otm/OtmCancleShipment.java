package com.zhiche.wms.domain.model.otm;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:41 2019/5/23
 */
public class OtmCancleShipment implements Serializable {

    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private long id;

    /**
     * 指令号
     */
    private String shipmentGid;

    /**
     * 运单号
     */
    private String releaseGid;

    /**
     * 入库状态
     */
    private String inboundStatus;

    private String vin;

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getInboundStatus () {
        return inboundStatus;
    }

    public void setInboundStatus (String inboundStatus) {
        this.inboundStatus = inboundStatus;
    }

    public long getId () {
        return id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("OtmCancleShipment{");
        sb.append("id=").append(id);
        sb.append(", shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", releaseGid='").append(releaseGid).append('\'');
        sb.append(", inboundStatus='").append(inboundStatus).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
