package com.zhiche.wms.domain.mapper.inbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.inbound.WMSCancleInbound;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 14:53 2019/9/19
 */
public interface WMSCancleInboundMapper extends BaseMapper<WMSCancleInbound> {
}
