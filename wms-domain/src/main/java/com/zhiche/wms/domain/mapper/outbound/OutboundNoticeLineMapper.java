package com.zhiche.wms.domain.mapper.outbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.dto.outbound.OutboundNoticeDTO;
import com.zhiche.wms.dto.outbound.OutboundShipQrCodeResultDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 出库通知明细 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface OutboundNoticeLineMapper extends BaseMapper<OutboundNoticeLine> {

    OutboundNoticeDTO getNoticeByLineId(@Param("id") Long id, @Param("houseId") Long houseId);

    List<OutboundNoticeDTO> queryPageNotice(Page<OutboundNoticeDTO> page, @Param("ew") Wrapper<OutboundNoticeDTO> ew);

    OutboundNoticeDTO getNoticeByKey(@Param("key") String id, @Param("houseId") Long houseId);

    List<OutboundNoticeDTO> queryListNotice(@Param("ew") Wrapper<OutboundNoticeDTO> ew);

    List<OutboundShipQrCodeResultDTO> selectQrCodeInfoByParams(HashMap<String, Object> params);

    List<OutboundNoticeLine> pageOutboundNotice(Page<OutboundNoticeLine> page,@Param("ew") EntityWrapper<OutboundNoticeLine> outEW);

    int selectCountWithHeader(@Param("ew") EntityWrapper<OutboundNoticeLine> nlEW);
    /**
     * 根据头ID更新明细出库状态
     */
    Integer updateOutboundLineStatus (@Param("headerId")Long headerId,@Param("noticeLineId") Long noticeLineId);
}
