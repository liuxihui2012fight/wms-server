package com.zhiche.wms.domain.mapper.movement;

import com.zhiche.wms.domain.model.movement.MovementHeader;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 移位单头 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface MovementHeaderMapper extends BaseMapper<MovementHeader> {

}
