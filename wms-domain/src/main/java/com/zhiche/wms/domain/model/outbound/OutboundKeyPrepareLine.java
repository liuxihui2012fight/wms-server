package com.zhiche.wms.domain.model.outbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: 钥匙备料详细表
 * @Date: Create in 17:12 2018/12/19
 */
@TableName("w_outbound_key_prepare_line")
public class OutboundKeyPrepareLine extends Model<OutboundKeyPrepareLine> {
    @Override
    protected Serializable pkVal () {
        return null;
    }

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 备料单头键
     */
    @TableField("header_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long headerId;
    /**
     * 通知单明细键
     */
    @TableField("notice_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long noticeLineId;
    /**
     * 储位
     */
    @TableField("location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long locationId;
    /**
     * 储位
     */
    @TableField("stock_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stockId;
    /**
     * 储位号
     */
    @TableField("location_no")
    private String locationNo;
    /**
     * 发货区
     */
    @TableField("ship_space")
    private String shipSpace;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 货主单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;
    /**
     * 物流订单号
     */
    @TableField("lgs_order_no")
    private String lgsOrderNo;
    /**
     * 物料ID
     */
    @TableField("materiel_id")
    private String materielId;
    /**
     * 物料代码
     */
    @TableField("materiel_code")
    private String materielCode;
    /**
     * 物料名称
     */
    @TableField("materiel_name")
    private String materielName;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 计划数量
     */
    @TableField("plan_qty")
    private BigDecimal planQty;
    /**
     * 计划净重
     */
    @TableField("plan_net_weight")
    private BigDecimal planNetWeight;
    /**
     * 计划毛重
     */
    @TableField("plan_gross_weight")
    private BigDecimal planGrossWeight;
    /**
     * 计划体积
     */
    @TableField("plan_gross_cubage")
    private BigDecimal planGrossCubage;
    /**
     * 计划件数
     */
    @TableField("plan_packed_count")
    private BigDecimal planPackedCount;
    /**
     * 实际数量
     */
    @TableField("actual_qty")
    private BigDecimal actualQty;
    /**
     * 实际净重
     */
    @TableField("actual_net_weight")
    private BigDecimal actualNetWeight;
    /**
     * 实际毛重
     */
    @TableField("actual_gross_weight")
    private BigDecimal actualGrossWeight;
    /**
     * 实际体积
     */
    @TableField("actual_gross_cubage")
    private BigDecimal actualGrossCubage;
    /**
     * 实际件数
     */
    @TableField("actual_packed_count")
    private BigDecimal actualPackedCount;
    /**
     * 备料员
     */
    private String preparetor;
    /**
     * 计划时间
     */
    @TableField("plan_time")
    private Date planTime;
    /**
     * 开始时间
     */
    @TableField("start_time")
    private Date startTime;
    /**
     * 完成时间
     */
    @TableField("finish_time")
    private Date finishTime;
    /**
     * 批号0
     */
    @TableField("lot_no0")
    private String lotNo0;
    /**
     * 批号1
     */
    @TableField("lot_no1")
    private String lotNo1;
    /**
     * 批号2
     */
    @TableField("lot_no2")
    private String lotNo2;
    /**
     * 批号3
     */
    @TableField("lot_no3")
    private String lotNo3;
    /**
     * 批号4
     */
    @TableField("lot_no4")
    private String lotNo4;
    /**
     * 批号5
     */
    @TableField("lot_no5")
    private String lotNo5;
    /**
     * 批号6
     */
    @TableField("lot_no6")
    private String lotNo6;
    /**
     * 批号7
     */
    @TableField("lot_no7")
    private String lotNo7;
    /**
     * 批号8
     */
    @TableField("lot_no8")
    private String lotNo8;
    /**
     * 批号9
     */
    @TableField("lot_no9")
    private String lotNo9;
    /**
     * 状态(10:未开始,20:已开始,30:已完成)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    /**
     * 实车校验状态:0 未设置 1校验通过 2校验不通过
     */
    @TableField("vehicle_checkout_status")
    private String vehicleCheckoutStatus;

    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("OutboundKeyPrepareLine{");
        sb.append("id=").append(id);
        sb.append(", headerId=").append(headerId);
        sb.append(", noticeLineId=").append(noticeLineId);
        sb.append(", locationId=").append(locationId);
        sb.append(", stockId=").append(stockId);
        sb.append(", locationNo='").append(locationNo).append('\'');
        sb.append(", shipSpace='").append(shipSpace).append('\'');
        sb.append(", ownerId='").append(ownerId).append('\'');
        sb.append(", ownerOrderNo='").append(ownerOrderNo).append('\'');
        sb.append(", lgsOrderNo='").append(lgsOrderNo).append('\'');
        sb.append(", materielId='").append(materielId).append('\'');
        sb.append(", materielCode='").append(materielCode).append('\'');
        sb.append(", materielName='").append(materielName).append('\'');
        sb.append(", uom='").append(uom).append('\'');
        sb.append(", planQty=").append(planQty);
        sb.append(", planNetWeight=").append(planNetWeight);
        sb.append(", planGrossWeight=").append(planGrossWeight);
        sb.append(", planGrossCubage=").append(planGrossCubage);
        sb.append(", planPackedCount=").append(planPackedCount);
        sb.append(", actualQty=").append(actualQty);
        sb.append(", actualNetWeight=").append(actualNetWeight);
        sb.append(", actualGrossWeight=").append(actualGrossWeight);
        sb.append(", actualGrossCubage=").append(actualGrossCubage);
        sb.append(", actualPackedCount=").append(actualPackedCount);
        sb.append(", preparetor='").append(preparetor).append('\'');
        sb.append(", planTime=").append(planTime);
        sb.append(", startTime=").append(startTime);
        sb.append(", finishTime=").append(finishTime);
        sb.append(", lotNo0='").append(lotNo0).append('\'');
        sb.append(", lotNo1='").append(lotNo1).append('\'');
        sb.append(", lotNo2='").append(lotNo2).append('\'');
        sb.append(", lotNo3='").append(lotNo3).append('\'');
        sb.append(", lotNo4='").append(lotNo4).append('\'');
        sb.append(", lotNo5='").append(lotNo5).append('\'');
        sb.append(", lotNo6='").append(lotNo6).append('\'');
        sb.append(", lotNo7='").append(lotNo7).append('\'');
        sb.append(", lotNo8='").append(lotNo8).append('\'');
        sb.append(", lotNo9='").append(lotNo9).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", remarks='").append(remarks).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", vehicleCheckoutStatus='").append(vehicleCheckoutStatus).append('\'');
        sb.append(", userModified='").append(userModified).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public Long getHeaderId () {
        return headerId;
    }

    public void setHeaderId (Long headerId) {
        this.headerId = headerId;
    }

    public Long getNoticeLineId () {
        return noticeLineId;
    }

    public void setNoticeLineId (Long noticeLineId) {
        this.noticeLineId = noticeLineId;
    }

    public Long getLocationId () {
        return locationId;
    }

    public void setLocationId (Long locationId) {
        this.locationId = locationId;
    }

    public Long getStockId () {
        return stockId;
    }

    public void setStockId (Long stockId) {
        this.stockId = stockId;
    }

    public String getLocationNo () {
        return locationNo;
    }

    public void setLocationNo (String locationNo) {
        this.locationNo = locationNo;
    }

    public String getShipSpace () {
        return shipSpace;
    }

    public void setShipSpace (String shipSpace) {
        this.shipSpace = shipSpace;
    }

    public String getOwnerId () {
        return ownerId;
    }

    public void setOwnerId (String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo () {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo (String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getLgsOrderNo () {
        return lgsOrderNo;
    }

    public void setLgsOrderNo (String lgsOrderNo) {
        this.lgsOrderNo = lgsOrderNo;
    }

    public String getMaterielId () {
        return materielId;
    }

    public void setMaterielId (String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode () {
        return materielCode;
    }

    public void setMaterielCode (String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName () {
        return materielName;
    }

    public void setMaterielName (String materielName) {
        this.materielName = materielName;
    }

    public String getUom () {
        return uom;
    }

    public void setUom (String uom) {
        this.uom = uom;
    }

    public BigDecimal getPlanQty () {
        return planQty;
    }

    public void setPlanQty (BigDecimal planQty) {
        this.planQty = planQty;
    }

    public BigDecimal getPlanNetWeight () {
        return planNetWeight;
    }

    public void setPlanNetWeight (BigDecimal planNetWeight) {
        this.planNetWeight = planNetWeight;
    }

    public BigDecimal getPlanGrossWeight () {
        return planGrossWeight;
    }

    public void setPlanGrossWeight (BigDecimal planGrossWeight) {
        this.planGrossWeight = planGrossWeight;
    }

    public BigDecimal getPlanGrossCubage () {
        return planGrossCubage;
    }

    public void setPlanGrossCubage (BigDecimal planGrossCubage) {
        this.planGrossCubage = planGrossCubage;
    }

    public BigDecimal getPlanPackedCount () {
        return planPackedCount;
    }

    public void setPlanPackedCount (BigDecimal planPackedCount) {
        this.planPackedCount = planPackedCount;
    }

    public BigDecimal getActualQty () {
        return actualQty;
    }

    public void setActualQty (BigDecimal actualQty) {
        this.actualQty = actualQty;
    }

    public BigDecimal getActualNetWeight () {
        return actualNetWeight;
    }

    public void setActualNetWeight (BigDecimal actualNetWeight) {
        this.actualNetWeight = actualNetWeight;
    }

    public BigDecimal getActualGrossWeight () {
        return actualGrossWeight;
    }

    public void setActualGrossWeight (BigDecimal actualGrossWeight) {
        this.actualGrossWeight = actualGrossWeight;
    }

    public BigDecimal getActualGrossCubage () {
        return actualGrossCubage;
    }

    public void setActualGrossCubage (BigDecimal actualGrossCubage) {
        this.actualGrossCubage = actualGrossCubage;
    }

    public BigDecimal getActualPackedCount () {
        return actualPackedCount;
    }

    public void setActualPackedCount (BigDecimal actualPackedCount) {
        this.actualPackedCount = actualPackedCount;
    }

    public String getPreparetor () {
        return preparetor;
    }

    public void setPreparetor (String preparetor) {
        this.preparetor = preparetor;
    }

    public Date getPlanTime () {
        return planTime;
    }

    public void setPlanTime (Date planTime) {
        this.planTime = planTime;
    }

    public Date getStartTime () {
        return startTime;
    }

    public void setStartTime (Date startTime) {
        this.startTime = startTime;
    }

    public Date getFinishTime () {
        return finishTime;
    }

    public void setFinishTime (Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getLotNo0 () {
        return lotNo0;
    }

    public void setLotNo0 (String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1 () {
        return lotNo1;
    }

    public void setLotNo1 (String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2 () {
        return lotNo2;
    }

    public void setLotNo2 (String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3 () {
        return lotNo3;
    }

    public void setLotNo3 (String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4 () {
        return lotNo4;
    }

    public void setLotNo4 (String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5 () {
        return lotNo5;
    }

    public void setLotNo5 (String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6 () {
        return lotNo6;
    }

    public void setLotNo6 (String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7 () {
        return lotNo7;
    }

    public void setLotNo7 (String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8 () {
        return lotNo8;
    }

    public void setLotNo8 (String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9 () {
        return lotNo9;
    }

    public void setLotNo9 (String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getVehicleCheckoutStatus () {
        return vehicleCheckoutStatus;
    }

    public void setVehicleCheckoutStatus (String vehicleCheckoutStatus) {
        this.vehicleCheckoutStatus = vehicleCheckoutStatus;
    }

    public String getUserModified () {
        return userModified;
    }

    public void setUserModified (String userModified) {
        this.userModified = userModified;
    }
}
