package com.zhiche.wms.domain.model.stock;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 库存
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@TableName("w_stock")
public class Stock extends Model<Stock> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 仓库
     */
    @TableField("store_house_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 储位
     */
    @TableField("location_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long locationId;
    /**
     * SKU
     */
    @TableField("sku_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long skuId;
    /**
     * 数量
     */
    private BigDecimal qty;
    /**
     * 净重
     */
    @TableField("net_weight")
    private BigDecimal netWeight;
    /**
     * 毛重
     */
    @TableField("gross_weight")
    private BigDecimal grossWeight;
    /**
     * 体积
     */
    @TableField("gross_cubage")
    private BigDecimal grossCubage;
    /**
     * 件数
     */
    @TableField("packed_count")
    private BigDecimal packedCount;
    /**
     * 状态(10:正常,20:失效)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    @TableField("stock_property")
    private String stockProperty;

    public String getStockProperty () {
        return stockProperty;
    }

    public void setStockProperty (String stockProperty) {
        this.stockProperty = stockProperty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public BigDecimal getGrossCubage() {
        return grossCubage;
    }

    public void setGrossCubage(BigDecimal grossCubage) {
        this.grossCubage = grossCubage;
    }

    public BigDecimal getPackedCount() {
        return packedCount;
    }

    public void setPackedCount(BigDecimal packedCount) {
        this.packedCount = packedCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Stock{" +
                ", id=" + id +
                ", storeHouseId=" + storeHouseId +
                ", locationId=" + locationId +
                ", skuId=" + skuId +
                ", qty=" + qty +
                ", netWeight=" + netWeight +
                ", grossWeight=" + grossWeight +
                ", grossCubage=" + grossCubage +
                ", packedCount=" + packedCount +
                ", status=" + status +
                ", remarks=" + remarks +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", stockProperty=" + stockProperty +
                "}";
    }
}
