package com.zhiche.wms.domain.model.opbaas;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 异常登记
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@TableName("exception_register")
public class ExceptionRegister extends Model<ExceptionRegister> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
    /**
     * OMS订单号
     */
    @TableField("oms_order_no")
    private String omsOrderNo;
    /**
     * VIN码
     */
    private String vin;
    /**
     * 二维码
     */
    @TableField("qr_code")
    private String qrCode;
    /**
     * 作业节点(0:指令,10:验车,20:寻车,30:提车,40:入库,50:出库,)
     */
    @TableField("task_node")
    private String taskNode;
    /**
     * 业务单据
     */
    @TableField("business_doc")
    private String businessDoc;
    /**
     * 一级异常(异常区域)
     */
    @TableField("ex_level1")
    private String exLevel1;
    /**
     * 二级异常(异常位置)
     */
    @TableField("ex_level2")
    private String exLevel2;
    /**
     * 三级异常(异常类别)
     */
    @TableField("ex_level3")
    private String exLevel3;
    /**
     * 描述
     */
    @TableField("ex_describe")
    private String exDescribe;
    /**
     * 登记人
     */
    @TableField("register_user")
    private String registerUser;
    /**
     * 登记时间
     */
    @TableField("register_time")
    private Date registerTime;
    /**
     * 责任方
     */
    @TableField("involved_party")
    private String involvedParty;
    /**
     * 处理状态(10:未处理,20:处理中,30;关闭,40:让步)
     */
    @TableField("deal_status")
    private String dealStatus;
    /**
     * 处理方式(10:返厂维修,20:出库维修,30:库内维修,40:带伤发运)
     */
    @TableField("deal_type")
    private String dealType;
    /**
     * 处理结果描述
     */
    @TableField("deal_result_desc")
    private String dealResultDesc;
    /**
     * 处理完成时间
     */
    @TableField("deal_end_time")
    private Date dealEndTime;
    /**
     * 状态(10：登记，20：处理，30：关闭)
     */
    private String status;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    /**
     * OTM标记是否可发运(Y:可发, N:不可发)
     */
    @TableField("otm_status")
    private String otmStatus;

    /**
     * 仓库id
     */
    @TableField("house_id")
    private String houseId;


    //预计处理异常时间
    @TableField("estimated_processing_time")
    private Date estimatedProcessingTime;

    //系统运单号OR
    @TableField("release_gid")
    private String releaseGid;

    //备注
    private String remark;

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public Date getEstimatedProcessingTime () {
        return estimatedProcessingTime;
    }

    public void setEstimatedProcessingTime (Date estimatedProcessingTime) {
        this.estimatedProcessingTime = estimatedProcessingTime;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getOtmStatus() {
        return otmStatus;
    }

    public void setOtmStatus(String otmStatus) {
        this.otmStatus = otmStatus;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOmsOrderNo() {
        return omsOrderNo;
    }

    public void setOmsOrderNo(String omsOrderNo) {
        this.omsOrderNo = omsOrderNo;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getTaskNode() {
        return taskNode;
    }

    public void setTaskNode(String taskNode) {
        this.taskNode = taskNode;
    }

    public String getBusinessDoc() {
        return businessDoc;
    }

    public void setBusinessDoc(String businessDoc) {
        this.businessDoc = businessDoc;
    }

    public String getExLevel1() {
        return exLevel1;
    }

    public void setExLevel1(String exLevel1) {
        this.exLevel1 = exLevel1;
    }

    public String getExLevel2() {
        return exLevel2;
    }

    public void setExLevel2(String exLevel2) {
        this.exLevel2 = exLevel2;
    }

    public String getExLevel3() {
        return exLevel3;
    }

    public void setExLevel3(String exLevel3) {
        this.exLevel3 = exLevel3;
    }

    public String getExDescribe() {
        return exDescribe;
    }

    public void setExDescribe(String exDescribe) {
        this.exDescribe = exDescribe;
    }

    public String getRegisterUser() {
        return registerUser;
    }

    public void setRegisterUser(String registerUser) {
        this.registerUser = registerUser;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public String getInvolvedParty() {
        return involvedParty;
    }

    public void setInvolvedParty(String involvedParty) {
        this.involvedParty = involvedParty;
    }

    public String getDealStatus() {
        return dealStatus;
    }

    public void setDealStatus(String dealStatus) {
        this.dealStatus = dealStatus;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealResultDesc() {
        return dealResultDesc;
    }

    public void setDealResultDesc(String dealResultDesc) {
        this.dealResultDesc = dealResultDesc;
    }

    public Date getDealEndTime() {
        return dealEndTime;
    }

    public void setDealEndTime(Date dealEndTime) {
        this.dealEndTime = dealEndTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExceptionRegister{" +
                ", id=" + id +
                ", omsOrderNo=" + omsOrderNo +
                ", vin=" + vin +
                ", qrCode=" + qrCode +
                ", taskNode=" + taskNode +
                ", businessDoc=" + businessDoc +
                ", exLevel1=" + exLevel1 +
                ", exLevel2=" + exLevel2 +
                ", exLevel3=" + exLevel3 +
                ", exDescribe=" + exDescribe +
                ", registerUser=" + registerUser +
                ", registerTime=" + registerTime +
                ", involvedParty=" + involvedParty +
                ", dealStatus=" + dealStatus +
                ", dealType=" + dealType +
                ", dealResultDesc=" + dealResultDesc +
                ", dealEndTime=" + dealEndTime +
                ", status=" + status +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
