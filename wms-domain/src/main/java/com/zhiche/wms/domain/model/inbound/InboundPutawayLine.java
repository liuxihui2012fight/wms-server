package com.zhiche.wms.domain.model.inbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 入库单明细
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
@TableName("w_inbound_putaway_line")
public class InboundPutawayLine extends Model<InboundPutawayLine> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 入库单头键
     */
    @TableField("header_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long headerId;
    /**
     * 通知单明细键
     */
    @TableField("notice_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long noticeLineId;

    /**
     * 储位
     */
    @TableField("location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long locationId;

    /**
     * 储位号
     */
    @TableField("location_no")
    private String locationNo;
    /**
     * 序号
     */
    private String seq;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 货主单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;
    /**
     * 物料ID
     */
    @TableField("materiel_id")
    private String materielId;
    /**
     * 物料代码
     */
    @TableField("materiel_code")
    private String materielCode;
    /**
     * 物料名称
     */
    @TableField("materiel_name")
    private String materielName;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 入库数量
     */
    @TableField("inbound_qty")
    private BigDecimal inboundQty;
    /**
     * 入库净重
     */
    @TableField("inbound_net_weight")
    private BigDecimal inboundNetWeight;
    /**
     * 入库毛重
     */
    @TableField("inbound_gross_weight")
    private BigDecimal inboundGrossWeight;
    /**
     * 入库体积
     */
    @TableField("inbound_gross_cubage")
    private BigDecimal inboundGrossCubage;
    /**
     * 入库件数
     */
    @TableField("inbound_packed_count")
    private BigDecimal inboundPackedCount;
    /**
     * 批号0
     */
    @TableField("lot_no0")
    private String lotNo0;
    /**
     * 批号1
     */
    @TableField("lot_no1")
    private String lotNo1;
    /**
     * 批号2
     */
    @TableField("lot_no2")
    private String lotNo2;
    /**
     * 批号3
     */
    @TableField("lot_no3")
    private String lotNo3;
    /**
     * 批号4
     */
    @TableField("lot_no4")
    private String lotNo4;
    /**
     * 批号5
     */
    @TableField("lot_no5")
    private String lotNo5;
    /**
     * 批号6
     */
    @TableField("lot_no6")
    private String lotNo6;
    /**
     * 批号7
     */
    @TableField("lot_no7")
    private String lotNo7;
    /**
     * 批号8
     */
    @TableField("lot_no8")
    private String lotNo8;
    /**
     * 批号9
     */
    @TableField("lot_no9")
    private String lotNo9;

    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    /**
     * 钥匙状态
     */
    @TableField("key_status")
    private String keyStatus;

    /**
     * 钥匙收取人
     */
    @TableField("take_key_user")
    private String takeKeyUser;

    @TableField("take_key_date")
    private Date takeKeyDate;

    /**
     * 发放合格证人
     */
    @TableField("giveout_user")
    private String giveoutUser;
    /**
     * 发放合格证时间
     */
    @TableField("giveout_date")
    private Date giveoutDate;
    /**
     * 领取合格证人
     */
    @TableField("receive_user")
    private String receiveUser;
    /**
     * 领取合格证时间
     */
    @TableField("receive_date")
    private Date receiveDate;

    /**
     * 合格证数量
     */
    @TableField("certificat_num")
    private Integer certificatNum;
    /**
     * 合格证状态
     */
    @TableField("certificat_status")
    private String certificatStatus;

    /**
     * 状态
     */
    @TableField("inbound_status")
    private String inboundStatus;

    /**
     * 调整后库位
     */
    @TableField("update_location_no")
    private String updateLocationNo;

    /**
     * 调整后的库位id
     */
    @TableField("update_location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateLocationId;


    public String getUpdateLocationNo () {
        return updateLocationNo;
    }

    public void setUpdateLocationNo (String updateLocationNo) {
        this.updateLocationNo = updateLocationNo;
    }

    public Long getUpdateLocationId () {
        return updateLocationId;
    }

    public void setUpdateLocationId (Long updateLocationId) {
        this.updateLocationId = updateLocationId;
    }

    public String getInboundStatus () {
        return inboundStatus;
    }

    public void setInboundStatus (String inboundStatus) {
        this.inboundStatus = inboundStatus;
    }

    public String getGiveoutUser () {
        return giveoutUser;
    }

    public void setGiveoutUser (String giveoutUser) {
        this.giveoutUser = giveoutUser;
    }

    public Date getGiveoutDate () {
        return giveoutDate;
    }

    public void setGiveoutDate (Date giveoutDate) {
        this.giveoutDate = giveoutDate;
    }

    public String getReceiveUser () {
        return receiveUser;
    }

    public void setReceiveUser (String receiveUser) {
        this.receiveUser = receiveUser;
    }

    public Date getReceiveDate () {
        return receiveDate;
    }

    public void setReceiveDate (Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public Integer getCertificatNum () {
        return certificatNum;
    }

    public void setCertificatNum (Integer certificatNum) {
        this.certificatNum = certificatNum;
    }

    public String getCertificatStatus () {
        return certificatStatus;
    }

    public void setCertificatStatus (String certificatStatus) {
        this.certificatStatus = certificatStatus;
    }

    public Date getTakeKeyDate () {
        return takeKeyDate;
    }

    public void setTakeKeyDate (Date takeKeyDate) {
        this.takeKeyDate = takeKeyDate;
    }

    public String getTakeKeyUser () {
        return takeKeyUser;
    }

    public void setTakeKeyUser (String takeKeyUser) {
        this.takeKeyUser = takeKeyUser;
    }

    public String getKeyStatus() {
        return keyStatus;
    }

    public void setKeyStatus(String keyStatus) {
        this.keyStatus = keyStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public Long getNoticeLineId() {
        return noticeLineId;
    }

    public void setNoticeLineId(Long noticeLineId) {
        this.noticeLineId = noticeLineId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getInboundQty() {
        return inboundQty;
    }

    public void setInboundQty(BigDecimal inboundQty) {
        this.inboundQty = inboundQty;
    }

    public BigDecimal getInboundNetWeight() {
        return inboundNetWeight;
    }

    public void setInboundNetWeight(BigDecimal inboundNetWeight) {
        this.inboundNetWeight = inboundNetWeight;
    }

    public BigDecimal getInboundGrossWeight() {
        return inboundGrossWeight;
    }

    public void setInboundGrossWeight(BigDecimal inboundGrossWeight) {
        this.inboundGrossWeight = inboundGrossWeight;
    }

    public BigDecimal getInboundGrossCubage() {
        return inboundGrossCubage;
    }

    public void setInboundGrossCubage(BigDecimal inboundGrossCubage) {
        this.inboundGrossCubage = inboundGrossCubage;
    }

    public BigDecimal getInboundPackedCount() {
        return inboundPackedCount;
    }

    public void setInboundPackedCount(BigDecimal inboundPackedCount) {
        this.inboundPackedCount = inboundPackedCount;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }


    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("InboundPutawayLine{");
        sb.append("id=").append(id);
        sb.append(", headerId=").append(headerId);
        sb.append(", noticeLineId=").append(noticeLineId);
        sb.append(", locationId=").append(locationId);
        sb.append(", locationNo='").append(locationNo).append('\'');
        sb.append(", seq='").append(seq).append('\'');
        sb.append(", ownerId='").append(ownerId).append('\'');
        sb.append(", ownerOrderNo='").append(ownerOrderNo).append('\'');
        sb.append(", materielId='").append(materielId).append('\'');
        sb.append(", materielCode='").append(materielCode).append('\'');
        sb.append(", materielName='").append(materielName).append('\'');
        sb.append(", uom='").append(uom).append('\'');
        sb.append(", inboundQty=").append(inboundQty);
        sb.append(", inboundNetWeight=").append(inboundNetWeight);
        sb.append(", inboundGrossWeight=").append(inboundGrossWeight);
        sb.append(", inboundGrossCubage=").append(inboundGrossCubage);
        sb.append(", inboundPackedCount=").append(inboundPackedCount);
        sb.append(", lotNo0='").append(lotNo0).append('\'');
        sb.append(", lotNo1='").append(lotNo1).append('\'');
        sb.append(", lotNo2='").append(lotNo2).append('\'');
        sb.append(", lotNo3='").append(lotNo3).append('\'');
        sb.append(", lotNo4='").append(lotNo4).append('\'');
        sb.append(", lotNo5='").append(lotNo5).append('\'');
        sb.append(", lotNo6='").append(lotNo6).append('\'');
        sb.append(", lotNo7='").append(lotNo7).append('\'');
        sb.append(", lotNo8='").append(lotNo8).append('\'');
        sb.append(", lotNo9='").append(lotNo9).append('\'');
        sb.append(", remarks='").append(remarks).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", keyStatus='").append(keyStatus).append('\'');
        sb.append(", takeKeyUser='").append(takeKeyUser).append('\'');
        sb.append(", takeKeyDate=").append(takeKeyDate);
        sb.append(", giveoutUser='").append(giveoutUser).append('\'');
        sb.append(", giveoutDate=").append(giveoutDate);
        sb.append(", receiveUser='").append(receiveUser).append('\'');
        sb.append(", receiveDate=").append(receiveDate);
        sb.append(", certificatNum=").append(certificatNum);
        sb.append(", certificatStatus='").append(certificatStatus).append('\'');
        sb.append(", inboundStatus='").append(inboundStatus).append('\'');
        sb.append(", updateLocationNo='").append(updateLocationNo).append('\'');
        sb.append(", updateLocationId='").append(updateLocationId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
