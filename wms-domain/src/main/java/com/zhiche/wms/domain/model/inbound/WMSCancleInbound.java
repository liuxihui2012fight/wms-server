package com.zhiche.wms.domain.model.inbound;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 14:51 2019/9/19
 */
@TableName("cancle_inbound_log")
public class WMSCancleInbound {

    /**
     * 主键ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 入库记录表主键id
     */
    @TableField("releation_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long releationId;
    /**
     * 指令号
     */
    @TableField("shipment_gid")
    private String shipmentGid;
    /**
     * 运单号
     */
    @TableField("release_gid")
    private String releaseGid;
    /**
     * 车架号
     */
    private String vin;
    /**
     * 取消入库操作人
     */
    @TableField("user_create")
    private String userCreate;

    /**
     * 入库仓库
     */
    @TableField("house_id")
    private String houseId;
    /**
     * 取消入库来源
     */
    @TableField("source_type")
    private String sourceType;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public Long getReleationId () {
        return releationId;
    }

    public void setReleationId (Long releationId) {
        this.releationId = releationId;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getUserCreate () {
        return userCreate;
    }

    public void setUserCreate (String userCreate) {
        this.userCreate = userCreate;
    }

    public String getSourceType () {
        return sourceType;
    }

    public void setSourceType (String sourceType) {
        this.sourceType = sourceType;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getHouseId () {
        return houseId;
    }

    public void setHouseId (String houseId) {
        this.houseId = houseId;
    }
}
