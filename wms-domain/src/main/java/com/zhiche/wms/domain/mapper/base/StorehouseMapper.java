package com.zhiche.wms.domain.mapper.base;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.dto.sys.StorehouseDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 仓库信息 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface StorehouseMapper extends BaseMapper<Storehouse> {

    List<StorehouseDTO> listStoreHouse(HashMap<String, Object> params);

    int countStoreHouse(HashMap<String, Object> params);

    List<StorehouseDTO> listStoreHousePage(Page<StorehouseDTO> page, @Param("ew") EntityWrapper<StorehouseDTO> shEw);

    List<Storehouse> queryActualStock (@Param("ew") Wrapper<Storehouse> ew);
}
