package com.zhiche.wms.domain.model.otm;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * <p>
 * 运单
 * </p>
 *
 * @author qichao
 * @since 2018-06-13
 */
@TableName("otm_order_release")
public class OtmOrderRelease extends Model<OtmOrderRelease> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 系统单号
     */
    @TableField("release_gid")
    private String releaseGid;
    /**
     * 调度指令ID
     */
    @TableField("shipment_gid")
    private String shipmentGid;
    /**
     * 客户ID
     */
    @TableField("customer_id")
    private String customerId;
    /**
     * 客户订单号
     */
    @TableField("cus_order_no")
    private String cusOrderNo;

    /**
     * 系统订单号
     */
    @TableField("order_no")
    private String orderNo;
    /**
     * 客户运单号
     */
    @TableField("cus_waybill_no")
    private String cusWaybillNo;
    /**
     * 客户运输模式分类
     */
    @TableField("cus_trans_mode")
    private String cusTransMode;
    /**
     * 是否急发
     */
    @TableField("is_urgent")
    private String isUrgent;
    /**
     * 是否超期
     */
    @TableField("pickup_is_appt")
    private String pickupIsAppt;
    /**
     * 订单属性
     */
    @TableField("order_att")
    private String orderAtt;
    /**
     * 要求入库时间
     */
    @TableField("expect_Inbound_date")
    private Date expectInboundDate;
    /**
     * 要求回单时间
     */
    @TableField("expect_receipt_date")
    private Date expectReceiptDate;
    /**
     * 要求发运时间
     */
    @TableField("expcet_ship_date")
    private Date expcetShipDate;
    /**
     * 要求抵达时间
     */
    @TableField("expect_arrive_date")
    private Date expectArriveDate;
    /**
     * 日志头ID
     */
    @TableField("log_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long logLineId;
    /**
     * 来源唯一键
     */
    @TableField("source_key")
    private String sourceKey;
    /**
     * 来源单据号
     */
    @TableField("source_no")
    private String sourceNo;
    /**
     * 实车始发位置
     */
    @TableField("real_vehicle_address")
    private String realVehicleAddress;

    /**
     * 起运地编码
     */
    @TableField("origin_location_gid")
    private String originLocationGid;

    @TableField(exist = false)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long outboundHouseId;

    /**
     * 起运地名称
     */
    @TableField("origin_location_name")
    private String originLocationName;
    /**
     * 起运地省份
     */
    @TableField("origin_location_province")
    private String originLocationProvince;
    /**
     * 起运地城市
     */
    @TableField("origin_location_city")
    private String originLocationCity;
    /**
     * 起运地区县
     */
    @TableField("origin_location_county")
    private String originLocationCounty;
    /**
     * 起运地地址
     */
    @TableField("origin_location_address")
    private String originLocationAddress;
    /**
     * 目的地编码
     */
    @TableField("dest_location_gid")
    private String destLocationGid;

    @TableField(exist = false)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inboundHouseId;
    /**
     * 目的地名称
     */
    @TableField("dest_location_name")
    private String destLocationName;
    /**
     * 目的地省份
     */
    @TableField("dest_location_province")
    private String destLocationProvince;
    /**
     * 目的地城市
     */
    @TableField("dest_location_city")
    private String destLocationCity;
    /**
     * 目的地区县
     */
    @TableField("dest_location_county")
    private String destLocationCounty;
    /**
     * 目的地地址
     */
    @TableField("dest_location_address")
    private String destLocationAddress;

    /**
     * 订单最终目的地
     */
    @TableField("order_dest_address")
    private String orderDestAddress;
    /**
     * 客户车型
     */
    @TableField("cus_vehicle_type")
    private String cusVehicleType;
    /**
     * 车型描述
     */
    @TableField("vehicle_describe")
    private String vehicleDescribe;
    /**
     * 标准车型
     */
    @TableField("stan_vehicle_type")
    private String stanVehicleType;
    /**
     * VIN码
     */
    private String vin;
    /**
     * 是否改装车
     */
    @TableField("is_mod_vehicle")
    private String isModVehicle;
    /**
     * 改装后的长宽高重
     */
    @TableField("mod_vehicle_size")
    private String modVehicleSize;
    /**
     * 二维码
     */
    @TableField("qr_code")
    private String qrCode;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 状态(10:正常,50:取消)
     */
    private String status;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;

    /**
     * 修改后的车型
     */
    @TableField("modified_vehicle_type")
    private String modifiedVehicleType;

    /**
     * 修改车型的用户
     */
    @TableField("modified_vehicle_user")
    private String modifiedVehicleUser;

    /**
     * 修改车型的时间
     */
    @TableField("modified_vehicle_time")
    private Date modifiedVehicleTime;

    /**
     * 实际操作发运时间
     */
    @TableField(value = "real_ship_date")
    private Date realShipDate;

    /**
     * 发运操作人
     */
    @TableField(value = "ship_user")
    private String shipUser;

    /**
     * 发运时间
     */
    @TableField(value = "ship_date")
    private Date shipDate;

    @TableField(exist = false)
    private String dataStatus;

    public Date getRealShipDate () {
        return realShipDate;
    }

    public Date getShipDate () {
        return shipDate;
    }

    public void setShipDate (Date shipDate) {
        this.shipDate = shipDate;
    }

    public void setRealShipDate (Date realShipDate) {
        this.realShipDate = realShipDate;
    }

    public String getShipUser () {
        return shipUser;
    }

    public void setShipUser (String shipUser) {
        this.shipUser = shipUser;
    }

    public String getRealVehicleAddress () {
        return realVehicleAddress;
    }

    public void setRealVehicleAddress (String realVehicleAddress) {
        this.realVehicleAddress = realVehicleAddress;
    }

    public String getOrderDestAddress () {
        return orderDestAddress;
    }

    public void setOrderDestAddress (String orderDestAddress) {
        this.orderDestAddress = orderDestAddress;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("OtmOrderRelease{");
        sb.append("id=").append(id);
        sb.append(", releaseGid='").append(releaseGid).append('\'');
        sb.append(", shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", customerId='").append(customerId).append('\'');
        sb.append(", cusOrderNo='").append(cusOrderNo).append('\'');
        sb.append(", cusWaybillNo='").append(cusWaybillNo).append('\'');
        sb.append(", cusTransMode='").append(cusTransMode).append('\'');
        sb.append(", isUrgent='").append(isUrgent).append('\'');
        sb.append(", pickupIsAppt='").append(pickupIsAppt).append('\'');
        sb.append(", orderAtt='").append(orderAtt).append('\'');
        sb.append(", expectInboundDate=").append(expectInboundDate);
        sb.append(", expectReceiptDate=").append(expectReceiptDate);
        sb.append(", expcetShipDate=").append(expcetShipDate);
        sb.append(", expectArriveDate=").append(expectArriveDate);
        sb.append(", logLineId=").append(logLineId);
        sb.append(", sourceKey='").append(sourceKey).append('\'');
        sb.append(", sourceNo='").append(sourceNo).append('\'');
        sb.append(", originLocationGid='").append(originLocationGid).append('\'');
        sb.append(", outboundHouseId=").append(outboundHouseId);
        sb.append(", originLocationName='").append(originLocationName).append('\'');
        sb.append(", originLocationProvince='").append(originLocationProvince).append('\'');
        sb.append(", originLocationCity='").append(originLocationCity).append('\'');
        sb.append(", originLocationCounty='").append(originLocationCounty).append('\'');
        sb.append(", originLocationAddress='").append(originLocationAddress).append('\'');
        sb.append(", destLocationGid='").append(destLocationGid).append('\'');
        sb.append(", inboundHouseId=").append(inboundHouseId);
        sb.append(", destLocationName='").append(destLocationName).append('\'');
        sb.append(", destLocationProvince='").append(destLocationProvince).append('\'');
        sb.append(", destLocationCity='").append(destLocationCity).append('\'');
        sb.append(", destLocationCounty='").append(destLocationCounty).append('\'');
        sb.append(", destLocationAddress='").append(destLocationAddress).append('\'');
        sb.append(", cusVehicleType='").append(cusVehicleType).append('\'');
        sb.append(", vehicleDescribe='").append(vehicleDescribe).append('\'');
        sb.append(", stanVehicleType='").append(stanVehicleType).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", isModVehicle='").append(isModVehicle).append('\'');
        sb.append(", modVehicleSize='").append(modVehicleSize).append('\'');
        sb.append(", qrCode='").append(qrCode).append('\'');
        sb.append(", remarks='").append(remarks).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", modifiedVehicleType='").append(modifiedVehicleType).append('\'');
        sb.append(", modifiedVehicleUser='").append(modifiedVehicleUser).append('\'');
        sb.append(", modifiedVehicleTime=").append(modifiedVehicleTime);
        sb.append(", shipUser=").append(shipUser);
        sb.append(", realShipDate=").append(realShipDate);
        sb.append(", shipDate=").append(shipDate);
        sb.append(", orderDestAddress=").append(orderDestAddress);
        sb.append('}');
        return sb.toString();
    }



    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getCustomerId () {
        return customerId;
    }

    public void setCustomerId (String customerId) {
        this.customerId = customerId;
    }

    public String getCusOrderNo () {
        return cusOrderNo;
    }

    public void setCusOrderNo (String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusWaybillNo () {
        return cusWaybillNo;
    }

    public void setCusWaybillNo (String cusWaybillNo) {
        this.cusWaybillNo = cusWaybillNo;
    }

    public String getCusTransMode () {
        return cusTransMode;
    }

    public void setCusTransMode (String cusTransMode) {
        this.cusTransMode = cusTransMode;
    }

    public String getIsUrgent () {
        return isUrgent;
    }

    public void setIsUrgent (String isUrgent) {
        this.isUrgent = isUrgent;
    }

    public String getPickupIsAppt () {
        return pickupIsAppt;
    }

    public void setPickupIsAppt (String pickupIsAppt) {
        this.pickupIsAppt = pickupIsAppt;
    }

    public String getOrderAtt () {
        return orderAtt;
    }

    public void setOrderAtt (String orderAtt) {
        this.orderAtt = orderAtt;
    }

    public Date getExpectInboundDate () {
        return expectInboundDate;
    }

    public void setExpectInboundDate (Date expectInboundDate) {
        this.expectInboundDate = expectInboundDate;
    }

    public Date getExpectReceiptDate () {
        return expectReceiptDate;
    }

    public void setExpectReceiptDate (Date expectReceiptDate) {
        this.expectReceiptDate = expectReceiptDate;
    }

    public Date getExpcetShipDate () {
        return expcetShipDate;
    }

    public void setExpcetShipDate (Date expcetShipDate) {
        this.expcetShipDate = expcetShipDate;
    }

    public Date getExpectArriveDate () {
        return expectArriveDate;
    }

    public void setExpectArriveDate (Date expectArriveDate) {
        this.expectArriveDate = expectArriveDate;
    }

    public Long getLogLineId () {
        return logLineId;
    }

    public void setLogLineId (Long logLineId) {
        this.logLineId = logLineId;
    }

    public String getSourceKey () {
        return sourceKey;
    }

    public void setSourceKey (String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getSourceNo () {
        return sourceNo;
    }

    public void setSourceNo (String sourceNo) {
        this.sourceNo = sourceNo;
    }

    public String getOriginLocationGid () {
        return originLocationGid;
    }

    public void setOriginLocationGid (String originLocationGid) {
        this.originLocationGid = originLocationGid;
    }

    public Long getOutboundHouseId () {
        return outboundHouseId;
    }

    public void setOutboundHouseId (Long outboundHouseId) {
        this.outboundHouseId = outboundHouseId;
    }

    public String getOriginLocationName () {
        return originLocationName;
    }

    public void setOriginLocationName (String originLocationName) {
        this.originLocationName = originLocationName;
    }

    public String getOriginLocationProvince () {
        return originLocationProvince;
    }

    public void setOriginLocationProvince (String originLocationProvince) {
        this.originLocationProvince = originLocationProvince;
    }

    public String getOriginLocationCity () {
        return originLocationCity;
    }

    public void setOriginLocationCity (String originLocationCity) {
        this.originLocationCity = originLocationCity;
    }

    public String getOriginLocationCounty () {
        return originLocationCounty;
    }

    public void setOriginLocationCounty (String originLocationCounty) {
        this.originLocationCounty = originLocationCounty;
    }

    public String getOriginLocationAddress () {
        return originLocationAddress;
    }

    public void setOriginLocationAddress (String originLocationAddress) {
        this.originLocationAddress = originLocationAddress;
    }

    public String getDestLocationGid () {
        return destLocationGid;
    }

    public void setDestLocationGid (String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    public Long getInboundHouseId () {
        return inboundHouseId;
    }

    public void setInboundHouseId (Long inboundHouseId) {
        this.inboundHouseId = inboundHouseId;
    }

    public String getDestLocationName () {
        return destLocationName;
    }

    public void setDestLocationName (String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestLocationProvince () {
        return destLocationProvince;
    }

    public void setDestLocationProvince (String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getDestLocationCity () {
        return destLocationCity;
    }

    public void setDestLocationCity (String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty () {
        return destLocationCounty;
    }

    public void setDestLocationCounty (String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationAddress () {
        return destLocationAddress;
    }

    public void setDestLocationAddress (String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

    public String getCusVehicleType () {
        return cusVehicleType;
    }

    public void setCusVehicleType (String cusVehicleType) {
        this.cusVehicleType = cusVehicleType;
    }

    public String getVehicleDescribe () {
        return vehicleDescribe;
    }

    public void setVehicleDescribe (String vehicleDescribe) {
        this.vehicleDescribe = vehicleDescribe;
    }

    public String getStanVehicleType () {
        return stanVehicleType;
    }

    public void setStanVehicleType (String stanVehicleType) {
        this.stanVehicleType = stanVehicleType;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getIsModVehicle () {
        return isModVehicle;
    }

    public void setIsModVehicle (String isModVehicle) {
        this.isModVehicle = isModVehicle;
    }

    public String getModVehicleSize () {
        return modVehicleSize;
    }

    public void setModVehicleSize (String modVehicleSize) {
        this.modVehicleSize = modVehicleSize;
    }

    public String getQrCode () {
        return qrCode;
    }

    public void setQrCode (String qrCode) {
        this.qrCode = qrCode;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getModifiedVehicleType () {
        return modifiedVehicleType;
    }

    public void setModifiedVehicleType (String modifiedVehicleType) {
        this.modifiedVehicleType = modifiedVehicleType;
    }

    public String getModifiedVehicleUser () {
        return modifiedVehicleUser;
    }

    public void setModifiedVehicleUser (String modifiedVehicleUser) {
        this.modifiedVehicleUser = modifiedVehicleUser;
    }

    public Date getModifiedVehicleTime () {
        return modifiedVehicleTime;
    }

    public void setModifiedVehicleTime (Date modifiedVehicleTime) {
        this.modifiedVehicleTime = modifiedVehicleTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OtmOrderRelease)) return false;
        OtmOrderRelease that = (OtmOrderRelease) o;
        return Objects.equals(releaseGid, that.releaseGid) &&
                Objects.equals(shipmentGid, that.shipmentGid) &&
                Objects.equals(customerId, that.customerId) &&
                Objects.equals(cusOrderNo, that.cusOrderNo) &&
                Objects.equals(cusWaybillNo, that.cusWaybillNo) &&
                Objects.equals(cusTransMode, that.cusTransMode) &&
                Objects.equals(isUrgent, that.isUrgent) &&
                Objects.equals(pickupIsAppt, that.pickupIsAppt) &&
                Objects.equals(orderAtt, that.orderAtt) &&
                Objects.equals(expectInboundDate, that.expectInboundDate) &&
                Objects.equals(expectReceiptDate, that.expectReceiptDate) &&
                Objects.equals(expcetShipDate, that.expcetShipDate) &&
                Objects.equals(expectArriveDate, that.expectArriveDate) &&
                Objects.equals(sourceKey, that.sourceKey) &&
                Objects.equals(sourceNo, that.sourceNo) &&
                Objects.equals(originLocationGid, that.originLocationGid) &&
                Objects.equals(originLocationName, that.originLocationName) &&
                Objects.equals(originLocationProvince, that.originLocationProvince) &&
                Objects.equals(originLocationCity, that.originLocationCity) &&
                Objects.equals(originLocationCounty, that.originLocationCounty) &&
                Objects.equals(originLocationAddress, that.originLocationAddress) &&
                Objects.equals(destLocationGid, that.destLocationGid) &&
                Objects.equals(destLocationName, that.destLocationName) &&
                Objects.equals(destLocationProvince, that.destLocationProvince) &&
                Objects.equals(destLocationCity, that.destLocationCity) &&
                Objects.equals(destLocationCounty, that.destLocationCounty) &&
                Objects.equals(destLocationAddress, that.destLocationAddress) &&
                Objects.equals(cusVehicleType, that.cusVehicleType) &&
                Objects.equals(vehicleDescribe, that.vehicleDescribe) &&
                Objects.equals(stanVehicleType, that.stanVehicleType) &&
                Objects.equals(vin, that.vin) &&
                Objects.equals(isModVehicle, that.isModVehicle);
    }

    public String getOrderNo () {
        return orderNo;
    }

    public void setOrderNo (String orderNo) {
        this.orderNo = orderNo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(releaseGid, shipmentGid, customerId, cusOrderNo, cusWaybillNo, cusTransMode, isUrgent, pickupIsAppt, orderAtt, expectInboundDate, expectReceiptDate, expcetShipDate, expectArriveDate, sourceKey, sourceNo, originLocationGid, originLocationName, originLocationProvince, originLocationCity, originLocationCounty, originLocationAddress, destLocationGid, destLocationName, destLocationProvince, destLocationCity, destLocationCounty, destLocationAddress, cusVehicleType, vehicleDescribe, stanVehicleType, vin, isModVehicle);
    }

    public String getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(String dataStatus) {
        this.dataStatus = dataStatus;
    }
}
