package com.zhiche.wms.domain.mapper.opbaas;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.opbaas.StatusLog;

/**
 * <p>
 * 状态日志 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-11
 */
public interface StatusLogMapper extends BaseMapper<StatusLog> {

}
