package com.zhiche.wms.domain.mapper.base;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.base.StoreArea;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.base.ValidArea;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 库区配置 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface StoreAreaMapper extends BaseMapper<StoreArea> {

    List<StoreArea> queryByPage(Page<StoreArea> page, @Param("ew") Wrapper<StoreArea> ew);

    int getCountByCode(@Param("ew") Wrapper<StoreArea> ew);

    List<ValidArea> queryValidArea ( @Param("storeHouseId")  String storeHouseId);

    List<ValidArea> queryWDValidArea (@Param("storeHouseId") Long storeHouseId);
}
