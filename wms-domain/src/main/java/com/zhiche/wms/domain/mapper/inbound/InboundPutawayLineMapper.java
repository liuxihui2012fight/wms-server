package com.zhiche.wms.domain.mapper.inbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.inbound.InboundPutawayLine;
import com.zhiche.wms.dto.inbound.InboundPutawayDTO;
import com.zhiche.wms.dto.inbound.LocChangeDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 入库单明细 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface InboundPutawayLineMapper extends BaseMapper<InboundPutawayLine> {

    InboundPutawayDTO getPutWayByLineId(@Param("id") Long id);

    List<InboundPutawayDTO> queryPutWayListByPage(Page<InboundPutawayDTO> page, @Param("ew") EntityWrapper<InboundPutawayDTO> ew);

    List<InboundPutawayDTO> selectExportData(@Param("ew") EntityWrapper<InboundPutawayDTO> ew);

    List<InboundPutawayDTO> getLineWithHouseId(@Param("ew") EntityWrapper<InboundPutawayLine> ew);

    List<LocChangeDTO> queryChangeLocList(Page<LocChangeDTO> page, @Param("ew") EntityWrapper<LocChangeDTO> ew);

    List<LocChangeDTO> queryExportLocList(@Param("ew") EntityWrapper<LocChangeDTO> ew);

    List<InboundPutawayLine> queryKeyStatus (@Param("ew")Wrapper<InboundPutawayLine> ew);

    List<InboundPutawayDTO> exportPutawayKeyData ( @Param("ew")EntityWrapper<InboundPutawayDTO> ew);
}
