package com.zhiche.wms.domain.mapper.base;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.base.StoreArea;
import com.zhiche.wms.domain.model.base.StoreLocation;
import com.zhiche.wms.dto.inbound.StoreAreaAndLocationDTO;
import com.zhiche.wms.dto.stock.StockDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 储位配置 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface StoreLocationMapper extends BaseMapper<StoreLocation> {

    /**
     * 查询所有的可用储位
     */
    List<StoreLocation> selectUsableLocations(@Param("storeHouseId") Long storeHouseId);

    /**
     * 根据储位类型查询所有的可用储位
     */
    List<StoreLocation> selectUsableLocationsByType(@Param("storeHouseId") Long storeHouseId, @Param("type") String type);

    /**
     * 模糊查询所有储位
     */
    List<StoreLocation> selectLocationsByPage(Page<StoreLocation> page, @Param("ew") Wrapper<StoreLocation> ew);

    /**
     * 得到第一个可用储位
     */
    StoreLocation selectFirstUsableLocation(@Param("storeHouseId") Long storeHouseId);

    /**
     * 按类型得到第一个可用储位
     */
    StoreLocation selectFirstUsableLocationByType(@Param("storeHouseId") Long storeHouseId, @Param("type") String type);

    /**
     * 按类型得到第一个可用储位
     */
    StoreLocation selectFirstUsableLocationByAreaType(@Param("storeHouseId") Long storeHouseId, @Param("areaId") Long areaId, @Param("type") String type);

    StoreLocation selectLocationByLocationId(@Param("id") Long id);

    /**
     * 查询所有库位
     */
    List<StoreAreaAndLocationDTO> selectAllLocations(Map<String, String> condition);

    int getCountByCode(@Param("ew") Wrapper<StoreLocation> ew);

    List<StoreLocation> selectLocationsByAppPage (Page<StoreLocation> page,@Param("ew") Wrapper<StoreLocation> ew);

    StoreLocation queryUsableLocationByAreaId (@Param("storeHouseId") Long storeHouseId, @Param("areaId") Long areaId);


}
