package com.zhiche.wms.domain.mapper.inbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.inbound.FactoryPrepareInbound;
import com.zhiche.wms.dto.inbound.FPInboundDTO;
import com.zhiche.wms.dto.outbound.FPOutboundDTO;
import com.zhiche.wms.dto.outbound.FactoryShipmentOutDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description: 工厂备料入库
 * @Date: Create in 11:08 2019/11/20
 */
public interface FactoryPrepareInboundMapper extends BaseMapper<FactoryPrepareInbound> {

    List<FPOutboundDTO> queryFactoryOutBound (@Param("ew") EntityWrapper<FPOutboundDTO> factoryEw, Page<FPOutboundDTO> page);

    List<FPInboundDTO> queryFactoryInBound (@Param("ew") EntityWrapper<FPInboundDTO> factoryEw, Page<FPInboundDTO> page);

    List<FPInboundDTO> queryFactoryInBound (@Param("ew") EntityWrapper<FPInboundDTO> factoryEw);

    List<FPOutboundDTO> queryFactoryOutBound (@Param("ew") EntityWrapper<FPOutboundDTO> factoryEw);

    List<FactoryPrepareInbound> queryMustMatter (@Param("ew") EntityWrapper<FactoryPrepareInbound> ew);

    FactoryShipmentOutDTO queryStoreArea (@Param("factoryId") Long fpiew);
}
