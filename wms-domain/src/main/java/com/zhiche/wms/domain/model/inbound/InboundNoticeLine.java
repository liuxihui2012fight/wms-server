package com.zhiche.wms.domain.model.inbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 入库通知明细
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
@TableName("w_inbound_notice_line")
public class InboundNoticeLine extends Model<InboundNoticeLine> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 入库通知单头键
     */
    @TableField("header_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long headerId;
    /**
     * 序号
     */
    private String seq;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 货主单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;
    /**
     * 日志行ID
     */
    @TableField("log_line_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long logLineId;
    /**
     * 行来源唯一键
     */
    @TableField("line_source_key")
    private String lineSourceKey;
    /**
     * 行来源单据号
     */
    @TableField("line_source_no")
    private String lineSourceNo;
    /**
     * 物料ID
     */
    @TableField("materiel_id")
    private String materielId;
    /**
     * 物料代码
     */
    @TableField("materiel_code")
    private String materielCode;
    /**
     * 物料名称
     */
    @TableField("materiel_name")
    private String materielName;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 预计数量
     */
    @TableField("expect_qty")
    private BigDecimal expectQty;
    /**
     * 预计净重
     */
    @TableField("expect_net_weight")
    private BigDecimal expectNetWeight;
    /**
     * 预计毛重
     */
    @TableField("expect_gross_weight")
    private BigDecimal expectGrossWeight;
    /**
     * 预计体积
     */
    @TableField("expect_gross_cubage")
    private BigDecimal expectGrossCubage;
    /**
     * 预计件数
     */
    @TableField("expect_packed_count")
    private BigDecimal expectPackedCount;
    /**
     * 入库数量
     */
    @TableField("inbound_qty")
    private BigDecimal inboundQty;
    /**
     * 入库净重
     */
    @TableField("inbound_net_weight")
    private BigDecimal inboundNetWeight;
    /**
     * 入库毛重
     */
    @TableField("inbound_gross_weight")
    private BigDecimal inboundGrossWeight;
    /**
     * 入库体积
     */
    @TableField("inbound_gross_cubage")
    private BigDecimal inboundGrossCubage;
    /**
     * 入库件数
     */
    @TableField("inbound_packed_count")
    private BigDecimal inboundPackedCount;
    /**
     * 批号0
     */
    @TableField("lot_no0")
    private String lotNo0;
    /**
     * 批号1
     */
    @TableField("lot_no1")
    private String lotNo1;
    /**
     * 批号2
     */
    @TableField("lot_no2")
    private String lotNo2;
    /**
     * 批号3
     */
    @TableField("lot_no3")
    private String lotNo3;
    /**
     * 批号4
     */
    @TableField("lot_no4")
    private String lotNo4;
    /**
     * 批号5
     */
    @TableField("lot_no5")
    private String lotNo5;
    /**
     * 批号6
     */
    @TableField("lot_no6")
    private String lotNo6;
    /**
     * 批号7
     */
    @TableField("lot_no7")
    private String lotNo7;
    /**
     * 批号8
     */
    @TableField("lot_no8")
    private String lotNo8;
    /**
     * 批号9
     */
    @TableField("lot_no9")
    private String lotNo9;
    /**
     * 状态(10:未入库,20:部分入库,30:全部入库,40:关闭入库,40:取消)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 目的地-省
     */
    @TableField("dest_location_province")
    private String destLocationProvince;
    /**
     * 目的地-市
     */
    @TableField("dest_location_city")
    private String destLocationCity;
    /**
     * 目的地-区
     */
    @TableField("dest_location_county")
    private String destLocationCounty;
    /**
     * 目的地-详细地址
     */
    @TableField("dest_location_detail")
    private String destLocationDetail;

    /**
     * 表示取消入库
     */
    @TableField("is_cancle")
    private String isCancle;

    /**
     * 取消入库操作人
     */
    @TableField("cancle_user")
    private String cancleUser;

    public String getIsCancle () {
        return isCancle;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("InboundNoticeLine{");
        sb.append("id=").append(id);
        sb.append(", headerId=").append(headerId);
        sb.append(", seq='").append(seq).append('\'');
        sb.append(", ownerId='").append(ownerId).append('\'');
        sb.append(", ownerOrderNo='").append(ownerOrderNo).append('\'');
        sb.append(", logLineId=").append(logLineId);
        sb.append(", lineSourceKey='").append(lineSourceKey).append('\'');
        sb.append(", lineSourceNo='").append(lineSourceNo).append('\'');
        sb.append(", materielId='").append(materielId).append('\'');
        sb.append(", materielCode='").append(materielCode).append('\'');
        sb.append(", materielName='").append(materielName).append('\'');
        sb.append(", uom='").append(uom).append('\'');
        sb.append(", expectQty=").append(expectQty);
        sb.append(", expectNetWeight=").append(expectNetWeight);
        sb.append(", expectGrossWeight=").append(expectGrossWeight);
        sb.append(", expectGrossCubage=").append(expectGrossCubage);
        sb.append(", expectPackedCount=").append(expectPackedCount);
        sb.append(", inboundQty=").append(inboundQty);
        sb.append(", inboundNetWeight=").append(inboundNetWeight);
        sb.append(", inboundGrossWeight=").append(inboundGrossWeight);
        sb.append(", inboundGrossCubage=").append(inboundGrossCubage);
        sb.append(", inboundPackedCount=").append(inboundPackedCount);
        sb.append(", lotNo0='").append(lotNo0).append('\'');
        sb.append(", lotNo1='").append(lotNo1).append('\'');
        sb.append(", lotNo2='").append(lotNo2).append('\'');
        sb.append(", lotNo3='").append(lotNo3).append('\'');
        sb.append(", lotNo4='").append(lotNo4).append('\'');
        sb.append(", lotNo5='").append(lotNo5).append('\'');
        sb.append(", lotNo6='").append(lotNo6).append('\'');
        sb.append(", lotNo7='").append(lotNo7).append('\'');
        sb.append(", lotNo8='").append(lotNo8).append('\'');
        sb.append(", lotNo9='").append(lotNo9).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", remarks='").append(remarks).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", destLocationProvince='").append(destLocationProvince).append('\'');
        sb.append(", destLocationCity='").append(destLocationCity).append('\'');
        sb.append(", destLocationCounty='").append(destLocationCounty).append('\'');
        sb.append(", destLocationDetail='").append(destLocationDetail).append('\'');
        sb.append(", isCancle='").append(isCancle).append('\'');
        sb.append(", cancleUser='").append(cancleUser).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public void setIsCancle (String isCancle) {
        this.isCancle = isCancle;
    }

    public String getCancleUser () {
        return cancleUser;
    }

    public void setCancleUser (String cancleUser) {
        this.cancleUser = cancleUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public Long getLogLineId() {
        return logLineId;
    }

    public void setLogLineId(Long logLineId) {
        this.logLineId = logLineId;
    }

    public String getLineSourceKey() {
        return lineSourceKey;
    }

    public void setLineSourceKey(String lineSourceKey) {
        this.lineSourceKey = lineSourceKey;
    }

    public String getLineSourceNo() {
        return lineSourceNo;
    }

    public void setLineSourceNo(String lineSourceNo) {
        this.lineSourceNo = lineSourceNo;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getExpectQty() {
        return expectQty;
    }

    public void setExpectQty(BigDecimal expectQty) {
        this.expectQty = expectQty;
    }

    public BigDecimal getExpectNetWeight() {
        return expectNetWeight;
    }

    public void setExpectNetWeight(BigDecimal expectNetWeight) {
        this.expectNetWeight = expectNetWeight;
    }

    public BigDecimal getExpectGrossWeight() {
        return expectGrossWeight;
    }

    public void setExpectGrossWeight(BigDecimal expectGrossWeight) {
        this.expectGrossWeight = expectGrossWeight;
    }

    public BigDecimal getExpectGrossCubage() {
        return expectGrossCubage;
    }

    public void setExpectGrossCubage(BigDecimal expectGrossCubage) {
        this.expectGrossCubage = expectGrossCubage;
    }

    public BigDecimal getExpectPackedCount() {
        return expectPackedCount;
    }

    public void setExpectPackedCount(BigDecimal expectPackedCount) {
        this.expectPackedCount = expectPackedCount;
    }

    public BigDecimal getInboundQty() {
        return inboundQty;
    }

    public void setInboundQty(BigDecimal inboundQty) {
        this.inboundQty = inboundQty;
    }

    public BigDecimal getInboundNetWeight() {
        return inboundNetWeight;
    }

    public void setInboundNetWeight(BigDecimal inboundNetWeight) {
        this.inboundNetWeight = inboundNetWeight;
    }

    public BigDecimal getInboundGrossWeight() {
        return inboundGrossWeight;
    }

    public void setInboundGrossWeight(BigDecimal inboundGrossWeight) {
        this.inboundGrossWeight = inboundGrossWeight;
    }

    public BigDecimal getInboundGrossCubage() {
        return inboundGrossCubage;
    }

    public void setInboundGrossCubage(BigDecimal inboundGrossCubage) {
        this.inboundGrossCubage = inboundGrossCubage;
    }

    public BigDecimal getInboundPackedCount() {
        return inboundPackedCount;
    }

    public void setInboundPackedCount(BigDecimal inboundPackedCount) {
        this.inboundPackedCount = inboundPackedCount;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getDestLocationProvince() {
        return destLocationProvince;
    }

    public void setDestLocationProvince(String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getDestLocationCity() {
        return destLocationCity;
    }

    public void setDestLocationCity(String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty() {
        return destLocationCounty;
    }

    public void setDestLocationCounty(String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationDetail() {
        return destLocationDetail;
    }

    public void setDestLocationDetail(String destLocationDetail) {
        this.destLocationDetail = destLocationDetail;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
