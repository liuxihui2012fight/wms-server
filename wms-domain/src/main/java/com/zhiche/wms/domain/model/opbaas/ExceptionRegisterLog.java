package com.zhiche.wms.domain.model.opbaas;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: 异常日志表
 * @Date: Create in 17:18 2019/1/11
 */
@TableName("w_exception_log")
public class ExceptionRegisterLog extends Model<ExceptionRegisterLog> {


    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;


    @JsonSerialize(using = ToStringSerializer.class)
    @TableField("exception_id")
    private Long exceptionId;


    @TableField("push_time")
    private Date pushTime;


    @TableField("push_data")
    private String pushData;

    @TableField("otm_result")
    private String otmResult;


    @TableField("estimated_processing_time")
    private Date estimatedProcessingTime;

    private String status;


    @TableField("user_create")
    private String userCreate;


    @TableField("user_modified")
    private String userModified;

    @TableField("gmt_create")
    private Date gmtCreate;

    @TableField("gmt_modified")
    private Date gmtModified;


    private String remark;

    public String getOtmResult () {
        return otmResult;
    }

    public void setOtmResult (String otmResult) {
        this.otmResult = otmResult;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public Long getExceptionId () {
        return exceptionId;
    }

    public void setExceptionId (Long exceptionId) {
        this.exceptionId = exceptionId;
    }

    public Date getPushTime () {
        return pushTime;
    }

    public void setPushTime (Date pushTime) {
        this.pushTime = pushTime;
    }

    public String getPushData () {
        return pushData;
    }

    public void setPushData (String pushData) {
        this.pushData = pushData;
    }

    public Date getEstimatedProcessingTime () {
        return estimatedProcessingTime;
    }

    public void setEstimatedProcessingTime (Date estimatedProcessingTime) {
        this.estimatedProcessingTime = estimatedProcessingTime;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public String getUserCreate () {
        return userCreate;
    }

    public void setUserCreate (String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified () {
        return userModified;
    }

    public void setUserModified (String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("ExceptionRegisterLog{");
        sb.append("id=").append(id);
        sb.append(", exceptionId=").append(exceptionId);
        sb.append(", pushTime=").append(pushTime);
        sb.append(", pushData='").append(pushData).append('\'');
        sb.append(", otmResult='").append(otmResult).append('\'');
        sb.append(", estimatedProcessingTime=").append(estimatedProcessingTime);
        sb.append(", status='").append(status).append('\'');
        sb.append(", userCreate='").append(userCreate).append('\'');
        sb.append(", userModified='").append(userModified).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", remark='").append(remark).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    protected Serializable pkVal () {
        return this.id;
    }
}
