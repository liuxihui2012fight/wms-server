package com.zhiche.wms.domain.mapper.inbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;
import com.zhiche.wms.dto.inbound.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 入库通知明细 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface InboundNoticeLineMapper extends BaseMapper<InboundNoticeLine> {

    /**
     * 根据头ID更新明细入库状态
     */
    Integer updateStatus (@Param("headerId") Long headerId,@Param("noticeLineId") Long noticeLineId);

    /**
     * 根据id号
     */
    InboundNoticeDTO getNoticeByLineId(@Param("id") Long id, @Param("houseId") Long houseId);

    /**
     * 根据二维码
     */
    List<InboundNoticeDTO> getNoticeByKey(@Param("key") String id, @Param("houseId") Long houseId);

    /**
     * 根据分页与Wrapper条件
     */
    List<InboundNoticeDTO> queryPageNotice(Page<InboundNoticeDTO> page, @Param("ew") Wrapper<InboundNoticeDTO> ew);

    List<InboundNoticeDTO> queryListNotice(@Param("ew") Wrapper<InboundNoticeDTO> ew);

    /**
     * 查询入库信息
     *
     * @param page 分页对象
     * @param ew   查询条件
     * @return 包含入库信息，质检信息及确认入库的结果
     */
    List<InboundDTO> queryInboundDetailPage(Page<InboundDTO> page, @Param("ew") Wrapper<InboundDTO> ew);
    //查询未收取钥匙的已入库车辆信息（包括库位） hgy 20200825
    List<InboundPutawayDTO> queryInboundNoKeyPage(Page<InboundPutawayDTO> page, @Param("ew") Wrapper<InboundPutawayDTO> ew);

    List<InboundDTO> getInboundDetailByLotNo(@Param("vinNo") String id, @Param("houseId") Long houseId);

    InboundDTO getInboundDetailById(@Param("id") String id, @Param("houseId") Long houseId);

    List<InboundDTO> queryExportData(@Param("ew") EntityWrapper<InboundDTO> ew);

    List<InboundNoticeLine> pageInboundNotice(Page<InboundNoticeLine> page,@Param("ew") EntityWrapper<InboundNoticeLine> inEW);

    int selectCountWithHead(@Param("ew") EntityWrapper<InboundNoticeLine> noticeLineEntityWrapper);

    List<InboundNoticeLine> queryStoreHousePutway (@Param("ew") EntityWrapper<InboundNoticeLine> headerEw);

    List<ShipmentInbound> queryInboundData (Page<ShipmentInbound> page);

    List<CancleInboundDTO> queryCancleInbound (@Param("ew") EntityWrapper<CancleInboundDTO> inboundEw, Page<CancleInboundDTO> page);

    List<FactoryOutboundShipmentDTO> tcShipmentGidOut (@Param("ew") EntityWrapper<FactoryOutboundShipmentDTO> outEw);
}
