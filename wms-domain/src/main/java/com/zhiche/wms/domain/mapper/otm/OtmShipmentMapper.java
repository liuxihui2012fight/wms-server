package com.zhiche.wms.domain.mapper.otm;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.otm.OtmShipment;
import com.zhiche.wms.dto.opbaas.resultdto.OtmReleaseForShipDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 调度指令 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
public interface OtmShipmentMapper extends BaseMapper<OtmShipment> {

    List<ShipmentForShipDTO> queryShipmentReleaseList(@Param("page") Page<ShipmentForShipDTO> page, @Param("param") HashMap<String, Object> param, @Param("ew") EntityWrapper<ShipmentForShipDTO> ew);

    List<ShipmentForShipDTO> getShipDetail(@Param("ew") EntityWrapper<ShipmentForShipDTO> ew);

    int countShipmentReleaseList(@Param("page") HashMap<String, Object> params, @Param("ew") EntityWrapper<ShipmentForShipDTO> ew);

    List<ShipmentForShipDTO> queryShipmentReleases(@Param("ew") EntityWrapper<ShipmentForShipDTO>dataEW);

    List<OtmReleaseForShipDTO> queryOtmReleaseList (@Param("ew") EntityWrapper<OtmReleaseForShipDTO> ew);

    List<OtmReleaseForShipDTO> queryChannelOtmReleaseList (@Param("ew") EntityWrapper<OtmReleaseForShipDTO> ew);
}
