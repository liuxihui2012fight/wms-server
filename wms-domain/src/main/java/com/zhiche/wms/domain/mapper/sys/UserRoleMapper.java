package com.zhiche.wms.domain.mapper.sys;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.sys.UserRole;

/**
 * <p>
 * 用户角色关联表 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
