package com.zhiche.wms.domain.mapper.outbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.zhiche.wms.domain.model.outbound.OutboundKeyPrepareLine;
import com.zhiche.wms.dto.outbound.BatchPrintPrepare;
import com.zhiche.wms.dto.outbound.PreparationPrintDTO;
import com.zhiche.wms.dto.outbound.PreparePrintDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:19 2018/12/19
 */
public interface OutboundKeyPrepareLineMapper extends BaseMapper<OutboundKeyPrepareLine> {
    /**
     *  钥匙备料批量打印
     * @return
     * @param ew
     */
    List<PreparationPrintDTO> queryKeyPerparePrintData (@Param("ew") Wrapper<PreparationPrintDTO> ew);

    /**
     * 批量打印钥匙备料数据
     */
    List<BatchPrintPrepare> batchPrintPrepareHeader (@Param("ew") EntityWrapper<BatchPrintPrepare> headEw);

    /**
     * 批量查询备料单详情
     */
    List<PreparePrintDetail> queryPreparePrintDetail (@Param("ew") EntityWrapper<PreparePrintDetail> detailEw);
}
