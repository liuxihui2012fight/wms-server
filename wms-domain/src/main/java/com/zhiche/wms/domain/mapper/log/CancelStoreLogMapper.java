package com.zhiche.wms.domain.mapper.log;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.log.CancelStoreLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:40 2018/12/18
 */
public interface CancelStoreLogMapper extends BaseMapper<CancelStoreLog> {

    List<CancelStoreLog> queryCancelStoreLog (Page<CancelStoreLog> page, @Param("ew") Wrapper<CancelStoreLog> ew);

    CancelStoreLog queryCancelStoreDetail (@Param("ew") Wrapper<CancelStoreLog> ew);

    List<CancelStoreLog> queryCancleList (Page<CancelStoreLog> page, @Param("ew") EntityWrapper<CancelStoreLog> ew);
}
