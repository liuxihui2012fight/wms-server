package com.zhiche.wms.domain.mapper.outbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.dto.outbound.OutNoticeListResultDTO;
import com.zhiche.wms.dto.outbound.OutboundNoticeHeaderDTO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 出库通知单头 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface OutboundNoticeHeaderMapper extends BaseMapper<OutboundNoticeHeader> {

    Integer updateStatus(@Param("noticeId") Long noticeId,
                         @Param("status") String status);

    List<OutboundNoticeHeaderDTO> queryNoticePage(Page<OutboundNoticeHeaderDTO> page,
                                                  @Param("ew") Wrapper<OutboundNoticeHeaderDTO> ew);

    OutboundNoticeHeaderDTO getNotice(@Param("houseId") Long houseId,
                                      @Param("id") Long id);

    Integer updateStatus(@Param("noticeId") Long noticeId,
                         @Param("status") String status,
                         @Param("outboundQty") BigDecimal outboundQty);

    List<OutNoticeListResultDTO> selectNoticeListByParams(Page<OutNoticeListResultDTO> page,
                                                          @Param("ew") Wrapper<OutNoticeListResultDTO> ew);

    List<OutNoticeListResultDTO> selectExportONData(@Param("ew") EntityWrapper<OutNoticeListResultDTO> ew);

    List<OutboundNoticeHeader> queryPrepareShipment (@Param("ew")EntityWrapper<OutboundNoticeHeader> hEw);
}
