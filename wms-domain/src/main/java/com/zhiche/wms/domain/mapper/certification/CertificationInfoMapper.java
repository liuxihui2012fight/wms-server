package com.zhiche.wms.domain.mapper.certification;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.certification.CertificationInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 10:00 2019/9/5
 */
public interface CertificationInfoMapper extends BaseMapper<CertificationInfo> {

    List<CertificationInfo> queryCertificationPage (Page<CertificationInfo> page, @Param("ew") EntityWrapper<CertificationInfo> certificationInfoEw);

    List<CertificationInfo> exportCertificationPage ( @Param("ew") EntityWrapper<CertificationInfo> certificationInfoEw);
}
