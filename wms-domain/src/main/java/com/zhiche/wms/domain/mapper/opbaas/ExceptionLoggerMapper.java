package com.zhiche.wms.domain.mapper.opbaas;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegisterLog;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:07 2019/1/11
 */
public interface ExceptionLoggerMapper extends BaseMapper<ExceptionRegisterLog> {
}
