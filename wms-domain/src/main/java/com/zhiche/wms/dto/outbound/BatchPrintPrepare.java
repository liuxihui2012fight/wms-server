package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author: caiHua
 * @Description: 批量打印备料单
 * @Date: Create in 14:32 2019/7/16
 */
public class BatchPrintPrepare implements Serializable {
    /**
     * 备料单号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long headId;

    /**
     * 指令号
     */
    private String shipmentGid;

    /**
     * 车牌号
     */
    private String plateNumber;

    /**
     * 装车道
     */
    private String loadingArea;

    /**
     * 备料单生成时间
     */
    private String gmtCreate;

    /**
     * 司机
     */
    private String driverName;

    /**
     * 备料单详情
     */
    private List<PreparePrintDetail> preparePrintDetail;

    public Long getHeadId () {
        return headId;
    }

    public void setHeadId (Long headId) {
        this.headId = headId;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getPlateNumber () {
        return plateNumber;
    }

    public void setPlateNumber (String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getLoadingArea () {
        return loadingArea;
    }

    public void setLoadingArea (String loadingArea) {
        this.loadingArea = loadingArea;
    }

    public String getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getDriverName () {
        return driverName;
    }

    public void setDriverName (String driverName) {
        this.driverName = driverName;
    }

    public List<PreparePrintDetail> getPreparePrintDetail () {
        return preparePrintDetail;
    }

    public void setPreparePrintDetail (List<PreparePrintDetail> preparePrintDetail) {
        this.preparePrintDetail = preparePrintDetail;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("BatchPrintPrepare{");
        sb.append("headId=").append(headId);
        sb.append(", shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", plateNumber='").append(plateNumber).append('\'');
        sb.append(", loadingArea='").append(loadingArea).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", driverName='").append(driverName).append('\'');
        sb.append(", preparePrintDetail=").append(preparePrintDetail);
        sb.append('}');
        return sb.toString();
    }
}
