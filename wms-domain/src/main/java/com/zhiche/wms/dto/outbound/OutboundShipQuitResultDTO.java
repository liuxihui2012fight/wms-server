package com.zhiche.wms.dto.outbound;

import java.io.Serializable;
import java.util.Date;

public class OutboundShipQuitResultDTO implements Serializable {
    private String shipLineId;
    private String shipHeadId;
    private String materielId;
    private String materielCode;
    private String materielName;
    private String lotNo1;
    private String lotNo2;
    private String lotNo3;
    private String lotNo4;
    private String lotNo5;
    private String lotNo6;
    private String lotNo7;
    private String lotNo8;
    private String lotNo9;
    private String noticeLineStatus;
    private String ownerOrderNo;
    private String ownerId;
    private String noticeLineId;
    private Date gmtCreate;

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getShipLineId() {
        return shipLineId;
    }

    public void setShipLineId(String shipLineId) {
        this.shipLineId = shipLineId;
    }

    public String getShipHeadId() {
        return shipHeadId;
    }

    public void setShipHeadId(String shipHeadId) {
        this.shipHeadId = shipHeadId;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public String getNoticeLineStatus() {
        return noticeLineStatus;
    }

    public void setNoticeLineStatus(String noticeLineStatus) {
        this.noticeLineStatus = noticeLineStatus;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getNoticeLineId() {
        return noticeLineId;
    }

    public void setNoticeLineId(String noticeLineId) {
        this.noticeLineId = noticeLineId;
    }
}
