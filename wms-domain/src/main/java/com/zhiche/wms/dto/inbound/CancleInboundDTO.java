package com.zhiche.wms.dto.inbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: caiHua
 * @Description: 取消指令
 * @Date: Create in 16:41 2019/8/14
 */
public class CancleInboundDTO implements Serializable {
    /**
     * 入库记录id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private long putwaylineId;
    /**
     * 入库车架号
     */
    private String putWayVin;
    /**
     * 仓库id
     */
    private long storeHouseId;
    /**
     * 仓库名称
     */
    private String storeName;
    /**
     * 库位
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private long locationId;
    /**
     * 库存id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private long stockId;
    /**
     * 库存量
     */
    private BigDecimal qty;
    /**
     * 指令号
     */
    private String sourceKey;
    /**
     * 系统运单号
     */
    private String releaseGid;
    /**
     * 运单表备注
     */
    private String remarks;

    public long getPutwaylineId () {
        return putwaylineId;
    }

    public void setPutwaylineId (long putwaylineId) {
        this.putwaylineId = putwaylineId;
    }

    public String getPutWayVin () {
        return putWayVin;
    }

    public void setPutWayVin (String putWayVin) {
        this.putWayVin = putWayVin;
    }

    public long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStoreName () {
        return storeName;
    }

    public void setStoreName (String storeName) {
        this.storeName = storeName;
    }

    public long getLocationId () {
        return locationId;
    }

    public void setLocationId (long locationId) {
        this.locationId = locationId;
    }

    public long getStockId () {
        return stockId;
    }

    public void setStockId (long stockId) {
        this.stockId = stockId;
    }

    public BigDecimal getQty () {
        return qty;
    }

    public void setQty (BigDecimal qty) {
        this.qty = qty;
    }

    public String getSourceKey () {
        return sourceKey;
    }

    public void setSourceKey (String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }
}
