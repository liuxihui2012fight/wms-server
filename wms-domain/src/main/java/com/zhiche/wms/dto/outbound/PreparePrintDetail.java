package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;

/**
 * @Author: caiHua
 * @Description: 批量打印备料单详情信息
 * @Date: Create in 14:56 2019/7/16
 */
public class PreparePrintDetail implements Serializable {

    /**
     * 客户运单号
     */
    private String cusWaybillNo;

    /**
     * 库位
     */
    private String locationNo;

    /**
     * 底盘号
     */
    private String lotNo1;


    /**
     * 车型描述
     */
    private String vehicleDescribe;

    /**
     * 车型分类
     */
    private String materielId;

    /**
     * 目的地
     */
    private String destLocationCity;
    private String destLocationCounty;
    private String destLocationGid;

    /**
     * 钥匙备料详情id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long keyPrepareId;

    /**
     * 库存属性
     */
    private String stockProperty;

    public String getStockProperty () {
        return stockProperty;
    }

    public void setStockProperty (String stockProperty) {
        this.stockProperty = stockProperty;
    }

    public Long getKeyPrepareId () {
        return keyPrepareId;
    }

    public void setKeyPrepareId (Long keyPrepareId) {
        this.keyPrepareId = keyPrepareId;
    }

    public String getCusWaybillNo () {
        return cusWaybillNo;
    }

    public void setCusWaybillNo (String cusWaybillNo) {
        this.cusWaybillNo = cusWaybillNo;
    }

    public String getLocationNo () {
        return locationNo;
    }

    public void setLocationNo (String locationNo) {
        this.locationNo = locationNo;
    }

    public String getLotNo1 () {
        return lotNo1;
    }

    public void setLotNo1 (String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getVehicleDescribe () {
        return vehicleDescribe;
    }

    public void setVehicleDescribe (String vehicleDescribe) {
        this.vehicleDescribe = vehicleDescribe;
    }

    public String getMaterielId () {
        return materielId;
    }

    public void setMaterielId (String materielId) {
        this.materielId = materielId;
    }

    public String getDestLocationCity () {
        return destLocationCity;
    }

    public void setDestLocationCity (String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty () {
        return destLocationCounty;
    }

    public void setDestLocationCounty (String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationGid () {
        return destLocationGid;
    }

    public void setDestLocationGid (String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("PreparePrintDetail{");
        sb.append("cusWaybillNo='").append(cusWaybillNo).append('\'');
        sb.append(", locationNo='").append(locationNo).append('\'');
        sb.append(", lotNo1='").append(lotNo1).append('\'');
        sb.append(", vehicleDescribe='").append(vehicleDescribe).append('\'');
        sb.append(", materielId='").append(materielId).append('\'');
        sb.append(", destLocationCity='").append(destLocationCity).append('\'');
        sb.append(", destLocationCounty='").append(destLocationCounty).append('\'');
        sb.append(", destLocationGid='").append(destLocationGid).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
