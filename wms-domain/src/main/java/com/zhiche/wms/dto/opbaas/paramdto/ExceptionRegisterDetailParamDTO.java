package com.zhiche.wms.dto.opbaas.paramdto;

import java.io.Serializable;

public class ExceptionRegisterDetailParamDTO implements Serializable {

    private String level2Code;
    private String level2Name;
    private String level3Code;
    private String level3Name;
    private String picKeys;//逗号分隔key
    private String remark;

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public String getLevel2Code() {
        return level2Code;
    }

    public void setLevel2Code(String level2Code) {
        this.level2Code = level2Code;
    }

    public String getLevel2Name() {
        return level2Name;
    }

    public void setLevel2Name(String level2Name) {
        this.level2Name = level2Name;
    }

    public String getLevel3Code() {
        return level3Code;
    }

    public void setLevel3Code(String level3Code) {
        this.level3Code = level3Code;
    }

    public String getLevel3Name() {
        return level3Name;
    }

    public void setLevel3Name(String level3Name) {
        this.level3Name = level3Name;
    }

    public String getPicKeys() {
        return picKeys;
    }

    public void setPicKeys(String picKeys) {
        this.picKeys = picKeys;
    }
}
