package com.zhiche.wms.dto.opbaas.resultdto;

import java.util.Date;

public class TaskDetailResultDTO extends TaskResultDTO {
    private Date taskCreate;
    private Date taskStart;
    private String taskFinish;
    private String taskNode;
    private String qrCode;
    private Date bindTime;
    private String bindId;
    private String isCanSend;
    private String pointId;
    private String orderDestAddress;

    public String getIsCanSend() {
        return isCanSend;
    }

    public void setIsCanSend(String isCanSend) {
        this.isCanSend = isCanSend;
    }

    public String getBindId() {
        return bindId;
    }

    public void setBindId(String bindId) {
        this.bindId = bindId;
    }

    public String getTaskNode() {
        return taskNode;
    }

    public void setTaskNode(String taskNode) {
        this.taskNode = taskNode;
    }

    public Date getTaskCreate() {
        return taskCreate;
    }

    public void setTaskCreate(Date taskCreate) {
        this.taskCreate = taskCreate;
    }

    public Date getTaskStart() {
        return taskStart;
    }

    public void setTaskStart(Date taskStart) {
        this.taskStart = taskStart;
    }

    public Date getBindTime() {
        return bindTime;
    }

    public void setBindTime(Date bindTime) {
        this.bindTime = bindTime;
    }

    public String getTaskFinish() {
        return taskFinish;
    }

    public void setTaskFinish(String taskFinish) {
        this.taskFinish = taskFinish;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    @Override
    public String getPointId() {
        return pointId;
    }

    @Override
    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    @Override
    public String getOrderDestAddress () {
        return orderDestAddress;
    }

    @Override
    public void setOrderDestAddress (String orderDestAddress) {
        this.orderDestAddress = orderDestAddress;
    }
}
