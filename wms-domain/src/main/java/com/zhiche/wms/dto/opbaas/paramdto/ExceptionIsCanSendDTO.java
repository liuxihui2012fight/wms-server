package com.zhiche.wms.dto.opbaas.paramdto;

import java.io.Serializable;
import java.util.Date;

public class ExceptionIsCanSendDTO implements Serializable {

    /* 车架号*/
    private String vin;

    /* 异常处理方式*/
    private String exception_result;

    /* 异常描述*/
    private String exception_desc;

    /* 照片连接*/
    private String phone_url;

    /* wmsid*/
    private String wms_id;

    /* 客户订单号*/
    private String cus_order_no;

    /* 工作节点*/
    private String work_node;

    /*登记人 */
    private String registrant;

    /* 异常类型*/
    private String exception_type;

    /*异常处理状态 */
    private String exception_handle_status;

    /* 是否工厂异常*/
    private String is_factory_exception;

    /* 订单是否可发*/
    private String is_send;

    /* 登记时间*/
    private String register_time;

    /**
     * 系统订单号
     */
    private String order_id;

    //OR运单号
    private String order_release_xid;

    //预计处理完成时间
    private String estimated_processing_time;

    //删除为Y
    private String deleted;

    //手动输入的异常信息
    private String remark;

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public String getDeleted () {
        return deleted;
    }

    public void setDeleted (String deleted) {
        this.deleted = deleted;
    }

    public String getEstimated_processing_time () {
        return estimated_processing_time;
    }

    public void setEstimated_processing_time (String estimated_processing_time) {
        this.estimated_processing_time = estimated_processing_time;
    }

    public String getOrder_release_xid () {
        return order_release_xid;
    }

    public void setOrder_release_xid (String order_release_xid) {
        this.order_release_xid = order_release_xid;
    }

    public String getOrder_id () {
        return order_id;
    }

    public void setOrder_id (String order_id) {
        this.order_id = order_id;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getException_result () {
        return exception_result;
    }

    public void setException_result (String exception_result) {
        this.exception_result = exception_result;
    }

    public String getException_desc () {
        return exception_desc;
    }

    public void setException_desc (String exception_desc) {
        this.exception_desc = exception_desc;
    }

    public String getPhone_url () {
        return phone_url;
    }

    public void setPhone_url (String phone_url) {
        this.phone_url = phone_url;
    }

    public String getWms_id () {
        return wms_id;
    }

    public void setWms_id (String wms_id) {
        this.wms_id = wms_id;
    }

    public String getCus_order_no () {
        return cus_order_no;
    }

    public void setCus_order_no (String cus_order_no) {
        this.cus_order_no = cus_order_no;
    }

    public String getWork_node () {
        return work_node;
    }

    public void setWork_node (String work_node) {
        this.work_node = work_node;
    }

    public String getRegistrant () {
        return registrant;
    }

    public void setRegistrant (String registrant) {
        this.registrant = registrant;
    }

    public String getException_type () {
        return exception_type;
    }

    public void setException_type (String exception_type) {
        this.exception_type = exception_type;
    }

    public String getException_handle_status () {
        return exception_handle_status;
    }

    public void setException_handle_status (String exception_handle_status) {
        this.exception_handle_status = exception_handle_status;
    }

    public String getIs_factory_exception () {
        return is_factory_exception;
    }

    public void setIs_factory_exception (String is_factory_exception) {
        this.is_factory_exception = is_factory_exception;
    }

    public String getIs_send () {
        return is_send;
    }

    public void setIs_send (String is_send) {
        this.is_send = is_send;
    }

    public String getRegister_time () {
        return register_time;
    }

    public void setRegister_time (String register_time) {
        this.register_time = register_time;
    }
}
