package com.zhiche.wms.dto.inbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 10:45 2019/11/26
 */
public class FactoryOutboundShipmentDTO implements Serializable {


    private String shipment_gid;
    private String release_gid;
    private String vin;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long noticeLineId;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long fpiId;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long areaId;

    public Long getAreaId () {
        return areaId;
    }

    public void setAreaId (Long areaId) {
        this.areaId = areaId;
    }

    public Long getFpiId () {
        return fpiId;
    }

    public void setFpiId (Long fpiId) {
        this.fpiId = fpiId;
    }

    public String getShipment_gid () {
        return shipment_gid;
    }

    public void setShipment_gid (String shipment_gid) {
        this.shipment_gid = shipment_gid;
    }

    public String getRelease_gid () {
        return release_gid;
    }

    public void setRelease_gid (String release_gid) {
        this.release_gid = release_gid;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public Long getNoticeLineId () {
        return noticeLineId;
    }

    public void setNoticeLineId (Long noticeLineId) {
        this.noticeLineId = noticeLineId;
    }
}
