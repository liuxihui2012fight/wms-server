package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareLine;

import java.io.Serializable;
import java.util.Date;

public class OutboundPrepareDTO extends OutboundPrepareLine implements Serializable {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;
    private String storeHouseName;
    private String ownerName;
    private String shipSpace;
    private String carrierId;
    private String carrierName;
    private String plateNumber;
    private Date recvDate;
    private String noticeNo;
    private String storeDetail;
    private String isCanSend;
    private String vehicleCheckoutStatus;
    private String qrCode;
    //系统运单号OR
    private String orderReleaseGid;

    public String getOrderReleaseGid () {
        return orderReleaseGid;
    }

    public void setOrderReleaseGid (String orderReleaseGid) {
        this.orderReleaseGid = orderReleaseGid;
    }

    public String getQrCode () {
        return qrCode;
    }

    public void setQrCode (String qrCode) {
        this.qrCode = qrCode;
    }

    public String getIsCanSend () {
        return isCanSend;
    }

    public void setIsCanSend (String isCanSend) {
        this.isCanSend = isCanSend;
    }

    public String getStoreDetail () {
        return storeDetail;
    }

    public void setStoreDetail (String storeDetail) {
        this.storeDetail = storeDetail;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public String getOwnerName () {
        return ownerName;
    }

    public void setOwnerName (String ownerName) {
        this.ownerName = ownerName;
    }

    public String getShipSpace () {
        return shipSpace;
    }

    public void setShipSpace (String shipSpace) {
        this.shipSpace = shipSpace;
    }

    public String getCarrierId () {
        return carrierId;
    }

    public void setCarrierId (String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName () {
        return carrierName;
    }

    public void setCarrierName (String carrierName) {
        this.carrierName = carrierName;
    }

    public String getPlateNumber () {
        return plateNumber;
    }

    public void setPlateNumber (String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Date getRecvDate () {
        return recvDate;
    }

    public void setRecvDate (Date recvDate) {
        this.recvDate = recvDate;
    }

    public String getNoticeNo () {
        return noticeNo;
    }

    public void setNoticeNo (String noticeNo) {
        this.noticeNo = noticeNo;
    }

    @Override
    public String getVehicleCheckoutStatus () {
        return vehicleCheckoutStatus;
    }

    @Override
    public void setVehicleCheckoutStatus (String vehicleCheckoutStatus) {
        this.vehicleCheckoutStatus = vehicleCheckoutStatus;
    }
}
