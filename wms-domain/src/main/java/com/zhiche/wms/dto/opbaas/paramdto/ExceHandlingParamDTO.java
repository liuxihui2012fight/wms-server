package com.zhiche.wms.dto.opbaas.paramdto;

import java.io.Serializable;

/**
 * 异常处理方式
 */
public class ExceHandlingParamDTO implements Serializable{

    private Long [] ids;
    /**
     * 异常处理方式类型
     */
    private String dealType;

    private String houseId;

    private String vin;

    private String pictureId;

    public String getPictureId () {
        return pictureId;
    }

    public void setPictureId (String pictureId) {
        this.pictureId = pictureId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Long[] getIds() {
        return ids;
    }



    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public void setIds(Long[] ids) {
        this.ids = ids;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }
}
