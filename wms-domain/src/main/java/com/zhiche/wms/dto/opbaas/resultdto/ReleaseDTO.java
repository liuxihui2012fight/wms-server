package com.zhiche.wms.dto.opbaas.resultdto;

import java.io.Serializable;

/**
 * @Author: caiHua
 * @Description: OTM 下发修改运单信息 ,禁止其他功能使用该实体类
 * @Date: Create in 9:26 2019/6/27
 */
public class ReleaseDTO implements Serializable {
    /**
     * 指令号
     */
    private String shipmentGid;
    /**
     * 系统运单号
     */
    private String releaseGid;
    /**
     * 客户订单号
     */
    private String cusOrderNo;
    /**
     * 客户运单号
     */
    private String cusWaybillNo;
    /**
     * 客户车型
     */
    private String cusVehicleType;
    /**
     * 标准车型
     */
    private String stanVehicleType;

    /**
     * 订单属性
     */
    private String orderAtt;

    /**
     * 车型
     */
    private String vin;

    /**
     * 客户名称
     */
    private String customerId;
    /**
     * 目的地
     */
    private String destAddress;

    /**
     * 业务主体
     */
    private String ciamsId;

    /**
     * 项目
     */
    private String itemName;

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getCusOrderNo () {
        return cusOrderNo;
    }

    public void setCusOrderNo (String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusWaybillNo () {
        return cusWaybillNo;
    }

    public void setCusWaybillNo (String cusWaybillNo) {
        this.cusWaybillNo = cusWaybillNo;
    }

    public String getCusVehicleType () {
        return cusVehicleType;
    }

    public void setCusVehicleType (String cusVehicleType) {
        this.cusVehicleType = cusVehicleType;
    }

    public String getStanVehicleType () {
        return stanVehicleType;
    }

    public void setStanVehicleType (String stanVehicleType) {
        this.stanVehicleType = stanVehicleType;
    }

    public String getOrderAtt () {
        return orderAtt;
    }

    public void setOrderAtt (String orderAtt) {
        this.orderAtt = orderAtt;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getCustomerId () {
        return customerId;
    }

    public void setCustomerId (String customerId) {
        this.customerId = customerId;
    }

    public String getDestAddress () {
        return destAddress;
    }

    public void setDestAddress (String destAddress) {
        this.destAddress = destAddress;
    }

    public String getCiamsId () {
        return ciamsId;
    }

    public void setCiamsId (String ciamsId) {
        this.ciamsId = ciamsId;
    }

    public String getItemName () {
        return itemName;
    }

    public void setItemName (String itemName) {
        this.itemName = itemName;
    }
}
