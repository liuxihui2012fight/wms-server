package com.zhiche.wms.dto.inbound;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zhiche.wms.domain.model.inbound.InboundInspectLine;
import com.zhiche.wms.dto.opbaas.resultdto.PictureWithExcpDescDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 入库质检单明细
 * </p>
 *
 * @author liuanshun
 * @since 2018-06-22
 */
public class InboundInspectDTO extends InboundInspectLine implements Serializable {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;
    private String storeHouseName;
    private String ownerId;
    private String ownerName;
    private Date recvDate;
    private String taskType;
    private String userCreate;
    private String taskNode;
    private String missDescStr;
    private String exDescStr;
    private String excpIds;
    private String[] ex_arr;
    private String[] miss_arr;
    private String carrierName;
    private String isCanSend;
    //    private List<InspectExcpDTO> excpDetails;
    private List<PictureWithExcpDescDTO> picInfo;

    //系统运单号OR
    private String orderReleaseGid;

    public String getOrderReleaseGid () {
        return orderReleaseGid;
    }

    public void setOrderReleaseGid (String orderReleaseGid) {
        this.orderReleaseGid = orderReleaseGid;
    }

    public String getIsCanSend() {
        return isCanSend;
    }

    public void setIsCanSend(String isCanSend) {
        this.isCanSend = isCanSend;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String[] getEx_arr() {
        return ex_arr;
    }

    public void setEx_arr(String[] ex_arr) {
        this.ex_arr = ex_arr;
    }

    public String[] getMiss_arr() {
        return miss_arr;
    }

    public void setMiss_arr(String[] miss_arr) {
        this.miss_arr = miss_arr;
    }

    public List<PictureWithExcpDescDTO> getPicInfo() {
        return picInfo;
    }

    public void setPicInfo(List<PictureWithExcpDescDTO> picInfo) {
        this.picInfo = picInfo;
    }

    public String getExcpIds() {
        return excpIds;
    }

    public void setExcpIds(String excpIds) {
        this.excpIds = excpIds;
    }

    public String getMissDescStr() {
        return missDescStr;
    }

    public void setMissDescStr(String missDescStr) {
        this.missDescStr = missDescStr;
    }

    public String getExDescStr() {
        return exDescStr;
    }

    public void setExDescStr(String exDescStr) {
        this.exDescStr = exDescStr;
    }

    public String getTaskNode() {
        return taskNode;
    }

    public void setTaskNode(String taskNode) {
        this.taskNode = taskNode;
    }

//    public List<InspectExcpDTO> getExcpDetails() {
//        return excpDetails;
//    }
//
//    public void setExcpDetails(List<InspectExcpDTO> excpDetails) {
//        this.excpDetails = excpDetails;
//    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStoreHouseName() {
        return storeHouseName;
    }

    public void setStoreHouseName(String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    @Override
    public String getOwnerId() {
        return ownerId;
    }

    @Override
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Date getRecvDate() {
        return recvDate;
    }

    public void setRecvDate(Date recvDate) {
        this.recvDate = recvDate;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }
}
