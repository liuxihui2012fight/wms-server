package com.zhiche.wms.dto.inbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zhiche.wms.domain.model.inbound.InboundPutawayLine;

import java.io.Serializable;
import java.util.Date;

public class InboundPutawayDTO extends InboundPutawayLine implements Serializable {
    //JOIN -->InboundPutawayHeader
    private String status;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;
    private String storeHouseName;
    private String ownerName;
    private String genMethod;
    private String carrierId;
    private String carrierName;
    private String plateNumber;
    private Date recvDate;
    private String noticeNo;
    private String userCreate;
    private String areaCode;
    private String areaName;
    private String locationCode;
    private String locationName;
    private String storeHouseCode;
    private String imgBase64;
    private String destAddress;
    private String inboundStatus;
    private String remarks;
    private String sourceKey;
    private String ownerOrderNo;
    private String giveoutUser;
    private String receiveUser;
    private String certificatStatus;
    private Date receiveDate;
    private Date giveoutDate;
    private String updateLocationNo;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateLocationId;


    @Override
    public String getUpdateLocationNo () {
        return updateLocationNo;
    }

    @Override
    public void setUpdateLocationNo (String updateLocationNo) {
        this.updateLocationNo = updateLocationNo;
    }

    @Override
    public Long getUpdateLocationId () {
        return updateLocationId;
    }

    public void setUpdateLocationId (Long updateLocationId) {
        this.updateLocationId = updateLocationId;
    }

    public String getSourceKey () {
        return sourceKey;
    }

    public void setSourceKey (String sourceKey) {
        this.sourceKey = sourceKey;
    }

    @Override
    public Date getReceiveDate () {
        return receiveDate;
    }

    @Override
    public void setReceiveDate (Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    @Override
    public Date getGiveoutDate () {
        return giveoutDate;
    }

    @Override
    public void setGiveoutDate (Date giveoutDate) {
        this.giveoutDate = giveoutDate;
    }

    @Override
    public String getOwnerOrderNo () {
        return ownerOrderNo;
    }

    @Override
    public void setOwnerOrderNo (String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getDestAddress () {
        return destAddress;
    }

    public void setDestAddress (String destAddress) {
        this.destAddress = destAddress;
    }

    public String getImgBase64 () {
        return imgBase64;
    }

    public void setImgBase64 (String imgBase64) {
        this.imgBase64 = imgBase64;
    }

    public String getStoreHouseCode () {
        return storeHouseCode;
    }

    public void setStoreHouseCode (String storeHouseCode) {
        this.storeHouseCode = storeHouseCode;
    }

    public String getAreaCode () {
        return areaCode;
    }

    public void setAreaCode (String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName () {
        return areaName;
    }

    public void setAreaName (String areaName) {
        this.areaName = areaName;
    }

    public String getLocationCode () {
        return locationCode;
    }

    public void setLocationCode (String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationName () {
        return locationName;
    }

    public void setLocationName (String locationName) {
        this.locationName = locationName;
    }

    public String getUserCreate () {
        return userCreate;
    }

    public void setUserCreate (String userCreate) {
        this.userCreate = userCreate;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public String getOwnerName () {
        return ownerName;
    }

    public void setOwnerName (String ownerName) {
        this.ownerName = ownerName;
    }

    public String getGenMethod () {
        return genMethod;
    }

    public void setGenMethod (String genMethod) {
        this.genMethod = genMethod;
    }

    public String getCarrierId () {
        return carrierId;
    }

    public void setCarrierId (String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName () {
        return carrierName;
    }

    public void setCarrierName (String carrierName) {
        this.carrierName = carrierName;
    }

    public String getPlateNumber () {
        return plateNumber;
    }

    public void setPlateNumber (String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Date getRecvDate () {
        return recvDate;
    }

    public void setRecvDate (Date recvDate) {
        this.recvDate = recvDate;
    }

    public String getNoticeNo () {
        return noticeNo;
    }

    public void setNoticeNo (String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getInboundStatus () {
        return inboundStatus;
    }

    public void setInboundStatus (String inboundStatus) {
        this.inboundStatus = inboundStatus;
    }

    @Override
    public String getRemarks () {
        return remarks;
    }

    @Override
    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String getGiveoutUser () {
        return giveoutUser;
    }

    @Override
    public void setGiveoutUser (String giveoutUser) {
        this.giveoutUser = giveoutUser;
    }

    @Override
    public String getReceiveUser () {
        return receiveUser;
    }

    @Override
    public void setReceiveUser (String receiveUser) {
        this.receiveUser = receiveUser;
    }

    @Override
    public String getCertificatStatus () {
        return certificatStatus;
    }

    @Override
    public void setCertificatStatus (String certificatStatus) {
        this.certificatStatus = certificatStatus;
    }
}