package com.zhiche.wms.dto.stock;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: 工厂未备料库存数据
 * @Date: Create in 14:33 2019/11/25
 */
public class FactoryPrepareStockDTO implements Serializable {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long stockId;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String vin;
    @JsonSerialize(using = ToStringSerializer.class)
    private String storeHouseId;
    private String storeHouseName;
    private String areaName;
    private String locationNo;
    private BigDecimal qty;
    private Date inboundTime;
    private String inboundUser;
    private String remark;
    private Date gmtCreated;
    private String stockProperty;
    private String targetStoreHouse;
    private String shipmentGid;
    private Date realInboundTime;
    private String isShipment;

    public String getIsShipment() {
        return isShipment;
    }

    public void setIsShipment(String isShipment) {
        this.isShipment = isShipment;
    }

    public Date getRealInboundTime () {
        return realInboundTime;
    }

    public void setRealInboundTime (Date realInboundTime) {
        this.realInboundTime = realInboundTime;
    }

    public String getStockProperty () {
        return stockProperty;
    }

    public Long getStockId () {
        return stockId;
    }

    public String getTargetStoreHouse () {
        return targetStoreHouse;
    }

    public void setTargetStoreHouse (String targetStoreHouse) {
        this.targetStoreHouse = targetStoreHouse;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public void setStockId (Long stockId) {
        this.stockId = stockId;
    }

    public void setStockProperty (String stockProperty) {
        this.stockProperty = stockProperty;
    }

    public String getLocationNo () {
        return locationNo;
    }

    public void setLocationNo (String locationNo) {
        this.locationNo = locationNo;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (String storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public String getAreaName () {
        return areaName;
    }

    public void setAreaName (String areaName) {
        this.areaName = areaName;
    }

    public BigDecimal getQty () {
        return qty;
    }

    public void setQty (BigDecimal qty) {
        this.qty = qty;
    }

    public Date getInboundTime () {
        return inboundTime;
    }

    public void setInboundTime (Date inboundTime) {
        this.inboundTime = inboundTime;
    }

    public String getInboundUser () {
        return inboundUser;
    }

    public void setInboundUser (String inboundUser) {
        this.inboundUser = inboundUser;
    }

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public Date getGmtCreated () {
        return gmtCreated;
    }

    public void setGmtCreated (Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }
}
