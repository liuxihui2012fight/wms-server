package com.zhiche.wms.dto.opbaas.paramdto;

import com.zhiche.wms.dto.base.PageVo;

import java.io.Serializable;

public class ExceptionRegisterParamDTO extends PageVo implements Serializable {

    private String userId;
    private String token;
    private String taskType;
    private String queryParam;
    private String taskId;
    private String userCode;
    private String vin;
    private String qrCode;
    private String orderNo;
    private String level1Code;
    private String level1Name;
    private String level2Code;
    private String level2Name;
    private String level3Code;
    private String level3Name;
    private String picKeys;//逗号分隔key
    private String visitType;//CLICK 点击请求异常信息,SCAN 扫描获取异常信息
    private String exceptionDetails;
    //系统运单号OR
    private String orderReleaseGid;
    //仓库id
    private String houseId;
    //异常描述
    private String remark;


    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public String getHouseId () {
        return houseId;
    }

    public void setHouseId (String houseId) {
        this.houseId = houseId;
    }

    public String getOrderReleaseGid () {
        return orderReleaseGid;
    }

    public void setOrderReleaseGid (String orderReleaseGid) {
        this.orderReleaseGid = orderReleaseGid;
    }

    public String getExceptionDetails () {
        return exceptionDetails;
    }

    public void setExceptionDetails (String exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public String getVisitType () {
        return visitType;
    }

    public void setVisitType (String visitType) {
        this.visitType = visitType;
    }

    public String getLevel1Name () {
        return level1Name;
    }

    public void setLevel1Name (String level1Name) {
        this.level1Name = level1Name;
    }

    public String getLevel2Name () {
        return level2Name;
    }

    public void setLevel2Name (String level2Name) {
        this.level2Name = level2Name;
    }

    public String getLevel3Name () {
        return level3Name;
    }

    public void setLevel3Name (String level3Name) {
        this.level3Name = level3Name;
    }

    public String getQueryParam () {
        return queryParam;
    }

    public void setQueryParam (String queryParam) {
        this.queryParam = queryParam;
    }

    public String getPicKeys () {
        return picKeys;
    }

    public void setPicKeys (String picKeys) {
        this.picKeys = picKeys;
    }

    public String getLevel1Code () {
        return level1Code;
    }

    public void setLevel1Code (String level1Code) {
        this.level1Code = level1Code;
    }

    public String getLevel2Code () {
        return level2Code;
    }

    public void setLevel2Code (String level2Code) {
        this.level2Code = level2Code;
    }

    public String getLevel3Code () {
        return level3Code;
    }

    public void setLevel3Code (String level3Code) {
        this.level3Code = level3Code;
    }

    public String getOrderNo () {
        return orderNo;
    }

    public void setOrderNo (String orderNo) {
        this.orderNo = orderNo;
    }

    public String getQrCode () {
        return qrCode;
    }

    public void setQrCode (String qrCode) {
        this.qrCode = qrCode;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getUserCode () {
        return userCode;
    }

    public void setUserCode (String userCode) {
        this.userCode = userCode;
    }

    public String getUserId () {
        return userId;
    }

    public void setUserId (String userId) {
        this.userId = userId;
    }

    public String getToken () {
        return token;
    }

    public void setToken (String token) {
        this.token = token;
    }

    public String getTaskType () {
        return taskType;
    }

    public void setTaskType (String taskType) {
        this.taskType = taskType;
    }

    public String getTaskId () {
        return taskId;
    }

    public void setTaskId (String taskId) {
        this.taskId = taskId;
    }
}
