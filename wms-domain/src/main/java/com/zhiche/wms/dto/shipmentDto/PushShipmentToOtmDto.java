package com.zhiche.wms.dto.shipmentDto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.util.Date;

/**
 * @Author: caiHua 补发推送发运数据
 * @Description:
 * @Date: Create in 14:21 2019/6/17
 */
public class PushShipmentToOtmDto {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long releaseId;
    private String releaseStatus;
    private String shipmentGid;
    private Date shipDate;
    private String releaseGid;
    private String vin;
    private String shipUser;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long explogId;
    private String requestId;
    private String exportType;
    private String pushType;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inboundId;
    private String content;

    public Long getReleaseId () {
        return releaseId;
    }

    public void setReleaseId (Long releaseId) {
        this.releaseId = releaseId;
    }

    public String getReleaseStatus () {
        return releaseStatus;
    }

    public void setReleaseStatus (String releaseStatus) {
        this.releaseStatus = releaseStatus;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public Date getShipDate () {
        return shipDate;
    }

    public void setShipDate (Date shipDate) {
        this.shipDate = shipDate;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getShipUser () {
        return shipUser;
    }

    public void setShipUser (String shipUser) {
        this.shipUser = shipUser;
    }

    public Long getExplogId () {
        return explogId;
    }

    public void setExplogId (Long explogId) {
        this.explogId = explogId;
    }

    public String getRequestId () {
        return requestId;
    }

    public void setRequestId (String requestId) {
        this.requestId = requestId;
    }

    public String getExportType () {
        return exportType;
    }

    public void setExportType (String exportType) {
        this.exportType = exportType;
    }

    public String getPushType () {
        return pushType;
    }

    public void setPushType (String pushType) {
        this.pushType = pushType;
    }

    public Long getInboundId () {
        return inboundId;
    }

    public void setInboundId (Long inboundId) {
        this.inboundId = inboundId;
    }

    public String getContent () {
        return content;
    }

    public void setContent (String content) {
        this.content = content;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("PushShipmentToOtmDto{");
        sb.append("releaseId=").append(releaseId);
        sb.append(", releaseStatus='").append(releaseStatus).append('\'');
        sb.append(", shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", shipDate=").append(shipDate);
        sb.append(", releaseGid='").append(releaseGid).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", shipUser='").append(shipUser).append('\'');
        sb.append(", explogId=").append(explogId);
        sb.append(", requestId='").append(requestId).append('\'');
        sb.append(", exportType='").append(exportType).append('\'');
        sb.append(", pushType='").append(pushType).append('\'');
        sb.append(", inboundId='").append(inboundId).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
