package com.zhiche.wms.dto.shipmentDto;

import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 15:22 2019/7/5
 */
public class PushShipmentToTmsDto {

    private String shipmentGid;
    private Date shipDate;
    private String releaseGid;
    private String content;

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public Date getShipDate () {
        return shipDate;
    }

    public void setShipDate (Date shipDate) {
        this.shipDate = shipDate;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getContent () {
        return content;
    }

    public void setContent (String content) {
        this.content = content;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("PushShipmentToTmsDto{");
        sb.append("shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", shipDate=").append(shipDate);
        sb.append(", releaseGid='").append(releaseGid).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
