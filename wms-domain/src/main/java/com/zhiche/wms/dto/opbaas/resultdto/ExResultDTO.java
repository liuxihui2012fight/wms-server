package com.zhiche.wms.dto.opbaas.resultdto;

import com.baomidou.mybatisplus.annotations.TableField;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ExResultDTO implements Serializable {
    private String vin;
    private String vehicle;
    private String orderNo;
    private String missName;
    private String taskId;
    private String taskType;
    private String userCode;
    private String exceptionId;
    //---以下对应表字段
    private String id;
    private String omsOrderNo;
    private String qrCode;
    private String taskNode;
    private String businessDoc;
    private String exLevel1;
    private String level1Name;
    private String exLevel2;
    private String level2Name;
    private String exLevel3;
    private String level3Name;
    private String exDescribe;
    private String registerUser;
    private Date registerTime;
    private String involvedParty;
    private String dealStatus;
    private String dealType;
    private String dealResultDesc;
    private Date dealEndTime;
    private String status;
    private String userCreate;
    private String userModified;
    private Date gmtCreate;
    private Date gmtModified;
    private String picKey;
    private String picUrl;
    private String otmStatus;
    private String houseId;
    private String remark;
    private String remarks;
    private Date estimatedProcessingTime;
    private String releaseGid;
    private List<Map<String, String>> picUrlInfoList;
    private List<ExceptionRegisterDetailDTO> exceptionDetails;

    public Date getEstimatedProcessingTime () {
        return estimatedProcessingTime;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public void setEstimatedProcessingTime (Date estimatedProcessingTime) {
        this.estimatedProcessingTime = estimatedProcessingTime;
    }

    public String getRemark () {
        return remark;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    private String pictureId;

    public String getPictureId () {
        return pictureId;
    }

    public void setPictureId (String pictureId) {
        this.pictureId = pictureId;
    }

    public String getOtmStatus () {
        return otmStatus;
    }

    public void setOtmStatus (String otmStatus) {
        this.otmStatus = otmStatus;
    }

    public String getHouseId () {
        return houseId;
    }

    public void setHouseId (String houseId) {
        this.houseId = houseId;
    }

    public String getTaskId () {
        return taskId;
    }

    public void setTaskId (String taskId) {
        this.taskId = taskId;
    }

    public String getTaskType () {
        return taskType;
    }

    public void setTaskType (String taskType) {
        this.taskType = taskType;
    }

    public String getUserCode () {
        return userCode;
    }

    public void setUserCode (String userCode) {
        this.userCode = userCode;
    }

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public String getOmsOrderNo () {
        return omsOrderNo;
    }

    public void setOmsOrderNo (String omsOrderNo) {
        this.omsOrderNo = omsOrderNo;
    }

    public String getQrCode () {
        return qrCode;
    }

    public void setQrCode (String qrCode) {
        this.qrCode = qrCode;
    }

    public String getTaskNode () {
        return taskNode;
    }

    public void setTaskNode (String taskNode) {
        this.taskNode = taskNode;
    }

    public String getBusinessDoc () {
        return businessDoc;
    }

    public void setBusinessDoc (String businessDoc) {
        this.businessDoc = businessDoc;
    }

    public String getExLevel1 () {
        return exLevel1;
    }

    public void setExLevel1 (String exLevel1) {
        this.exLevel1 = exLevel1;
    }

    public String getLevel1Name () {
        return level1Name;
    }

    public void setLevel1Name (String level1Name) {
        this.level1Name = level1Name;
    }

    public String getExLevel2 () {
        return exLevel2;
    }

    public void setExLevel2 (String exLevel2) {
        this.exLevel2 = exLevel2;
    }

    public String getLevel2Name () {
        return level2Name;
    }

    public void setLevel2Name (String level2Name) {
        this.level2Name = level2Name;
    }

    public String getExLevel3 () {
        return exLevel3;
    }

    public void setExLevel3 (String exLevel3) {
        this.exLevel3 = exLevel3;
    }

    public String getLevel3Name () {
        return level3Name;
    }

    public void setLevel3Name (String level3Name) {
        this.level3Name = level3Name;
    }

    public String getExDescribe () {
        return exDescribe;
    }

    public void setExDescribe (String exDescribe) {
        this.exDescribe = exDescribe;
    }

    public String getRegisterUser () {
        return registerUser;
    }

    public void setRegisterUser (String registerUser) {
        this.registerUser = registerUser;
    }

    public Date getRegisterTime () {
        return registerTime;
    }

    public void setRegisterTime (Date registerTime) {
        this.registerTime = registerTime;
    }

    public String getInvolvedParty () {
        return involvedParty;
    }

    public void setInvolvedParty (String involvedParty) {
        this.involvedParty = involvedParty;
    }

    public String getDealStatus () {
        return dealStatus;
    }

    public void setDealStatus (String dealStatus) {
        this.dealStatus = dealStatus;
    }

    public String getDealType () {
        return dealType;
    }

    public void setDealType (String dealType) {
        this.dealType = dealType;
    }

    public String getDealResultDesc () {
        return dealResultDesc;
    }

    public void setDealResultDesc (String dealResultDesc) {
        this.dealResultDesc = dealResultDesc;
    }

    public Date getDealEndTime () {
        return dealEndTime;
    }

    public void setDealEndTime (Date dealEndTime) {
        this.dealEndTime = dealEndTime;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public String getUserCreate () {
        return userCreate;
    }

    public void setUserCreate (String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified () {
        return userModified;
    }

    public void setUserModified (String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getExceptionId () {
        return exceptionId;
    }

    public void setExceptionId (String exceptionId) {
        this.exceptionId = exceptionId;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getVehicle () {
        return vehicle;
    }

    public void setVehicle (String vehicle) {
        this.vehicle = vehicle;
    }

    public String getOrderNo () {
        return orderNo;
    }

    public void setOrderNo (String orderNo) {
        this.orderNo = orderNo;
    }

    public String getMissName () {
        return missName;
    }

    public void setMissName (String missName) {
        this.missName = missName;
    }

    public List<ExceptionRegisterDetailDTO> getExceptionDetails () {
        return exceptionDetails;
    }

    public void setExceptionDetails (List<ExceptionRegisterDetailDTO> exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public String getPicKey () {
        return picKey;
    }

    public void setPicKey (String picKey) {
        this.picKey = picKey;
    }

    public String getPicUrl () {
        return picUrl;
    }

    public void setPicUrl (String picUrl) {
        this.picUrl = picUrl;
    }

    public List<Map<String, String>> getPicUrlInfoList () {
        return picUrlInfoList;
    }

    public void setPicUrlInfoList (List<Map<String, String>> picUrlInfoList) {
        this.picUrlInfoList = picUrlInfoList;
    }
}
