package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:53 2019/11/21
 */
public class FPOutboundDTO implements Serializable {
    //主键id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //车架号
    private String vin;
    //仓库id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;
    //库位名称
    private String locationNo;
    //入库指令号
    private String shipmentGid;
    //状态
    private String prepareStatus;
    //出库用户
    private String outboundUser;
    //出库时间
    private Date outboundTime;
    //创建时间
    private Date gmtCreated;
    //修改时间
    private Date gmtModified;
    //仓库名称
    private String storeHouseName;

    private String remarks;

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getLocationNo () {
        return locationNo;
    }

    public void setLocationNo (String locationNo) {
        this.locationNo = locationNo;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getPrepareStatus () {
        return prepareStatus;
    }

    public void setPrepareStatus (String prepareStatus) {
        this.prepareStatus = prepareStatus;
    }

    public String getOutboundUser () {
        return outboundUser;
    }

    public void setOutboundUser (String outboundUser) {
        this.outboundUser = outboundUser;
    }

    public Date getOutboundTime () {
        return outboundTime;
    }

    public void setOutboundTime (Date outboundTime) {
        this.outboundTime = outboundTime;
    }

    public Date getGmtCreated () {
        return gmtCreated;
    }

    public void setGmtCreated (Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }
}
