package com.zhiche.wms.dto.sys;

import com.zhiche.wms.domain.model.base.Storehouse;

import java.io.Serializable;
import java.util.List;

/**
 * 仓库管理 数据传输对象
 */
public class StorehouseDTO extends Storehouse implements Serializable {
    private List<StorehouseNodeDTO> storeNodes;

    public List<StorehouseNodeDTO> getStoreNodes() {
        return storeNodes;
    }

    public void setStoreNodes(List<StorehouseNodeDTO> storeNodes) {
        this.storeNodes = storeNodes;
    }
}
