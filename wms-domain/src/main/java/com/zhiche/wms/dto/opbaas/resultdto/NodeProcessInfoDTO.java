package com.zhiche.wms.dto.opbaas.resultdto;

/**
 * Created by hxh on 2018/8/7.
 */

public class NodeProcessInfoDTO {

    private String id;              //业务节点对应id
    private String taskNode;        //业务节点
    private String waybillNo;       //运单号
    private String waybillStatus;   //运单状态
    private String vin;             //vin码
    private String qrCode;          //二维码
    private String cusWaybillNo; //运单号
    private String cusOrderNo;//订单号

    public String getCusWaybillNo () {
        return cusWaybillNo;
    }

    public void setCusWaybillNo (String cusWaybillNo) {
        this.cusWaybillNo = cusWaybillNo;
    }

    public String getCusOrderNo () {
        return cusOrderNo;
    }

    public void setCusOrderNo (String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public String getTaskNode () {
        return taskNode;
    }

    public void setTaskNode (String taskNode) {
        this.taskNode = taskNode;
    }

    public String getWaybillNo () {
        return waybillNo;
    }

    public void setWaybillNo (String waybillNo) {
        this.waybillNo = waybillNo;
    }

    public String getWaybillStatus () {
        return waybillStatus;
    }

    public void setWaybillStatus (String waybillStatus) {
        this.waybillStatus = waybillStatus;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getQrCode () {
        return qrCode;
    }

    public void setQrCode (String qrCode) {
        this.qrCode = qrCode;
    }
}
