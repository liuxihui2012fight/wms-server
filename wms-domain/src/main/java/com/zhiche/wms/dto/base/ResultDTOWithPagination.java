package com.zhiche.wms.dto.base;

import java.io.Serializable;

/**
 * 带分页数据的封装
 */
public class ResultDTOWithPagination<T> implements Serializable {

    private boolean success;
    private T data;
    private String messageCode;
    private String message;
    private PageVo pageVo;
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public ResultDTOWithPagination() {
    }

    public ResultDTOWithPagination(boolean success, T data, String message) {
        this.success = success;
        this.data = data;
        this.message = message;
        this.pageVo = new PageVo();
    }

    public ResultDTOWithPagination(boolean success, T data, String message, PageVo pageVo) {
        this.success = success;
        this.data = data;
        this.message = message;
        this.pageVo = pageVo;
    }

    public ResultDTOWithPagination(boolean success, T data, Integer code, String message, PageVo pageVo) {
        this.success = success;
        this.data = data;
        this.message = message;
        this.code = code;
        this.pageVo = pageVo;
    }

    public ResultDTOWithPagination(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public ResultDTOWithPagination(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageCode() {
        return this.messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public PageVo getPageVo() {
        return this.pageVo;
    }

    public void setPageVo(PageVo pageVo) {
        this.pageVo = pageVo;
    }

    public String toString() {
        String data = this.getData() == null ? "" : this.getData().toString();
        String pageVo = this.getPageVo() == null ? "" : this.getPageVo().toString();
        return "ResultDTOWithPagination [success=" + this.success + ", messageCode='" + this.messageCode + '\'' + ", message='" + this.message + '\'' + ", data={" + data + "}, pageVo={" + pageVo + "}" + ']';
    }


}
