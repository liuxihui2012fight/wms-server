package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:38 2019/12/4
 */
public class FactoryShipmentOutDTO implements Serializable {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long oorId;
    private String storeHouseName;
    private String areaName;
    private String locationName;

    public String getLocationName () {
        return locationName;
    }

    public void setLocationName (String locationName) {
        this.locationName = locationName;
    }

    public Long getOorId () {
        return oorId;
    }

    public void setOorId (Long oorId) {
        this.oorId = oorId;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public String getAreaName () {
        return areaName;
    }

    public void setAreaName (String areaName) {
        this.areaName = areaName;
    }
}
