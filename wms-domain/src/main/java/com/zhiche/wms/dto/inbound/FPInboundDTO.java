package com.zhiche.wms.dto.inbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:52 2019/11/21
 */
public class FPInboundDTO implements Serializable {
    //备料入库id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //车架号
    private String vin;
    @JsonSerialize(using = ToStringSerializer.class)
    //仓库id
    private Long storeHouseId;
    //库位
    private String locationNo;
    //状态
    private String prepareStatus;
    //入库用户
    private String inboundUser;
    //入库时间
    private Date inboundTime;
    //创建时间
    private Date gmtCreated;
    //仓库名称
    private String storeHouseName;
    //库存量
    private BigDecimal qty;
    //库存属性(正常，无单，备转正)
    private String stockProperty;

    public BigDecimal getQty () {
        return qty;
    }

    public void setQty (BigDecimal qty) {
        this.qty = qty;
    }

    public String getStockProperty () {
        return stockProperty;
    }

    public void setStockProperty (String stockProperty) {
        this.stockProperty = stockProperty;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getLocationNo () {
        return locationNo;
    }

    public void setLocationNo (String locationNo) {
        this.locationNo = locationNo;
    }

    public String getPrepareStatus () {
        return prepareStatus;
    }

    public void setPrepareStatus (String prepareStatus) {
        this.prepareStatus = prepareStatus;
    }

    public String getInboundUser () {
        return inboundUser;
    }

    public void setInboundUser (String inboundUser) {
        this.inboundUser = inboundUser;
    }

    public Date getInboundTime () {
        return inboundTime;
    }

    public void setInboundTime (Date inboundTime) {
        this.inboundTime = inboundTime;
    }

    public Date getGmtCreated () {
        return gmtCreated;
    }

    public void setGmtCreated (Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }
}
