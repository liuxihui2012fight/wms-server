package com.zhiche.wms.dto.opbaas.resultdto;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:06 2018/12/21
 */
public class ShipmentRecordDTO extends Model<ShipmentRecordDTO> implements Serializable {

    // id
    private String id;
    // 运单号
    private String shipmentGid;
    // 运单号
    private String cusWaybillNo;
    //车辆订单
    private String cusOrderNo;
    //车牌号
    private String vin;
    //	状态
    private String releaseStatus;
    //起始地
    private String originLocationName;
    //目的地
    private String destName;
    //订单时间
    private Date orderTime;
    //发车点id
    private String pointId;
    //发车点id
    private String pointName;
    //入库时间
    private Date inboundTime;
    //出库状态
    private String onlStatus;
    //钥匙备料员
    private String keyPreparetor;
    //计划员
    private String preparetor;
    //备车司机
    private String userModified;
    //板车号
    private String plateNumber;
    //操作用户
    private String userCode;
    //创建时间
    private Date gmtCreate;
    //板车号
    private String shipmentId;
    //是否超期
    private String pickupIsAppt;
    //是否急发
    private String isUrgent;
    //订单属性
    private String orderAtt;
    //要求发运时间
    private Date expcetShipDate;
    //要求抵达时间
    private Date expectArriveDate;

    //操作发运实际时间
    private Date realShipDate;

    //发运操作人
    private String shipUser;
    //发运时间
    private String shipDate;

    /**
     * 发运状态
     */
    private String shipStatus;

    /**
     * 系统运单号
     */
    private String releaseGid;


    /**
     * 板车司机
     */
    private String driverName;
    /**
     * 板车司机联系方式
     */
    private String driverPhone;
    /**
     * 分供方
     */
    private String serviceProviderName;
    /**
     * 客户名称
     */
    private String customerId;

    public Date getRealShipDate () {
        return realShipDate;
    }

    public String getShipDate () {
        return shipDate;
    }

    public void setShipDate (String shipDate) {
        this.shipDate = shipDate;
    }

    public void setRealShipDate (Date realShipDate) {
        this.realShipDate = realShipDate;
    }

    public String getShipUser () {
        return shipUser;
    }

    public void setShipUser (String shipUser) {
        this.shipUser = shipUser;
    }

    public String getPickupIsAppt () {
        return pickupIsAppt;
    }

    public void setPickupIsAppt (String pickupIsAppt) {
        this.pickupIsAppt = pickupIsAppt;
    }

    public String getIsUrgent () {
        return isUrgent;
    }

    public void setIsUrgent (String isUrgent) {
        this.isUrgent = isUrgent;
    }

    public String getOrderAtt () {
        return orderAtt;
    }

    public void setOrderAtt (String orderAtt) {
        this.orderAtt = orderAtt;
    }

    public Date getExpcetShipDate () {
        return expcetShipDate;
    }

    public void setExpcetShipDate (Date expcetShipDate) {
        this.expcetShipDate = expcetShipDate;
    }

    public Date getExpectArriveDate () {
        return expectArriveDate;
    }

    public void setExpectArriveDate (Date expectArriveDate) {
        this.expectArriveDate = expectArriveDate;
    }

    public String getShipmentId () {
        return shipmentId;
    }

    public String getShipStatus () {
        return shipStatus;
    }

    public void setShipStatus (String shipStatus) {
        this.shipStatus = shipStatus;
    }

    public void setShipmentId (String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getPlateNumber () {
        return plateNumber;
    }

    public void setPlateNumber (String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getCusWaybillNo () {
        return cusWaybillNo;
    }

    public void setCusWaybillNo (String cusWaybillNo) {
        this.cusWaybillNo = cusWaybillNo;
    }

    public String getCusOrderNo () {
        return cusOrderNo;
    }

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public void setCusOrderNo (String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getReleaseStatus () {
        return releaseStatus;
    }

    public void setReleaseStatus (String releaseStatus) {
        this.releaseStatus = releaseStatus;
    }

    public String getOriginLocationName () {
        return originLocationName;
    }

    public void setOriginLocationName (String originLocationName) {
        this.originLocationName = originLocationName;
    }

    public String getDestName () {
        return destName;
    }

    public void setDestName (String destName) {
        this.destName = destName;
    }

    public Date getOrderTime () {
        return orderTime;
    }

    public void setOrderTime (Date orderTime) {
        this.orderTime = orderTime;
    }

    public String getPointId () {
        return pointId;
    }

    public void setPointId (String pointId) {
        this.pointId = pointId;
    }

    public Date getInboundTime () {
        return inboundTime;
    }

    public void setInboundTime (Date inboundTime) {
        this.inboundTime = inboundTime;
    }

    public String getOnlStatus () {
        return onlStatus;
    }

    public void setOnlStatus (String onlStatus) {
        this.onlStatus = onlStatus;
    }

    public String getKeyPreparetor () {
        return keyPreparetor;
    }

    public void setKeyPreparetor (String keyPreparetor) {
        this.keyPreparetor = keyPreparetor;
    }

    public String getPreparetor () {
        return preparetor;
    }

    public void setPreparetor (String preparetor) {
        this.preparetor = preparetor;
    }

    public String getUserModified () {
        return userModified;
    }

    public void setUserModified (String userModified) {
        this.userModified = userModified;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getPointName () {
        return pointName;
    }

    public String getUserCode () {
        return userCode;
    }

    public void setUserCode (String userCode) {
        this.userCode = userCode;
    }

    public void setPointName (String pointName) {
        this.pointName = pointName;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getDriverName () {
        return driverName;
    }

    public void setDriverName (String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone () {
        return driverPhone;
    }

    public void setDriverPhone (String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getServiceProviderName () {
        return serviceProviderName;
    }

    public void setServiceProviderName (String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public String getCustomerId () {
        return customerId;
    }

    public void setCustomerId (String customerId) {
        this.customerId = customerId;
    }


    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("ShipmentRecordDTO{");
        sb.append("id='").append(id).append('\'');
        sb.append(", shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", cusWaybillNo='").append(cusWaybillNo).append('\'');
        sb.append(", cusOrderNo='").append(cusOrderNo).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", releaseStatus='").append(releaseStatus).append('\'');
        sb.append(", originLocationName='").append(originLocationName).append('\'');
        sb.append(", destName='").append(destName).append('\'');
        sb.append(", orderTime=").append(orderTime);
        sb.append(", pointId='").append(pointId).append('\'');
        sb.append(", pointName='").append(pointName).append('\'');
        sb.append(", inboundTime=").append(inboundTime);
        sb.append(", onlStatus='").append(onlStatus).append('\'');
        sb.append(", keyPreparetor='").append(keyPreparetor).append('\'');
        sb.append(", preparetor='").append(preparetor).append('\'');
        sb.append(", userModified='").append(userModified).append('\'');
        sb.append(", plateNumber='").append(plateNumber).append('\'');
        sb.append(", userCode='").append(userCode).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", shipmentId='").append(shipmentId).append('\'');
        sb.append(", pickupIsAppt='").append(pickupIsAppt).append('\'');
        sb.append(", isUrgent='").append(isUrgent).append('\'');
        sb.append(", orderAtt='").append(orderAtt).append('\'');
        sb.append(", expcetShipDate=").append(expcetShipDate);
        sb.append(", expectArriveDate=").append(expectArriveDate);
        sb.append(", realShipDate=").append(realShipDate);
        sb.append(", shipUser='").append(shipUser).append('\'');
        sb.append(", shipDate='").append(shipDate).append('\'');
        sb.append(", shipStatus='").append(shipStatus).append('\'');
        sb.append(", releaseGid='").append(releaseGid).append('\'');
        sb.append(", driverName='").append(driverName).append('\'');
        sb.append(", driverPhone='").append(driverPhone).append('\'');
        sb.append(", serviceProviderName='").append(serviceProviderName).append('\'');
        sb.append(", customerId='").append(customerId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    protected Serializable pkVal () {
        return this.getCusOrderNo();
    }
}
