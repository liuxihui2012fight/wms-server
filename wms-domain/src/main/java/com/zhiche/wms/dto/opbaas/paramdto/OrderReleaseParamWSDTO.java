package com.zhiche.wms.dto.opbaas.paramdto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OrderReleaseParamWSDTO implements Serializable {
    /**
     * 系统单号
     */
    private String orderReleaseGid;
    /**
     * OMS订单号
     */
    private String attribute15;
    /**
     * 运输指令号
     */
    private String attribute16;
    /**
     * 下单时间
     */
    private Date attributeDate1;
    /**
     * 打印时间
     */
    private Date attributeDate2;
    /**
     * 收付类型
     */
    private String programName;
    /**
     * 客户订单号
     */
    private String attribute1;
    /**
     * 客户运单号
     */
    private String orderReleaseName;
    /**
     * 客户
     */
    private String attribute2;
    /**
     * 订单属性
     */
    private String attribute3;
    /**
     * 急发(1：是，0：否)
     */
    private Integer dutyPaid;
    /**
     * 超期(1：是，0：否)
     */
    private Integer pickupIsAppt;
    /**
     * 买断(1：是，0：否)
     */
    private Integer isKnownShipper;
    /**
     * 运输订单状态
     */
    private String userDefined1IconGid;
    /**
     * 运输订单计费状态
     */
    private String userDefined2iconGid;
    /**
     * 工厂出库时间
     */
    private Date attributeDate3;
    /**
     * 实际工厂出库时间
     */
    private Date attributeDate4;
    /**
     * 要求提车发运时间
     */
    private Date earlyPickupDate;
    /**
     * 实际提车发运时间
     */
    private Date attributeDate5;
    /**
     * 要求提车抵达时间
     */
    private Date lateDeliveryDate;
    /**
     * 实际提车抵达时间
     */
    private Date attributeDate6;
    /**
     * 要求提车入库时间
     */
    private Date attributeDate7;
    /**
     * 实际提车入库时间
     */
    private Date attributeDate8;
    /**
     * 分段标识
     */
    private String orderReleaseTypeGid;
    /**
     * 起运地编码
     */
    private String sourceLocationGid;
    /**
     * 起运地名称
     */
    private String sourceLocationName;
    /**
     * 起运地省份
     */
    private String sourceZone1;
    /**
     * 起运地城市
     */
    private String sourceZone2;
    /**
     * 起运地区县
     */
    private String sourceZone3;
    /**
     * 目的地编码
     */
    private String destLocationGid;
    /**
     * 目的地名称
     */
    private String destLocationName;
    /**
     * 目的地省份
     */
    private String destZone1;
    /**
     * 目的地城市
     */
    private String destZone2;
    /**
     * 目的地区县
     */
    private String destZone3;
    /**
     * 客户运输模式分类
     */
    private String attribute4;
    /**
     * 客户车型
     */
    private String attribute5;
    /**
     * 车型描述
     */
    private String attribute6;
    /**
     * VIN码
     */
    private String attribute7;
    /**
     * 商品车数量
     */
    private BigDecimal totalShipUnitCount;
    /**
     * 是否改装车(1：是，0，否)
     */
    private Integer deliveryIsAppt;
    /**
     * 改装后的长宽高重
     */
    private String attribute8;
    /**
     * 合格证车型代码
     */
    private String attribute9;
    /**
     * 标准车型
     */
    private String attribute10;
    /**
     * 主运输模式
     */
    private String attribute11;
    /**
     * 应付里程数
     */
    private BigDecimal attributeNumber1;
    /**
     * 实际应付里程数
     */
    private BigDecimal attributeNumber2;
    /**
     * 应付合同编号
     */
    private String attribute12;
    /**
     * 收款方
     */
    private String attribute13;
    /**
     * 付款方
     */
    private String attribute14;
    /**
     * 车辆二维码
     */
    private String attribute17;
    /**
     * 备注
     */
    private String attribute19;
    /**
     * 分供方
     */
    private String attribute18;
    /**
     * 运输方式
     */
    private String attribute20;

    public String getOrderReleaseGid() {
        return orderReleaseGid;
    }

    public void setOrderReleaseGid(String orderReleaseGid) {
        this.orderReleaseGid = orderReleaseGid;
    }

    public String getAttribute15() {
        return attribute15;
    }

    public void setAttribute15(String attribute15) {
        this.attribute15 = attribute15;
    }

    public String getAttribute16() {
        return attribute16;
    }

    public void setAttribute16(String attribute16) {
        this.attribute16 = attribute16;
    }

    public Date getAttributeDate1() {
        return attributeDate1;
    }

    public void setAttributeDate1(Date attributeDate1) {
        this.attributeDate1 = attributeDate1;
    }

    public Date getAttributeDate2() {
        return attributeDate2;
    }

    public void setAttributeDate2(Date attributeDate2) {
        this.attributeDate2 = attributeDate2;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getOrderReleaseName() {
        return orderReleaseName;
    }

    public void setOrderReleaseName(String orderReleaseName) {
        this.orderReleaseName = orderReleaseName;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }

    public String getAttribute3() {
        return attribute3;
    }

    public void setAttribute3(String attribute3) {
        this.attribute3 = attribute3;
    }

    public Integer getDutyPaid() {
        return dutyPaid;
    }

    public void setDutyPaid(Integer dutyPaid) {
        this.dutyPaid = dutyPaid;
    }

    public Integer getPickupIsAppt() {
        return pickupIsAppt;
    }

    public void setPickupIsAppt(Integer pickupIsAppt) {
        this.pickupIsAppt = pickupIsAppt;
    }

    public Integer getIsKnownShipper() {
        return isKnownShipper;
    }

    public void setIsKnownShipper(Integer isKnownShipper) {
        this.isKnownShipper = isKnownShipper;
    }

    public String getUserDefined1IconGid() {
        return userDefined1IconGid;
    }

    public void setUserDefined1IconGid(String userDefined1IconGid) {
        this.userDefined1IconGid = userDefined1IconGid;
    }

    public String getUserDefined2iconGid() {
        return userDefined2iconGid;
    }

    public void setUserDefined2iconGid(String userDefined2iconGid) {
        this.userDefined2iconGid = userDefined2iconGid;
    }

    public Date getAttributeDate3() {
        return attributeDate3;
    }

    public void setAttributeDate3(Date attributeDate3) {
        this.attributeDate3 = attributeDate3;
    }

    public Date getAttributeDate4() {
        return attributeDate4;
    }

    public void setAttributeDate4(Date attributeDate4) {
        this.attributeDate4 = attributeDate4;
    }

    public Date getEarlyPickupDate() {
        return earlyPickupDate;
    }

    public void setEarlyPickupDate(Date earlyPickupDate) {
        this.earlyPickupDate = earlyPickupDate;
    }

    public Date getAttributeDate5() {
        return attributeDate5;
    }

    public void setAttributeDate5(Date attributeDate5) {
        this.attributeDate5 = attributeDate5;
    }

    public Date getLateDeliveryDate() {
        return lateDeliveryDate;
    }

    public void setLateDeliveryDate(Date lateDeliveryDate) {
        this.lateDeliveryDate = lateDeliveryDate;
    }

    public Date getAttributeDate6() {
        return attributeDate6;
    }

    public void setAttributeDate6(Date attributeDate6) {
        this.attributeDate6 = attributeDate6;
    }

    public Date getAttributeDate7() {
        return attributeDate7;
    }

    public void setAttributeDate7(Date attributeDate7) {
        this.attributeDate7 = attributeDate7;
    }

    public Date getAttributeDate8() {
        return attributeDate8;
    }

    public void setAttributeDate8(Date attributeDate8) {
        this.attributeDate8 = attributeDate8;
    }

    public String getOrderReleaseTypeGid() {
        return orderReleaseTypeGid;
    }

    public void setOrderReleaseTypeGid(String orderReleaseTypeGid) {
        this.orderReleaseTypeGid = orderReleaseTypeGid;
    }

    public String getSourceLocationGid() {
        return sourceLocationGid;
    }

    public void setSourceLocationGid(String sourceLocationGid) {
        this.sourceLocationGid = sourceLocationGid;
    }

    public String getSourceLocationName() {
        return sourceLocationName;
    }

    public void setSourceLocationName(String sourceLocationName) {
        this.sourceLocationName = sourceLocationName;
    }

    public String getSourceZone1() {
        return sourceZone1;
    }

    public void setSourceZone1(String sourceZone1) {
        this.sourceZone1 = sourceZone1;
    }

    public String getSourceZone2() {
        return sourceZone2;
    }

    public void setSourceZone2(String sourceZone2) {
        this.sourceZone2 = sourceZone2;
    }

    public String getSourceZone3() {
        return sourceZone3;
    }

    public void setSourceZone3(String sourceZone3) {
        this.sourceZone3 = sourceZone3;
    }

    public String getDestLocationGid() {
        return destLocationGid;
    }

    public void setDestLocationGid(String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    public String getDestLocationName() {
        return destLocationName;
    }

    public void setDestLocationName(String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestZone1() {
        return destZone1;
    }

    public void setDestZone1(String destZone1) {
        this.destZone1 = destZone1;
    }

    public String getDestZone2() {
        return destZone2;
    }

    public void setDestZone2(String destZone2) {
        this.destZone2 = destZone2;
    }

    public String getDestZone3() {
        return destZone3;
    }

    public void setDestZone3(String destZone3) {
        this.destZone3 = destZone3;
    }

    public String getAttribute4() {
        return attribute4;
    }

    public void setAttribute4(String attribute4) {
        this.attribute4 = attribute4;
    }

    public String getAttribute5() {
        return attribute5;
    }

    public void setAttribute5(String attribute5) {
        this.attribute5 = attribute5;
    }

    public String getAttribute6() {
        return attribute6;
    }

    public void setAttribute6(String attribute6) {
        this.attribute6 = attribute6;
    }

    public String getAttribute7() {
        return attribute7;
    }

    public void setAttribute7(String attribute7) {
        this.attribute7 = attribute7;
    }

    public BigDecimal getTotalShipUnitCount() {
        return totalShipUnitCount;
    }

    public void setTotalShipUnitCount(BigDecimal totalShipUnitCount) {
        this.totalShipUnitCount = totalShipUnitCount;
    }

    public Integer getDeliveryIsAppt() {
        return deliveryIsAppt;
    }

    public void setDeliveryIsAppt(Integer deliveryIsAppt) {
        this.deliveryIsAppt = deliveryIsAppt;
    }

    public String getAttribute8() {
        return attribute8;
    }

    public void setAttribute8(String attribute8) {
        this.attribute8 = attribute8;
    }

    public String getAttribute9() {
        return attribute9;
    }

    public void setAttribute9(String attribute9) {
        this.attribute9 = attribute9;
    }

    public String getAttribute10() {
        return attribute10;
    }

    public void setAttribute10(String attribute10) {
        this.attribute10 = attribute10;
    }

    public String getAttribute11() {
        return attribute11;
    }

    public void setAttribute11(String attribute11) {
        this.attribute11 = attribute11;
    }

    public BigDecimal getAttributeNumber1() {
        return attributeNumber1;
    }

    public void setAttributeNumber1(BigDecimal attributeNumber1) {
        this.attributeNumber1 = attributeNumber1;
    }

    public BigDecimal getAttributeNumber2() {
        return attributeNumber2;
    }

    public void setAttributeNumber2(BigDecimal attributeNumber2) {
        this.attributeNumber2 = attributeNumber2;
    }

    public String getAttribute12() {
        return attribute12;
    }

    public void setAttribute12(String attribute12) {
        this.attribute12 = attribute12;
    }

    public String getAttribute13() {
        return attribute13;
    }

    public void setAttribute13(String attribute13) {
        this.attribute13 = attribute13;
    }

    public String getAttribute14() {
        return attribute14;
    }

    public void setAttribute14(String attribute14) {
        this.attribute14 = attribute14;
    }

    public String getAttribute17() {
        return attribute17;
    }

    public void setAttribute17(String attribute17) {
        this.attribute17 = attribute17;
    }

    public String getAttribute19() {
        return attribute19;
    }

    public void setAttribute19(String attribute19) {
        this.attribute19 = attribute19;
    }

    public String getAttribute18() {
        return attribute18;
    }

    public void setAttribute18(String attribute18) {
        this.attribute18 = attribute18;
    }

    public String getAttribute20() {
        return attribute20;
    }

    public void setAttribute20(String attribute20) {
        this.attribute20 = attribute20;
    }

}
