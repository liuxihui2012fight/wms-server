package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zhiche.wms.domain.model.outbound.OutboundShipLine;

import java.io.Serializable;
import java.util.Date;

public class OutboundShipDTO extends OutboundShipLine implements Serializable {
    private String status;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;
    private String storeHouseName;
    private String ownerName;
    private String carrierId;
    private String carrierName;
    private String plateNumber;
    private Date recvDate;
    private String noticeNo;
    private String genMethod;
    private String outboundStatus;
    private String prepareStatus;
    private String preparetor;
    private String outboundUser;
    private String sourceKey;
    private String remarks;

    public String getOutboundUser () {
        return outboundUser;
    }

    public void setOutboundUser (String outboundUser) {
        this.outboundUser = outboundUser;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public String getOwnerName () {
        return ownerName;
    }

    public void setOwnerName (String ownerName) {
        this.ownerName = ownerName;
    }

    public String getCarrierId () {
        return carrierId;
    }

    public void setCarrierId (String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName () {
        return carrierName;
    }

    public void setCarrierName (String carrierName) {
        this.carrierName = carrierName;
    }

    public String getPlateNumber () {
        return plateNumber;
    }

    public void setPlateNumber (String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Date getRecvDate () {
        return recvDate;
    }

    public void setRecvDate (Date recvDate) {
        this.recvDate = recvDate;
    }

    public String getNoticeNo () {
        return noticeNo;
    }

    public void setNoticeNo (String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getGenMethod () {
        return genMethod;
    }

    public void setGenMethod (String genMethod) {
        this.genMethod = genMethod;
    }

    public String getOutboundStatus () {
        return outboundStatus;
    }

    public void setOutboundStatus (String outboundStatus) {
        this.outboundStatus = outboundStatus;
    }

    public String getPrepareStatus () {
        return prepareStatus;
    }

    public void setPrepareStatus (String prepareStatus) {
        this.prepareStatus = prepareStatus;
    }

    public String getPreparetor () {
        return preparetor;
    }

    public void setPreparetor (String preparetor) {
        this.preparetor = preparetor;
    }

    public String getSourceKey () {
        return sourceKey;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public void setSourceKey (String sourceKey) {
        this.sourceKey = sourceKey;
    }
}
