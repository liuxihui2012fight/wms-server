package com.zhiche.wms.dto.outbound;

import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.dto.inbound.InboundNoticeDTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class OutboundNoticeHeaderDTO extends OutboundNoticeHeader implements Serializable {
    private String driverMess;
    private String storeHouseName;
    private Date expectRecvDateLower;
    private List<OutboundNoticeDTO> outboundNoticeLines;

    public void addOutboundNoticeLines(OutboundNoticeDTO outboundNoticeLine){
        if(Objects.isNull(outboundNoticeLine)){
            outboundNoticeLines= new ArrayList<>();
        }
        outboundNoticeLines.add(outboundNoticeLine);
    }

    public String getDriverMess() {
        return driverMess;
    }

    public void setDriverMess(String driverMess) {
        this.driverMess = driverMess;
    }

    public String getStoreHouseName() {
        return storeHouseName;
    }

    public void setStoreHouseName(String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public Date getExpectRecvDateLower() {
        return expectRecvDateLower;
    }

    public void setExpectRecvDateLower(Date expectRecvDateLower) {
        this.expectRecvDateLower = expectRecvDateLower;
    }

    public List<OutboundNoticeDTO> getOutboundNoticeLines() {
        return outboundNoticeLines;
    }

    public void setOutboundNoticeLines(List<OutboundNoticeDTO> outboundNoticeLines) {
        this.outboundNoticeLines = outboundNoticeLines;
    }
}
