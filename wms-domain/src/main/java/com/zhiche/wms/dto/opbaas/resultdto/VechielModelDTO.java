package com.zhiche.wms.dto.opbaas.resultdto;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:12 2018/12/11
 */
public class VechielModelDTO implements Serializable {

    private Integer id;
    /**
     * 编码
     */
    private String code;
    /**
     * 名称
     */
    private String name;
    /**
     * 车辆类型
     */
    private String vehicleType;
    /**
     * 品牌编码
     */
    private String vehicleBrandCode;

    public Integer getId () {
        return id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public String getCode () {
        return code;
    }

    public void setCode (String code) {
        this.code = code;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getVehicleType () {
        return vehicleType;
    }

    public void setVehicleType (String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleBrandCode () {
        return vehicleBrandCode;
    }

    public void setVehicleBrandCode (String vehicleBrandCode) {
        this.vehicleBrandCode = vehicleBrandCode;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("VechielModelDTO{");
        sb.append("id=").append(id);
        sb.append(", code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", vehicleType='").append(vehicleType).append('\'');
        sb.append(", vehicleBrandCode='").append(vehicleBrandCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
