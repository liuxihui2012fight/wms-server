package com.zhiche.wms.dto.stock;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: 调平库存
 * @Date: Create in 10:58 2019/9/3
 */
public class StockQty implements Serializable {
    /**
     * 库存id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stockId;

    /**
     * 车辆id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skuId;
    /**
     * 库存
     */
    private BigDecimal qty;
    @JsonSerialize(using = ToStringSerializer.class)
    /**
     * 仓库id
     */
    private Long storeHouseId;
    @JsonSerialize(using = ToStringSerializer.class)
    /**
     * 库区id
     */
    private Long locationId;
    /**
     * 车架号
     */
    private String lotNo1;
    /**
     * 仓库名称
     */
    private String storeHouseName;
    /**
     * 入库时间
     */
    private Date inboundTme;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 操作入库仓库人员
     */
    private String inboundUser;
    /**
     * 指令号
     */
    private String shipmentGid;

    /**
     * 库位
     */
    private String areaLocation;

    public String getAreaLocation () {
        return areaLocation;
    }

    public void setAreaLocation (String areaLocation) {
        this.areaLocation = areaLocation;
    }

    public Long getSkuId () {
        return skuId;
    }

    public void setSkuId (Long skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getQty () {
        return qty;
    }

    public void setQty (BigDecimal qty) {
        this.qty = qty;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public Long getLocationId () {
        return locationId;
    }

    public void setLocationId (Long locationId) {
        this.locationId = locationId;
    }

    public String getLotNo1 () {
        return lotNo1;
    }

    public void setLotNo1 (String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public Date getInboundTme () {
        return inboundTme;
    }

    public void setInboundTme (Date inboundTme) {
        this.inboundTme = inboundTme;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public String getInboundUser () {
        return inboundUser;
    }

    public void setInboundUser (String inboundUser) {
        this.inboundUser = inboundUser;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public Long getStockId () {
        return stockId;
    }

    public void setStockId (Long stockId) {
        this.stockId = stockId;
    }
}
