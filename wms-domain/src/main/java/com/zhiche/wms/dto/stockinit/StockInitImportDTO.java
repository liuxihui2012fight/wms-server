package com.zhiche.wms.dto.stockinit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * excel导入期初库存传输对象
 */
public class StockInitImportDTO implements Serializable {

    /**
     * 期初库存单号
     */
    private String initNo;
    /**
     * 仓库
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 货主
     */
    private String ownerId;
    /**
     * 单据日期
     */
    private Date orderDate;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 数量
     */
    private BigDecimal sumQty;
    /**
     * 明细行数
     */
    private Integer lineCount;
    /**
     * 备注
     */
    private String remarks;


    /**
     * 出库单头键
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long headerId;
    /**
     * 序号
     */
    private String seq;
    /**
     * 物料ID
     */
    private String materielId;
    /**
     * 物料代码
     */
    private String materielCode;
    /**
     * 物料名称
     */
    private String materielName;
    /**
     * 储位
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long locationId;
    /**
     * 数量
     */
    private BigDecimal qty;
    /**
     * 净重
     */
    private BigDecimal netWeight;
    /**
     * 毛重
     */
    private BigDecimal grossWeight;
    /**
     * 体积
     */
    private BigDecimal grossCubage;
    /**
     * 件数
     */
    private BigDecimal packedCount;
    /**
     * 批号0
     */
    private String lotNo0;
    /**
     * 批号1
     */
    private String lotNo1;
    /**
     * 批号1
     */
    private String lotNo2;
    /**
     * 批号3
     */
    private String lotNo3;
    /**
     * 批号4
     */
    private String lotNo4;
    /**
     * 批号5
     */
    private String lotNo5;
    /**
     * 批号6
     */
    private String lotNo6;
    /**
     * 批号7
     */
    private String lotNo7;
    /**
     * 批号8
     */
    private String lotNo8;
    /**
     * 批号9
     */
    private String lotNo9;

    private String warehouseName;

    private String customerName;
    private String locationCode;
    private String productStr;
    private String offLineStr;
    private String gearBox;

    public String getProductStr() {
        return productStr;
    }

    public void setProductStr(String productStr) {
        this.productStr = productStr;
    }

    public String getOffLineStr() {
        return offLineStr;
    }

    public void setOffLineStr(String offLineStr) {
        this.offLineStr = offLineStr;
    }

    public String getGearBox() {
        return gearBox;
    }

    public void setGearBox(String gearBox) {
        this.gearBox = gearBox;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getInitNo() {
        return initNo;
    }

    public void setInitNo(String initNo) {
        this.initNo = initNo;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getSumQty() {
        return sumQty;
    }

    public void setSumQty(BigDecimal sumQty) {
        this.sumQty = sumQty;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public BigDecimal getGrossCubage() {
        return grossCubage;
    }

    public void setGrossCubage(BigDecimal grossCubage) {
        this.grossCubage = grossCubage;
    }

    public BigDecimal getPackedCount() {
        return packedCount;
    }

    public void setPackedCount(BigDecimal packedCount) {
        this.packedCount = packedCount;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }


    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }
}