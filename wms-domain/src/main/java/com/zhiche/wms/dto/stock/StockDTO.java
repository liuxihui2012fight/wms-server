package com.zhiche.wms.dto.stock;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zhaoguixin on 2018/7/7.
 */
public class StockDTO {

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skuId;
    /**
     * 货主
     */
    private String ownerId;
    /**
     * 货主
     */
    private String ownerName;
    /**
     * 物料ID
     */
    private String materielId;
    /**
     * 物料ID
     */
    private String materielCode;
    /**
     * 物料ID
     */
    private String materielName;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 批号0
     */
    private String lotNo0;
    /**
     * 批号1
     */
    private String lotNo1;
    /**
     * 批号2
     */
    private String lotNo2;
    /**
     * 批号3
     */
    private String lotNo3;
    /**
     * 批号4
     */
    private String lotNo4;
    /**
     * 批号5
     */
    private String lotNo5;
    /**
     * 批号6
     */
    private String lotNo6;
    /**
     * 批号7
     */
    private String lotNo7;
    /**
     * 批号8
     */
    private String lotNo8;
    /**
     * 批号9
     */
    private String lotNo9;
    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stockId;
    /**
     * 仓库
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 仓库名称
     */
    private String storeHouseName;
    /**
     * 库区ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeAreaId;
    /**
     * 库区名称
     */
    private String storeAreaName;
    /**
     * 储位
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long locationId;
    /**
     * 储位编码
     */
    private String locationCode;
    /**
     * 储位名称
     */
    private String locationName;
    /**
     * 库存数量
     */
    private BigDecimal qty;
    /**
     * 库存净重
     */
    private BigDecimal netWeight;
    /**
     * 库存毛重
     */
    private BigDecimal grossWeight;
    /**
     * 库存体积
     */
    private BigDecimal grossCubage;
    /**
     * 库存件数
     */
    private BigDecimal packedCount;
    /**
     * 库存状态(10:正常,20:锁定)
     */
    private String stockStatus;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 二维码
     */
    private String qrCode;
    /**
     * 标准车型
     */
    private String stanVehicleType;

    private String storeDetail;

    /**
     * 锁定原因
     */
    private String lockReason;
    /**
     * 解锁原因
     */
    private String unlockReason;
    /**
     * 锁定时间
     */
    private Date lockTime;
    /**
     * 解锁时间
     */
    private Date unlockTime;

    /**
     * 入库时间
     */
    private Date inboundTime;

    /**
     * 客户运单号
     */
    private String cusQWaybillNo;


    /**
     * 目的城市
     */
    private String destLocationCity;

    /**
     * 目的省份
     */
    private String destLocationProvince;

    /**
     * 指令号
     */
    private String shipmentGid;

    /**
     * 库存属性
     */
    private String stockProperty;

    /**
     * 订单目的地
     */
    private String orderDestAddress;

    public String getOrderDestAddress () {
        return orderDestAddress;
    }

    public void setOrderDestAddress (String orderDestAddress) {
        this.orderDestAddress = orderDestAddress;
    }

    public String getStockProperty () {
        return stockProperty;
    }

    public void setStockProperty (String stockProperty) {
        this.stockProperty = stockProperty;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getDestLocationCity () {
        return destLocationCity;
    }

    public void setDestLocationCity (String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationProvince () {
        return destLocationProvince;
    }

    public void setDestLocationProvince (String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getCusQWaybillNo () {
        return cusQWaybillNo;
    }

    public void setCusQWaybillNo (String cusQWaybillNo) {
        this.cusQWaybillNo = cusQWaybillNo;
    }

    public Date getInboundTime () {
        return inboundTime;
    }

    public void setInboundTime (Date inboundTime) {
        this.inboundTime = inboundTime;
    }

    public String getLockReason () {
        return lockReason;
    }

    public void setLockReason (String lockReason) {
        this.lockReason = lockReason;
    }

    public String getUnlockReason () {
        return unlockReason;
    }

    public void setUnlockReason (String unlockReason) {
        this.unlockReason = unlockReason;
    }

    public Date getLockTime () {
        return lockTime;
    }

    public void setLockTime (Date lockTime) {
        this.lockTime = lockTime;
    }

    public Date getUnlockTime () {
        return unlockTime;
    }

    public void setUnlockTime (Date unlockTime) {
        this.unlockTime = unlockTime;
    }

    public String getStoreDetail () {
        return storeDetail;
    }

    public void setStoreDetail (String storeDetail) {
        this.storeDetail = storeDetail;
    }

    public Long getSkuId () {
        return skuId;
    }

    public void setSkuId (Long skuId) {
        this.skuId = skuId;
    }

    public String getOwnerId () {
        return ownerId;
    }

    public void setOwnerId (String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName () {
        return ownerName;
    }

    public void setOwnerName (String ownerName) {
        this.ownerName = ownerName;
    }

    public String getMaterielId () {
        return materielId;
    }

    public void setMaterielId (String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode () {
        return materielCode;
    }

    public void setMaterielCode (String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName () {
        return materielName;
    }

    public void setMaterielName (String materielName) {
        this.materielName = materielName;
    }

    public String getUom () {
        return uom;
    }

    public void setUom (String uom) {
        this.uom = uom;
    }

    public String getLotNo0 () {
        return lotNo0;
    }

    public void setLotNo0 (String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1 () {
        return lotNo1;
    }

    public void setLotNo1 (String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2 () {
        return lotNo2;
    }

    public void setLotNo2 (String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3 () {
        return lotNo3;
    }

    public void setLotNo3 (String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4 () {
        return lotNo4;
    }

    public void setLotNo4 (String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5 () {
        return lotNo5;
    }

    public void setLotNo5 (String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6 () {
        return lotNo6;
    }

    public void setLotNo6 (String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7 () {
        return lotNo7;
    }

    public void setLotNo7 (String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8 () {
        return lotNo8;
    }

    public void setLotNo8 (String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9 () {
        return lotNo9;
    }

    public void setLotNo9 (String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public Long getStockId () {
        return stockId;
    }

    public void setStockId (Long stockId) {
        this.stockId = stockId;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public Long getStoreAreaId () {
        return storeAreaId;
    }

    public void setStoreAreaId (Long storeAreaId) {
        this.storeAreaId = storeAreaId;
    }

    public String getStoreAreaName () {
        return storeAreaName;
    }

    public void setStoreAreaName (String storeAreaName) {
        this.storeAreaName = storeAreaName;
    }

    public Long getLocationId () {
        return locationId;
    }

    public void setLocationId (Long locationId) {
        this.locationId = locationId;
    }

    public String getLocationCode () {
        return locationCode;
    }

    public void setLocationCode (String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationName () {
        return locationName;
    }

    public void setLocationName (String locationName) {
        this.locationName = locationName;
    }

    public BigDecimal getQty () {
        return qty;
    }

    public void setQty (BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getNetWeight () {
        return netWeight;
    }

    public void setNetWeight (BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public BigDecimal getGrossWeight () {
        return grossWeight;
    }

    public void setGrossWeight (BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public BigDecimal getGrossCubage () {
        return grossCubage;
    }

    public void setGrossCubage (BigDecimal grossCubage) {
        this.grossCubage = grossCubage;
    }

    public BigDecimal getPackedCount () {
        return packedCount;
    }

    public void setPackedCount (BigDecimal packedCount) {
        this.packedCount = packedCount;
    }

    public String getStockStatus () {
        return stockStatus;
    }

    public void setStockStatus (String stockStatus) {
        this.stockStatus = stockStatus;
    }

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public String getQrCode () {
        return qrCode;
    }

    public void setQrCode (String qrCode) {
        this.qrCode = qrCode;
    }

    public String getStanVehicleType () {
        return stanVehicleType;
    }

    public void setStanVehicleType (String stanVehicleType) {
        this.stanVehicleType = stanVehicleType;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("StockDTO{");
        sb.append("skuId=").append(skuId);
        sb.append(", ownerId='").append(ownerId).append('\'');
        sb.append(", ownerName='").append(ownerName).append('\'');
        sb.append(", materielId='").append(materielId).append('\'');
        sb.append(", materielCode='").append(materielCode).append('\'');
        sb.append(", materielName='").append(materielName).append('\'');
        sb.append(", uom='").append(uom).append('\'');
        sb.append(", lotNo0='").append(lotNo0).append('\'');
        sb.append(", lotNo1='").append(lotNo1).append('\'');
        sb.append(", lotNo2='").append(lotNo2).append('\'');
        sb.append(", lotNo3='").append(lotNo3).append('\'');
        sb.append(", lotNo4='").append(lotNo4).append('\'');
        sb.append(", lotNo5='").append(lotNo5).append('\'');
        sb.append(", lotNo6='").append(lotNo6).append('\'');
        sb.append(", lotNo7='").append(lotNo7).append('\'');
        sb.append(", lotNo8='").append(lotNo8).append('\'');
        sb.append(", lotNo9='").append(lotNo9).append('\'');
        sb.append(", stockId=").append(stockId);
        sb.append(", storeHouseId=").append(storeHouseId);
        sb.append(", storeHouseName='").append(storeHouseName).append('\'');
        sb.append(", storeAreaId=").append(storeAreaId);
        sb.append(", storeAreaName='").append(storeAreaName).append('\'');
        sb.append(", locationId=").append(locationId);
        sb.append(", locationCode='").append(locationCode).append('\'');
        sb.append(", locationName='").append(locationName).append('\'');
        sb.append(", qty=").append(qty);
        sb.append(", netWeight=").append(netWeight);
        sb.append(", grossWeight=").append(grossWeight);
        sb.append(", grossCubage=").append(grossCubage);
        sb.append(", packedCount=").append(packedCount);
        sb.append(", stockStatus='").append(stockStatus).append('\'');
        sb.append(", remarks='").append(remarks).append('\'');
        sb.append(", qrCode='").append(qrCode).append('\'');
        sb.append(", stanVehicleType='").append(stanVehicleType).append('\'');
        sb.append(", storeDetail='").append(storeDetail).append('\'');
        sb.append(", lockReason='").append(lockReason).append('\'');
        sb.append(", unlockReason='").append(unlockReason).append('\'');
        sb.append(", lockTime=").append(lockTime);
        sb.append(", unlockTime=").append(unlockTime);
        sb.append(", inboundTime=").append(inboundTime);
        sb.append(", cusQWaybillNo=").append(cusQWaybillNo);
        sb.append(", destLocationCity=").append(destLocationCity);
        sb.append(", destLocationProvince=").append(destLocationProvince);
        sb.append(", shipmentGid=").append(shipmentGid);
        sb.append('}');
        return sb.toString();
    }
}
