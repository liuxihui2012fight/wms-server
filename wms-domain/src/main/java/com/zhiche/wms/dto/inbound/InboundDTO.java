package com.zhiche.wms.dto.inbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;

import java.util.Date;

public class InboundDTO extends InboundNoticeLine {
    //JOIN-->Header
    private String noticeNo;
    private String driverName;
    private String driverPhone;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;
    private String storeHouseName;
    private String carrierId;
    private String carrierName;

    //JOIN --Inspect
    private String inspectStatus;
    private String inspectRemark;
    private Date gmtCreate;
    private Date createDate;
    private Date expectDate;
    //JOIN --Store
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeAreaId;
    private String storeAreaName;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long locationId;
    private String locationCode;
    private Date inboundModified;
    private Date inboundTime;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long putwayLineId;

    public Long getPutwayLineId () {
        return putwayLineId;
    }

    public void setPutwayLineId (Long putwayLineId) {
        this.putwayLineId = putwayLineId;
    }

    public Date getInboundTime () {
        return inboundTime;
    }

    public void setInboundTime (Date inboundTime) {
        this.inboundTime = inboundTime;
    }

    public String getNoticeNo () {
        return noticeNo;
    }

    public void setNoticeNo (String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getDriverName () {
        return driverName;
    }

    public void setDriverName (String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone () {
        return driverPhone;
    }

    public void setDriverPhone (String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getStoreHouseName () {
        return storeHouseName;
    }

    public void setStoreHouseName (String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public String getCarrierId () {
        return carrierId;
    }

    public void setCarrierId (String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName () {
        return carrierName;
    }

    public void setCarrierName (String carrierName) {
        this.carrierName = carrierName;
    }

    public String getInspectStatus () {
        return inspectStatus;
    }

    public void setInspectStatus (String inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public String getInspectRemark () {
        return inspectRemark;
    }

    public void setInspectRemark (String inspectRemark) {
        this.inspectRemark = inspectRemark;
    }

    @Override
    public Date getGmtCreate () {
        return gmtCreate;
    }

    @Override
    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getCreateDate () {
        return createDate;
    }

    public void setCreateDate (Date createDate) {
        this.createDate = createDate;
    }

    public Date getExpectDate () {
        return expectDate;
    }

    public void setExpectDate (Date expectDate) {
        this.expectDate = expectDate;
    }

    public Long getStoreAreaId () {
        return storeAreaId;
    }

    public void setStoreAreaId (Long storeAreaId) {
        this.storeAreaId = storeAreaId;
    }

    public String getStoreAreaName () {
        return storeAreaName;
    }

    public void setStoreAreaName (String storeAreaName) {
        this.storeAreaName = storeAreaName;
    }

    public Long getLocationId () {
        return locationId;
    }

    public void setLocationId (Long locationId) {
        this.locationId = locationId;
    }

    public String getLocationCode () {
        return locationCode;
    }

    public void setLocationCode (String locationCode) {
        this.locationCode = locationCode;
    }

    public Date getInboundModified () {
        return inboundModified;
    }

    public void setInboundModified (Date inboundModified) {
        this.inboundModified = inboundModified;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("InboundDTO{");
        sb.append("noticeNo='").append(noticeNo).append('\'');
        sb.append(", driverName='").append(driverName).append('\'');
        sb.append(", driverPhone='").append(driverPhone).append('\'');
        sb.append(", storeHouseId=").append(storeHouseId);
        sb.append(", storeHouseName='").append(storeHouseName).append('\'');
        sb.append(", carrierId='").append(carrierId).append('\'');
        sb.append(", carrierName='").append(carrierName).append('\'');
        sb.append(", inspectStatus='").append(inspectStatus).append('\'');
        sb.append(", inspectRemark='").append(inspectRemark).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", createDate=").append(createDate);
        sb.append(", expectDate=").append(expectDate);
        sb.append(", storeAreaId=").append(storeAreaId);
        sb.append(", storeAreaName='").append(storeAreaName).append('\'');
        sb.append(", locationId=").append(locationId);
        sb.append(", locationCode='").append(locationCode).append('\'');
        sb.append(", inboundModified=").append(inboundModified);
        sb.append('}');
        return sb.toString();
    }
}
