package com.zhiche.wms.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;


@Configuration
@ConfigurationProperties(prefix = "wms.properties")
@Validated
public class MyConfigurationProperties {
    @NotNull
    private String fetchInboundFromTMSUrl;
    @NotNull
    private Integer socketTimeOut;
    @NotNull
    private String encodeKey;

    private String inboundWarehouse;

    private String outboundWarehouse;

    private String integrationhost;

    private String uaahost;

    private String otmhost;

    private String shipDataForBMS;

    private String cancleShipDataForBMS;

    private String logRedisIsTest;

    private String hcmAppid;

    private String secret;

    private String hcmUrl;

    private String getAccessTokenUrl;

    private String addUserUrl;
    private String queryUserUrl;

    public String getQueryUserUrl() {
        return queryUserUrl;
    }

    public void setQueryUserUrl(String queryUserUrl) {
        this.queryUserUrl = queryUserUrl;
    }

    public String getAddUserUrl() {
        return addUserUrl;
    }

    public void setAddUserUrl(String addUserUrl) {
        this.addUserUrl = addUserUrl;
    }

    public String getLogRedisIsTest() {
        return logRedisIsTest;
    }

    public void setLogRedisIsTest(String logRedisIsTest) {
        this.logRedisIsTest = logRedisIsTest;
    }

    public String getCancleShipDataForBMS () {
        return cancleShipDataForBMS;
    }

    public void setCancleShipDataForBMS (String cancleShipDataForBMS) {
        this.cancleShipDataForBMS = cancleShipDataForBMS;
    }

    public String getShipDataForBMS() {
        return shipDataForBMS;
    }

    public void setShipDataForBMS(String shipDataForBMS) {
        this.shipDataForBMS = shipDataForBMS;
    }

    public String getUaahost() {
        return uaahost;
    }

    public void setUaahost(String uaahost) {
        this.uaahost = uaahost;
    }

    public String getInboundWarehouse() {
        return inboundWarehouse;
    }

    public void setInboundWarehouse(String inboundWarehouse) {
        this.inboundWarehouse = inboundWarehouse;
    }

    public String getOutboundWarehouse() {
        return outboundWarehouse;
    }

    public void setOutboundWarehouse(String outboundWarehouse) {
        this.outboundWarehouse = outboundWarehouse;
    }

    public String getIntegrationhost() {
        return integrationhost;
    }

    public void setIntegrationhost(String integrationhost) {
        this.integrationhost = integrationhost;
    }

    public String getEncodeKey() {
        return encodeKey;
    }

    public void setEncodeKey(String encodeKey) {
        this.encodeKey = encodeKey;
    }

    public String getFetchInboundFromTMSUrl() {
        return fetchInboundFromTMSUrl;
    }

    public void setFetchInboundFromTMSUrl(String fetchInboundFromTMSUrl) {
        this.fetchInboundFromTMSUrl = fetchInboundFromTMSUrl;
    }

    public Integer getSocketTimeOut() {
        return socketTimeOut;
    }

    public void setSocketTimeOut(Integer socketTimeOut) {
        this.socketTimeOut = socketTimeOut;
    }

    public String getOtmhost() {
        return otmhost;
    }

    public void setOtmhost(String otmhost) {
        this.otmhost = otmhost;
    }

    public String getHcmAppid() {
        return hcmAppid;
    }

    public void setHcmAppid(String hcmAppid) {
        this.hcmAppid = hcmAppid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getHcmUrl() {
        return hcmUrl;
    }

    public void setHcmUrl(String hcmUrl) {
        this.hcmUrl = hcmUrl;
    }

    public String getGetAccessTokenUrl() {
        return getAccessTokenUrl;
    }

    public void setGetAccessTokenUrl(String getAccessTokenUrl) {
        this.getAccessTokenUrl = getAccessTokenUrl;
    }
}
