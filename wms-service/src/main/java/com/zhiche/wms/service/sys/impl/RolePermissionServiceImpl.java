package com.zhiche.wms.service.sys.impl;

import com.zhiche.wms.domain.model.sys.RolePermission;
import com.zhiche.wms.domain.mapper.sys.RolePermissionMapper;
import com.zhiche.wms.service.sys.IRolePermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限关联表 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements IRolePermissionService {

}
