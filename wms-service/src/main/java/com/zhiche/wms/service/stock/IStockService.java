package com.zhiche.wms.service.stock;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.log.CancelStoreLog;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.domain.model.stock.Stock;
import com.zhiche.wms.domain.model.stock.StockProperty;
import com.zhiche.wms.dto.outbound.StockSkuToPrepareDTO;
import com.zhiche.wms.dto.stock.FactoryPrepareStockDTO;
import com.zhiche.wms.dto.stock.StockDTO;
import com.zhiche.wms.dto.stock.StockQty;
import com.zhiche.wms.dto.stock.StockWithSkuDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库存 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface IStockService extends IService<Stock> {

    /**
     * 得到唯一库存
     *
     * @param stockProperty 库存属性
     */
    Stock getUniqueStock(StockProperty stockProperty);

    /**
     * 增加库存
     *
     * @param stockProperty  库存属性
     * @param businessType   业务类型(10:移位,20:期初库存,30:库存调整)
     * @param relationLineId 相关数据ID
     */
    Stock addStock(StockProperty stockProperty, String businessType, Long relationLineId);

    /**
     * 减少库存
     *
     * @param stockProperty  库存属性
     * @param businessType   业务类型(10:移位,15:撤销移位,20:期初库存,25:撤销移位,30:库存调整,35:撤销移位,)
     * @param relationLineId 相关数据ID
     */
    Stock minusStock(StockProperty stockProperty, String businessType, Long relationLineId);

    /**
     * 根据仓库属性得到已有库存
     */
    List<Stock> queryStockList(StockProperty stockProperty);

    /**
     * 查询出库数据
     */
    Page<StockDTO> queryPageStock(Page<StockDTO> page);

    /**
     * 通过二维码或车架号查询出库数据
     */
    Page<StockDTO> queryPageStockByQrCode(Page<StockDTO> stockDTOPage);

    List<StockSkuToPrepareDTO> selectStockToPrepareByParams(OutboundNoticeLine v, Long storeHouseId);

    /**
     * 批量锁定库存
     * @param stockIds
     */
    void lockStockBatch(Map<String, Object> stockIds);

    /**
     * 批量解锁库存
     * @param stockIds
     */
    void unlockStockBatch(Map<String, Object> stockIds);

    /**
     * 更新库存库位信息
     */
    void updateStockLocation(Map<String, String> condition);

    List<StockWithSkuDTO> selectStockWithSkuByParam(HashMap<String, Object> params);

    /**
     * 导出库存
     */
    List<StockDTO> exportStockData(Map<String, String> condition);

    /**
     * 备料退库记录查询
     * @param page 入参
     * @return 返回数据
     */
    Page<CancelStoreLog> queryCancelStoreLog (Page<CancelStoreLog> page);

    /**
     * 司机领取退库任务
     * @param condition
     */
    void driverReceiveTask (Map<String, String> condition);

    /**
     * 退库明细查询
     * @param houseId 仓库id
     * @param key 扫码编码
     * @param visitType 查询方式
     * @return
     */
    CancelStoreLog queryCancelStoreDetail (Long houseId, String key, String visitType);

    List<StockSkuToPrepareDTO> selectStockToKeyPrepareByParams (OutboundNoticeLine noticeLine, Long storeHouseId);

    /**
     * 查询仓库的实际库存
     * @param condition
     * @return
     */
    List<Map<String, String>> queryActualStock (Map<String, String> condition);


    void cleanStock (Map<String, String> param);

    /**
     * 查询库存量大于1的数据
     * @param page 分页/入参
     * @return 返回数据
     */
    Page<StockQty> queryStockQty (Page<StockQty> page);

    /**
     * 库存明细查询
     */
    List<StockQty> queryStockQtyDetail (Map<String, Object> param);

    void cleanStockData (Long stockId,String message);

    Page<FactoryPrepareStockDTO> queryFactoryStock (Page<FactoryPrepareStockDTO> page);

    void updateFactoryStockLocation (Map<String, String> condition);
}
