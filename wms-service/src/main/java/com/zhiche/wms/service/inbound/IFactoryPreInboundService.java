package com.zhiche.wms.service.inbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.inbound.FactoryPrepareInbound;
import com.zhiche.wms.dto.inbound.FPInboundDTO;
import com.zhiche.wms.dto.outbound.FPOutboundDTO;
import com.zhiche.wms.service.dto.FactoryPrepareInboundVo;

import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description: 工厂备料接口
 * @Date: Create in 11:06 2019/11/20
 */
public interface IFactoryPreInboundService extends IService<FactoryPrepareInbound> {

    List<String> prepareInbound (FactoryPrepareInboundVo factoryPrepareInboundVo);

    List<String> outboundPrepare (FactoryPrepareInboundVo param);

    Page<FPOutboundDTO> queryFactoryOutbound (Page<FPOutboundDTO> page);

    Page<FPInboundDTO> queryFactoryInbound (Page<FPInboundDTO> page);

    /**
     * 查询无单的必办事项
     */
    Map<String, Object> queryMustMatter (Map<String, String> param);
}
