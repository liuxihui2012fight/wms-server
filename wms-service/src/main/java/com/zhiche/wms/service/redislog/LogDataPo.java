package com.zhiche.wms.service.redislog;

/**
 * 指令日志相关PO类
 *
 * @author huangzhigang
 */
public class LogDataPo {

    /**
     * 事件来源系统
     */
    private String sourceSystem;

    /**
     * 目标系统
     */
    private String targetSystem;

    /**
     * 目标API标识
     */
    private String funcIdx;

    /**
     * 业务事件key/数据ID
     */
    private String busEventKey;

    /**
     * 业务事件时序标识
     */
    private String busEventSequence;

    /**
     * 请求参数/七牛数据标识
     */
    private String reqParam;

    /**
     * 状态（1：成功，-1：忽略，0：待处理）
     */
    private Integer isSuc;

    /**
     * 错误信息
     */
    private String errCont;

    /**
     * 自动处理次数(累加)
     */
    private Integer execCnt;

    /**
     * 是否已发送邮件（1：已发送，0：未发送）
     */
    private Integer isSendEmail;

    /**
     * 数据阶段标识（1：下发；2：接收；3：处理）
     */
    private Integer dataStatus;

    public LogDataPo() {
        super();
    }

    public LogDataPo(String sourceSystem, String targetSystem, String funcIdx, String busEventKey, String busEventSequence, String reqParam, Integer isSuc, String errCont, Integer execCnt, Integer isSendEmail, Integer dataStatus) {
        this.sourceSystem = sourceSystem;
        this.targetSystem = targetSystem;
        this.funcIdx = funcIdx;
        this.busEventKey = busEventKey;
        this.busEventSequence = busEventSequence;
        this.reqParam = reqParam;
        this.isSuc = isSuc;
        this.errCont = errCont;
        this.execCnt = execCnt;
        this.isSendEmail = isSendEmail;
        this.dataStatus = dataStatus;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getTargetSystem() {
        return targetSystem;
    }

    public void setTargetSystem(String targetSystem) {
        this.targetSystem = targetSystem;
    }

    public String getFuncIdx() {
        return funcIdx;
    }

    public void setFuncIdx(String funcIdx) {
        this.funcIdx = funcIdx;
    }

    public String getReqParam() {
        return reqParam;
    }

    public void setReqParam(String reqParam) {
        this.reqParam = reqParam;
    }

    public Integer getIsSuc() {
        return isSuc;
    }

    public void setIsSuc(Integer isSuc) {
        this.isSuc = isSuc;
    }

    public Integer getExecCnt() {
        return execCnt;
    }

    public void setExecCnt(Integer execCnt) {
        this.execCnt = execCnt;
    }

    public Integer getIsSendEmail() {
        return isSendEmail;
    }

    public void setIsSendEmail(Integer isSendEmail) {
        this.isSendEmail = isSendEmail;
    }

    public String getErrCont() {
        return errCont;
    }

    public void setErrCont(String errCont) {
        this.errCont = errCont;
    }

    public String getBusEventKey() {
        return busEventKey;
    }

    public void setBusEventKey(String busEventKey) {
        this.busEventKey = busEventKey;
    }

    public String getBusEventSequence() {
        return busEventSequence;
    }

    public void setBusEventSequence(String busEventSequence) {
        this.busEventSequence = busEventSequence;
    }

    public Integer getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(Integer dataStatus) {
        this.dataStatus = dataStatus;
    }
}
