package com.zhiche.wms.service.outbound;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.dto.outbound.OutboundNoticeDTO;
import com.zhiche.wms.dto.outbound.OutboundShipQrCodeResultDTO;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 出库通知明细 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IOutboundNoticeLineService extends IService<OutboundNoticeLine> {

    /**
     * 根据头ID更新明细的出库状态
     */
    boolean updateOutboundStatus (Long headerId, Long noticeLineId);

    /**
     * 模糊查询入库单信息
     */
    Page<OutboundNoticeDTO> selectOutboundsPage(String lotNo, Long houseIds, Integer size, Integer current);

    /**
     * 查询单个信息结果
     */
    OutboundNoticeDTO selectOutbound(Long queryParam, Long houseId);

    OutboundNoticeDTO selectInboundByQrCode(String queryParam, Long houseId);

    List<OutboundNoticeDTO> selectOutboundByHeadId(Long headId, Long houseIds);

    List<OutboundShipQrCodeResultDTO> selectQrCodeInfoByParams(HashMap<String, Object> params);

    List<OutboundNoticeLine> pageOutboundNotice(Page<OutboundNoticeLine> page, EntityWrapper<OutboundNoticeLine> outEW);

    int selectCountWithHeader(EntityWrapper<OutboundNoticeLine> nlEW);

    OutboundNoticeLine selectOutboundByHeadId (String noticeLineId);
}
