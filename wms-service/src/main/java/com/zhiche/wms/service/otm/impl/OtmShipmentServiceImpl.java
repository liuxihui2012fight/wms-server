package com.zhiche.wms.service.otm.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.configuration.MyConfigurationProperties;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceAddrEnum;
import com.zhiche.wms.core.supports.enums.InterfaceEventEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.HttpClientUtil;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.mapper.inbound.InboundNoticeHeaderMapper;
import com.zhiche.wms.domain.mapper.inbound.InboundNoticeLineMapper;
import com.zhiche.wms.domain.mapper.log.ItfExplogLineMapper;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.domain.mapper.otm.OtmShipmentMapper;
import com.zhiche.wms.domain.mapper.otm.ShipmentRecordMapper;
import com.zhiche.wms.domain.mapper.outbound.OutboundNoticeHeaderMapper;
import com.zhiche.wms.domain.mapper.outbound.OutboundNoticeLineMapper;
import com.zhiche.wms.domain.mapper.outbound.OutboundPrepareHeaderMapper;
import com.zhiche.wms.domain.mapper.outbound.OutboundPrepareLineMapper;
import com.zhiche.wms.domain.model.inbound.InboundNoticeHeader;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;
import com.zhiche.wms.domain.model.log.ItfExplogLine;
import com.zhiche.wms.domain.model.opbaas.StatusLog;
import com.zhiche.wms.domain.model.otm.*;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareHeader;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareLine;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentRecordDTO;
import com.zhiche.wms.dto.shipmentDto.PushShipmentToOtmDto;
import com.zhiche.wms.dto.shipmentDto.PushShipmentToTmsDto;
import com.zhiche.wms.service.common.IntegrationService;
import com.zhiche.wms.service.constant.PutAwayType;
import com.zhiche.wms.service.constant.SourceSystem;
import com.zhiche.wms.service.dto.OTMEvent;
import com.zhiche.wms.service.dto.ShipParamDTO;
import com.zhiche.wms.service.inbound.IInboundPutawayHeaderService;
import com.zhiche.wms.service.log.IItfExplogLineService;
import com.zhiche.wms.service.opbaas.IStatusLogService;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import com.zhiche.wms.service.otm.IOtmShipmentService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import com.zhiche.wms.service.outbound.IOutboundShipLineService;
import com.zhiche.wms.service.sys.IUserService;
import com.zhiche.wms.service.utils.CommonMethod;
import com.zhiche.wms.service.utils.WmsMethodUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * <p>
 * 调度指令 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
@Service
public class OtmShipmentServiceImpl extends ServiceImpl<OtmShipmentMapper, OtmShipment> implements IOtmShipmentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OtmShipmentServiceImpl.class);
    @Autowired
    private SnowFlakeId snowFlakeId;

    @Autowired
    private IOtmOrderReleaseService otmOrderReleaseService;

    @Autowired
    private ShipmentRecordMapper shipmentRecordMapper;
    @Autowired
    private OtmOrderReleaseMapper otmOrderReleaseMapper;
    @Autowired
    private IItfExplogLineService logLineService;
    @Autowired
    private MyConfigurationProperties properties;
    @Autowired
    private ItfExplogLineMapper itfExplogLineMapper;
    @Autowired
    private IUserService userService;
    @Autowired
    private OtmShipmentMapper otmShipmentMapper;
    @Autowired
    private IStatusLogService statusLogService;
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    private IOutboundNoticeLineService outboundNoticeLineService;
    @Autowired
    private IOutboundShipLineService outboundShipLineService;
    @Autowired
    private IOutboundPrepareHeaderService prepareHeaderService;
    @Autowired
    private IInboundPutawayHeaderService iInboundPutawayHeaderService;
    @Autowired
    private InboundNoticeLineMapper inboundNoticeLineMapper;
    @Autowired
    private IOtmShipmentService otmShipmentService;
    @Autowired
    private OutboundNoticeHeaderMapper outboundNoticeHeaderMapper;
    @Autowired
    private OutboundNoticeLineMapper outboundNoticeLineMapper;
    @Autowired
    private OutboundPrepareHeaderMapper outboundPrepareHeaderMapper;
    @Autowired
    private OutboundPrepareLineMapper outboundPrepareLineMapper;
    @Autowired
    private InboundNoticeHeaderMapper inboundNoticeHeaderMapper;
    @Override
    @Transactional
    public boolean insertShipment(OtmShipment otmShipment) {
        otmShipment.setId(snowFlakeId.nextId());
        for(OtmOrderRelease otmOrderRelease : otmShipment.getOtmOrderReleaseList()){
            otmOrderRelease.setId(snowFlakeId.nextId());
        }
        if(CollectionUtils.isEmpty(otmShipment.getOtmOrderReleaseList())){
            return false;
        }
        return otmOrderReleaseService.insertBatch(otmShipment.getOtmOrderReleaseList()) && insert(otmShipment);
    }

    /**
     * 模糊查询指令列表  -- 关联发车配置点
     *
     */
    @Override
    public List<ShipmentForShipDTO> queryShipmentReleaseList(Page<ShipmentForShipDTO> page, HashMap<String, Object> param, EntityWrapper<ShipmentForShipDTO> ew) {
        return baseMapper.queryShipmentReleaseList(page,param, ew);
    }

    /**
     * 装车发运-点击获取指令详情
     */
    @Override
    public List<ShipmentForShipDTO > getShipDetail(EntityWrapper<ShipmentForShipDTO > ew) {
        return baseMapper.getShipDetail(ew);
    }

    @Override
    public int countShipmentReleaseList(HashMap<String, Object> params, EntityWrapper<ShipmentForShipDTO> ew) {
        return baseMapper.countShipmentReleaseList(params, ew);
    }

    @Override
    public List<ShipmentForShipDTO> queryShipmentReleases(EntityWrapper<ShipmentForShipDTO> dataEW) {
        return baseMapper.queryShipmentReleases(dataEW);
    }

    @Override
    public List<ShipmentRecordDTO> queryShipRecordList (Page<ShipmentRecordDTO> page, EntityWrapper<ShipmentRecordDTO> ew) {
        return shipmentRecordMapper.queryShipRecordList(page,ew);
    }

    @Override
    public List<OtmOrderRelease> queryShipmentStatus (List<Map<String, String>> shipmentGid) {
        if (CollectionUtils.isEmpty(shipmentGid)) {
            throw new BaseException("查询参数为空，请传递指令号和运单号");
        }

        StringBuffer shipmentGids = new StringBuffer();
        StringBuffer releaseGids = new StringBuffer();
        for (Map<String, String> param : shipmentGid) {
            shipmentGids.append(param.get("shipmentGid")).append(",");
            releaseGids.append(param.get("releaseGid")).append(",");
        }

        Wrapper<OtmOrderRelease> ew = new EntityWrapper<>();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode());
        ew.in("shipment_gid", Arrays.asList(shipmentGids.toString().split(",")));
        ew.in("release_gid", Arrays.asList(releaseGids.toString().split(","))).orderBy("gmt_modified", false);
        List<OtmOrderRelease> otmOrderRelease = otmOrderReleaseMapper.selectList(ew);

        if(CollectionUtils.isNotEmpty(otmOrderRelease)){
            for(OtmOrderRelease orderRelease : otmOrderRelease){
                //if(!orderRelease.getStatus().equals(TableStatusEnum.STATUS_BS_DISPATCH.getCode())){
                if(Objects.nonNull(orderRelease.getShipDate()) && StringUtils.isNotEmpty(String.valueOf(orderRelease.getShipDate()))){
                    orderRelease.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                }
            }
        }
        LOGGER.info("人送/运力根据指令号和运单号查询结果为{}", otmOrderRelease);
        return otmOrderRelease;
    }

    @Override
    public List<ShipmentRecordDTO> queryNewChannelReleaseList (Page<ShipmentRecordDTO> page, EntityWrapper<ShipmentRecordDTO> ew) {
        return shipmentRecordMapper.queryNewChannelReleaseList(page,ew);
    }

    @Override
    public Page<OtmReleaseOtmDTO> queryPushShipmentOtm (Page<OtmReleaseOtmDTO> page) {
        EntityWrapper<OtmReleaseOtmDTO> oorEW = new EntityWrapper<>();
        oorEW.isNotNull("shipDate");
        oorEW.ne("oorStatus",TableStatusEnum.STATUS_50.getCode());
        Map<String, Object> condition = page.getCondition();
        if (Objects.nonNull(condition)) {
            if (Objects.nonNull(condition.get("shipmentGid")) && StringUtils.isNotEmpty((String) condition.get("shipmentGid"))) {
                oorEW.like("shipmentGid", (String) condition.get("shipmentGid"));
            }
            if (Objects.nonNull(condition.get("releaseGid")) && StringUtils.isNotEmpty((String) condition.get("releaseGid"))) {
                oorEW.like("releaseGid", (String) condition.get("releaseGid"));
            }
            if (Objects.nonNull(condition.get("originLocationName")) && StringUtils.isNotEmpty((String) condition.get("originLocationName"))) {
                oorEW.like("originLocationName", (String) condition.get("originLocationName"));
            }
            if (Objects.nonNull(condition.get("vin")) && StringUtils.isNotEmpty((String) condition.get("vin"))) {
                List<String> vins = Arrays.asList(CommonMethod.setVins((String) condition.get("vin")));
                oorEW.in("vin", vins);
            }
            if (Objects.nonNull(condition.get("startTime")) && StringUtils.isNotEmpty((String) condition.get("startTime"))) {
                oorEW.gt("gmtModified", (String) condition.get("startTime"));
            }
            if (Objects.nonNull(condition.get("endTime")) && StringUtils.isNotEmpty((String) condition.get("endTime"))) {
                oorEW.lt("gmtModified", (String) condition.get("endTime"));
            }
        }
        oorEW.orderBy("id", false);
        oorEW.orderBy("gmtModified", false);
        List<OtmReleaseOtmDTO> orderReleases = otmOrderReleaseMapper.queryPushShipmentOtm(page, oorEW);
        LOGGER.info("补发发运数据查询结果为：{}",orderReleases);
        return page.setRecords(orderReleases);
    }

    @Override
    public void pushShipmentOtm (Map<String, String> param) {
        if (Objects.isNull(param)) {
            throw new BaseException("入参不能为空");
        }
        if (StringUtils.isEmpty(param.get("keys"))) {
            throw new BaseException("入参【keys】不能为空");
        }
        if (StringUtils.isEmpty(param.get("pushType"))) {
            throw new BaseException("入参【pushType】不能为空");
        }

        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("未查询到登录用户");
        }

        EntityWrapper<OtmOrderRelease> ew = new EntityWrapper<>();
        List<String> oorIds = Arrays.asList(param.get("keys").split(","));
        ew.in("id", oorIds);
        List<OtmOrderRelease> otmOrderReleases = otmOrderReleaseMapper.selectList(ew);
        LOGGER.info("wms推送发运数据给tms的查询结果为{}", otmOrderReleases);
        String pushUrl = this.setPushUrl(param.get("pushType"));
        LOGGER.info("需要推送的系统url为{}",pushUrl);
        for (OtmOrderRelease oor : otmOrderReleases) {
            EntityWrapper<ItfExplogLine> logEW = new EntityWrapper<>();
            logEW.eq("relation_id", oor.getId());
            logEW.eq("export_type", InterfaceEventEnum.BS_OP_DELIVERY.getCode());
            List<ItfExplogLine> lines = logLineService.selectList(logEW);
            if (CollectionUtils.isNotEmpty(lines)) {
                ItfExplogLine explogLine = lines.get(0);
                try {
                    LOGGER.info("wms推送发运releaseGid:{},shipmentGid:{}", oor.getReleaseGid(), oor.getShipmentGid());
                    //推送系统数据
                    String requestId = WmsMethodUtils.pushSystemData(pushUrl, explogLine.getDataContent());
                    LOGGER.info("补发发运数据推送返回数据 resJson《requestId》：{}", requestId);
                    if(StringUtils.isNotEmpty(requestId)){
                        ItfExplogLine itfParam = new ItfExplogLine();
                        itfParam.setRequestId(requestId);
                        itfParam.setUserCreate(loginUser.getName());
                        itfParam.setId(explogLine.getId());
                        logLineService.updateById(itfParam);
                    }
                } catch (Exception e) {
                    LOGGER.error("wms推送发运失败-->{}", e);
                }
            } else {
                //如果推送表为空，就新增发运数据到status_log和itf_explog_line表
                insertPushLog(oor,pushUrl);
            }
        }
    }

    /**
     * 推送日志表为空，新增
     *
     * @param oor 运单表信息
     * @param pushUrl
     */
    private void insertPushLog (OtmOrderRelease oor, String pushUrl) {
        //1、指令发运记录表新增数据
        ArrayList<StatusLog> insertLogs = Lists.newArrayList();
        //2、是否标记了异常不可发运
        oor.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
        //3、运单发运记录表新增数据
        this.insertStatusLog(oor.getId(), TableStatusEnum.STATUS_10.getCode(),"PC补发数据至Otm");

        //4、推送otm
        this.insertExplogToOtm(oor,pushUrl,"PC补发数据至Otm");

    }

    private void insertExplogToOtm (OtmOrderRelease oor, String pushUrl, String shipMessage) {
        //查询是否已经推送过发运，如果已经推送过，不需要再次推送
        EntityWrapper<ItfExplogLine> wifEw = new EntityWrapper<>();
        wifEw.eq("relation_id", oor.getId());
        wifEw.eq("export_type", InterfaceEventEnum.BS_OP_DELIVERY.getCode());
        List<ItfExplogLine> itfExplogLines = itfExplogLineMapper.selectList(wifEw);
        if (CollectionUtils.isEmpty(itfExplogLines)) {
            String loginUserName = null;
            try {
                User loginUser = userService.getLoginUser();
                loginUserName = loginUser.getName();
            } catch (Exception e) {
                loginUserName = "admin";
            }
            OTMEvent otmEvent = integrationService.getOtmEvent(String.valueOf(oor.getId()),
                    oor.getReleaseGid(),
                    InterfaceEventEnum.BS_OP_DELIVERY.getCode(),
                    oor.getShipmentGid(),
                    "已发运回传OTM/BMS");
            otmEvent.setRecdDate(CommonMethod.datetoString(oor.getShipDate()));
            otmEvent.setOccurDate(CommonMethod.datetoString(oor.getShipDate()));

            //*****调整参数--传递更多信息
            ShipParamDTO paramDTO = new ShipParamDTO();
            EntityWrapper<OtmShipment> otmShipmentEntityWrapper = new EntityWrapper<>();
            otmShipmentEntityWrapper.eq("shipment_gid", oor.getShipmentGid());
            otmShipmentEntityWrapper.ne("status", TableStatusEnum.STATUS_50.getCode());
            List<OtmShipment> otmShipment = otmShipmentMapper.selectList(otmShipmentEntityWrapper);
            ShipmentForShipDTO shipDTO = new ShipmentForShipDTO();
            shipDTO.setDriverGid(otmShipment.get(0).getDriverGid());
            shipDTO.setServiceProviderGid(otmShipment.get(0).getServiceProviderGid());
            shipDTO.setPlateNo(otmShipment.get(0).getPlateNo());
            shipDTO.setTrailerNo(otmShipment.get(0).getTrailerNo());
            this.setParamDTO(paramDTO, shipDTO, oor, otmEvent);
            //5、推送otm
            this.pushOTMBms(paramDTO, oor.getReleaseGid(), oor.getShipmentGid(), oor.getId(), pushUrl, shipMessage, loginUserName);
        }
    }

    /**
     * 发运日志
     * @param id
     * @param code
     */
    private void insertStatusLog (Long id, String code,String message) {
        new Thread(() -> {
            StatusLog sl = new StatusLog();
            sl.setTableType(code);
            sl.setTableId(String.valueOf(id));
            sl.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            sl.setStatusName(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            sl.setUserCreate(message);
            statusLogService.insert(sl);
        }).start();
    }

    /**
     * 发运  更新车辆为已出库
     *
     * @param releaseGid 运单号
     */
    private void updateVinOutbound (String releaseGid) {
        ArrayList<String> status = Lists.newArrayList();
        status.add(TableStatusEnum.STATUS_10.getCode());
        status.add(TableStatusEnum.STATUS_20.getCode());
        EntityWrapper<OutboundNoticeLine> nlEW = new EntityWrapper<>();
        nlEW.eq("line_source_key", releaseGid)
                .in("status", status)
                .orderBy("id", false);
        OutboundNoticeLine noticeLine = outboundNoticeLineService.selectOne(nlEW);
        if (noticeLine != null) {
            try {
                //调用方法出库
                outboundShipLineService.shipByNoticeLineId(noticeLine.getId(), PutAwayType.NOTICE_PUTAWAY, SourceSystem.AUTO);
                //调用方法修改备料状态
                prepareHeaderService.updatePrepareFinishByLineId(noticeLine.getId(), "统一数据调整发运");
            } catch (Exception e) {
                LOGGER.error("自动调整出库失败 车架号:{}...运单号:{}", noticeLine.getLotNo1(), noticeLine.getLineSourceKey());
            }
        }
    }

    private void pushOTMBms (ShipParamDTO paramDTO, String releaseGid, String shipmentGid, Long oorId, String pushUrl, String shipMessage, String name) {
        try {
            String paramJSON = JSONObject.toJSONString(paramDTO);
            LOGGER.info("wms推送TMS发运  releaseGid:{},shipmentGid:{},paramJSON{}", releaseGid, shipmentGid, paramJSON);
            //推送数据
            String requestId = WmsMethodUtils.pushSystemData(pushUrl,paramJSON);
            LOGGER.info("wms推送TMS发运结果 -->{}", requestId);
            integrationService.insertExportLogNew(String.valueOf(oorId),
                    paramDTO,
                    requestId,
                    shipMessage,
                    InterfaceEventEnum.BS_OP_DELIVERY.getCode(), name,pushUrl);
        } catch (Exception e) {
            LOGGER.error("wms推送TMS发运失败-->{}", e);
        }
    }

    /**
     * 组装推送参数
     * @param paramDTO
     * @param shipDTO
     * @param oor
     * @param otmEvent
     */
    private void setParamDTO (ShipParamDTO paramDTO, ShipmentForShipDTO shipDTO, OtmOrderRelease oor, OTMEvent otmEvent) {
        paramDTO.setPlateNo(shipDTO.getPlateNo());
        paramDTO.setTrailerNo(shipDTO.getTrailerNo());
        paramDTO.setVehicleName(oor.getStanVehicleType());
        paramDTO.setVin(oor.getVin());
        paramDTO.setOriginProvince(oor.getOriginLocationProvince());
        paramDTO.setOriginCity(oor.getOriginLocationCity());
        paramDTO.setOriginCounty(oor.getOriginLocationCounty());
        paramDTO.setOriginAddr(oor.getOriginLocationAddress());
        paramDTO.setDestProvince(oor.getDestLocationProvince());
        paramDTO.setDestCity(oor.getDestLocationCity());
        paramDTO.setDestCounty(oor.getDestLocationCounty());
        paramDTO.setDestAddr(oor.getDestLocationAddress());
        paramDTO.setDriverGid(shipDTO.getDriverGid());
        paramDTO.setProviderGid(shipDTO.getServiceProviderGid());
        if(null == oor.getShipDate()){
            paramDTO.setShipTime(String.valueOf(new Date().getTime()));
        }else {
            paramDTO.setShipTime(String.valueOf(oor.getShipDate().getTime()));
        }
        paramDTO.setOriginCode(oor.getOriginLocationGid());
        paramDTO.setDestCode(oor.getDestLocationGid());
        paramDTO.setExportKey(otmEvent.getExportKey());
        paramDTO.setCallBackUrl(otmEvent.getCallBackUrl());
        paramDTO.setEventType(otmEvent.getEventType());
        paramDTO.setOccurDate(otmEvent.getOccurDate());
        paramDTO.setRecdDate(otmEvent.getRecdDate());
        paramDTO.setSort(otmEvent.getSort());
        paramDTO.setDescribe(otmEvent.getDescribe());
        paramDTO.setOrderReleaseId(otmEvent.getOrderReleaseId());
        paramDTO.setShipmentId(otmEvent.getShipmentId());
        paramDTO.setQrCode(otmEvent.getQrCode());
        paramDTO.setCusOrderNo(oor.getCusOrderNo());
        paramDTO.setCusWaybill(oor.getCusWaybillNo());
    }

    @Override
    public List<String> queryShipmentInfo (List<String> param) {
        List<String> shipmentList = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(param)){
            EntityWrapper<OtmShipment> shipmentEntityWrapper = new EntityWrapper<>();
            shipmentEntityWrapper.in("shipment_gid",param);
            List<OtmShipment> otmShipments = baseMapper.selectList(shipmentEntityWrapper);
            for (OtmShipment otmShipment:otmShipments){
                shipmentList.add(otmShipment.getShipmentGid());
            }
        }
        return shipmentList;
    }

    @Override
    public List<ShipmentInfo> getShipOrderItemCount (List<ShipmentInfo> page) {
        if(CollectionUtils.isNotEmpty(page)){
            for (ShipmentInfo shipmentInfo : page){
                EntityWrapper<OtmOrderRelease> ew = new EntityWrapper<>();
                ew.eq("shipment_gid",shipmentInfo.getShipmwnt_xid());
                ew.ne("status",TableStatusEnum.STATUS_50.getCode());
                int count = otmOrderReleaseMapper.selectCount(ew);
                shipmentInfo.setVehicleQty(count);
            }
        }
        return page;
    }

    @Override
    public void otmShipDateToWms (List<OtmUpdateShipWmsDate> param) {
        //1、查询指令是否存在wms
        if (CollectionUtils.isNotEmpty(param)) {
            for (OtmUpdateShipWmsDate otmUpdateShipWmsDate : param) {
                if (!judgeShipDataParam(otmUpdateShipWmsDate)) {
                    continue;
                }
                EntityWrapper<OtmOrderRelease> releaseEw = new EntityWrapper<>();

                if (StringUtils.isNotBlank(otmUpdateShipWmsDate.getShipmengGid())) {
                    String shipmentGid = otmUpdateShipWmsDate.getShipmengGid();
                    if (otmUpdateShipWmsDate.getShipmengGid().contains("ULC/ZC.")) {
                        shipmentGid = otmUpdateShipWmsDate.getShipmengGid().substring("ULC/ZC.".length());
                    }
                    releaseEw.eq("shipment_gid", shipmentGid);
                }

                if (StringUtils.isNotBlank(otmUpdateShipWmsDate.getReleaseGid())) {
                    String releaseGid = otmUpdateShipWmsDate.getReleaseGid();
                    if (otmUpdateShipWmsDate.getReleaseGid().contains("ULC/ZC.")) {
                        releaseGid = otmUpdateShipWmsDate.getReleaseGid().substring("ULC/ZC.".length());
                    }
                    releaseEw.eq("release_gid", releaseGid);
                }
                releaseEw.ne("status", TableStatusEnum.STATUS_50.getCode());
                List<OtmOrderRelease> releaseList = otmOrderReleaseMapper.selectList(releaseEw);
                LOGGER.info("查询结果为空：{}",otmUpdateShipWmsDate);
                if (CollectionUtils.isNotEmpty(releaseList)) {
                    for (OtmOrderRelease otmOrderRelease : releaseList) {
                        otmOrderReleaseMapper.updateShipDate(otmOrderRelease.getId(), CommonMethod.stampToDate(String.valueOf(otmUpdateShipWmsDate.getShipDate())));
                        //推送发运时间给bms
                        pushShipDatetoBms(otmOrderRelease);
                    }

                }
            }
        }
    }

    private boolean judgeShipDataParam (OtmUpdateShipWmsDate otmUpdateShipWmsDate) {
        if (StringUtils.isEmpty(String.valueOf(otmUpdateShipWmsDate.getShipDate()))) {
            LOGGER.info("更改发运时间的时间为空");
            return false;
        }

        if (StringUtils.isEmpty(otmUpdateShipWmsDate.getShipmengGid()) && StringUtils.isEmpty(otmUpdateShipWmsDate.getReleaseGid())) {
            LOGGER.info("更改发运时间的指令号或者运单号为空");
            return false;
        }
        return true;
    }

    /**
     * 组装json并推送发运数据给bms
     *
     * @param oor 发运数据
     */
    private void pushShipDatetoBms (OtmOrderRelease oor) {
        LOGGER.info("组装发运数据给bms，数据由otm下发！");
        OTMEvent otmEvent = integrationService.getOtmEvent(String.valueOf(oor.getId()),
                oor.getReleaseGid(),
                InterfaceEventEnum.BS_OP_DELIVERY.getCode(),
                oor.getShipmentGid(),
                "已发运回传OTM/BMS");
        otmEvent.setRecdDate(CommonMethod.datetoString(oor.getShipDate()));
        otmEvent.setOccurDate(CommonMethod.datetoString(oor.getShipDate()));

        //*****调整参数--传递更多信息
        ShipParamDTO paramDTO = new ShipParamDTO();
        //1、根据运单号获取指令信息
        Wrapper<OtmShipment> ew = new EntityWrapper<>();
        ew.eq("shipment_gid", oor.getShipmentGid());
        ew.ne("status", TableStatusEnum.STATUS_50.getCode());
        OtmShipment otmShipments = otmShipmentService.selectOne(ew);
        ShipmentForShipDTO shipDTO = new ShipmentForShipDTO();
        shipDTO.setDriverGid(otmShipments.getDriverGid());
        shipDTO.setServiceProviderGid(otmShipments.getServiceProviderGid());
        shipDTO.setPlateNo(otmShipments.getPlateNo());
        shipDTO.setTrailerNo(otmShipments.getTrailerNo());

        //设置json数据
        this.setParamDTO(paramDTO, shipDTO, oor, otmEvent);
        try {
            String paramJSON = JSONObject.toJSONString(paramDTO);
            LOGGER.info("wms推送BMS发运  releaseGid:{},shipmentGid:{},paramJSON{}", oor.getReleaseGid(), oor.getShipmentGid(), paramJSON);
            String resJson = HttpClientUtil.postJson(properties.getShipDataForBMS(), null, paramJSON, 60000);
            if (StringUtils.isNotEmpty(resJson)) {
                RestfulResponse<String> restfulResponse = JSON.parseObject(resJson, new TypeReference<RestfulResponse<String>>() {
                });
            }
            LOGGER.info("wms推送BMS发运结果 -->{}", resJson);
        } catch (Exception e) {
            LOGGER.error("wms推送BMS发运失败-->{}", e);
        }
    }

    @Override
    public List<ShipmentRecordDTO> queryShipRecordList (EntityWrapper<ShipmentRecordDTO> srdEw) {
        return shipmentRecordMapper.exportShipRecordList(srdEw);
    }

    @Override
    public void pushShipmentLogToOtm () {
        //1、查询为推送给otm的数据,包含已经推送但otm处理失败，未推送日志表为空两种情况
        List<PushShipmentToOtmDto> list = otmOrderReleaseService.queryShipmentLogToOtm();
        LOGGER.info("补推发运数据给otm，总共{}",list.size());
        if (CollectionUtils.isNotEmpty(list)) {
            for (PushShipmentToOtmDto pushShipmentToOtmDto : list) {
                if ("insertPush".equals(pushShipmentToOtmDto.getPushType())) {
                    this.insertPushLog(pushShipmentToOtmDto.getReleaseId());
                } else if ("push".equals(pushShipmentToOtmDto.getPushType())) {
                    this.pushShipmentOtm(pushShipmentToOtmDto.getReleaseId());
                }
            }
        }
    }

    @Override
    public void senShipToOtm (OtmOrderRelease orderRelease, String shipMessage) {
        String pushUrl = this.setPushUrl("OTM");
        this.insertExplogToOtm(orderRelease,pushUrl,shipMessage);
    }

    @Override
    public void pushInboundToOtm (String oorStatus, String logStatus) {
        List<PushShipmentToOtmDto> pushInboundData = otmOrderReleaseService.pushInboundToOtm();
        if (CollectionUtils.isNotEmpty(pushInboundData)) {
            for (PushShipmentToOtmDto pushShipmentToOtmDto : pushInboundData) {
                //查询是否有推送日志
                if ("push".equals(pushShipmentToOtmDto.getPushType())) {
                    String result = HttpClientUtil.postJson(properties.getIntegrationhost() + InterfaceAddrEnum.EVENT_URI.getAddress(),
                            null,
                            pushShipmentToOtmDto.getContent(),
                            properties.getSocketTimeOut());
                    if (!org.springframework.util.StringUtils.isEmpty(result)) {
                        RestfulResponse<String> restfulResponse = JSON.parseObject(result, new TypeReference<RestfulResponse<String>>() {
                        });
                        if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                            LOGGER.info("入库推送成功，返回的requestid为{}", restfulResponse.getData());
                        }
                        ItfExplogLine param = new ItfExplogLine();
                        param.setRequestId(restfulResponse.getData());
                        param.setId(pushShipmentToOtmDto.getExplogId());
                        logLineService.updateById(param);
                    }
                } else if ("insertPush".equals(pushShipmentToOtmDto.getPushType())) {
                    EntityWrapper<InboundNoticeLine> ew = new EntityWrapper<>();
                    InboundNoticeLine inboundNoticeLine = inboundNoticeLineMapper.selectById(pushShipmentToOtmDto.getInboundId());
                    if(null != inboundNoticeLine){
                        iInboundPutawayHeaderService.pushInboundToOtm(pushShipmentToOtmDto.getInboundId(), inboundNoticeLine, "定时补发入库");
                    }
                }
            }
        }
    }

    @Override
    public void toBmsShipDate (List<String> release) {
        if(CollectionUtils.isEmpty(release)){
            LOGGER.info("推送发运时间给tms入参数据为空");
            return;
        }

        //查询发运时间
        StringBuffer sb = new StringBuffer();
        EntityWrapper<PushShipmentToTmsDto> ew = new EntityWrapper<>();
        ew.in("oor.release_gid",release);
        List<PushShipmentToTmsDto> otmOrderReleases = otmOrderReleaseMapper.queryToBmsShipDate(ew);
        LOGGER.info("推送发运时间给tms的数据结果为：{}",otmOrderReleases.size());
        if (CollectionUtils.isNotEmpty(otmOrderReleases)) {
            for (PushShipmentToTmsDto pushShipmentToTmsDto : otmOrderReleases) {
                if (StringUtils.isNotEmpty(pushShipmentToTmsDto.getContent())) {
                    sb.append(pushShipmentToTmsDto.getReleaseGid()).append(",");
                    //推送数据给tms
                    String pushUrl = this.setPushUrl("TMS");
                    //推送系统数据
                    try {
                        String requestId = WmsMethodUtils.pushSystemData(pushUrl, pushShipmentToTmsDto.getContent());
                        LOGGER.info("推送发运时间给tms返回结果为：requestId{}", requestId);
                    } catch (Exception e) {
                        LOGGER.error("推送发运时间给tms异常{}", e.getMessage());
                    }
                }
            }
        }
        LOGGER.info("推送成功数据为：releaseGids:{}", sb.toString());
    }

    @Override
    public void pullOtmShipDate () {
        try {
            //1、获取慧运车数据
            List<OtmCancleShipment> list = otmOrderReleaseMapper.pullOtmShipDate();
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            for (OtmCancleShipment shipment : list) {
                //2、调用otm获取临牌发运时间
                Map<String, String> paramMap = new HashMap<>();
                paramMap.put("iorderids", "ULC/ZC." + shipment.getShipmentGid());
                String paramJSON = JSONObject.toJSONString(paramMap);
                String url = properties.getOtmhost() + InterfaceAddrEnum.UPDATE_SHIP_DATE_OTM.getAddress();
                LOGGER.info(" 获取otm临牌发运时间{}:入参为 ：paramJSON{}", url, paramJSON);
                String resJson = HttpClientUtil.postJson(url, null, paramJSON, 60000);
                LOGGER.info(" 获取otm临牌发运时间结果为 ：resJson{}", resJson);
                if (StringUtils.isNotEmpty(resJson)) {
                    RestfulResponse<List<Map<String, Object>>> restfulResponse = JSON.parseObject(resJson, new TypeReference<RestfulResponse<List<Map<String, Object>>>>() {
                    });
                    if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                        List<Map<String, Object>> responList = restfulResponse.getData();
                        if (CollectionUtils.isNotEmpty(responList)) {
                            this.updateShipStatus(responList.get(0),shipment.getShipmentGid(),shipment.getId());
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("获取临牌发运时间异常!{}", e.getMessage());
        }
    }

    @Override
    public void pullOtmWaterIronShipDate () {
        try {
            //1、获取水铁未发运数据
            List<OtmCancleShipment> queryList = otmOrderReleaseMapper.queryWaterIronData();
            if (CollectionUtils.isEmpty(queryList)) {
                return;
            }
            for (OtmCancleShipment shipment : queryList) {
                //2、调用otm获取水铁发运时间
                Map<String, String> paramMap = new HashMap<>();
                paramMap.put("shipmentGid", "ULC/ZC." + shipment.getShipmentGid());
                paramMap.put("releaseGid", "ULC/ZC." + shipment.getReleaseGid());
                String paramJSON = JSONObject.toJSONString(paramMap);
                String url = properties.getOtmhost() + InterfaceAddrEnum.QUERY_WATERIRON_SHIP_DATE_OTM.getAddress();
                LOGGER.info(" 获取otm水铁发运时间{}:入参为 ：paramJSON{}", url, paramJSON);
                String resJson = HttpClientUtil.postJson(url, null, paramJSON, 60000);
                LOGGER.info(" 获取otm水铁发运时间结果为 ：resJson{}", resJson);
                if (StringUtils.isNotEmpty(resJson)) {
                    Map<String, Object> restfulResponse = JSON.parseObject(resJson);
                    if (Objects.nonNull(restfulResponse) && Integer.valueOf(restfulResponse.get("messageCode").toString()) == 200) {
                       String shipDate = String.valueOf(restfulResponse.get("ship_date"));
                            if (StringUtils.isNotBlank(shipDate)) {
                                Map<String,Object> responMap = new HashMap<>();
                                responMap.put("DTSHIPDATE",shipDate);
                                this.updateShipStatus(responMap,shipment.getShipmentGid(),shipment.getId());
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("获取水铁发运时间异常!{}", e.getMessage());
        }
    }

    @Override
    public void deleteShipmentInfo (String shipmentXid) {
        //指令表更改状态为50
        OtmShipment otmShipment = new OtmShipment();
        otmShipment.setStatus(TableStatusEnum.STATUS_50.getCode());
        otmShipment.setRemarks("入库检测OTM指令状态无效");
        EntityWrapper otmshipmentEw = new EntityWrapper();
        otmshipmentEw.eq("shipment_gid", shipmentXid);
        otmShipmentMapper.update(otmShipment, otmshipmentEw);
        //运单表更改状态为50
        OtmOrderRelease otmOrderRelease = new OtmOrderRelease();
        otmOrderRelease.setStatus(TableStatusEnum.STATUS_50.getCode());
        otmOrderRelease.setRemarks("入库检测OTM指令状态无效");
        EntityWrapper releaseEw = new EntityWrapper();
        releaseEw.eq("shipment_gid", shipmentXid);
        otmOrderReleaseMapper.update(otmOrderRelease, releaseEw);
        //出库通知单状态为50
        EntityWrapper<OutboundNoticeHeader> outnoticeheadEw = new EntityWrapper<>();
        outnoticeheadEw.eq("source_key", shipmentXid);
        outnoticeheadEw.ne("status", TableStatusEnum.STATUS_50.getCode());
        List<OutboundNoticeHeader> outboundNoticeHeaders = outboundNoticeHeaderMapper.selectList(outnoticeheadEw);
        if (com.baomidou.mybatisplus.toolkit.CollectionUtils.isNotEmpty(outboundNoticeHeaders)) {
            OutboundNoticeHeader outboundNoticeHeader = outboundNoticeHeaders.get(0);
            OutboundNoticeHeader updateOHeadStatus = new OutboundNoticeHeader();
            updateOHeadStatus.setId(outboundNoticeHeader.getId());
            updateOHeadStatus.setStatus(TableStatusEnum.STATUS_50.getCode());
            updateOHeadStatus.setRemarks("入库检测OTM指令状态无效");
            outboundNoticeHeaderMapper.updateById(updateOHeadStatus);
            //删除备料信息
            EntityWrapper<OutboundPrepareHeader> outprepareEw = new EntityWrapper<>();
            outprepareEw.eq("notice_id", outboundNoticeHeader.getId());
            outboundPrepareHeaderMapper.delete(outprepareEw);

            EntityWrapper<OutboundNoticeLine> outboundNoticeLineEw = new EntityWrapper<>();
            outboundNoticeLineEw.eq("header_id", outboundNoticeHeader.getId());
            outboundNoticeLineEw.ne("status", TableStatusEnum.STATUS_50.getCode());
            List<OutboundNoticeLine> outboundNoticeLines = outboundNoticeLineMapper.selectList(outboundNoticeLineEw);
            for (OutboundNoticeLine outline : outboundNoticeLines) {
                OutboundNoticeLine updatelineStatus = new OutboundNoticeLine();
                updatelineStatus.setStatus(TableStatusEnum.STATUS_50.getCode());
                updatelineStatus.setRemarks("入库检测OTM指令状态无效");
                updatelineStatus.setId(outline.getId());
                outboundNoticeLineMapper.updateById(updatelineStatus);
                EntityWrapper<OutboundPrepareLine> outprepareLineEw = new EntityWrapper<>();
                outprepareLineEw.eq("notice_id", outline.getId());
                outboundPrepareLineMapper.delete(outprepareLineEw);
            }
        }

        //入库通知单状态为50
        EntityWrapper<InboundNoticeHeader> innoticeheadEw = new EntityWrapper<>();
        innoticeheadEw.eq("source_key", shipmentXid);
        innoticeheadEw.ne("status", TableStatusEnum.STATUS_50.getCode());
        List<InboundNoticeHeader> inboundNoticeHeaders = inboundNoticeHeaderMapper.selectList(innoticeheadEw);
        if (com.baomidou.mybatisplus.toolkit.CollectionUtils.isNotEmpty(inboundNoticeHeaders)) {
            InboundNoticeHeader inboundNoticeHeader = inboundNoticeHeaders.get(0);
            InboundNoticeHeader updateinboundStatus = new InboundNoticeHeader();
            updateinboundStatus.setStatus(TableStatusEnum.STATUS_50.getCode());
            updateinboundStatus.setRemarks("入库检测OTM指令状态无效");
            updateinboundStatus.setId(inboundNoticeHeader.getId());
            inboundNoticeHeaderMapper.updateById(updateinboundStatus);
            //入库通知单
            InboundNoticeLine inboundUpdateParam = new InboundNoticeLine();
            inboundUpdateParam.setRemarks("入库检测OTM指令状态无效");
            inboundUpdateParam.setStatus(TableStatusEnum.STATUS_50.getCode());
            EntityWrapper<InboundNoticeLine> inboundnoticeEw = new EntityWrapper<>();
            inboundnoticeEw.eq("header_id", inboundNoticeHeader.getId());
            inboundNoticeLineMapper.update(inboundUpdateParam, inboundnoticeEw);
        }
    }

    private void updateShipStatus (Map<String, Object> responMap, String shipmentGid, long oorId) {
        if (null != responMap && !responMap.isEmpty()) {
            if (null != responMap.get("DTSHIPDATE")) {
                otmOrderReleaseMapper.updateShipStatus(shipmentGid, CommonMethod.stringToDate1(responMap.get("DTSHIPDATE").toString()),oorId);
                //发运时间推送运力平台
                pushShipDateToBMS(shipmentGid,CommonMethod.stringToDate1(responMap.get("DTSHIPDATE").toString()));
            }
            EntityWrapper<OtmOrderRelease> releaseEw = new EntityWrapper<>();
            releaseEw.eq("id", oorId);
            releaseEw.eq("shipment_gid", shipmentGid);
            releaseEw.ne("status", TableStatusEnum.STATUS_50.getCode());
            releaseEw.isNull("ship_date");
            int releaseCount = otmOrderReleaseMapper.selectCount(releaseEw);
            if (releaseCount == 0) {
                OtmShipment otmShipment = new OtmShipment();
                otmShipment.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                otmShipment.setGmtModified(new Date());
                EntityWrapper<OtmShipment> shipmentEw = new EntityWrapper<>();
                shipmentEw.eq("shipment_gid", shipmentGid);
                shipmentEw.ne("status", TableStatusEnum.STATUS_50.getCode());
                otmShipmentMapper.update(otmShipment, shipmentEw);
            }
        }
    }

    /**
     * 同步OTM人送/慧运车发运时间给TMS
     *
     * @param shipmentGid 指令号
     * @param dtshipdate  发运时间
     */
    private void pushShipDateToTMS (String shipmentGid, Date dtshipdate) {
        try {
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.INSTANCE).build();
            PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            HttpClients.custom().setConnectionManager(connManager);

            //创建自定义的httpclient对象
            CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build();

            //创建post方式请求对象
            HttpPost httpPost = new HttpPost(properties.getIntegrationhost() + InterfaceAddrEnum.SHIPDATE_TO_TMS.getAddress());

            //装填参数
            Map<String, String> param = new HashMap<>();
            param.put("orderCode", shipmentGid);
            param.put("dtshipdate", CommonMethod.getStringDate1(dtshipdate));
            param.put("phone", "");
            List<BasicNameValuePair> nvps = new ArrayList<BasicNameValuePair>();
            if (param != null) {
                for (Map.Entry<String, String> entry : param.entrySet()) {
                    nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
            }
            //设置参数到请求对象中
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
            //执行请求操作，并拿到结果（同步阻塞）
            CloseableHttpResponse response = client.execute(httpPost);
            //获取结果实体
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                //按指定编码转换结果实体为String类型
                String body = EntityUtils.toString(entity, "UTF-8");
                LOGGER.info("人送/慧运车发运时间推送TMS结果{}", body);
            }
            EntityUtils.consume(entity);
            //释放链接
            response.close();
        } catch (Exception e) {
            LOGGER.error("同步OTM人送/慧运车发运时间给TMS异常{}", e.getMessage());
        }
    }

    /**
     * 同步OTM人送/慧运车发运时间给TMS
     *
     * @param shipmentGid 指令号
     * @param dtshipdate  发运时间
     */
    private void pushShipDateToBMS (String shipmentGid, Date dtshipdate) {
        LOGGER.info("同步OTM发运时间给TMS{},{}",shipmentGid,dtshipdate);
        try {
            EntityWrapper<OtmOrderRelease> releaseEw = new EntityWrapper<>();
            releaseEw.eq("shipment_gid", shipmentGid);
            releaseEw.ne("status", TableStatusEnum.STATUS_50.getCode());
            List<OtmOrderRelease> releaseList = otmOrderReleaseMapper.selectList(releaseEw);
            if (CollectionUtils.isNotEmpty(releaseList)) {
                for (OtmOrderRelease otmOrderRelease : releaseList) {
                    //推送发运时间给bms
                    pushShipDatetoBms(otmOrderRelease);
                }

            }
        } catch (Exception e) {
            LOGGER.error("同步OTM人送/慧运车发运时间给TMS异常{}", e.getMessage());
        }
    }

    /**
     * 推送运单发运数据至otm
     * @param releaseId 运单id
     */
    private void pushShipmentOtm (Long releaseId) {
        EntityWrapper<ItfExplogLine> wifEw = new EntityWrapper<>();
        wifEw.eq("relation_id", releaseId);
        wifEw.eq("export_type", InterfaceEventEnum.BS_OP_DELIVERY.getCode());
        List<ItfExplogLine> itfExplogLines = logLineService.selectList(wifEw);
        if (CollectionUtils.isNotEmpty(itfExplogLines)) {
            ItfExplogLine itfExplogLine = itfExplogLines.get(0);
            LOGGER.info("推送运单发运数据至otm,运单主键id{}", releaseId);
            //获取推送路径
            String pushUrl = this.setPushUrl("OTM");
            //推送系统数据
            String requestId = WmsMethodUtils.pushSystemData(pushUrl, itfExplogLine.getDataContent());
            if(StringUtils.isNotEmpty(requestId)){
                ItfExplogLine param = new ItfExplogLine();
                param.setRequestId(requestId);
                param.setId(itfExplogLine.getId());
                logLineService.updateById(param);
            }
        }
    }



    /**
     * 插入status和explog日志
     *
     * @param releaseId 运单主键id
     */
    private void insertPushLog (Long releaseId) {
        LOGGER.info("补发发运数据至otm，新增status_log");
        this.insertStatusLog(releaseId, TableStatusEnum.STATUS_10.getCode(),"定时调整发运");
        //5、推送otm
        OtmOrderRelease oor = otmOrderReleaseMapper.selectById(releaseId);
        if(null != oor){
            String pushUrl = this.setPushUrl("OTM");
            this.insertExplogToOtm(oor,pushUrl,"定时补发至OTM");
        }
    }



    /**
     * @param pushType OTM和BMS两个系统
     * @return
     */
    private String setPushUrl (String pushType) {
        String url = "";
        if ("OTM".equals(pushType)) {
            url = properties.getIntegrationhost() + InterfaceAddrEnum.EVENT_URI.getAddress();
        } else if ("TMS".equals(pushType)) {
            url = properties.getShipDataForBMS();
        } else {
            throw new BaseException("推送类型【pushType】不合法");
        }
        return url;
    }
}
