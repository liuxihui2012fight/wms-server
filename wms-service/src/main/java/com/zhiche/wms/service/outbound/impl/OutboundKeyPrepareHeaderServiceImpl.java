package com.zhiche.wms.service.outbound.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.outbound.OutboundKeyPrepareHeaderMapper;
import com.zhiche.wms.domain.model.outbound.OutboundKeyPrepareHeader;
import com.zhiche.wms.service.outbound.IOutboundKeyPrepareHeaderService;
import org.springframework.stereotype.Service;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:51 2018/12/24
 */
@Service
public class OutboundKeyPrepareHeaderServiceImpl extends ServiceImpl<OutboundKeyPrepareHeaderMapper, OutboundKeyPrepareHeader> implements IOutboundKeyPrepareHeaderService {
}
