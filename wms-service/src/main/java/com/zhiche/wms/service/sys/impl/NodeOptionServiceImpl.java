package com.zhiche.wms.service.sys.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.base.NodeOptionMapper;
import com.zhiche.wms.domain.model.base.NodeOption;
import com.zhiche.wms.service.sys.INodeOptionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关联表 服务实现类
 * </p>
 *
 * @author user
 */
@Service
public class NodeOptionServiceImpl extends ServiceImpl<NodeOptionMapper, NodeOption> implements INodeOptionService {

}
