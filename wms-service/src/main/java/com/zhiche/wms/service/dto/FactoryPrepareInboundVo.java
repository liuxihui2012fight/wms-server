package com.zhiche.wms.service.dto;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:17 2019/11/20
 */

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;

/**
 * @Author: caiHua
 * @Description: 工厂备料入库入参
 * @Date: Create in 10:13 2019/11/20
 */
public class FactoryPrepareInboundVo implements Serializable {

    /**
     * 车架号
     */
    private String vins;

    /**
     * 仓库
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeHouseId;

    /**
     * 库区id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long areaId;

    /**
     * 备料入库id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private String prepareInboundId;

    /**
     * 操作类型
     * 30、提车指令出库（判断必须有提车段指令才能出库）
     * 20、移实顺（出库后不能重复再次备料入库）
     * 40、返工厂（出库后可以再次入库）
     */

    private String handlerType;

    private String remarks;

    public String getRemarks () {
        return remarks;
    }

    public void setRemarks (String remarks) {
        this.remarks = remarks;
    }

    public String getHandlerType () {
        return handlerType;
    }

    public void setHandlerType (String handlerType) {
        this.handlerType = handlerType;
    }

    public String getPrepareInboundId () {
        return prepareInboundId;
    }

    public void setPrepareInboundId (String prepareInboundId) {
        this.prepareInboundId = prepareInboundId;
    }

    public String getVins () {
        return vins;
    }

    public void setVins (String vins) {
        this.vins = vins;
    }

    public Long getStoreHouseId () {
        return storeHouseId;
    }

    public void setStoreHouseId (Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public Long getAreaId () {
        return areaId;
    }

    public void setAreaId (Long areaId) {
        this.areaId = areaId;
    }
}