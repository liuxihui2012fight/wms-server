package com.zhiche.wms.service.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.math.BigDecimal;

/**
 * @Author: caiHua
 * @Description: 取消入库
 * @Date: Create in 14:56 2019/9/18
 */
public class CancleInboundVO {

    /**
     * 入库记录明细id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long putWayLineId;

    /**
     * 入库记录头表id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long putwayHeaderId;

    /**
     * 通知单头表id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long noticeId;

    /**
     * 通知单明细id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long noticeLineId;

    /**
     * 修改后的库位id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateLocationId;


    /**
     * 库位id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long locationId;

    /**
     * 车架号sku_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skuId;

    /**
     * 仓库id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long houseId;

    /**
     * 车架号
     */
    private String vin;

    /**
     * 指令号
     */
    private String shipmentGid;
    /**
     * 运单号
     */
    private String releaseGid;

    /**
     * 入库数量
     */
    private BigDecimal inboundSumQty;

    public Long getUpdateLocationId () {
        return updateLocationId;
    }

    public void setUpdateLocationId (Long updateLocationId) {
        this.updateLocationId = updateLocationId;
    }

    public BigDecimal getInboundSumQty () {
        return inboundSumQty;
    }

    public void setInboundSumQty (BigDecimal inboundSumQty) {
        this.inboundSumQty = inboundSumQty;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public Long getLocationId () {
        return locationId;
    }

    public Long getSkuId () {
        return skuId;
    }

    public void setSkuId (Long skuId) {
        this.skuId = skuId;
    }

    public void setLocationId (Long locationId) {
        this.locationId = locationId;
    }

    public Long getPutWayLineId () {
        return putWayLineId;
    }

    public void setPutWayLineId (Long putWayLineId) {
        this.putWayLineId = putWayLineId;
    }

    public Long getPutwayHeaderId () {
        return putwayHeaderId;
    }

    public void setPutwayHeaderId (Long putwayHeaderId) {
        this.putwayHeaderId = putwayHeaderId;
    }

    public Long getNoticeId () {
        return noticeId;
    }

    public void setNoticeId (Long noticeId) {
        this.noticeId = noticeId;
    }

    public Long getNoticeLineId () {
        return noticeLineId;
    }

    public void setNoticeLineId (Long noticeLineId) {
        this.noticeLineId = noticeLineId;
    }

    public Long getHouseId () {
        return houseId;
    }

    public void setHouseId (Long houseId) {
        this.houseId = houseId;
    }
}
