package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/14.
 */
public class SourceSystem {

    /**
     * 手工创建
     */
    public static final String HAND_MADE = "10";

    /**
     * 订单中心
     */
    public static final String OMS = "20";

    /**
     * OTM
     */
    public static final String OTM = "30";

    /**
     * APP
     */
    public static final String APP = "40";

    /**
     * 君马
     */
    public static final String JUNMA = "50";

    /**
     * 扫码触发
     */
    public static final String SCAN = "60";

    /**
     * 道闸
     */
    public static final String BARRIER = "70";


    /**
     * 仓库节点自动触发
     */
    public static final String AUTO= "80";


    /**
     * 霍尼扫码触发
     */
    public static final String HONEYWELL ="90";


}
