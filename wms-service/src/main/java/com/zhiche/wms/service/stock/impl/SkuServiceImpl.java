package com.zhiche.wms.service.stock.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.mapper.stock.SkuMapper;
import com.zhiche.wms.domain.mapper.stock.StockDetailMapper;
import com.zhiche.wms.domain.mapper.stock.StockMapper;
import com.zhiche.wms.domain.model.stock.Sku;
import com.zhiche.wms.domain.model.stock.SkuProperty;
import com.zhiche.wms.domain.model.stock.Stock;
import com.zhiche.wms.service.movement.impl.MovementHeaderServiceImpl;
import com.zhiche.wms.service.stock.ISkuService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 库存量单位(Stock Keeping Unit) 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements ISkuService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SkuServiceImpl.class);
    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private SnowFlakeId snowFlakeId;
    @Autowired
    private StockMapper stockMapper;
    @Autowired
    private StockDetailMapper stockDetailMapper;

    @Override
    public Sku getUniqueSku (SkuProperty skuProperty) {
        LOGGER.info("车架号 ： {}插入出库量单位表SkU",skuProperty.getLotNo1());
        EntityWrapper<Sku> ew = new EntityWrapper<>();
        // 调整skuProperty取消客户的限制
        if (StringUtils.isNotBlank(skuProperty.getUom())) {
            ew.eq("uom", skuProperty.getUom());
        }

        if (StringUtils.isNotBlank(skuProperty.getLotNo0())) {
            ew.eq("lot_No0", skuProperty.getLotNo0());
        }

        if (StringUtils.isNotBlank(skuProperty.getLotNo1())) {
            ew.eq("lot_No1", skuProperty.getLotNo1());
        }

        if (StringUtils.isNotBlank(skuProperty.getLotNo2())) {
            ew.eq("lot_No2", skuProperty.getLotNo2());
        }

        if (StringUtils.isNotBlank(skuProperty.getLotNo3())) {
            ew.eq("lot_No3", skuProperty.getLotNo3());
        }

        if (StringUtils.isNotBlank(skuProperty.getLotNo4())) {
            ew.eq("lot_No4", skuProperty.getLotNo4());
        }

       /* if (StringUtils.isNotBlank(skuProperty.getLotNo5())) {
            ew.eq("lot_No5", skuProperty.getLotNo5());
        }*/

        if (StringUtils.isNotBlank(skuProperty.getLotNo6())) {
            ew.eq("lot_No6", skuProperty.getLotNo6());
        }

        if (StringUtils.isNotBlank(skuProperty.getLotNo7())) {
            ew.eq("lot_No7", skuProperty.getLotNo7());
        }

        if (StringUtils.isNotBlank(skuProperty.getLotNo8())) {
            ew.eq("lot_No8", skuProperty.getLotNo8());
        }

        if (StringUtils.isNotBlank(skuProperty.getLotNo9())) {
            ew.eq("lot_No9", skuProperty.getLotNo9());
        }
        Sku sku = selectOne(ew);
        if (Objects.equals(null, sku)) {
            sku = new Sku();
            sku.setOwnerId(skuProperty.getOwnerId());
            sku.setMaterielId(skuProperty.getMaterielId());
            sku.setUom(skuProperty.getUom());
            sku.setLotNo0(skuProperty.getLotNo0());
            sku.setLotNo1(skuProperty.getLotNo1());
            sku.setLotNo2(skuProperty.getLotNo2());
            sku.setLotNo3(skuProperty.getLotNo3());
            sku.setLotNo4(skuProperty.getLotNo4());
            sku.setLotNo5(skuProperty.getLotNo5());
            sku.setLotNo6(skuProperty.getLotNo6());
            sku.setLotNo7(skuProperty.getLotNo7());
            sku.setLotNo8(skuProperty.getLotNo8());
            sku.setLotNo9(skuProperty.getLotNo9());
            sku.setId(snowFlakeId.nextId());
            try {
                boolean result = insert(sku);
                if (result) {
                    return sku;
                } else {
                    throw new BaseException(1001, "保存SKU失败!");
                }
            } catch (Exception e) {
                throw e;
            }
        } else {
            if (StringUtils.isBlank(sku.getOwnerId()) && StringUtils.isBlank(sku.getMaterielId())) {
                Sku updateSku = new Sku();
                updateSku.setOwnerId(skuProperty.getOwnerId());
                updateSku.setMaterielId(skuProperty.getMaterielId());
                updateSku.setUom(skuProperty.getUom());
                updateSku.setLotNo0(skuProperty.getLotNo0());
                updateSku.setLotNo1(skuProperty.getLotNo1());
                updateSku.setLotNo2(skuProperty.getLotNo2());
                updateSku.setLotNo3(skuProperty.getLotNo3());
                updateSku.setLotNo4(skuProperty.getLotNo4());
                updateSku.setLotNo5(skuProperty.getLotNo5());
                updateSku.setLotNo6(skuProperty.getLotNo6());
                updateSku.setLotNo7(skuProperty.getLotNo7());
                updateSku.setLotNo8(skuProperty.getLotNo8());
                updateSku.setLotNo9(skuProperty.getLotNo9());
                updateSku.setId(sku.getId());
                baseMapper.updateById(updateSku);
            }
            return sku;
        }
    }

    @Override
    public void updateSkuInfo (String otmVin, String wmsVin, Long storeHouseId, String cusVehicleType, long locationId) {
        Sku skuParam = new Sku();
        skuParam.setLotNo1(wmsVin);
        Sku sku = baseMapper.selectOne(skuParam);
        if (null != sku) {
            Sku otmSkuParam = new Sku();
            otmSkuParam.setLotNo1(otmVin);
            Sku otmSku = baseMapper.selectOne(otmSkuParam);
            long skuId;
            if (null == otmSku) {
                Sku insertSkuParam = new Sku();
                insertSkuParam.setOwnerId(sku.getOwnerId());
                if (StringUtils.isNotEmpty(cusVehicleType)) {
                    insertSkuParam.setMaterielId(cusVehicleType);
                } else {
                    insertSkuParam.setMaterielId(sku.getMaterielId());
                }
                insertSkuParam.setUom(sku.getUom());
                insertSkuParam.setLotNo0(sku.getLotNo0());
                insertSkuParam.setLotNo1(otmVin);
                insertSkuParam.setLotNo2(sku.getLotNo2());
                insertSkuParam.setLotNo3(sku.getLotNo3());
                insertSkuParam.setLotNo4(sku.getLotNo4());
                insertSkuParam.setLotNo5(sku.getLotNo5());
                insertSkuParam.setLotNo6(sku.getLotNo6());
                insertSkuParam.setLotNo7(sku.getLotNo7());
                insertSkuParam.setLotNo8(sku.getLotNo8());
                insertSkuParam.setLotNo9(sku.getLotNo9());
                skuId = snowFlakeId.nextId();
                insertSkuParam.setId(skuId);
                //新增车架号信息
                baseMapper.insert(insertSkuParam);
            } else {
                skuId = otmSku.getId();
            }

            //修改库存表信息
            EntityWrapper<Stock> stockEntityWrapper = new EntityWrapper<>();
            stockEntityWrapper.eq("sku_id", sku.getId());
            stockEntityWrapper.eq("store_house_id", storeHouseId);
            if (locationId != 0) {
                stockEntityWrapper.eq("location_id", locationId);
            }
            List<Stock> stocks = stockMapper.selectList(stockEntityWrapper);
            if (CollectionUtils.isNotEmpty(stocks)) {
                Stock stock = new Stock();
                stock.setSkuId(skuId);
                EntityWrapper<Stock> stockEW = new EntityWrapper<>();
                stockEW.eq("sku_id", sku.getId());
                stockEW.eq("store_house_id", storeHouseId);
                if (locationId != 0) {
                    stockEW.eq("location_id", locationId);
                }
                stockMapper.update(stock, stockEW);
            }
        }
    }

    @Override
    public Sku querySkuInfo (String vin) {
        Sku param = new Sku();
        param.setLotNo1(vin);
        Sku sku = baseMapper.selectOne(param);
        return sku;
    }

}
