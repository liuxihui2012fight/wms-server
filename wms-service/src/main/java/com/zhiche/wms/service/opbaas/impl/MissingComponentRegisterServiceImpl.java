package com.zhiche.wms.service.opbaas.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.opbaas.MissingComponentRegisterMapper;
import com.zhiche.wms.domain.model.opbaas.ComponentConfig;
import com.zhiche.wms.domain.model.opbaas.MissingComponentRegister;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.dto.opbaas.paramdto.MissingRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.MissingRegidterDetailDTO;
import com.zhiche.wms.dto.opbaas.resultdto.NodeProcessInfoDTO;
import com.zhiche.wms.service.common.INodeProcessService;
import com.zhiche.wms.service.opbaas.IComponentConfigService;
import com.zhiche.wms.service.opbaas.IMissingComponentRegisterService;
import com.zhiche.wms.service.sys.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 缺件登记 服务实现类
 * </p>
 *
 * @author hxh
 * @since 2018-08-07
 */
@Service
public class MissingComponentRegisterServiceImpl extends ServiceImpl<MissingComponentRegisterMapper, MissingComponentRegister> implements IMissingComponentRegisterService {

    @Autowired
    private IUserService userService;
    @Autowired
    private INodeProcessService nodeProcessService;
    @Autowired
    private IComponentConfigService componentConfigService;

    @Override
    public List<MissingRegidterDetailDTO> queryMissingInfo(MissingRegisterParamDTO dto) {
        if (Objects.isNull(dto.getVin())) throw new BaseException("车架号为空");
        if (Objects.isNull(dto.getTaskNode())) throw new BaseException("作业节点为空");
        Wrapper<MissingRegidterDetailDTO> ew = new EntityWrapper<>();
        ew.eq("vin", dto.getVin());
        ew.eq("task_node", dto.getTaskNode());
        ew.eq("status", TableStatusEnum.STATUS_10.getCode());
        ew.andNew();
        ew.eq("deal_status", TableStatusEnum.STATUS_10.getCode()).or().eq("deal_status", TableStatusEnum.STATUS_20.getCode());
        List<MissingComponentRegister> missingRegisteredList = this.baseMapper.queryMissing(ew);
        List<String> componentLevel2List = new ArrayList<>();
        for (MissingComponentRegister missingBean : missingRegisteredList) {
            componentLevel2List.add(missingBean.getComponentLevel2());
        }
        List<MissingRegidterDetailDTO> componentDTOList = componentConfigService.queryComponent();
        for (MissingRegidterDetailDTO component : componentDTOList) {
            List<ComponentConfig> childComponent = component.getChildComponent();
            for (ComponentConfig config : childComponent) {
                config.setDescribe(config.getName());
                if (componentLevel2List.contains(config.getCode())) {
                    config.setIsReigister("1");
                    if ("5001001".equals(config.getCode())) {
                        config.setDescribe(missingRegisteredList.get(componentLevel2List.indexOf("5001001")).getComponentDescribe());
                    }
                } else {
                    config.setIsReigister("0");
                }
            }
        }
        return componentDTOList;
    }

    @Override
    public List<MissingRegidterDetailDTO> updateMissingInfo (MissingRegisterParamDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空!");
        }
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("未查询到登录用户");
        }
        if (StringUtils.isBlank(loginUser.getCode())) {
            throw new BaseException("用户编码不能为空!");
        }

        String vin = dto.getVin();
        String taskNode = dto.getTaskNode();
        String userCode = loginUser.getCode();
        String orderNo = dto.getOrderNo();
        String describe = dto.getDescribe();
        String businessDoc = dto.getTaskId();

        String[] codes = (StringUtils.isNotBlank(dto.getCodes()) ? dto.getCodes().split(",") : null);
        String[] names = (StringUtils.isNotBlank(dto.getNames()) ? dto.getNames().split(",") : null);
        MissingComponentRegister bean = new MissingComponentRegister();
        bean.setStatus(TableStatusEnum.STATUS_30.getCode());
        Wrapper<MissingComponentRegister> ew = new EntityWrapper<>();
        ew.eq("vin", vin);
        ew.eq("task_node", taskNode);
        ew.eq("status", TableStatusEnum.STATUS_10.getCode());
        ew.andNew();
        ew.eq("deal_status", TableStatusEnum.STATUS_10.getCode()).or().eq("deal_status", TableStatusEnum.STATUS_20.getCode());
        this.baseMapper.update(bean, ew);
        NodeProcessInfoDTO nodeProcessInfo = new NodeProcessInfoDTO();
        nodeProcessInfo.setId(businessDoc);
        nodeProcessInfo.setTaskNode(taskNode);
        NodeProcessInfoDTO nodeProcessInfoDTO = nodeProcessService.queryNodeInfo(nodeProcessInfo);
        if (codes != null && names != null) {
            for (int i = 0; i < codes.length; i++) {
                MissingComponentRegister register = new MissingComponentRegister();
                if (Objects.nonNull(nodeProcessInfoDTO)) {
                    register.setWaybillNo(nodeProcessInfoDTO.getWaybillNo());
                    register.setWaybillStatus(nodeProcessInfoDTO.getWaybillStatus());
                    register.setQrCode(nodeProcessInfoDTO.getQrCode());
                }
                register.setOmsOrderNo(orderNo);
                register.setVin(vin);
                register.setTaskNode(taskNode);
                register.setBusinessDoc(businessDoc);
                register.setComponentLevel1(codes[i].substring(0, 5));
                register.setComponentLevel2(codes[i]);
                if ("5001001".equals(codes[i])) {
                    register.setComponentDescribe(describe);
                } else {
                    register.setComponentDescribe(names[i]);
                }
                register.setRegisterUser(userCode);
                register.setRegisterTime(new Date());
                register.setDealStatus(TableStatusEnum.STATUS_10.getCode());
                register.setStatus(TableStatusEnum.STATUS_10.getCode());
                register.setUserCreate(userCode);
                register.setUserModified(userCode);
                register.setGmtCreate(new Date());
                register.setGmtModified(new Date());
                this.baseMapper.insert(register);
            }
        }
        return null;
    }
}
