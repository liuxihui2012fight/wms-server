package com.zhiche.wms.service.otm.impl;

import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.dto.opbaas.resultdto.TaskReleaseResultDTO;
import com.zhiche.wms.dto.shipmentDto.PushShipmentToOtmDto;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 运单 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
@Service
public class OtmOrderReleaseServiceImpl extends ServiceImpl<OtmOrderReleaseMapper, OtmOrderRelease> implements IOtmOrderReleaseService {

    @Override
    public List<TaskReleaseResultDTO> queryRealeaseInfo(HashMap<String, Object> params) {
        return baseMapper.selectReleaseInfo(params);
    }

    @Override
    public List<PushShipmentToOtmDto> queryShipmentLogToOtm () {
        return baseMapper.queryShipmentLogToOtm();
    }

    @Override
    public List<PushShipmentToOtmDto> pushInboundToOtm () {
        return baseMapper.pushInboundToOtm();
    }
}
