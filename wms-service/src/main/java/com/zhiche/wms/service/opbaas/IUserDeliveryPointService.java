package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.OpUserDeliveryPoint;

/**
 * <p>
 * 用户发车点配置 服务类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface IUserDeliveryPointService extends IService<OpUserDeliveryPoint> {

}
