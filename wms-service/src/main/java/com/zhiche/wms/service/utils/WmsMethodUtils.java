package com.zhiche.wms.service.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.utils.HttpClientUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 18:01 2019/7/2
 */
public class WmsMethodUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(WmsMethodUtils.class);

    public static String pushSystemData (String pushUrl, String dataContent) {
        String requestId = null;
        String resJson = HttpClientUtil.postJson(pushUrl, null, dataContent, 60000);
        LOGGER.info("数据推送结果为：resJson{}", resJson);
        if (StringUtils.isNotEmpty(resJson)) {
            RestfulResponse<String> restfulResponse = JSON.parseObject(resJson, new TypeReference<RestfulResponse<String>>() {
            });
            if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                requestId = restfulResponse.getData();
            }
        }
        return requestId;
    }
}
