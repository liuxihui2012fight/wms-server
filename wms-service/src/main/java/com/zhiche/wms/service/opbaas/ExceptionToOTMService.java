package com.zhiche.wms.service.opbaas;

import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;

import java.util.List;

/**
 * <p>
 * 异常推送otm
 * </p>
 *
 * @author yzy
 * @since 2018-11-09
 */
public interface ExceptionToOTMService {

    void deleteExceptionToOTM (String[] ids, String code);

    void isCanSend(CommonConditionParamDTO conditionParamDTO);

    String isSend(String vin ,String originName);

    void serviceFactoryToOTM (List<ExceptionRegister> exceptionRegisters, String vin);

    List<CommonConditionParamDTO> setToOtmParam (CommonConditionParamDTO conditionParamDTO);
}
