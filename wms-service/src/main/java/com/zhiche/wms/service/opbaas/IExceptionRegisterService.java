package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.dto.opbaas.paramdto.ExceHandlingParamDTO;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 异常登记 服务类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface IExceptionRegisterService extends IService<ExceptionRegister> {

    ArrayList<ExResultDTO> getExceptionCountByTaskId(ExceptionRegisterParamDTO dto);

    void updateNoteException(ExceptionRegisterParamDTO dto);

    ArrayList<ExResultDTO> updateExceptionInfoForScan(ExceptionRegisterParamDTO dto);

    List<ExResultDTO> getExCount(ExceptionRegisterParamDTO dto);

    List<ExResultDTO> getExceptionPicUrl(ExceptionRegisterParamDTO dto);

    /**
     * 异常列表查询
     */
    Page<ExResultDTO> queryExceptionList(Page<ExResultDTO> page);

    /**
     * 异常列表导出
     */
    List<ExResultDTO> queryExportExcpList(Page<ExResultDTO> page);

    /**
     * 异常列表明细查询
     */
    Page<ExResultDTO> queryExceptionDetail(Page<ExResultDTO> page);

    /**
     *异常处理
     */
    void exceptionHandling(ExceHandlingParamDTO exceHandlingParamDTO);

    /**
     * 删除异常
     */
    void deleteException (Map<String, String> param);

    /**
     * 根据OR号批量新增异常信息
     */
    List<String> saveException (Map<String, String> param);

    /**
     * 将删除的异常信息推送给OTM
     */
    void pushExceptionOTM (ExceptionRegister register, ExResultDTO exResultDTO, Map<String, String> params);

    /**
     * 添加异常日志
     */
    void saveExceptionLog (Map<String, String> condition, String jsonParam, String result);

    void pushException (Map<String, String> params);
}
