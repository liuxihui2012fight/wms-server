package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExConfigurationDTO;
import com.zhiche.wms.domain.model.opbaas.ExceptionConfiguration;

import java.util.List;

/**
 * <p>
 * 异常配置 服务类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface IExceptionConfigurationService extends IService<ExceptionConfiguration> {

    List<ExConfigurationDTO> getConfigurationList(ExceptionRegisterParamDTO dto);

    String queryNameByCode(String code);
}
