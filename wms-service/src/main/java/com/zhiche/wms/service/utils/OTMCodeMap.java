package com.zhiche.wms.service.utils;

import com.zhiche.wms.core.supports.enums.TableStatusEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 14:10 2019/9/16
 */
public class OTMCodeMap {

    public final static Map<String, String> EXCEPTION_DEAL_TYPE = new HashMap<>();

    static {
        EXCEPTION_DEAL_TYPE.put(TableStatusEnum.STATUS_10.getCode(), "返厂维修");
        EXCEPTION_DEAL_TYPE.put(TableStatusEnum.STATUS_20.getCode(), "出库维修");
        EXCEPTION_DEAL_TYPE.put(TableStatusEnum.STATUS_30.getCode(), "库内维修");
        EXCEPTION_DEAL_TYPE.put(TableStatusEnum.STATUS_40.getCode(), "带伤发运");
        EXCEPTION_DEAL_TYPE.put(TableStatusEnum.STATUS_50.getCode(), "异常关闭");
        EXCEPTION_DEAL_TYPE.put(TableStatusEnum.STATUS_60.getCode(), "驳回发运");
    }


    public final static Map<String, String> EXCEPTION_TASK_NODE = new HashMap<>();

    static {
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_0.getCode(), "指令");
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_1.getCode(), "运单");
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_10.getCode(), "寻车");
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_20.getCode(), "移车");
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_30.getCode(), "提车");
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_40.getCode(), "收车质检");
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_42.getCode(), "分配入库");
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_50.getCode(), "出库");
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_51.getCode(), "出库确认");
        EXCEPTION_TASK_NODE.put(TableStatusEnum.STATUS_60.getCode(), "装车交验");
    }


    public final static Map<String, String> EXCEPTION_DEAL_STATUS = new HashMap<>();

    static {
        EXCEPTION_DEAL_STATUS.put(TableStatusEnum.STATUS_10.getCode(), "未处理");
        EXCEPTION_DEAL_STATUS.put(TableStatusEnum.STATUS_20.getCode(), "处理中");
        EXCEPTION_DEAL_STATUS.put(TableStatusEnum.STATUS_30.getCode(), "关闭");
        EXCEPTION_DEAL_STATUS.put(TableStatusEnum.STATUS_40.getCode(), "让步");
        EXCEPTION_DEAL_STATUS.put(TableStatusEnum.STATUS_50.getCode(), "已处理");
    }

}
