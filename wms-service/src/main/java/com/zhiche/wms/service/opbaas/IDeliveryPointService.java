package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;
import com.zhiche.wms.dto.opbaas.resultdto.OpDeliveryPointResultDTO;

import java.util.List;

/**
 * <p>
 * 发车点配置 服务类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface IDeliveryPointService extends IService<OpDeliveryPoint> {

    /**
     * 查询用户下发车点
     */
    Page<OpDeliveryPointResultDTO> listPointPage(Page<OpDeliveryPointResultDTO> page);

    /**
     * 保存发车点配置
     */
    void savePoint(OpDeliveryPointResultDTO dto);

    /**
     * 修改发车点
     */
    void updatePoint(OpDeliveryPointResultDTO dto);

    /**
     * 全部可用发车点配置列表查询
     */
    Page<OpDeliveryPoint> allUsablePoint(Page<OpDeliveryPoint> page);

    /**
     * 查询登录用户配置的发车点
     */
    List<OpDeliveryPoint> userPoint(Integer userId);

    List<String> queryOpdeliveryPoint (Integer oPointWrapper);
}
