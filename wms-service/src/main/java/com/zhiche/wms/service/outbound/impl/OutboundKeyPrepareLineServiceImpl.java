package com.zhiche.wms.service.outbound.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.outbound.OutboundKeyPrepareLineMapper;
import com.zhiche.wms.domain.model.outbound.OutboundKeyPrepareLine;
import com.zhiche.wms.service.outbound.IOutboundKeyPrepareLineService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 16:47 2018/12/24
 */
@Service
public class OutboundKeyPrepareLineServiceImpl extends ServiceImpl<OutboundKeyPrepareLineMapper, OutboundKeyPrepareLine> implements IOutboundKeyPrepareLineService {
    @Override
    public void updateKeyPrepareStatus (Map<String, String> param) {

    }
}
