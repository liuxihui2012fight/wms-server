package com.zhiche.wms.service.outbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.domain.model.outbound.OutboundShipHeader;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.dto.outbound.OutboundShipListDTO;
import com.zhiche.wms.dto.outbound.OutboundShipParamDTO;
import com.zhiche.wms.dto.outbound.OutboundShipQrCodeResultDTO;
import com.zhiche.wms.dto.outbound.OutboundShipQuitResultDTO;

import java.util.ArrayList;

/**
 * <p>
 * 出库单头 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IOutboundShipHeaderService extends IService<OutboundShipHeader> {


    ArrayList<String> updateOutboundShipConfirm(OutboundShipParamDTO dto);

    void updateOutShipConfirmScan(OutboundShipParamDTO dto);

    void updateOutboundShipQuit(OutboundShipParamDTO dto);

    OutboundShipQuitResultDTO getOutboundShipInfoForQuit(OutboundShipParamDTO dto);

    OutboundShipQrCodeResultDTO getOutShipInfoByScan(OutboundShipParamDTO dto);

    void setReleaseAndStatusLog(Long noticeLineId, OutboundNoticeLine outboundNoticeLine, User loginUser);

    /**
     * 出库列表查询
     */
    Page<OutboundShipListDTO> queryOutboundShipPage(Page<OutboundShipListDTO> page);

    boolean saveShip(OutboundShipHeader outboundShipHeader);

    boolean insertMovementByShip(OutboundShipHeader outboundShipHeader, User loginUser);
}
