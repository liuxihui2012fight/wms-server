package com.zhiche.wms.service.sys;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.domain.model.sys.UserRole;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
public interface IUserService extends IService<User> {

    /**
     * 分配角色
     */
    int assignRole(Integer userId, List<Integer> roleIds) throws Exception;

    /**
     * 得到用户所有权限
     */
    List<UserRole> listUserRole(Integer userId);

    /**
     * 得到登录用户
     */
    User getLoginUser() throws BaseException;

    /**
     * 得到用户仓库
     */
    List<Storehouse> listUserStorehouse(Integer userId);

    /**
     * 分配仓库
     */
    int assignHouse(Integer userId, List<Long> houseIds) throws Exception;

    /**
     * 得到用户发车点
     */
    List<OpDeliveryPoint> listUserDeliveryPoint(Integer userId);

    /**
     * 分配发车点
     */
    int assignPoint(Integer userId, List<OpDeliveryPoint> points);

    List<Storehouse> getHousesListByToken();

    /**
     * 用户注册
     */
    void saveUser(Map<String, Object> param, String authorization);

    /**
     * 分页查询用户
     */
    Page<User> selectUserPage(Page<User> page);

    /**
     * 修改用户密码
     */
    String updatePwd(Map<String, Object> param, String authorization);

    /**
     * 禁用用户信息
     * @param userIds 主键id
     */
    void disableUserInfo (Map<String, Object> userIds);

    /**
     * 查询用户登录信息
     */
    User queryLoginUser (String genMethod);
}
