package com.zhiche.wms.service.log;

import com.zhiche.wms.dto.opbaas.resultdto.ReleaseDTO;
import com.zhiche.wms.service.dto.ShipmentDTO;

public interface ImportShipment {

    /**
     * 更新OTM的调度指令至WMS
     * @param shipmentDTO
     */
    void importOTMShipment(ShipmentDTO shipmentDTO);

    /**
     * otm更新运单信息
     * @param releaseDTOs 更新运单信息
     */
    void updateReleaseInfo (ReleaseDTO releaseDTOs);
}
