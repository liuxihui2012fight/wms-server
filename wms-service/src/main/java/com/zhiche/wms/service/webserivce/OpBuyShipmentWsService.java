package com.zhiche.wms.service.webserivce;

import com.zhiche.wms.dto.opbaas.paramdto.BuyShipmentParamWSDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(name = "OpBuyShipmentWsService",
        targetNamespace = "http://webservice.wms.zhiche.com")
public interface OpBuyShipmentWsService {

    @WebMethod(operationName = "saveBuyShipment")
    @WebResult(name = "result")
    String saveBuyShipment(@WebParam(name = "buyShipments") BuyShipmentParamWSDTO[] buyShipments);


    String test();

}
