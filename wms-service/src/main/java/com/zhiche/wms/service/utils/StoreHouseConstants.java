package com.zhiche.wms.service.utils;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 10:24 2019/11/22
 */
public enum StoreHouseConstants {


    //车架号
    NC_1("10004", "南昌发运库1库"),
    NC_2("10015", "南昌发运库2库"),
    NC_3("10014", "南昌发运库3库");

    private final String code;
    private final String detail;

    public String getCode () {
        return code;
    }

    public String getDetail () {
        return detail;
    }

    StoreHouseConstants (String code, String detail) {
        this.code = code;
        this.detail = detail;
    }
}
