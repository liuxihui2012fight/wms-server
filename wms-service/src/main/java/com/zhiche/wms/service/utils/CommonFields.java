package com.zhiche.wms.service.utils;

/**
 * @Author: caiHua
 * @Description: 字段
 * @Date: Create in 14:27 2019/11/20
 */
public enum CommonFields {

    //车架号
    ID("id", "主键id"),
    VIN("vin", "车架号"),
    CODE("code", "code"),
    LOT_NO1("lot_no1", "车架号"),
    STORE_HOUSE_ID("store_house_id", "仓库id"),
    STORE_HOUSE_ID_1("storeHouseId", "仓库id"),
    STORE_HOUSE_Name_1("storeHouseName", "仓库名称"),
    STARTDATE("startDate", "开始时间"),
    ENDDATE("endDate", "结束时间"),
    STATUS("status", "状态"),
    PREPARE_STATUS("prepare_status", "备料入库状态");

    private final String code;
    private final String detail;

    public String getCode () {
        return code;
    }

    public String getDetail () {
        return detail;
    }

    CommonFields (String code, String detail) {
        this.code = code;
        this.detail = detail;
    }
}
