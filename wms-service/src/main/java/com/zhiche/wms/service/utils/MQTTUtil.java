package com.zhiche.wms.service.utils;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 10:04 2019/8/29
 */
public class MQTTUtil {

    //public static String serviceURI = "ws://10.20.30.169:8083/mqtt";
    public static String serviceURI = "tcp://10.20.30.169:1883";
    public static String clientID = "G7_clientId";
    //如果mqtt服务配置了匿名访问，则不需要使用用户名和密码就可以实现消息的订阅和发布
    public static String username = "admin";
    public static String password = "public";
    public static String topic = "G7_topic";
    /*
     * 消息服务质量，一共有三个：
     * 0：尽力而为。消息可能会丢，但绝不会重复传输
     * 1：消息绝不会丢，但可能会重复传输
     * 2：恰好一次。每条消息肯定会被传输一次且仅传输一次
     */
    public static int qos = 1;
}
