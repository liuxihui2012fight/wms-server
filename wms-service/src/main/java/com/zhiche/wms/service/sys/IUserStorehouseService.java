package com.zhiche.wms.service.sys;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.sys.UserStorehouse;
import com.zhiche.wms.dto.inbound.StoreAreaAndLocationDTO;
import com.zhiche.wms.dto.sys.StorehouseDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户仓库权限表 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface IUserStorehouseService extends IService<UserStorehouse> {

    List<UserStorehouse> getUserHouses();

    /**
     * 查询所有库区库位
     */
    List<StoreAreaAndLocationDTO> queryAllArea(Map<String, String> condition);

}
