package com.zhiche.wms.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 16:42 2020/3/19
 */
public class OriginationDTO implements Serializable {
    /**
     * id
     */
    private Integer id;

    private String locationGid;
    /**
     * 编码
     */
    private String code;
    /**
     * 名称
     */
    private String name;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 县
     */
    private String county;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 经度
     */
    private BigDecimal longitude;
    /**
     * 纬度
     */
    private BigDecimal latitude;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 客户
     */
    private String customer;
    /**
     * 类型(10：发车点，20：中转库)
     */
    private String type;
    /**
     * 状态(Y：正常，N：失效)
     */
    private String status;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 省编码
     */
    private String provinceCode;
    /**
     * 市编码
     */
    private String cityCode;
    /**
     * 县/区编码
     */
    private String countyCode;

    public Integer getId () {
        return id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public String getLocationGid () {
        return locationGid;
    }

    public void setLocationGid (String locationGid) {
        this.locationGid = locationGid;
    }

    public String getCode () {
        return code;
    }

    public void setCode (String code) {
        this.code = code;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getProvince () {
        return province;
    }

    public void setProvince (String province) {
        this.province = province;
    }

    public String getCity () {
        return city;
    }

    public void setCity (String city) {
        this.city = city;
    }

    public String getCounty () {
        return county;
    }

    public void setCounty (String county) {
        this.county = county;
    }

    public String getAddress () {
        return address;
    }

    public void setAddress (String address) {
        this.address = address;
    }

    public BigDecimal getLongitude () {
        return longitude;
    }

    public void setLongitude (BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude () {
        return latitude;
    }

    public void setLatitude (BigDecimal latitude) {
        this.latitude = latitude;
    }

    public String getContact () {
        return contact;
    }

    public void setContact (String contact) {
        this.contact = contact;
    }

    public String getPhone () {
        return phone;
    }

    public void setPhone (String phone) {
        this.phone = phone;
    }

    public String getCustomer () {
        return customer;
    }

    public void setCustomer (String customer) {
        this.customer = customer;
    }

    public String getType () {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getProvinceCode () {
        return provinceCode;
    }

    public void setProvinceCode (String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode () {
        return cityCode;
    }

    public void setCityCode (String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCountyCode () {
        return countyCode;
    }

    public void setCountyCode (String countyCode) {
        this.countyCode = countyCode;
    }
}
