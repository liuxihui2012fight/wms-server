package com.zhiche.wms.service.base;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.base.StoreLocation;
import com.zhiche.wms.dto.inbound.StoreAreaAndLocationDTO;
import com.zhiche.wms.dto.stock.StockDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 储位配置 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IStoreLocationService extends IService<StoreLocation> {

    /**
     * 得到仓库的可用储位
     */
    List<StoreLocation> listUsableLocation(Long storeHouseId);

    /**
     * 模糊查询得到仓库的可用储位
     */
    Page<StoreLocation> getUsableLocationByPage(Page<StoreLocation> stockDTOPage);


    /**
     * 按类型得到仓库的可用储位
     *
     * @param type 10:普通储位，20：大储位，30：临时储位，40：虚拟储位, 50:中库位
     */
    List<StoreLocation> listUsableLocationByType(Long storeHouseId, String type);

    /**
     * 得到第一个可用储位(默认规则，按正常储位、大储位、临时储位、虚拟储位顺序)
     */
    StoreLocation getUsableLocation(Long storeHouseId);

    /**
     * 按类型得到第一个可用储位
     *
     * @param type 10:普通储位，20：大储位，30：临时储位，40：虚拟储位, 50:中库位
     */
    StoreLocation getUsableLocationByType(Long storeHouseId, String type);

    /**
     * 按库区和类型得到第一个可用储位
     *
     * @param type 10:普通储位，20：大储位，30：临时储位，40：虚拟储位, 50:中库位
     */
    StoreLocation getUsableLocationByAreaType(Long storeHouseId, Long areaId, String type);

    StoreLocation selectLocationByLocationId(Long storeHouseId);

    /**
     * 查询所有库位
     */
    List<StoreAreaAndLocationDTO> selectAllLocations(Map<String, String> condition);

    /**
     * 通过code查询数据数
     */
    int getCountByCodeName(StoreLocation storeLocation);

    /**
     * 导入库位
     */
    Map<String,JSONArray> saveImportLocation(String data);

    boolean updateStoreLocation(StoreLocation storeLocation);
    /**
     * 得到第一个可用储位(默认规则，按正常储位、大储位、临时储位、虚拟储位顺序)
     */
    StoreLocation queryUsableLocationByAreaId (Long storeHouseId, Long areaId);


    String queryAreaCodeDetail (String areaName, String locationName);
}
