package com.zhiche.wms.service.otm;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.otm.*;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentRecordDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 调度指令 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
public interface IOtmShipmentService extends IService<OtmShipment> {

    /**
     * 保存调度指令
     */
    boolean insertShipment(OtmShipment otmShipment);

    /**
     * 装车发运-模糊查询指令列表  -- 关联发车配置点
     */
    List<ShipmentForShipDTO> queryShipmentReleaseList(Page<ShipmentForShipDTO> page, HashMap<String, Object> param, EntityWrapper<ShipmentForShipDTO> ew);

    /**
     * 装车发运-点击获取指令详情
     */
    List<ShipmentForShipDTO > getShipDetail(EntityWrapper<ShipmentForShipDTO> ew);

    int countShipmentReleaseList(HashMap<String, Object> params, EntityWrapper<ShipmentForShipDTO> ew);

    List<ShipmentForShipDTO> queryShipmentReleases(@Param("ew") EntityWrapper<ShipmentForShipDTO> dataEW);

    List<ShipmentRecordDTO> queryShipRecordList (Page<ShipmentRecordDTO> page,@Param("ew") EntityWrapper<ShipmentRecordDTO> ew);

    List<OtmOrderRelease> queryShipmentStatus (List<Map<String, String>> shipmentGid);

    List<ShipmentRecordDTO> queryNewChannelReleaseList (Page<ShipmentRecordDTO> page,EntityWrapper<ShipmentRecordDTO> ew);

    /**
     * 查询指令发运推送数据
     */
    Page<OtmReleaseOtmDTO> queryPushShipmentOtm (Page<OtmReleaseOtmDTO> page);

    /**
     * 推送发运数据
     */
    void pushShipmentOtm (Map<String,String> param);

    /**
     * 查询指令下发情况
     */
    List<String> queryShipmentInfo (List<String> param);

    /**
     * 查询商品车数量
     * @param page
     */
    List<ShipmentInfo> getShipOrderItemCount (List<ShipmentInfo> page);

    /**
     * 更新发运时间
     * @param param
     */
    void otmShipDateToWms (List<OtmUpdateShipWmsDate> param);

    List<ShipmentRecordDTO> queryShipRecordList (@Param("ew") EntityWrapper<ShipmentRecordDTO> srdEw);

    /**
     * 补推送已经发运但未推送给otm的数据
     */
    void pushShipmentLogToOtm ();

    void senShipToOtm (OtmOrderRelease orderRelease, String shipMessage);

    void pushInboundToOtm (String oorStatus, String logStatus);

    void toBmsShipDate (List<String> currentDate);

    void pullOtmShipDate ();

    /**
     * 同步水铁发运时间
     */
    void pullOtmWaterIronShipDate ();

    void deleteShipmentInfo (String shipmentXid);
}
