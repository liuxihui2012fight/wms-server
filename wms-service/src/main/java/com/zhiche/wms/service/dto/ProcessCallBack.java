package com.zhiche.wms.service.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 接口回调处理
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public class ProcessCallBack {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	private Integer id;
    /**
     * 请求ID
     */
	private String requestId;
    /**
     * 处理结果
     */
	private String processStatus;
    /**
     * 处理备注
     */
	private String processRemark;
    /**
     * 处理时间
     */
	private Date processTime;
    /**
     * 创建时间
     */
	private Date gmtCreate;
    /**
     * 修改时间
     */
	private Date gmtModified;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public String getProcessRemark() {
		return processRemark;
	}

	public void setProcessRemark(String processRemark) {
		this.processRemark = processRemark;
	}

	public Date getProcessTime() {
		return processTime;
	}

	public void setProcessTime(Date processTime) {
		this.processTime = processTime;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	@Override
	public String toString() {
		return "ProcessCallBack{" +
			", id=" + id +
			", requestId=" + requestId +
			", processStatus=" + processStatus +
			", processRemark=" + processRemark +
			", processTime=" + processTime +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
