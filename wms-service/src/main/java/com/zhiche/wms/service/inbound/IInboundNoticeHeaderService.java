package com.zhiche.wms.service.inbound;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.inbound.InboundNoticeHeader;
import com.zhiche.wms.dto.inbound.InboundNoticeHeaderDTO;
import com.zhiche.wms.dto.inbound.InboundNoticeParamDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 入库通知单头 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IInboundNoticeHeaderService extends IService<InboundNoticeHeader> {

    /**
     * 根据通知单ID更新入库状态
     */
    boolean updateStatus (Long noticeId, Long noticeLineId);

    Map<String, Object> saveInboundNotice(List<InboundNoticeParamDTO> paramDTOS);

    boolean insertInboundNotice(InboundNoticeHeader inboundNoticeHeader);

    List<InboundNoticeHeaderDTO> selectHeaderList(Long houseId, String status, Integer size, Integer current);

    InboundNoticeHeaderDTO selectHeaderDeatil(Long houseId, Long id);
}
