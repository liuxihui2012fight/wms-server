package com.zhiche.wms.service.outbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareHeader;
import com.zhiche.wms.dto.outbound.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出库备料单头 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
public interface IOutboundPrepareHeaderService extends IService<OutboundPrepareHeader> {

    Integer updateStatus(Long id, Long houseId, Integer status, BigDecimal damagedSum);

    void createPreparation(OutboundPreparationParamDTO dto);

    List<PreparationListDetailDTO> getPreparationDetail(OutboundPreparationParamDTO dto);

    void updateChangePreparation(OutboundPreparationParamDTO dto);

    void updatePrepareFinishByLineId(Long noticeLineId, String modifiedName);

    /**
     * 查询备料任务列表
     */
    Page<PreparationListHeadDTO> queryOutPreparePage(Page<PreparationListHeadDTO> page);

    void singlePreparation(OutboundPreparationParamDTO dto);

    /**
     * 查询未备料任务列表
     */
    Page<PreparationListHeadDTO> notPreparationList(Page<PreparationListHeadDTO> page);

    Page<OutboundPrepareVehicleDTO> getPrepareVehicle(Page<OutboundPrepareVehicleDTO> page);

    /**
     * 钥匙备料头表
     * @param page
     * @return
     */
    Page<PreparationListHeadDTO> queryOutKeyPreparePage (Page<PreparationListHeadDTO> page);

    /**
     * 查询详情
     * @param dto
     * @return
     */
    List<PreparationListDetailDTO> getKeyPrepareDetail (OutboundPreparationParamDTO dto);


    void updateKeyPrepareStatus (Map<String, String> param);

    /**
     * 批量打印钥匙备料单
     * @param params
     * @return
     */
    List<PreparationPrintDTO> queryKeyPerparePrintData (Map<String, String> params);

    /**
     * 批量打印备料单
     */
    List<BatchPrintPrepare> batchPrintPrepare (Map<String, String> params);

    /**
     * 打印备料单同时进行确认备料
     * @param param
     */
    void confirmPrepare (List<BatchPrintPrepare> param);
}
