package com.zhiche.wms.service.opbaas;


import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.ComponentConfig;
import com.zhiche.wms.dto.opbaas.paramdto.MissingRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.MissingRegidterDetailDTO;

import java.util.List;

/**
 * <p>
 * 缺件配置 服务类
 * </p>
 *
 * @author hxh
 * @since 2018-08-07
 */
public interface IComponentConfigService extends IService<ComponentConfig> {

    List<MissingRegidterDetailDTO> queryComponent();
}
