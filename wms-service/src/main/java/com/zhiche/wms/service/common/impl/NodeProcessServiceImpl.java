package com.zhiche.wms.service.common.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.service.common.INodeProcessService;
import com.zhiche.wms.dto.opbaas.resultdto.NodeProcessInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class NodeProcessServiceImpl implements INodeProcessService {

    @Autowired
    OtmOrderReleaseMapper orderReleaseMapper;

    @Override
    public NodeProcessInfoDTO queryNodeInfo(NodeProcessInfoDTO dto) {

//        Wrapper<NodeProcessInfoDTO> ew = new EntityWrapper<>();
        if (Objects.isNull(dto.getId())) throw new BaseException("id为空！");
        if (Objects.isNull(dto.getTaskNode())) throw new BaseException("类型为空！");
        Map<String,Object> param = new HashMap<>();
        param.put("id",dto.getId());
        param.put("task_node",dto.getTaskNode());
        NodeProcessInfoDTO nodeProcessInfo = orderReleaseMapper.queryNodeInfo(param);
        return nodeProcessInfo;
    }
}
