package com.zhiche.wms.service.base;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.base.StoreArea;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.base.ValidArea;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库区配置 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IStoreAreaService extends IService<StoreArea> {

    /**
     * 查询库区信息
     */
    Page<StoreArea> queryByPage(Page<StoreArea> page);

    /**
     * 通过code查询数据数
     */
    int getCountByCodeName(StoreArea storeArea);

    boolean updateStoreArea(StoreArea storeArea);

    /**
     * 批量禁用库区
     * @param areaIds
     */
    void batchUpdateStatus (Map<String, String> areaIds);

    List<ValidArea> queryValidArea (String storeHouseId);

    List<ValidArea> queryWDValidArea (Long valueOf);

    List<StoreArea> queryMovementArea (Map<String, String> param);
}
