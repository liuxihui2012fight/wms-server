package com.zhiche.wms.service.base;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.dto.sys.StorehouseDTO;
import com.zhiche.wms.dto.sys.StorehouseNodeDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 仓库信息 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IStorehouseService extends IService<Storehouse> {


    /**
     * 仓库配置 - 查询仓库列表
     */
    Page<Storehouse> listStoreHousePage(Page<Storehouse> page);

    /**
     * 新增仓库
     */
    void saveStoreHouse(StorehouseDTO dto);

    /**
     * 修改仓库
     */
    void updateStoreHouse(StorehouseDTO dto);

    /**
     * 编辑仓库 信息回显
     * @param condition
     */
    List<StorehouseNodeDTO> getHoseInfo(Map<String, String> condition);

    /**
     * 查询全部可用仓库
     */
    Page<Storehouse> allUsableStoreHouse(Page<Storehouse> page);

    Storehouse getStorehouseByName (String storeHouseName);

    List<Storehouse> queryStoreHouseById (List<String> list);
}
