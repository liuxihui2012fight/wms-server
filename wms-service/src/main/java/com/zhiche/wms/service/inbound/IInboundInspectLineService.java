package com.zhiche.wms.service.inbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.inbound.InboundInspectLine;
import com.zhiche.wms.dto.inbound.InboundInspectDTO;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * 入库质检单明细 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-22
 */
public interface IInboundInspectLineService extends IService<InboundInspectLine> {
    /**
     * 模糊查询入库通知单信息
     */
    Page<InboundInspectDTO> queryList(String key, Long houseId, Integer size, Integer current);

    /**
     * 查询或生成质检单
     */
    InboundInspectDTO updateInspectInfo(String key, Long houseId);

    /**
     * 二维码扫描方式
     */
    InboundInspectDTO updateInspectBykey(String key, Long houseId);

    void updateInspectStatus (String key, Long houseId, Integer status, String isCanSend, String orderReleaseGid,String estimatedProcessingTime);

    /**
     * 入库质检查询
     */
    Page<InboundInspectDTO> listInspectPage(Page<InboundInspectDTO> page);

    /**
     * 入库质检导出
     */
    List<InboundInspectDTO> queryExportInspect(Map<String, Object> condition);

}
