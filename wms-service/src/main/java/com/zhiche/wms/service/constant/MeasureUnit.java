package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/18.
 */
public class MeasureUnit {

    /**
     * 吨
     */
    public static final String TOM = "T";

    /**
     * 千克
     */
    public static final String KG = "kg";

    /**
     * 包
     */
    public static final String PACK = "pack";

    /**
     * 台
     */
    public static final String TAI = "台";
}
