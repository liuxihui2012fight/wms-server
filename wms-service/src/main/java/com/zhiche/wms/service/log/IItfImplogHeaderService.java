package com.zhiche.wms.service.log;


import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.log.ItfImplogHeader;
import com.zhiche.wms.dto.interfacedto.UpdateInboundFromTMSDTO;
import com.zhiche.wms.service.dto.ShipmentDTO;

import java.util.ArrayList;
import java.util.Map;

/**
 * <p>
 * 仓储接口导入日志头 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
public interface IItfImplogHeaderService extends IService<ItfImplogHeader> {

    void otmShipmentImport(String xml) throws Exception;

    void saveInboundDataFromTMSBySchedule();

    ArrayList<Map<String, String>> updateInboundOrder(UpdateInboundFromTMSDTO interfaceDTO);

    void saveOutboundDataFromTMSBySchedule();

    void updateOutboundNoticeFromTMS(String message);

    /**
     * OTM调度指令存储
     */
    void updateShipmentOTM(ShipmentDTO shipmentDTO);
}
