package com.zhiche.wms.service.opbaas.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.google.common.collect.Maps;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.opbaas.ExceptionConfigurationMapper;
import com.zhiche.wms.domain.model.opbaas.ExceptionConfiguration;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExConfigurationDTO;
import com.zhiche.wms.service.opbaas.IExceptionConfigurationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 异常配置 服务实现类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@Service
public class ExceptionConfigurationServiceImpl extends ServiceImpl<ExceptionConfigurationMapper, ExceptionConfiguration> implements IExceptionConfigurationService {

    private ExceptionConfigurationMapper configurationMapper;

    @Autowired
    public void setConfigurationMapper(ExceptionConfigurationMapper configurationMapper) {
        this.configurationMapper = configurationMapper;
    }


    /**
     * <p>
     * 获取菜单信息--传入level1Code
     * </p>
     */
    @Override
    public List<ExConfigurationDTO> getConfigurationList(ExceptionRegisterParamDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空!");
        }
        String level1Code = dto.getLevel1Code();
        if (StringUtils.isBlank(level1Code)) {
            throw new BaseException("一级区域code不能为空!");
        }
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("level1Code", level1Code);
        params.put("status10", TableStatusEnum.STATUS_10.getCode());
        params.put("orderBy", "b.sort,c.sort");
        return configurationMapper.selectConfigurationListByParams(params);
    }

    @Override
    public String queryNameByCode (String code) {
        if (StringUtils.isEmpty(code)) {
            return "";
        }
        EntityWrapper<ExceptionConfiguration> conEw = new EntityWrapper<>();
        conEw.eq("code", code);
        List<ExceptionConfiguration> list = baseMapper.selectList(conEw);
        if (CollectionUtils.isEmpty(list)) {
            return "";
        }
        return list.get(0).getName();
    }
}
