package com.zhiche.wms.service.utils;

import com.zhiche.wms.core.supports.BaseException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 10:02 2019/4/23
 */
public class CommonMethod {

    /**
     *  过滤车架号的换行符
     * @param vin 车架号
     * @return 车架号数据
     */
    public static String[] setVins (String vin) {
        String[] split = vin.trim().replaceAll("\n",",").split(",");
        return split;
    }

    /**
     * 获取时间
     * @param date String类型的时间
     * @return 时间 yyyy-MM-dd HH:mm:ss
     */
    public static String getStringDate (Date date) {
        //设置要获取到什么样的时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //获取String类型的时间
        return sdf.format(date);
    }

    /**
     * 获取时间
     * @param date String类型的时间
     * @return 时间 yyyy-MM-dd HH:mm:ss
     */
    public static String getStringDate1 (Date date) {
        //设置要获取到什么样的时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        //获取String类型的时间
        return sdf.format(date);
    }


    /**
     *
     * @param shipTime
     * @return 返回格式 yyyyMMddHHmmss
     */
    private String stringToDate (String shipTime) {
        String resultDate = "";
        SimpleDateFormat sdftime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date date = sdftime.parse(shipTime);
            resultDate = sdf.format(date);
        } catch (ParseException e) {
            resultDate = sdf.format(new Date());
        }
        return resultDate;
    }

    /**
     * 时间格式取消特殊字符
     * yyyyMMddHHmmss
     */
    public static String datetoString (Date shipTime) {
        if (null == shipTime && Objects.isNull(shipTime)) {
            shipTime = new Date();
        }
        String resultDate = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        resultDate = sdf.format(shipTime);
        return resultDate;
    }

    /*
     * 将时间戳转换为时间
     * yyyy-MM-dd HH:mm:ss
     */
    public static Date stampToDate (String str){
        Date resultDate = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long longDate = new Long(str);
            Date date = new Date(longDate);
            String res = simpleDateFormat.format(date);
            resultDate = simpleDateFormat.parse(res);
        } catch (Exception e) {
            throw new BaseException(e.getMessage());
        }
        return resultDate;
    }

    /**
     * 获取时间 格式为：yyyyMMddHHmmss
     * @param shipTime 时间 yyyy-MM-dd HH:mm:ss
     */
    public static String getDate (String shipTime) {
        String resultDate = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat sdftime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = sdftime.parse(shipTime);
            resultDate = sdf.format(date);
        } catch (ParseException e) {
            resultDate = sdf.format(new Date());
        }
        return resultDate;
    }


    /**
     * 获取时间戳
     */
    public static String getOTMShipTme (String shipTime) {
        String resultDate = "";
        SimpleDateFormat sdftime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = sdftime.parse(shipTime);
            resultDate = String.valueOf(date.getTime());
        } catch (ParseException e) {
            resultDate = String.valueOf(new Date().getTime());
        }
        return resultDate;
    }


    /**
     * 时间转换
     * yyyy-MM-dd HH:mm:ss
     */
    public static Date stringToDate1 (String shipTime) {
        Date date = null;

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date dateSf = simpleDateFormat.parse(shipTime);

            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = simpleDateFormat1.format(dateSf);
            date = simpleDateFormat1.parse(strDate);
        } catch (Exception e) {
            throw new BaseException("时间转换异常");
        }
        return date;
    }


    /**
     * 时间转换
     * yyyy-MM-dd HH:mm:ss
     */
    public static Date stringToDate2 (String shipTime) {
        Date date = null;

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateSf = simpleDateFormat.parse(shipTime);

            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = simpleDateFormat1.format(dateSf);
            date = simpleDateFormat1.parse(strDate);
        } catch (Exception e) {
            throw new BaseException("时间转换异常");
        }
        return date;
    }


    /**
     * 时间转换
     * yyyy-MM-dd
     */
    public static Date dateFormat (Date paramDate) {
        Date date = null;
        try {
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = simpleDateFormat1.format(paramDate);
            date = simpleDateFormat1.parse(strDate);
        } catch (Exception e) {
            throw new BaseException("时间转换异常");
        }
        return date;
    }

    /**
     * 时间转换
     * yyyy-MM-dd
     */
    public static String dateFormatString (Date paramDate) {
        String date = null;
        try {
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
            date = simpleDateFormat1.format(paramDate);
        } catch (Exception e) {
            throw new BaseException("时间转换异常");
        }
        return date;
    }

    /**
     * amount 表示数字，整数为某个整数之后的时间，负数为某个之前的时间
     */
    public static Date afterHour (int amount) {
        Date timeDate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.HOUR, amount);
            Date d = c.getTime();
            String stringDate = format.format(d);
            timeDate = format.parse(stringDate);
        } catch (Exception e) {
            throw new BaseException("时间转换异常！");
        }
        return timeDate;
    }


    /**
     * 获取时间
     *
     * @param date String类型的时间
     * @return 时间
     */
    public static Date getDate (Date date) {
        Date timeDate;
        try {
            //设置要获取到什么样的时间
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //获取String类型的时间
            timeDate = sdf.parse(sdf.format(date));
        } catch (Exception e) {
            throw new BaseException("时间转换异常");
        }
        return timeDate;

    }

}
