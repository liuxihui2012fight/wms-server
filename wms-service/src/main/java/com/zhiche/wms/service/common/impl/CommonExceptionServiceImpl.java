package com.zhiche.wms.service.common.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.google.common.collect.Maps;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.qiniu.QiNiuUtil;
import com.zhiche.wms.domain.mapper.opbaas.ExceptionRegisterMapper;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExceptionRegisterDetailDTO;
import com.zhiche.wms.service.common.CommonExceptionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 公用的异常信息获取(因多个界面需要此信息,提取为公用(除提车外))
 */
@Service
public class CommonExceptionServiceImpl implements CommonExceptionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonExceptionServiceImpl.class);
    @Autowired
    private ExceptionRegisterMapper registerMapper;

    /**
     * 公用查询九宫格区域异常信息
     */
    @Override
    public List<ExceptionRegisterDetailDTO> getExceptionInfo(ExceptionRegisterParamDTO dto) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("CommonExceptionServiceImpl.getExceptionInfo params:{}", dto);
        }
        //区域信息
        if (dto == null) {
            throw new BaseException("参数不能为空!");
        }
        String vin = dto.getVin();
        String orderNo = dto.getOrderNo();
        String taskType = dto.getTaskType();
        if (StringUtils.isBlank(taskType)) {
            throw new BaseException("taskType不能为空!");
        }
        if (StringUtils.isBlank(vin)) {
            throw new BaseException("车架号不能为空!");
        }
        if (StringUtils.isBlank(orderNo)) {
            throw new BaseException("订单号不能为空!");
        }
        //查询任务详情,及异常信息
        HashMap<Object, Object> exceptionParam = Maps.newHashMap();
        exceptionParam.put("taskType", taskType);
        exceptionParam.put("status10", TableStatusEnum.STATUS_10.getCode());
        exceptionParam.put("dealing", TableStatusEnum.STATUS_20.getCode());
        exceptionParam.put("parentCode", TableStatusEnum.STATUS_0.getCode());
        exceptionParam.put("vin", vin);
        exceptionParam.put("orderNo", orderNo);
        exceptionParam.put("orderBy", "a.CODE");
        registerMapper.updateSQLMode();
        //区域下异常信息
        return registerMapper.countExDetail(exceptionParam);
    }

    @Override
    public int getExceptionTotal(ExceptionRegisterParamDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空");
        }
        Wrapper<Integer> ew = new EntityWrapper<>();
        if (StringUtils.isNotBlank(dto.getVin())) ew.eq("vin", dto.getVin());
        if (StringUtils.isNotBlank(dto.getOrderNo())) ew.eq("oms_order_no", dto.getOrderNo());
        ew.eq("status", TableStatusEnum.STATUS_10.getCode()).or().eq("status", TableStatusEnum.STATUS_20.getCode());
        ew.eq("deal_status", TableStatusEnum.STATUS_10.getCode()).or().eq("deal_status", TableStatusEnum.STATUS_20.getCode());
        return registerMapper.countExceptionTotal(ew);
    }

    @Override
    public Page<ExResultDTO> getAllExceptionPicUrl(ExceptionRegisterParamDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空!");
        }
        Page<ExResultDTO> page = new Page<>();

        Wrapper<Integer> ew = new EntityWrapper<>();

        String orderNo = dto.getOrderNo();
        String vin = dto.getVin();
        if (StringUtils.isBlank(orderNo)) {
            throw new BaseException("订单号不能为空!");
        }
        if (StringUtils.isBlank(vin)) {
            throw new BaseException("车架号不能为空!");
        }
        ew.eq("vin", vin);
        ew.eq("oms_order_no", orderNo);
        ew.eq("status", TableStatusEnum.STATUS_10.getCode());
        ew.andNew().eq("deal_status", TableStatusEnum.STATUS_10.getCode()).or().eq("deal_status", TableStatusEnum.STATUS_20.getCode());
        ew.orderBy("ex_level3 asc");
        page.setCurrent(dto.getPageNo());
        page.setSize(dto.getPageSize());
        HashMap<String, Object> pageMap = Maps.newHashMap();
        pageMap.put("start", page.getOffsetCurrent());
        pageMap.put("end", page.getSize());
        List<ExResultDTO> resultDTOS = registerMapper.queryExceptionDetailsByPage(pageMap, ew);
        if (CollectionUtils.isNotEmpty(resultDTOS)) {
            //返回图片的url
            resultDTOS.forEach(v -> {
                List<ExceptionRegisterDetailDTO> detailDTOS = v.getExceptionDetails();
                detailDTOS.forEach(e -> e.setPicUrl(QiNiuUtil.generateDownloadURL(e.getPicKey(), "")));
            });
        }
        page.setRecords(resultDTOS);
        return page;
    }


}
