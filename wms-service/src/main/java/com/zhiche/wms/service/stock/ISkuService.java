package com.zhiche.wms.service.stock;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.stock.Sku;
import com.zhiche.wms.domain.model.stock.SkuProperty;

/**
 * <p>
 * 库存量单位(Stock Keeping Unit) 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface ISkuService extends IService<Sku> {

    /**
     * 得到唯一的sku(根据sku属性-ownerId、materielId、uom、lotNo0~lotNo9得到SKU)
     *
     * @param skuProperty
     * @return
     */
    Sku getUniqueSku (SkuProperty skuProperty);

    void updateSkuInfo (String otmVin, String wmsVin, Long storeHouseId, String modifiedVehicleType, long putWayLocationId);

    Sku querySkuInfo (String vin);
}
