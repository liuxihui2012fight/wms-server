package com.zhiche.wms.service.dto;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 10:41 2019/11/21
 */
public class ReleaseResDTO {

    /**
     * 指令号
     */
    private String shipmentGid;
    /**
     * 系统运单号
     */
    private String releaseGid;
    /**
     * 车架号
     */
    private String vin;

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getReleaseGid () {
        return releaseGid;
    }

    public void setReleaseGid (String releaseGid) {
        this.releaseGid = releaseGid;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }
}
