package com.zhiche.wms.service.inbound;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;
import com.zhiche.wms.domain.model.log.ItfExplogLine;
import com.zhiche.wms.domain.model.stock.SkuStore;
import com.zhiche.wms.dto.inbound.*;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 入库通知明细 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IInboundNoticeLineService extends IService<InboundNoticeLine> {

    /**
     * 根据头ID更新明细的入库状态
     */
    boolean updateInboundStatus (Long headerId, Long noticeLineId);

    /**
     * 模糊查询入库单信息
     */
    Page<InboundNoticeDTO> selectInboundsPage(String key, Long houseIds, Integer size, Integer current);

    /**
     * 查询单个信息结果
     */
    InboundNoticeDTO selectInbound(Long queryParam, Long houseId);

    /**
     * 附加匹配扫码信息
     */
    InboundNoticeDTO selectInboundByQrCode(String queryParam, Long houseId);

    List<InboundNoticeDTO> selectInboundByHeadId(Long headId, Long houseIds);

    Page<InboundDTO> selectInboundTask(Page<InboundDTO> page);

    //未收取钥匙的已入库车辆信息 hgy 20200825
    Page<InboundPutawayDTO> selectNoKeyTask(Page<InboundPutawayDTO> page);

    /**
     * 批量入库
     */
    List<String> updateInboundConfirm (List<Long> ids, Long houseId, Long areaId);

    /**
     * 根据车架号查询详情
     */
    InboundDTO inboundDetailByVinNo(String vinNo, Long houseId);

    InboundDTO inboundDetailById(String id, Long houseId);

    /**
     * 获取库区list
     */
    List<StoreAreaAndLocationDTO> queryStoreLocation(Long houseId);

    String getBarcode(String vinNo);

    /**
     * 导出数据查询
     */
    List<InboundDTO> queryExportData(Map<String, String> condition);

    /**
     * 入库通知单查询
     */
    List<InboundNoticeLine> pageInboundNotice(Page<InboundNoticeLine> page, EntityWrapper<InboundNoticeLine> inEW);

    /**
     * 绑定实车停放位置
     */
    SkuStore updateBindStoreDetail(CommonConditionParamDTO dto);

    int selectCountWithHead(EntityWrapper<InboundNoticeLine> noticeLineEntityWrapper);

    /**
     * 仓库下的所有入库信息进行确认入库
     * @param key 仓库id
     * @return
     */
    List<String> storeHousePutway (String key);

    /**
     * 监控入库指令
     * @param condition 入参
     * @return 返回数据
     */
    Page<ShipmentInbound> queryInboundData (Page<ShipmentInbound> condition);

    /**
     * 查询推送给otm的入库数据
     * @param condition
     * @return
     */
    Page<ItfExplogLine> queryPushInboundOtm (Page<ItfExplogLine> condition);

    /**
     * 推送给otm的入库数据
     * @param condition
     * @return
     */
    void pushInboundOtm (Page<ItfExplogLine> condition);

    /**
     * 取消入库
     * @param params 入参
     * @return 返回值
     */
    String cancleInbound (List<String> params);

    /**
     * 取消入库
     * @param condition 入参
     * @return 返回值
     */
    Page<CancleInboundDTO> queryCancleInbound (Page<CancleInboundDTO> condition);
}
