package com.zhiche.wms.service.stockinit.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.stockinit.StockInitLineMapper;
import com.zhiche.wms.domain.model.stockinit.StockInitLine;
import com.zhiche.wms.service.stockinit.IStockInitLineService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 期初库存明细 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-05-30
 */
@Service
public class StockInitLineServiceImpl extends ServiceImpl<StockInitLineMapper, StockInitLine> implements IStockInitLineService {

}
