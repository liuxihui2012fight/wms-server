package com.zhiche.wms.service.otm;

import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.dto.opbaas.resultdto.TaskReleaseResultDTO;
import com.zhiche.wms.dto.shipmentDto.PushShipmentToOtmDto;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 运单 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
public interface IOtmOrderReleaseService extends IService<OtmOrderRelease> {

    List<TaskReleaseResultDTO> queryRealeaseInfo(HashMap<String, Object> params);

    List<PushShipmentToOtmDto> queryShipmentLogToOtm ();

    List<PushShipmentToOtmDto> pushInboundToOtm ();
}
