package com.zhiche.wms.service.certification;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.certification.CertificationInfo;

import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description: 合格证接口
 * @Date: Create in 10:10 2019/9/5
 */
public interface CertificationService extends IService<CertificationInfo> {
    /**
     * 收取合格证
     */
    List<String> receiveCertification (Map<String, String> param);

    /**
     * 查询合格证列表
     */
    Page<CertificationInfo> queryCertificationPage (Page<CertificationInfo> page);

    List<CertificationInfo> exportCertificationPage (Page<CertificationInfo> page);
}
