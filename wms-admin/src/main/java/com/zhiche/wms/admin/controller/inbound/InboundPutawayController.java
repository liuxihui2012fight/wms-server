package com.zhiche.wms.admin.controller.inbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.dto.inbound.InboundDTO;
import com.zhiche.wms.dto.inbound.InboundPutawayDTO;
import com.zhiche.wms.dto.inbound.LocChangeDTO;
import com.zhiche.wms.service.inbound.IInboundPutawayLineService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/inboundPutaway")
public class InboundPutawayController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InboundPutawayController.class);
    @Autowired
    private IInboundPutawayLineService iInboundPutawayLineService;

    @PostMapping("/selectPutawayPage")
    public RestfulResponse<Page<InboundPutawayDTO>> queryLinePage(@RequestBody Page<InboundPutawayDTO> page) {
        RestfulResponse<Page<InboundPutawayDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            Page<InboundPutawayDTO> outPage = iInboundPutawayLineService.queryLinePage(page);
            restfulResponse.setData(outPage);
        } catch (BaseException e) {
            LOGGER.error("Controller:" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("查询异常，请重试");
        }
        return restfulResponse;
    }

    @PostMapping("/exportINRData")
    public RestfulResponse<List<InboundPutawayDTO>> exportINRData(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<InboundPutawayDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        List<InboundPutawayDTO> data = iInboundPutawayLineService.queryExportData(condition);
        restfulResponse.setData(data);
        return restfulResponse;
    }

    @PostMapping("/getReceiveKeyInfo")
    public RestfulResponse<InboundPutawayDTO> getReceiveKeyInfo(@RequestBody Map<String, String> condition) {
        InboundPutawayDTO data = iInboundPutawayLineService.getReceiveKeyInfo(condition);
        return new RestfulResponse<>(0, "success", data);
    }

    @PostMapping("/getKeyPrintInfo")
    public RestfulResponse<InboundPutawayDTO> getKeyPrintInfo(@RequestBody Map<String, String> condition) {
        InboundPutawayDTO data = iInboundPutawayLineService.getKeyPrintInfo(condition);
        return new RestfulResponse<>(0, "success", data);
    }

    @PostMapping("/confirmReceiveKey")
    public RestfulResponse<String> confirmReceiveKey(@RequestBody Map<String, String> condition) {
        try {
            iInboundPutawayLineService.updateReceiveKey(condition);
        } catch (BaseException e) {
            //对自定义异常信息的处理
            return new RestfulResponse<>(e.getCode(), e.getMessage(), null);
        } catch (Exception e) {
            return new RestfulResponse<>(-1, e.getMessage(), null);
        }
        return new RestfulResponse<>(0, "success", null);
    }

    @PostMapping("/changeLocList")
    public RestfulResponse<Page<LocChangeDTO>> changeLocList(@RequestBody Page<LocChangeDTO> page) {
        Page<LocChangeDTO> data = iInboundPutawayLineService.queryChangLocList(page);
        return new RestfulResponse<>(0, "查询成功", data);
    }

    @PostMapping("/exportLocList")
    public RestfulResponse<List<LocChangeDTO>> exportLocList(@RequestBody Map<String,Object> condition) {
        List<LocChangeDTO> data = iInboundPutawayLineService.queryExportLocList(condition);
        return new RestfulResponse<>(0, "查询成功", data);
    }

    @PostMapping("/queryKeyStatus")
    public RestfulResponse queryKeyStatus(@RequestBody Map<String,String> condition) {
        RestfulResponse<Boolean> result = new RestfulResponse<>();
        result.setCode(0);
        result.setMessage("success");
        try{
            boolean flag = iInboundPutawayLineService.queryKeyStatus(condition);
            result.setData(flag);
        }catch (Exception e){
            result.setCode(-1);
            result.setMessage(e.getMessage());
            result.setData(false);
        }

        return result;
    }


    /**
     * 发放合格证书
     */
    @PostMapping("/giveOutCertificat")
    public RestfulResponse giveOutCertificat (@RequestBody Map<String, String> condition) {
        RestfulResponse restfulResponse = new RestfulResponse<>(0, "发放合格证成功", null);
        String result = iInboundPutawayLineService.giveOutCertificat(condition);
        if (StringUtils.isNotEmpty(result)) {

            List<String> list = new ArrayList<>();
            list.add(result);
            restfulResponse.setCode(-2);
            restfulResponse.setMessage("处理结果");
            restfulResponse.setData(list);
        }
        return restfulResponse;
    }

    /**
     * 领取合格证书
     */
    @PostMapping("/receiveOutCertificat")
    public RestfulResponse receiveOutCertificat (@RequestBody Map<String, String> condition) {
        RestfulResponse restfulResponse = new RestfulResponse<>(0, "领取合格证书成功", null);
        String result = iInboundPutawayLineService.receiveOutCertificat(condition);
        if (StringUtils.isNotEmpty(result)) {
            List<String> resultList = new ArrayList<>();
            resultList.add(result);
            restfulResponse.setMessage("处理结果");
            restfulResponse.setCode(-2);
            restfulResponse.setData(resultList);
        }
        return restfulResponse;
    }

    @PostMapping("/exportPutawayKeyData")
    public RestfulResponse<List<InboundPutawayDTO>> exportPutawayKeyData (@RequestBody Page<InboundPutawayDTO> page) {
        RestfulResponse<List<InboundPutawayDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        List<InboundPutawayDTO> outPage = iInboundPutawayLineService.exportPutawayKeyData(page);
        restfulResponse.setData(outPage);
        return restfulResponse;
    }

    /**
     * 入库记录界面取消入库
     */
    @PostMapping("/wmsCancleInbound")
    public RestfulResponse wmsCancleInbound (@RequestBody Map<String,String> param) {
        RestfulResponse restfulResponse = new RestfulResponse<>(0, "success", "操作成功");
        param.put("sourceType", TableStatusEnum.CANCLE_INBOUND_SOURCE.getCode());
        List<String> result = iInboundPutawayLineService.wmsCancleInbound(param);
        LOGGER.info("/wmsCancleInbound返回结果 ： {} ",result);
        if (CollectionUtils.isNotEmpty(result)) {
            restfulResponse.setMessage("处理结果");
            restfulResponse.setCode(-2);
            restfulResponse.setData(result);
        }
        return restfulResponse;
    }
}
