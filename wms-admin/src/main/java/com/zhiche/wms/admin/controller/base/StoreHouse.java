package com.zhiche.wms.admin.controller.base;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.service.base.IStorehouseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by zhaoguixin on 2018/6/9.
 */
@RestController
@RequestMapping("/sorehouse")
public class StoreHouse {

    private static final Logger LOGGER = LoggerFactory.getLogger(StoreHouse.class);

    @Autowired
    private IStorehouseService storehouseService;

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    @PostMapping("/queryPage")
    public RestfulResponse<Object> queryPage (@RequestBody Page<Storehouse> page) {
        LOGGER.info("ShouldPaymentController.queryPage page: {}", page);
        RestfulResponse<Object> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            Page<Storehouse> storehousePage = storehouseService.selectPage(page);
            result.setData(storehousePage);
        } catch (Exception e) {
            LOGGER.error("ShouldPaymentController.queryPage baseException: {}", e);
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }
}
