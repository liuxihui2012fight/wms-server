package com.zhiche.wms.admin.vo;

import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.inbound.FactoryPrepareInbound;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhaoguixin on 2018/6/10.
 */
public class AccountUserVo implements Serializable {
    private String accountCode;

    private Boolean isMain;

    private Integer entityId;

    private Integer belongTo;

    private Integer status;

    private String userCode;

    private Integer orgId;

    private String orgCode;

    private String orgName;

    private String userName;

    private String mobile;

    private String tel;

    private String email;

    private Integer gender;

    private String avatar;

    private Integer accountId;

    private String entityCode;

    private String entityName;

    private String pwd;

    private Integer userId;

    private List<String> roles;

    private List<Storehouse> storehouses;

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public Boolean getMain() {
        return isMain;
    }

    public void setMain(Boolean main) {
        isMain = main;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    public Integer getBelongTo() {
        return belongTo;
    }

    public void setBelongTo(Integer belongTo) {
        this.belongTo = belongTo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "AccountUserBo{" +
                "accountCode='" + accountCode + '\'' +
                ", isMain=" + isMain +
                ", entityId=" + entityId +
                ", belongTo=" + belongTo +
                ", status=" + status +
                ", userCode='" + userCode + '\'' +
                ", orgId=" + orgId +
                ", orgCode='" + orgCode + '\'' +
                ", orgName='" + orgName + '\'' +
                ", userName='" + userName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", tel='" + tel + '\'' +
                ", email='" + email + '\'' +
                ", gender=" + gender +
                ", avatar='" + avatar + '\'' +
                ", accountId=" + accountId +
                ", entityCode='" + entityCode + '\'' +
                ", entityName='" + entityName + '\'' +
                ", pwd='" + pwd + '\'' +
                ", userId=" + userId +
                ", roles=" + roles +
                '}';
    }

    public List<Storehouse> getStorehouses() {
        return storehouses;
    }

    public void setStorehouses(List<Storehouse> storehouses) {
        this.storehouses = storehouses;
    }
}
