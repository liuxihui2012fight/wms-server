package com.zhiche.wms.admin.controller.itf;

import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.service.otm.IOtmShipmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description: 提供bms系统相关接口
 * @Date: Create in 11:15 2019/7/2
 */
@RestController
@RequestMapping(value = "/bmsShipment")
public class BmsShipmentController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BmsShipmentController.class);

    @Autowired
    private IOtmShipmentService iOtmShipmentService;

    /**
     * 发运时间补推tms
     */
    @PostMapping("/toBmsShipDate")
    public RestfulResponse<String> toBmsShipDate (@RequestBody Map<String, List<String>> release) {
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0, "success", null);
        LOGGER.info("TMS调用补发发运时间至bms开始。。。。。。。。。。。。。。");
        iOtmShipmentService.toBmsShipDate(release.get("releaseGids"));
        LOGGER.info("TMS调用补发发运时间至bms结束。。。。。。。。。。。。。。");
        return restfulResponse;
    }

}
