package com.zhiche.wms.admin.surpports.schedule;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.opbaas.ExceptionRegisterMapper;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;
import com.zhiche.wms.service.opbaas.IExceptionRegisterService;
import com.zhiche.wms.service.utils.CommonMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description: 删除异常信息同步OTM
 * @Date: Create in 19:13 2020/3/10
 */
@Component
public class ExceptionSchedule {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionSchedule.class);
    @Autowired
    private ExceptionRegisterMapper exceptionRegisterMapper;
    @Autowired
    IExceptionRegisterService iExceptionRegisterService;

    @Scheduled(cron = "0 0 0/2 * * ?")
//    @Scheduled(cron = "0 * * * * ?")
    public void pushInboundToOtm () {
        LOGGER.info("定时获取删除异常库数据至otm,start.......................");
        Wrapper<ExResultDTO> pictureEw = new EntityWrapper<>();
        pictureEw.eq("a.status", TableStatusEnum.STATUS_0.getCode());
        pictureEw.ge("a.gmt_create", CommonMethod.afterHour(-6));
        pictureEw.le("a.gmt_create",new Date());
        List<ExResultDTO> pictureResult = exceptionRegisterMapper.selectExListDetail(new Page<>(), pictureEw);
        if (CollectionUtils.isNotEmpty(pictureResult)) {
            for (ExResultDTO exResultDTO : pictureResult) {
                ExceptionRegister register = exceptionRegisterMapper.selectById(exResultDTO.getId());
                Map<String, String> params = new HashMap<>();
                params.put("isCanSend", TableStatusEnum.STATUS_Y.getCode());
                params.put("lotNo1", register.getVin());
                params.put("houseId", register.getHouseId());
                params.put("deleted", TableStatusEnum.STATUS_0.getCode());

                iExceptionRegisterService.pushExceptionOTM(register, new ExResultDTO(), params);
            }
        }
        LOGGER.info("定时获取删除异常数据至otm,end.......................");
    }
}
