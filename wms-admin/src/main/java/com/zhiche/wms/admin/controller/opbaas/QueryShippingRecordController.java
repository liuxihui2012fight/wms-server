package com.zhiche.wms.admin.controller.opbaas;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.opbaas.paramdto.AppCommonQueryDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentRecordDTO;
import com.zhiche.wms.service.opbaas.IOrderReleaseService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 10:04 2018/12/20
 */
@RestController
@RequestMapping("/orderRelease")
public class QueryShippingRecordController {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryShippingRecordController.class);
    @Autowired
    private IOrderReleaseService releaseService;

    /**
     * 装车发运--模糊查询发车点下的发运列表,PC目前所用
     */
    @PostMapping("/queryShipRecordList")
    public RestfulResponse<Page<ShipmentRecordDTO>> queryShipRecordList (@RequestBody Page<ShipmentRecordDTO> page) {
        Page<ShipmentRecordDTO> data = releaseService.queryShipRecordList(page);
        return new RestfulResponse<>(0, "success", data);
    }

    /**
     * 确认发运，老方法，需和App确认
     * @param param
     * @return
     */
    @PostMapping(value = "/confirmShipment")
    @ApiOperation(value = "装车发运--确认发运(指令发运)")
    public RestfulResponse queryCarModelInfo (@RequestBody Map<String, String> param) {
        LOGGER.info("/confirmShipment （装车发运--确认发运） param : {} ", param);
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        String key = param.get("key");
        if (StringUtils.isEmpty(key)) {
            throw new BaseException(99, "请确认参数是否正确！");
        }
        //1、状态是否为已发运
        String[] keys = key.split(",");
        this.isRepeatShip(keys);

        //2、触发“确认发运”
        AppCommonQueryDTO dto = new AppCommonQueryDTO();
        Map<String, String> condition = new HashMap<>();
        for (String keyId : keys) {
            condition.put("key", keyId);
            dto.setCondition(condition);
            releaseService.updateShip(dto);
        }
        result.setData(keys);
        return result;
    }

    private void isRepeatShip (String[] keys) {
        List<String> tempList = new ArrayList<>();
        //判断是否有重复数据，如果没有就将数据装进临时集合
        for (String key : keys) {
            if (!tempList.contains(key)) {
                tempList.add(key);
            }
        }
        releaseService.isRepeatShipment(tempList);
    }

    /**
     * PC目前所用的单车发运
     * @param param
     * @return
     */
    @PostMapping(value = "/releaseDespatch")
    @ApiOperation(value = "装车发运--确认发运(运单发运，单车发运)")
    public RestfulResponse releaseDespatch (@RequestBody Map<String, String> param) {
        LOGGER.info("/confirmShipment （运单发运，单车发运） param : {} ", param);
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        List<String> resultList = releaseService.releaseDespatch(param);
        if (CollectionUtils.isNotEmpty(resultList)){
            result.setCode(-2);
            result.setMessage("处理结果");
            result.setData(resultList);
        }
        return result;
    }

    /**
     * 二手车列表查询
     */
    @PostMapping("/queryNewChannelReleaseList")
    public RestfulResponse<Page<ShipmentRecordDTO>> queryNewChannelReleaseList (@RequestBody Page<ShipmentRecordDTO> page) {
        Page<ShipmentRecordDTO> data = releaseService.queryNewChannelReleaseList(page);
        return new RestfulResponse<>(0, "success", data);
    }

    /**
     * PC目前所用的二手车发运
     * @param param
     * @return
     */
    @PostMapping(value = "/despatchNewChannel")
    @ApiOperation(value = "装车发运--确认发运(运单发运，二手车单车发运)")
    public RestfulResponse despatchNewChannel (@RequestBody Map<String, String> param) {
        LOGGER.info("/confirmShipment （运单发运，二手车单车发运） param : {} ", param);
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        releaseService.despatchNewChannel(param);
        return result;
    }


    /**
     * 取消发运
     */
    @PostMapping(value = "/cancleShipment")
    @ApiOperation(value = "取消发运")
    public RestfulResponse<String> cancleShipment(@RequestBody ShipmentRecordDTO param){
        LOGGER.info("/cancleShipment （取消发运） param : {} ", param);

        //TODO 记录接收取消发运到日志系统

        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        releaseService.cancleShipment(param.getShipmentGid(),param.getShipUser());
        return result;
    }

}
