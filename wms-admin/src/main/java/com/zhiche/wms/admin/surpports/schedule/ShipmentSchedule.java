package com.zhiche.wms.admin.surpports.schedule;

import com.zhiche.wms.service.otm.IOtmShipmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author: caiHua 补发发运数据至otm
 * @Description:
 * @Date: Create in 14:02 2019/6/17
 */
@Component
public class ShipmentSchedule {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShipmentSchedule.class);

    @Autowired
    private IOtmShipmentService iOtmShipmentService;

    @Scheduled(cron = "0 0 0/4 * * ?")
//    @Scheduled(cron = "0 * * * * ? ")
    public void pushShipmentLogOtm () {
        LOGGER.info("定时补发发运数据至otm,start.......................");
        iOtmShipmentService.pushShipmentLogToOtm();
        LOGGER.info("定时补发发运数据至otm,end.......................");
    }

    /**
     * 同步otm的临牌发运时间.每10分钟触发一次
     */
    @Scheduled(cron = "0 0 * * * ?")
//    @Scheduled(cron = "0 * * * * ? ")
    public void pullOtmShipDate () {
        LOGGER.info("同步otm的临牌发运时间,start.......................");
        iOtmShipmentService.pullOtmShipDate();
        LOGGER.info("同步otm的临牌发运时间,end.......................");
    }


    /**
     * 同步otm的水铁发运时间.每1小时触发一次
     */
//    @Scheduled(cron = "0 * * * * ?")
    @Scheduled(cron = "0 0 * * * ?")
    public void pullOtmWaterIronShipDate () {
        LOGGER.info("同步otm的水铁发运时间,start.......................");
        iOtmShipmentService.pullOtmWaterIronShipDate();
        LOGGER.info("同步otm的水铁发运时间,end.......................");
    }


}
