package com.zhiche.wms.admin.controller.sys;


import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.sys.SysConfig;
import com.zhiche.wms.service.sys.SysConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * WMS各功能节点配置 前端控制器
 * </p>
 *
 * @author yangzhiyu
 * @since 2018-11-30
 */
@Controller
@RequestMapping("/sysConfig")
public class SysConfigController {


    @Autowired
    private SysConfigService sysConfigService;

    @PostMapping(value = "/configPage")
    public RestfulResponse<Page<SysConfig>> queryPage(@RequestBody Page<SysConfig> page) {
        RestfulResponse<Page<SysConfig>> response = new RestfulResponse<>(0, "success", null);
        Page<SysConfig> sysConfigPage = sysConfigService.selectUserPage(page);
        response.setData(sysConfigPage);
        return response;
    }
}

