package com.zhiche.wms.admin.controller.stock;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.stock.FactoryPrepareStockDTO;
import com.zhiche.wms.dto.stock.StockDTO;
import com.zhiche.wms.dto.stock.StockQty;
import com.zhiche.wms.service.stock.IStockService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by zhaoguixin on 2018/7/7.
 */
@Controller
@RequestMapping(value = "/stock", produces = MediaType.APPLICATION_JSON_VALUE)
public class StockController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private IStockService stockService;

    @PostMapping(value = "/queryPage", consumes = "application/json")
    @ResponseBody
    public RestfulResponse<Page<StockDTO>> queryPage(@RequestBody Page<StockDTO> page) {
        LOGGER.info("Controller:stock/queryPage data: {}", page.toString());
        RestfulResponse<Page<StockDTO>> result = new RestfulResponse<>(0, "success");
        try {
            Page<StockDTO> stockDTOPage = stockService.queryPageStock(page);
            result.setData(stockDTOPage);
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    /**
     * 仓库管理锁定商品车所在储位
     * @return
     */
    @PostMapping(value = "/lockStock", consumes = "application/json")
    @ResponseBody
    public RestfulResponse<List<String>> lockStock(@RequestBody Map<String,Object> params) {
        LOGGER.info("Controller:stock/lockStock data: {}`", params);
        RestfulResponse<List<String>> result = new RestfulResponse<>(0, "success");
        try {
            stockService.lockStockBatch(params);
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    /**
     * 解锁储位
     * @param params
     * @return
     */
    @PostMapping(value = "/unlockStock", consumes = "application/json")
    @ResponseBody
    public RestfulResponse<List<String>> unlockStock(@RequestBody Map<String,Object> params) {
        LOGGER.info("Controller:stock/unlockStock data: {}", params);
        RestfulResponse<List<String>> result = new RestfulResponse<>(0, "success");
        try {
            stockService.unlockStockBatch(params);
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    /**
     * 更新库位
     */
    @PostMapping("/updateStockLocation")
    @ResponseBody
    public RestfulResponse<Object> updateStockLocation (@RequestBody Map<String, String> condition) {
        stockService.updateStockLocation(condition);
        return new RestfulResponse<>(0, "调整成功");
    }

    /**
     * 无单车更新库位
     */
    @PostMapping("/updateFactoryLocation")
    @ResponseBody
    public RestfulResponse<Object> updateFactoryLocation (@RequestBody Map<String, String> condition) {
        stockService.updateFactoryStockLocation(condition);
        return new RestfulResponse<>(0, "调整成功");
    }

    /**
     * 库存导出
     */
    @PostMapping(value = "/exportStockData")
    @ResponseBody
    public RestfulResponse<List<StockDTO>> exportStockData(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<StockDTO>> result = new RestfulResponse<>(0, "success");
        List<StockDTO> data = stockService.exportStockData(condition);
        result.setData(data);
        return result;
    }

    /**
     * 查询实际库存
     */
    @PostMapping(value = "/queryActualStock")
    @ResponseBody
    public RestfulResponse<List<Map<String,String>>> queryActualStock(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<Map<String,String>>> result = new RestfulResponse<>(0, "success");
        List<Map<String,String>> list = stockService.queryActualStock(condition);
        result.setData(list);
        return result;
    }

    /**
     * 取消入库清理对应的库存记录
     *
     * @param param
     * @return
     */
    @PostMapping("/cleanStock")
    @ResponseBody
    public RestfulResponse cleanStock (@RequestBody Map<String,String> param) {
         stockService.cleanStock(param);
        return  new RestfulResponse<>(0, "操作成功",null);
    }

    @PostMapping(value = "/queryStockQty", consumes = "application/json")
    @ResponseBody
    public RestfulResponse<Page<StockQty>> queryStockQty (@RequestBody Page<StockQty> page) {
        LOGGER.info("Controller:stock/queryStockQty data: {}", page.toString());
        RestfulResponse<Page<StockQty>> result = new RestfulResponse<>(0, "success");
        Page<StockQty> stockDTOPage = stockService.queryStockQty(page);
        result.setData(stockDTOPage);
        return result;
    }

    @PostMapping(value = "/queryStockQtyDetail", consumes = "application/json")
    @ResponseBody
    public RestfulResponse<List<StockQty>> queryStockQtyDetail (@RequestBody Map<String,Object> param) {
        LOGGER.info("Controller:stock/queryStockQtyDetail data: {}", param);
        RestfulResponse<List<StockQty>> result = new RestfulResponse<>(0, "success");
        List<StockQty> stockDTOPage = stockService.queryStockQtyDetail(param);
        result.setData(stockDTOPage);
        return result;
    }

    /**
     * 查询工厂未备料库存数据
     */
    @PostMapping(value = "/queryFactoryStock", consumes = "application/json")
    @ResponseBody
    public RestfulResponse<Page<FactoryPrepareStockDTO>> queryFactoryStock (@RequestBody Page<FactoryPrepareStockDTO> page) {
        LOGGER.info("Controller:/stock/queryFactoryStock data: {}", page);
        RestfulResponse<Page<FactoryPrepareStockDTO>> result = new RestfulResponse<>(0, "success");
        result.setData(stockService.queryFactoryStock(page));
        return result;
    }


}
