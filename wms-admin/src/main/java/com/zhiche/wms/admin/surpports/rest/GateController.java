package com.zhiche.wms.admin.surpports.rest;

import com.zhiche.wms.service.base.IGateControlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gateController")
@Api(value = "/gateController", description = "重庆库闸门控制")
public class GateController {

    @Autowired
    private IGateControlService gateService;


    @ApiOperation(value = "inboundOpen-API", notes = "入库闸门自动开启控制")
    @RequestMapping("/inboundOpenToPlateNum")
    public void inboundOpen() {
        gateService.saveInboundOpen();
    }
}
