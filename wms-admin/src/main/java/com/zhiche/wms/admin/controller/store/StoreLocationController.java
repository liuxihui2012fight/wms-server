package com.zhiche.wms.admin.controller.store;


import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.model.base.StoreArea;
import com.zhiche.wms.domain.model.base.StoreLocation;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.base.ValidArea;
import com.zhiche.wms.service.base.IStoreAreaService;
import com.zhiche.wms.service.base.IStoreLocationService;
import com.zhiche.wms.service.base.IStorehouseService;
import com.zhiche.wms.service.utils.CommonFields;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 储位配置 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2018-08-15
 */
@Controller
@RequestMapping("/storeLocation")
public class StoreLocationController {

    @Autowired
    private IStoreLocationService storeLocationService;
    @Autowired
    private IStoreAreaService storeAreaService;
    @Autowired
    private IStorehouseService iStorehouseService;

    @PostMapping(value = "/queryLocationByPage")
    @ResponseBody
    public RestfulResponse<Page<StoreLocation>> queryLocationPage(@RequestBody Page<StoreLocation> page) {
        RestfulResponse<Page<StoreLocation>> result = new RestfulResponse<>(0, "success");
        Page<StoreLocation> resultPage = storeLocationService.getUsableLocationByPage(page);
        result.setData(resultPage);
        return result;
    }

    @PostMapping(value = "/addStoreLocation")
    @ResponseBody
    public RestfulResponse<String> addStoreLocation(@RequestBody StoreLocation storeLocation) {
        RestfulResponse<String> result = new RestfulResponse<>(0, "success");
        if (storeLocationService.getCountByCodeName(storeLocation) < 1) {
            boolean insert = storeLocationService.insert(storeLocation);
            if (insert) {
                return result;
            } else {
                result.setCode(-1);
                result.setMessage("新增库位失败，请重试");
                return result;
            }
        } else {
            result.setCode(-1);
            result.setMessage("库位已存在");
            return result;
        }
    }

    @PostMapping(value = "/updateStoreLocation")
    @ResponseBody
    public RestfulResponse<String> updateStoreLocation(@RequestBody StoreLocation storeLocation) {
        RestfulResponse<String> result = new RestfulResponse<>(0, "success");
        boolean update = storeLocationService.updateStoreLocation(storeLocation);
        if (update) {
            return result;
        } else {
            result.setCode(-1);
            result.setMessage("编辑库位失败，请重试");
            return result;
        }
    }

    /**
     * 导入期初库存 保存状态status20
     */
    @PostMapping("/importLocationInit")
    @ResponseBody
    public RestfulResponse<Map<String, JSONArray>> saveImportLocationInit (@RequestBody String data) {
        Map<String, JSONArray> result = storeLocationService.saveImportLocation(data);
        return new RestfulResponse<>(0, "请求成功", result);
    }

    @PostMapping("/queryStoreArea")
    @ResponseBody
    public RestfulResponse<List<Map<String, Object>>> queryStoreArea () {
        RestfulResponse<List<Map<String, Object>>> restfulResponse = new RestfulResponse<>(0, "success", null);
        EntityWrapper<Storehouse> storehouseEntityWrapper = new EntityWrapper<>();
        storehouseEntityWrapper.ne("property", TableStatusEnum.STATUS_10.getCode());
        List<Storehouse> storeHouses = iStorehouseService.selectList(storehouseEntityWrapper);
        List<Map<String, Object>> result = new ArrayList<>();
        for (Storehouse storehouse : storeHouses) {
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put(CommonFields.STORE_HOUSE_ID_1.getCode(), String.valueOf(storehouse.getId()));
            resultMap.put(CommonFields.STORE_HOUSE_Name_1.getCode(), storehouse.getName());
            List<ValidArea> validAreas = storeAreaService.queryWDValidArea(storehouse.getId());
            resultMap.put("areaList", validAreas);
            result.add(resultMap);
        }
        restfulResponse.setData(result);
        return restfulResponse;
    }

    @PostMapping("/queryMovementArea")
    @ResponseBody
    public RestfulResponse<List<StoreArea>> queryMovementArea (@RequestBody Map<String, String> param) {
        RestfulResponse<List<StoreArea>> restfulResponse = new RestfulResponse<>(0, "success", null);
        List<StoreArea> validAreas = storeAreaService.queryMovementArea(param);
        restfulResponse.setData(validAreas);
        return restfulResponse;
    }

}

