package com.zhiche.wms.admin.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.util.List;

public class InboundVo {
    private String key;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long houseId;
    private List<Long> lineIds;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long areaId;

    public Long getAreaId () {
        return areaId;
    }

    public void setAreaId (Long areaId) {
        this.areaId = areaId;
    }

    public String getKey () {
        return key;
    }

    public void setKey (String key) {
        this.key = key;
    }

    public Long getHouseId () {
        return houseId;
    }

    public void setHouseId (Long houseId) {
        this.houseId = houseId;
    }

    public List<Long> getLineIds () {
        return lineIds;
    }

    public void setLineIds (List<Long> lineIds) {
        this.lineIds = lineIds;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("InboundVo{");
        sb.append("key='").append(key).append('\'');
        sb.append(", houseId=").append(houseId);
        sb.append(", lineIds=").append(lineIds);
        sb.append('}');
        return sb.toString();
    }

}
