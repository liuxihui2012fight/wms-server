package com.zhiche.wms.admin.surpports.schedule;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.service.mqtt.PublishSubscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: caiHua
 * @Description: 本类禁止启用
 * @Date: Create in 9:23 2019/8/29
 */
@Component
public class MQTTSchedule {
    @Autowired
    private OtmOrderReleaseMapper otmOrderReleaseMapper;

//    @Scheduled(cron = "0 0/2 * * * ?")
    public void publishData () {
        Page<OtmOrderRelease> page = new Page<>();
        page.setSize(2000);
        EntityWrapper<OtmOrderRelease> ew = new EntityWrapper<>();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode());
        List<OtmOrderRelease> list = otmOrderReleaseMapper.selectPage(page, ew);
        String json = JSONObject.toJSONString(list);
        PublishSubscribe.publish(json);
        PublishSubscribe.subscribe();
    }

}
