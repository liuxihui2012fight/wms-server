package com.zhiche.wms.admin.controller.certification;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.certification.CertificationInfo;
import com.zhiche.wms.service.certification.CertificationService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description: 合格证
 * @Date: Create in 10:05 2019/9/5
 */
@RestController
@RequestMapping("/certification")
public class CertificationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CertificationController.class);

    @Autowired
    private CertificationService certificationService;

    @PostMapping(value = "/updateCertification")
    @ApiOperation(value = "收取合格证")
    public RestfulResponse<Object> receiveCertification (@RequestBody Map<String, String> param) {
        RestfulResponse<Object> restfulResponse = new RestfulResponse<>(0, "success", "操作成功");
        LOGGER.info("/updateCertification （收取合格证） params : {} ", param);
        List<String> reslut = certificationService.receiveCertification(param);
        if (CollectionUtils.isNotEmpty(reslut)) {
            restfulResponse.setCode(-2);
            restfulResponse.setMessage("处理结果");
            restfulResponse.setData(reslut);
        }
        return restfulResponse;
    }



    @PostMapping(value = "/queryCertificationPage")
    @ApiOperation(value = "查询合格证列表")
    public RestfulResponse<Page<CertificationInfo>> queryCertificationPage (@RequestBody Page<CertificationInfo> page) {
        LOGGER.info("/queryCertificationPage （查询合格证列表） page : {} ", page);
        RestfulResponse<Page<CertificationInfo>> result = new RestfulResponse<>(0, "success", null);
        Page<CertificationInfo> certificationInfoPage = certificationService.queryCertificationPage(page);
        result.setData(certificationInfoPage);
        return result;
    }

    @PostMapping(value = "/exportCertificationPage")
    @ApiOperation(value = "查询合格证列表")
    public RestfulResponse<List<CertificationInfo>> exportCertificationPage (@RequestBody Page<CertificationInfo> page) {
        LOGGER.info("/exportCertificationPage （查询合格证列表） page : {} ", page);
        RestfulResponse<List<CertificationInfo>> result = new RestfulResponse<>(0, "success", null);
        List<CertificationInfo> certificationInfoPage = certificationService.exportCertificationPage(page);
        result.setData(certificationInfoPage);
        return result;
    }

}
