package com.zhiche.wms.admin.surpports.rest;


import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.service.surpports.IHnInterfaceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Api(value = "/WmsInterfaceForHN", description = "WMS提供霍尼接口")
@RestController
@RequestMapping("/WmsInterfaceForHN")
public class HnInterfaceController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HnInterfaceController.class);

    @Autowired
    private IHnInterfaceService hnInterfaceService;

    /**
     * <p>
     * 2018-8-22 wms_v2对接霍尼韦尔扫码自动入库功能
     * </p>
     */
    @ApiOperation(value = "inboundCar-API", notes = "扫码自动入库接口")
    @ApiResponses({@ApiResponse(code = 0, message = "操作成功", response = HashMap.class),
            @ApiResponse(code = -1, message = "操作失败")})
    @PostMapping(value = "/inboundCar")
    public RestfulResponse<HashMap<String, Object>> inboundCar(@RequestBody Map<String, String> condition) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("WmsInterfaceForHNController.inboundCar param:{}", condition);
        }
        HashMap<String, Object> data = hnInterfaceService.updateInboundCar(condition);
        return new RestfulResponse<>(0, "操作成功", data);
    }

    /**
     * <p>
     * 2018-8-22 wms_v2扫码自动出库
     * </p>
     */
    @ApiOperation(value = "outboundCar-API", notes = "扫码自动出库接口")
    @ApiResponses({@ApiResponse(code = 0, message = "操作成功", response = HashMap.class),
            @ApiResponse(code = -1, message = "操作失败")})
    @PostMapping(value = "/outboundCar")
    public RestfulResponse<HashMap<String, Object>> outboundCar(@RequestBody Map<String, String> condition) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("WmsInterfaceForHNController.outboundCar param:{}", condition);
        }
        HashMap<String, Object> data = hnInterfaceService.updateOutBoundCar(condition);
        return new RestfulResponse<>(0, "操作成功", data);
    }
}
