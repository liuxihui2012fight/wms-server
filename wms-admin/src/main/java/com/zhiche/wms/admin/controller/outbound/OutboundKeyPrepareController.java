package com.zhiche.wms.admin.controller.outbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.dto.outbound.*;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @Author: caiHua
 * @Description: 钥匙备料
 * @Date: Create in 16:43 2018/12/19
 */
@RestController
@RequestMapping("/keyPrepare")
public class OutboundKeyPrepareController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OutboundKeyPrepareController.class);

    @Autowired
    private IOutboundPrepareHeaderService prepareHeaderService;


    @PostMapping(value = "/queryKeyPreparePage")
    @ApiOperation(value = "钥匙备料数据查询")
    public RestfulResponse<Page<PreparationListHeadDTO>> queryKeyPreparePage (@RequestBody Page<PreparationListHeadDTO> page) {
        LOGGER.info("/queryKeyPreparePage （钥匙备料数据查询） params : {} ", page);
        RestfulResponse<Page<PreparationListHeadDTO>> result = new RestfulResponse<>(0, "success", null);
        Page<PreparationListHeadDTO> list = prepareHeaderService.queryOutKeyPreparePage(page);
        result.setData(list);
        return result;
    }

    /**
     * 查询钥匙备料详情
     */
    @PostMapping(value = "/getKeyPrepareDetail")
    @ApiOperation(value = "钥匙备料详情数据查询")
    public RestfulResponse<List<PreparationListDetailDTO>> getKeyPrepareDetail (@RequestBody OutboundPreparationParamDTO dto) {
        LOGGER.info("/queryKeyPreparePage （钥匙备料数据查询） params : {} ", dto);
        RestfulResponse<List<PreparationListDetailDTO>> result = new RestfulResponse<>(0, "success", null);
        List<PreparationListDetailDTO> list = prepareHeaderService.getKeyPrepareDetail(dto);
        result.setData(list);
        return result;
    }

    /**
     * 修改钥匙的备料状态
     */
    @PostMapping(value = "/updateKeyPrepareStatus")
    @ApiOperation(value = "修改钥匙的备料状态")
    public RestfulResponse updateKeyPrepareStatus (@RequestBody Map<String, String> param) {
        LOGGER.info("/updateKeyPrepareStatus （修改钥匙的备料状态） params : {} ", param);
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        prepareHeaderService.updateKeyPrepareStatus(param);
        return result;
    }

    /**
     * 打印备料单同时进行确认备料
     */
    @PostMapping(value = "/confirmPrepare")
    @ApiOperation(value = "打印备料单同时进行确认备料")
    public RestfulResponse confirmPrepare (@RequestBody Map<String, String> param) {
        LOGGER.info("/confirmPrepare （打印备料单同时进行确认备料） params : {} ", param);
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        List<BatchPrintPrepare> list = prepareHeaderService.batchPrintPrepare(param);
        if(CollectionUtils.isNotEmpty(list)){
            prepareHeaderService.confirmPrepare(list);
        }
        return result;
    }



    /**
     * 查询钥匙备料打印详细数据查询
     *
     */
    @PostMapping(value = "/queryKeyPerparePrintData")
    @ApiOperation(value = "查询钥匙备料打印详细数据查询")
    public RestfulResponse<List<PreparationPrintDTO>> queryKeyPerparePrintData (@RequestBody Map<String, String> params) {
        LOGGER.info("/queryKeyPreparePage （钥匙备料数据查询） params : {} ", params);
        RestfulResponse<List<PreparationPrintDTO>> result = new RestfulResponse<>(0, "success", null);
        List<PreparationPrintDTO> list = prepareHeaderService.queryKeyPerparePrintData(params);
        result.setData(list);
        return result;
    }

    /**
     * 批量打印备料单
     */
    @PostMapping(value = "/batchPrintPrepare")
    @ApiOperation(value = "批量打印备料单")
    public RestfulResponse<List<BatchPrintPrepare>> batchPrintPrepare (@RequestBody Map<String, String> params) {
        LOGGER.info("/batchPrintPrepare （批量打印备料单） params : {} ", params);
        RestfulResponse<List<BatchPrintPrepare>> result = new RestfulResponse<>(0, "success", null);
        List<BatchPrintPrepare> list = prepareHeaderService.batchPrintPrepare(params);
        result.setData(list);
        return result;
    }

}
