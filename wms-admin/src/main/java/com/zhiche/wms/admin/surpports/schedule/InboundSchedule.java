package com.zhiche.wms.admin.surpports.schedule;

import com.zhiche.wms.core.supports.enums.InterfaceEventEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.service.log.IItfImplogHeaderService;
import com.zhiche.wms.service.otm.IOtmShipmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * wms和TMS 系统对接的接口
 */
@Component
public class InboundSchedule {
    private static final Logger LOGGER = LoggerFactory.getLogger(InboundSchedule.class);
    @Autowired
    private IOtmShipmentService iOtmShipmentService;

    @Autowired
    private IItfImplogHeaderService logHeaderService;

    //fix 调整ILS切换关闭定时任务
    //@Scheduled(cron = "0 1/5 * * * ?")
    public void inboundDataFromTMSBySchedule () {
        logHeaderService.saveInboundDataFromTMSBySchedule();
    }

    /**
     * 补发入库数据至otm，每四个小时跑一次线程
     */
    @Scheduled(cron = "0 0 0/4 * * ?")
//    @Scheduled(cron = "0 * * * * ? ")
    public void pushInboundToOtm () {
        LOGGER.info("定时补发入库数据至otm,start.......................");
        iOtmShipmentService.pushInboundToOtm(TableStatusEnum.STATUS_BS_INBOUND.getCode(), InterfaceEventEnum.BS_OP_DELIVERY.getCode());
        LOGGER.info("定时补发入库数据至otm,end.......................");
    }
}
