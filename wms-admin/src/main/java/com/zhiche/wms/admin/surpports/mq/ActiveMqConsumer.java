package com.zhiche.wms.admin.surpports.mq;

import com.zhiche.wms.service.log.IItfImplogHeaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * activemq 消费者
 */
@Component
public class ActiveMqConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActiveMqConsumer.class);

    @Autowired
    private IItfImplogHeaderService iItfImplogHeaderService;


    /**
     * queue 点对点
     */
    //@JmsListener(destination = "erp_to_wms_ship_queue_test", containerFactory = "jmsListenerContainerQueue")
    public void updateOutboundNoticeFromTMS(String message) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("ActiveMqConsumer.updateOutboundNoticeFromTMS ---->start");
        }
        iItfImplogHeaderService.updateOutboundNoticeFromTMS(message);

    }


    ///**
    // * topic 订阅者消费
    // */
    //@JmsListener(destination = "", containerFactory = "jmsListenerContainerTopic")
    //public void messageFromTopic() {
    //
    //}


}
