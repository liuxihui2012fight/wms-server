package com.zhiche.wms.admin.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.util.Date;

/**
 * Created by zhaoguixin on 2018/6/23.
 */
public class OutboundOrderVo {

    private String customer;
    private String ownerNo;
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storehouseId;
    private String storehouseName;
    private String vehicleMode;
    private String vin;
    private int qty;
    private String carrierName;
    private Date expectOutboundDate;
    private String plateNumber;
    private String destLocationDetail;
    String strQty;

    String expectArriveDate;

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getOwnerNo() {
        return ownerNo;
    }

    public void setOwnerNo(String ownerNo) {
        this.ownerNo = ownerNo;
    }

    public Long getStorehouseId() {
        return storehouseId;
    }

    public void setStorehouseId(Long storehouseId) {
        this.storehouseId = storehouseId;
    }

    public String getStorehouseName() {
        return storehouseName;
    }

    public void setStorehouseName(String storehouseName) {
        this.storehouseName = storehouseName;
    }

    public String getVehicleMode() {
        return vehicleMode;
    }

    public void setVehicleMode(String vehicleMode) {
        this.vehicleMode = vehicleMode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getStrQty() {
        return strQty;
    }

    public void setStrQty(String strQty) {
        this.strQty = strQty;
    }

    public Date getExpectOutboundDate() {
        return expectOutboundDate;
    }

    public void setExpectOutboundDate(Date expectOutboundDate) {
        this.expectOutboundDate = expectOutboundDate;
    }

    public String getExpectArriveDate() {
        return expectArriveDate;
    }

    public void setExpectArriveDate(String expectArriveDate) {
        this.expectArriveDate = expectArriveDate;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDestLocationDetail() {
        return destLocationDetail;
    }

    public void setDestLocationDetail(String destLocationDetail) {
        this.destLocationDetail = destLocationDetail;
    }
}
