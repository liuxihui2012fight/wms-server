package com.zhiche.wms.admin.controller.store;


import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.base.StoreArea;
import com.zhiche.wms.domain.model.base.ValidArea;
import com.zhiche.wms.service.base.IStoreAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库区配置 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2018-08-15
 */
@RestController
@RequestMapping("/storeArea")
public class StoreAreaController {

    @Autowired
    private IStoreAreaService storeAreaService;

    @PostMapping("/queryByPage")
    public RestfulResponse<Page<StoreArea>> queryByPage(@RequestBody Page<StoreArea> page) {
        RestfulResponse<Page<StoreArea>> restfulResponse = new RestfulResponse<>(0, "success");
        Page<StoreArea> resultPage = storeAreaService.queryByPage(page);
        restfulResponse.setData(resultPage);
        return restfulResponse;
    }

    @PostMapping("/addStoreArea")
    public RestfulResponse<String> addStoreArea(@RequestBody StoreArea storeArea) {
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0, "success");
        //判断同一仓库下是否存在该库区
        if (storeAreaService.getCountByCodeName(storeArea) < 1) {
            boolean insert = storeAreaService.insert(storeArea);
            if (insert) {
                return restfulResponse;
            } else {
                restfulResponse.setCode(-1);
                restfulResponse.setMessage("新增库区失败，请重试");
                return restfulResponse;
            }
        } else {
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("库区已存在");
            return restfulResponse;
        }
    }

    @PostMapping("/updateStoreArea")
    public RestfulResponse<String> updateStoreArea(@RequestBody StoreArea storeArea) {
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0, "success");
        boolean update = storeAreaService.updateStoreArea(storeArea);
        if (update) {
            return restfulResponse;
        } else {
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("更新库区失败，请重试");
            return restfulResponse;
        }
    }

    /**
     * 批量禁用库区
     *
     * @param params 库区id
     */
    @PostMapping("/batchUpdateStatus")
    public RestfulResponse<String> batchUpdateStatus (@RequestBody Map<String,String> params) {
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0, "success");
        storeAreaService.batchUpdateStatus(params);
        return restfulResponse;
    }

    /**
     * 批量修改库位类型
     *
     * @param params 库区id，库位类型locationType
     */
    @PostMapping("/batchUpdateLocationType")
    public RestfulResponse<String> batchUpdateLocationType (@RequestBody Map<String,String> params) {
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0, "success");
        storeAreaService.batchUpdateStatus(params);
        return restfulResponse;
    }

    /**
     * 查询有效库区
     */
    @PostMapping("/queryValidArea")
    public RestfulResponse<List<ValidArea>> queryValidArea (@RequestBody Map<String, String> params) {
        RestfulResponse<List<ValidArea>> restfulResponse = new RestfulResponse<>(0, "success");
        if (null == params || params.isEmpty()) {
            throw new BaseException("仓库【storeHouseId】不能为空");
        }
        List<ValidArea> validAreas = storeAreaService.queryValidArea(params.get("storeHouseId"));
        restfulResponse.setData(validAreas);
        return restfulResponse;
    }


}

