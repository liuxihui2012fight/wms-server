package com.zhiche.wms.admin.controller.inbound;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.log.ItfExplogLine;
import com.zhiche.wms.dto.base.ResultDTO;
import com.zhiche.wms.dto.inbound.InboundDTO;
import com.zhiche.wms.dto.inbound.InboundNoticeParamDTO;
import com.zhiche.wms.dto.inbound.ShipmentInbound;
import com.zhiche.wms.service.inbound.IInboundNoticeHeaderService;
import com.zhiche.wms.service.inbound.IInboundNoticeLineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * wms入库controller
 * </p>
 */
@RestController
@RequestMapping("/inboundNotice")
public class InboundNoticeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InboundNoticeController.class);
    @Autowired
    private IInboundNoticeHeaderService iInboundNoticeHeaderService;
    @Autowired
    private IInboundNoticeLineService inboundNoticeLineService;

    /**
     * 保存或者更新入库通知单
     */
    @PostMapping("/saveInboundNotice")
    public ResultDTO<Object> saveOrUpdateInboundNotice(@RequestBody List<InboundNoticeParamDTO> paramDTOS) {
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "操作成功!");
        Map<String, Object> exMap = iInboundNoticeHeaderService.saveInboundNotice(paramDTOS);
        if (!exMap.isEmpty()) {
            resultDTO.setData(null);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(JSONObject.toJSONString(exMap));
        }
        return resultDTO;
    }

    /**
     * 查询入库通知的分页数据
     */
    @PostMapping("/selectInboundPage")
    public RestfulResponse<Page<InboundDTO>> selectInboundPage(@RequestBody Page<InboundDTO> page) {
        RestfulResponse<Page<InboundDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            Page<InboundDTO> inboundDTOPage = inboundNoticeLineService.selectInboundTask(page);
            restfulResponse.setData(inboundDTOPage);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 入库通知  导出
     */
    @PostMapping("/exportINData")
    public RestfulResponse<List<InboundDTO>> selectExportData(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<InboundDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        List<InboundDTO> data = inboundNoticeLineService.queryExportData(condition);
        restfulResponse.setData(data);
        return restfulResponse;
    }


    /**
     * OTM监控入库指令
     */
    @PostMapping("/queryInboundData")
    public RestfulResponse<Page<ShipmentInbound>> queryInboundData(@RequestBody Page<ShipmentInbound> condition) {
        RestfulResponse<Page<ShipmentInbound>> restfulResponse = new RestfulResponse<>(0, "success", null);
        LOGGER.info("/queryShipmentInbound查询入库数据入参conditon{}",condition);
        Page<ShipmentInbound> data = inboundNoticeLineService.queryInboundData(condition);
        restfulResponse.setData(data);
        return restfulResponse;
    }

    /**
     * 查询推送给otm的入库数据
     */
    @PostMapping("/queryPushInboundOtm")
    public RestfulResponse<Page<ItfExplogLine>> queryPushInboundOtm(@RequestBody Page<ItfExplogLine> condition) {
        RestfulResponse<Page<ItfExplogLine>> restfulResponse = new RestfulResponse<>(0, "success", null);
        Page<ItfExplogLine> data = inboundNoticeLineService.queryPushInboundOtm(condition);
        restfulResponse.setData(data);
        return restfulResponse;
    }


    /**
     * 入库回传数据重新推送
     */
    @PostMapping("/pushInboundOtm")
    public RestfulResponse<String> pushInboundOtm(@RequestBody Page<ItfExplogLine> condition) {
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0, "success", null);
        inboundNoticeLineService.pushInboundOtm(condition);
        return restfulResponse;
    }

    /**
     * 取消入库
     */
    @PostMapping("/cancleInbound")
    public RestfulResponse<String> cancleInbound(@RequestBody List<String> params) {
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0, "success", null);
        String result = inboundNoticeLineService.cancleInbound(params);
        return restfulResponse;
    }

}
