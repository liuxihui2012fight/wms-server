package com.zhiche.wms.admin.controller.outbound;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.zhiche.wms.admin.vo.OutboundOrderVo;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.service.base.IBusinessDocNumberService;
import com.zhiche.wms.service.base.IStorehouseService;
import com.zhiche.wms.service.constant.MeasureUnit;
import com.zhiche.wms.service.constant.Status;
import com.zhiche.wms.service.outbound.IOutboundNoticeHeaderService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by zhaoguixin on 2018/6/23.
 */
@RestController
@RequestMapping("/outbound/orderImport")
public class OutOrderImport {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutOrderImport.class);

    @Autowired
    private IOutboundNoticeHeaderService outboundNoticeHeaderService;

    @Autowired
    private IOutboundNoticeLineService outboundNoticeLineService;

    @Autowired
    private IStorehouseService storehouseService;

    @Autowired
    private IBusinessDocNumberService businessDocNumberService;

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static Map<String, Storehouse> storehouseMap = new HashMap<>();

    @Autowired
    private SnowFlakeId snowFlakeId;

    /**
     * 更新
     *
     * @param data
     * @return
     */
    @PostMapping("/importData")
    public RestfulResponse<Map<String, JSONArray>> importData(@RequestBody String data) {
        LOGGER.info("controller:/outbound/orderImport/importData data: {}", data);
        JSONObject jsonObject = JSONObject.parseObject(data);
        JSONArray arrayHeader = JSONObject.parseArray(jsonObject.getString("header"));
        if (!arrayHeader.get(arrayHeader.size() - 1).equals("备注")) {
            arrayHeader.add("导入结果");
            arrayHeader.add("备注");
        }

        JSONArray arrayRows = JSONObject.parseArray(jsonObject.getString("rows"));

        Long batch = new Date().getTime();

        Map<String, JSONArray> map = new HashMap<>();
        for (int i = 0; i < arrayRows.size(); i++) {
            OutboundOrderVo outboundOrderVo = new OutboundOrderVo();

            outboundOrderVo.setCustomer(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("客户")) ?
                    null : arrayRows.getJSONObject(i).getString("客户").trim());
            outboundOrderVo.setOwnerNo(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("单号")) ?
                    null : arrayRows.getJSONObject(i).getString("单号").trim());
            outboundOrderVo.setStorehouseName(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("发货仓库")) ?
                    null : arrayRows.getJSONObject(i).getString("发货仓库").trim());
            outboundOrderVo.setVehicleMode(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("车系")) ?
                    null : arrayRows.getJSONObject(i).getString("车系").trim());
            outboundOrderVo.setVin(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("车架号")) ?
                    null : arrayRows.getJSONObject(i).getString("车架号").trim());
            outboundOrderVo.setStrQty(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("数量")) ?
                    null : arrayRows.getJSONObject(i).getString("数量").trim());
            outboundOrderVo.setCarrierName(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("承运商")) ?
                    null : arrayRows.getJSONObject(i).getString("承运商").trim());
            outboundOrderVo.setExpectArriveDate(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("预计出库日期")) ?
                    null : arrayRows.getJSONObject(i).getString("预计出库日期").trim());
            outboundOrderVo.setPlateNumber(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("车船号")) ?
                    null : arrayRows.getJSONObject(i).getString("车船号").trim());
            outboundOrderVo.setDestLocationDetail(StringUtils.isBlank(arrayRows.getJSONObject(i).getString("目的地")) ?
                    null : arrayRows.getJSONObject(i).getString("目的地").trim());

            if (StringUtils.isEmpty(outboundOrderVo.getCustomer())) {
                arrayRows.getJSONObject(i).put("导入结果", "失败");
                arrayRows.getJSONObject(i).put("备注", "客户为空！");
                continue;
            }
            if (StringUtils.isEmpty(outboundOrderVo.getOwnerNo())) {
                arrayRows.getJSONObject(i).put("导入结果", "失败");
                arrayRows.getJSONObject(i).put("备注", "单号为空！");
                continue;
            }
            if (StringUtils.isEmpty(outboundOrderVo.getStorehouseName())) {
                arrayRows.getJSONObject(i).put("导入结果", "失败");
                arrayRows.getJSONObject(i).put("备注", "发货仓库为空！");
                continue;
            } else {
                Storehouse storehouse = getStorehouseByName(outboundOrderVo.getStorehouseName());
                if (Objects.isNull(storehouse)) {
                    arrayRows.getJSONObject(i).put("导入结果", "失败");
                    arrayRows.getJSONObject(i).put("备注", "发货仓库不存在！");
                    continue;
                } else {
                    outboundOrderVo.setStorehouseId(storehouse.getId());
                }
            }

            if (StringUtils.isEmpty(outboundOrderVo.getVin())) {
                arrayRows.getJSONObject(i).put("导入结果", "失败");
                arrayRows.getJSONObject(i).put("备注", "车架号为空！");
                continue;
            }

            if (StringUtils.isEmpty(outboundOrderVo.getStrQty())) {
                arrayRows.getJSONObject(i).put("导入结果", "失败");
                arrayRows.getJSONObject(i).put("备注", "数量为空！");
                continue;
            } else {
                try {
                    outboundOrderVo.setQty(Integer.valueOf(outboundOrderVo.getStrQty()));
                } catch (Exception e) {
                    arrayRows.getJSONObject(i).put("导入结果", "失败");
                    arrayRows.getJSONObject(i).put("备注", "数量错误！");
                    continue;
                }
            }

            if (!StringUtils.isEmpty(outboundOrderVo.getExpectArriveDate())) {
                try {
                    outboundOrderVo.setExpectOutboundDate(DATE_FORMAT.parse(outboundOrderVo.getExpectArriveDate()));
                } catch (Exception e) {
                    arrayRows.getJSONObject(i).put("导入结果", "失败");
                    arrayRows.getJSONObject(i).put("备注", "预计到达日期转换错误！");
                    continue;
                }
            }

            try {
                saveOutboundNotice(outboundOrderVo);
                arrayRows.getJSONObject(i).put("导入结果", "成功");
                arrayRows.getJSONObject(i).put("备注", "");
            } catch (Exception e) {
                arrayRows.getJSONObject(i).put("导入结果", "失败");
                arrayRows.getJSONObject(i).put("备注", e.getMessage());
                continue;
            }
        }
        map.put("header", arrayHeader);
        map.put("rows", arrayRows);

        RestfulResponse<Map<String, JSONArray>> restVO = new RestfulResponse<>(0, "导入成功", map);
        return restVO;
    }

    private OutboundNoticeHeader saveOutboundNotice(OutboundOrderVo outboundOrderVo) {

        Wrapper<OutboundNoticeLine> ew = new EntityWrapper<>();
        ew.eq("owner_id", outboundOrderVo.getCustomer());
        ew.eq("owner_order_no", outboundOrderVo.getOwnerNo());
        ew.eq("lot_no1", outboundOrderVo.getVin());
        ew.ne("status", TableStatusEnum.STATUS_50.getCode());
        if (outboundNoticeLineService.selectCount(ew) > 0) throw new BaseException("订单已导入！");

        OutboundNoticeHeader outboundNoticeHeader = new OutboundNoticeHeader();
        outboundNoticeHeader.setStoreHouseId(outboundOrderVo.getStorehouseId());
        outboundNoticeHeader.setOwnerId(outboundOrderVo.getCustomer());
        outboundNoticeHeader.setOwnerOrderNo(outboundOrderVo.getOwnerNo());
        outboundNoticeHeader.setExpectSumQty(new BigDecimal(outboundOrderVo.getQty()));
        outboundNoticeHeader.setOrderDate(new Date());
        outboundNoticeHeader.setCarrierName(outboundOrderVo.getCarrierName());
        outboundNoticeHeader.setExpectShipDateLower(outboundOrderVo.getExpectOutboundDate());
        outboundNoticeHeader.setLineCount(1);
        outboundNoticeHeader.setNoticeNo(businessDocNumberService.getOutboundNoticeNo());
        outboundNoticeHeader.setStatus(Status.Inbound.NOT);
        outboundNoticeHeader.setPlateNumber(outboundOrderVo.getPlateNumber());
        outboundNoticeHeader.setRemarks("WMS_pc手动导入入库头表");

        OutboundNoticeLine outboundNoticeLine = new OutboundNoticeLine();
        outboundNoticeLine.setOwnerId(outboundOrderVo.getCustomer());
        outboundNoticeLine.setOwnerOrderNo(outboundOrderVo.getOwnerNo());
        outboundNoticeLine.setMaterielId(outboundOrderVo.getVehicleMode());
        outboundNoticeLine.setMaterielName(outboundOrderVo.getVehicleMode());
        outboundNoticeLine.setLotNo1(outboundOrderVo.getVin());
        outboundNoticeLine.setStatus(Status.Outbound.NOT);
        outboundNoticeLine.setUom(MeasureUnit.TAI);
        outboundNoticeLine.setExpectQty(new BigDecimal(outboundOrderVo.getQty()));
        outboundNoticeLine.setDestLocationDetail(outboundOrderVo.getDestLocationDetail());
        outboundNoticeLine.setRemarks("WMS_pc手动导入入库明细");
        outboundNoticeHeader.addOutboundNoticeLine(outboundNoticeLine);

        if (outboundNoticeHeaderService.insertOutboundNotice(outboundNoticeHeader)) {
            return outboundNoticeHeader;
        } else {
            throw new BaseException("导入订单失败");
        }
    }

    private Storehouse getStorehouseByName(String houseName) {
        if (storehouseMap.containsKey(houseName)) {
            return storehouseMap.get(houseName);
        } else {
            Wrapper<Storehouse> ew = new EntityWrapper<>();
            ew.eq("name", houseName);
            Storehouse storehouse = storehouseService.selectOne(ew);
            if (!Objects.isNull(storehouse)) {
                storehouseMap.put(houseName, storehouse);
                return storehouse;
            } else {
                return null;
            }
        }
    }
}
