package com.zhiche.wms.admin.surpports.schedule;

import com.zhiche.wms.service.log.IItfImplogHeaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class OutboundSchedule {

    @Autowired
    private IItfImplogHeaderService logHeaderService;
    //fix 调整ILS切换关闭定时任务
    //@Scheduled(cron = "0 0/5 * * * ?")
    public void inboundDataFromTMSBySchedule() {
        logHeaderService.saveOutboundDataFromTMSBySchedule();
    }

}
