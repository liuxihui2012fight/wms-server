package com.zhiche.wms.admin.surpports.schedule;

import com.zhiche.wms.service.opbaas.ITaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @Description: 司机提车任务上报HR
 * @Date: Create in 19:13 2020/6/30
 */
@Component
public class DriverCarTaskSchedule {
    private static final Logger LOGGER = LoggerFactory.getLogger(DriverCarTaskSchedule.class);
    // @Autowired
    // private ITaskService taskService;

    // @Scheduled(cron = "0 55 23 * * ?")
    // public void pushDriverCarTaskCount () {
    //     LOGGER.info("定时获取每月司机提车任务的次数,start.......................");
    //     //taskService.queryDriverTask();
    //     LOGGER.info("定时获取每月司机提车任务的次数,end.......................");
    // }
}
