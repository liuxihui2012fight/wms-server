package com.zhiche.wms.admin.test.qiniu;

import com.zhiche.wms.WebApplication;
import com.zhiche.wms.core.utils.qiniu.QiNiuUtil;
import com.zhiche.wms.core.utils.qiniu.config.QiNiuConfigurationBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:48 2019/6/4
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class QiNiuTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(QiNiuTest.class);

    private static QiNiuConfigurationBean qiNiuConfigurationBean;

    @Test
    public void getQiniuFile () {
        String downloadUrl = QiNiuUtil.getDownloadUrl("otm_23_K19060300049_371239_763465");
        LOGGER.info("copy此链接访问：" + downloadUrl);
    }


}
