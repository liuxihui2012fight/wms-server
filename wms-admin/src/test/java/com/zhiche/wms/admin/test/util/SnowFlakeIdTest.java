package com.zhiche.wms.admin.test.util;

import com.zhiche.wms.WebApplication;
import com.zhiche.wms.core.utils.SnowFlakeId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by zhaoguixin on 2018/5/29.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= WebApplication.class)
public class SnowFlakeIdTest {

    @Autowired
    private SnowFlakeId snowFlakeId;

    private static final Logger LOGGER = LoggerFactory.getLogger(SnowFlakeIdTest.class);

    @Test
    public void genIdTest(){
//        SnowFlakeId snowFlakeId = new SnowFlakeId(0,0);
        for(int i=0;i<10;i++){
            LOGGER.info("generated id is {}:",snowFlakeId.nextId());
        }
//		LOGGER.info("generated id is {}:",snowFlakeId.nextId());
    }

}
