package com.zhiche.wms.admin.test.outbound;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.toolkit.StringUtils;
import com.zhiche.wms.WebApplication;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceEventEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.HttpClientUtil;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.domain.model.log.ItfExplogLine;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.domain.model.opbaas.StatusLog;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.domain.model.otm.OtmShipment;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import com.zhiche.wms.service.common.IntegrationService;
import com.zhiche.wms.service.constant.PutAwayType;
import com.zhiche.wms.service.constant.ShipType;
import com.zhiche.wms.service.constant.SourceSystem;
import com.zhiche.wms.service.dto.OTMEvent;
import com.zhiche.wms.service.dto.ShipParamDTO;
import com.zhiche.wms.service.log.IItfExplogLineService;
import com.zhiche.wms.service.opbaas.IExceptionRegisterService;
import com.zhiche.wms.service.opbaas.IStatusLogService;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import com.zhiche.wms.service.otm.IOtmShipmentService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import com.zhiche.wms.service.outbound.IOutboundShipHeaderService;
import com.zhiche.wms.service.outbound.IOutboundShipLineService;
import com.zhiche.wms.service.utils.CommonMethod;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by zhaoguixin on 2018/6/20.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OutboundShipTest {
    private static final Logger logger = LoggerFactory.getLogger(OutboundShipTest.class);

    @Autowired
    private IOutboundShipHeaderService outboundShipHeaderService;
    @Autowired
    private IOutboundShipLineService shipLineService;
    @Autowired
    private IOtmOrderReleaseService releaseService;
    @Autowired
    private IOtmShipmentService shipmentService;
    @Autowired
    private OtmOrderReleaseMapper otmOrderReleaseMapper;
    @Autowired
    private IItfExplogLineService logLineService;
    @Autowired
    private IStatusLogService statusLogService;
    @Autowired
    private IExceptionRegisterService exceptionRegisterService;
    @Autowired
    private IOutboundNoticeLineService outboundNoticeLineService;
    @Autowired
    private IOutboundShipLineService outboundShipLineService;
    @Autowired
    private IOutboundPrepareHeaderService prepareHeaderService;
    @Autowired
    private IntegrationService integrationService;

    @Test
    public void outboundByNoticeLineId() {
        shipLineService.shipByNoticeLineId(new Long("46716385618817024"),
                ShipType.NOTICE_SHIP, SourceSystem.HAND_MADE);
    }
    /**
     * 读取文件获取车架号
     *
     * @return
     */
    private String readFile () {
        StringBuffer obj = new StringBuffer();
        String pathname = "D://shipment.txt";
        try {
            FileReader reader = new FileReader(pathname);
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
                // 一次读入一行数据
                logger.info("循环读取每一行数据：{}", line);
                obj.append(line);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return obj.toString();
    }

    /**
     * <p>
     * 手动推送wms已发运数据到TMS(补推发运数据)
     * </p>
     */
    @Test
    public void outShipHandler () {
        EntityWrapper<OtmOrderRelease> oorEW = new EntityWrapper<>();
/*        List<String> releaseList = new ArrayList<>();
        releaseList.add("OR2019022000352");*/

        List<String> shipmentList = new ArrayList<>();
        shipmentList.add("S19013000683");

        /*List<String> vinList = new ArrayList<>();
        vinList.add("LEFAFCG23KHN05581");*/

//        String obj = this.readFile();
//        String[] split = obj.split(",");
//        List<String> shipmentList = Arrays.asList(split);

        oorEW.isNotNull("ship_date")
                .in("shipment_gid", shipmentList)
//                .in("release_gid", releaseList)
                .ne("status", TableStatusEnum.STATUS_50.getCode())
//                .in("vin", vinList)
                .orderBy("id", false);
        int count = releaseService.selectCount(oorEW);
        Page<OtmOrderRelease> pg = new Page<>();
        pg.setSize(11000);
        pg.setCurrent(1);
        List<OtmOrderRelease> orderReleases = otmOrderReleaseMapper.selectPage(pg, oorEW);
        logger.info("wms共查询到-----{}条-----已发运车辆", count);
        int i = 0;
        for (OtmOrderRelease oor : orderReleases) {
            EntityWrapper<ItfExplogLine> logEW = new EntityWrapper<>();
            logEW.eq("relation_id", oor.getId())
                    .eq("export_remarks","已发运回传OTM/BMS")
                    .eq("export_type", InterfaceEventEnum.BS_OP_DELIVERY.getCode());
            List<ItfExplogLine> lines = logLineService.selectList(logEW);
            if (CollectionUtils.isNotEmpty(lines)) {
                ItfExplogLine explogLine = lines.get(0);
                try {
                    logger.info("wms推送TMS发运  releaseGid:{},shipmentGid:{} 次数i:{}", oor.getReleaseGid(), oor.getShipmentGid(), ++i);
//                    String resJson = HttpClientUtil.postJson("http://10.20.30.154:8080/lisa-integration/event/export", null, explogLine.getDataContent(), 60000);
                    String resJson = HttpClientUtil.postJson("http://10.20.30.155:8280/lisa-tms/shipTask/shipDataForBMS", null, explogLine.getDataContent(), 60000);
                    logger.info("wms推送TMS发运结果-->{}", resJson);
                } catch (Exception e) {
                    logger.error("wms推送TMS发运失败-->{}", e);
                }
            } else {
                logger.error("WMS推送发运失败--->or的表主键:{},运单:{},指令:{}", oor.getId(), oor.getReleaseGid(), oor.getShipmentGid());
            }
        }
    }

    private List<String> shipmentList () {
        List<String> list = new ArrayList<>();
        list.add("S19052200232");
        return list;
    }

    /**
     * 自动已发运数据到TMS/BMS(用于手动调整WMS数据的)
     */
    @Test
    public void otmShipAuto() {
        EntityWrapper<ShipmentForShipDTO> ew = new EntityWrapper<>();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode())
                //指令号
                .in("re_shipment_gid", this.getShipmentGid())
//                .ge("re_gmt_modify", "2019-01-22 13:50:00")
//                .le("re_gmt_modify", "2019-01-22 13:57:00")
//                .eq("id","151478842744107008")
//                .in("re_origin_location_gid","南昌发运库2库")
//                .in("re_vin",this.getVins())
                .ne("releaseStatus", TableStatusEnum.STATUS_50.getCode())
//                .eq("releaseStatus", TableStatusEnum.STATUS_BS_DISPATCH.getCode())
                .isNotNull("ship_date")
                .eq("is_ship", TableStatusEnum.STATUS_1.getCode())
                .orderBy("gmt_create", false)
                .orderBy("id", false)
                .orderBy("releaseId", false);
        List<ShipmentForShipDTO> list = shipmentService.getShipDetail(ew);
        if (CollectionUtils.isNotEmpty(list)) {
            for (ShipmentForShipDTO shipDTO : list) {
                OtmShipment os = new OtmShipment();
                os.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                os.setId(shipDTO.getId());
                shipmentService.updateById(os);

                //1、指令发运记录表新增数据
                this.insertStatusLog(shipDTO.getId(),TableStatusEnum.STATUS_20.getCode());

                shipDTO.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                //更新运单明细 状态为已发运
                List<OtmOrderRelease> releaseList = shipDTO.getOtmOrderReleaseList();
                ArrayList<StatusLog> insertLogs = Lists.newArrayList();
                for (OtmOrderRelease oor : releaseList) {
                    Wrapper<ExceptionRegister> registerWrapper = new EntityWrapper<>();
                    registerWrapper.eq("vin", oor.getVin()).eq("otm_status", TableStatusEnum.STATUS_N.getCode());
                    ExceptionRegister exceptionRegister = exceptionRegisterService.selectOne(registerWrapper);
                    if (Objects.nonNull(exceptionRegister)) {
                        throw new BaseException("车架号:" + oor.getVin() + "异常不发运,请联系调度!");
                    }
                    OtmOrderRelease release = new OtmOrderRelease();
                    release.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                    release.setId(oor.getId());
                    releaseService.updateById(release);
                    oor.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());

                    //运单发运数据
                    this.insertStatusLog(oor.getId(),TableStatusEnum.STATUS_10.getCode());

                    //发运  更新车辆为已出库
                    this.updateVinOutbound(oor.getReleaseGid());


                    OTMEvent otmEvent = integrationService.getOtmEvent(String.valueOf(oor.getId()),
                            oor.getReleaseGid(),
                            InterfaceEventEnum.BS_OP_DELIVERY.getCode(),
                            oor.getShipmentGid(),
                            "已发运回传OTM/BMS");
                    otmEvent.setRecdDate(CommonMethod.datetoString(oor.getShipDate()));
                    otmEvent.setOccurDate(CommonMethod.datetoString(oor.getShipDate()));


                    //*****调整参数--传递更多信息
                    ShipParamDTO paramDTO = new ShipParamDTO();
                    this.setParamDTO(paramDTO,shipDTO,oor,otmEvent);


                    //6、推送otm
                    this.pushOTMBms(paramDTO, oor.getReleaseGid(), oor.getShipmentGid(),oor.getId());

                }
                if (CollectionUtils.isNotEmpty(insertLogs)) {
                    statusLogService.insertBatch(insertLogs);
                }
            }
        }
    }


    /**
     * 手动补发发运时间给OTM/BMS（只组装json补发发运，不新增其他数据）
     */
    @Test
    public void caihuaShipDataToBms () {
        EntityWrapper<ShipmentForShipDTO> ew = new EntityWrapper<>();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode())
                //指令号
                .in("re_shipment_gid", this.getShipmentGid())
//                .ge("re_gmt_modify", "2019-01-22 13:50:00")
//                .le("re_gmt_modify", "2019-01-22 13:57:00")
//                .eq("id","151478842744107008")
//                .in("re_origin_location_gid","南昌发运库2库")
//                .in("re_release_gid",this.releaseGids())
//                .in("re_vin",this.getVins())
                .ne("releaseStatus", TableStatusEnum.STATUS_50.getCode())
                .isNotNull("ship_date")
                .eq("is_ship", TableStatusEnum.STATUS_1.getCode())
//                .eq("re_release_gid", "OR2019042902902")
                .orderBy("gmt_create", false)
                .orderBy("id", false)
                .orderBy("releaseId", false);
        List<ShipmentForShipDTO> list = shipmentService.getShipDetail(ew);
        if (CollectionUtils.isNotEmpty(list)) {
            for (ShipmentForShipDTO shipDTO : list) {
                shipDTO.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());

                //2、更新运单明细，状态为已发运
                List<OtmOrderRelease> releaseList = shipDTO.getOtmOrderReleaseList();
                ArrayList<StatusLog> insertLogs = Lists.newArrayList();
                for (OtmOrderRelease oor : releaseList) {
                    //3、是否标记了异常不可发运
                    this.exceptionNoShip(oor.getVin());
                    oor.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());

                    //4、运单发运记录表新增数据
                    this.insertStatusLog(oor.getId(), TableStatusEnum.STATUS_10.getCode());

                    //5、发运  更新车辆为已出库
                    this.updateVinOutbound(oor.getReleaseGid());

                    OTMEvent otmEvent = integrationService.getOtmEvent(String.valueOf(oor.getId()),
                            oor.getReleaseGid(),
                            InterfaceEventEnum.BS_OP_DELIVERY.getCode(),
                            oor.getShipmentGid(),
                            "已发运回传OTM/BMS");
                    otmEvent.setRecdDate(CommonMethod.datetoString(oor.getShipDate()));
                    otmEvent.setOccurDate(CommonMethod.datetoString(oor.getShipDate()));


                    //*****调整参数--传递更多信息
                    ShipParamDTO paramDTO = new ShipParamDTO();
                    this.setParamDTO(paramDTO, shipDTO, oor, otmEvent);

                    //6、推送otm
                    this.pushOTMBms(paramDTO, oor.getReleaseGid(), oor.getShipmentGid(), oor.getId());

                }
            }
        }
    }

    private List<String> releaseGids () {
        List<String> releaseGids = new ArrayList<>();
        releaseGids.add("OR2019052300270");
        return releaseGids;
    }

    private void pushOTMBms (ShipParamDTO paramDTO, String releaseGid, String shipmentGid, Long oorId) {
//        String pushUrl = "http://10.20.30.154:8080/lisa-integration/event/export";
        String pushUrl = "http://10.20.30.155:8280/lisa-tms/shipTask/shipDataForBMS";
        try {
            String paramJSON = JSONObject.toJSONString(paramDTO);
            logger.info("wms推送TMS发运  releaseGid:{},shipmentGid:{},paramJSON{}", releaseGid, shipmentGid, paramJSON);
            String resJson = HttpClientUtil.postJson(pushUrl, null, paramJSON, 60000);
            String requestId = "";
            if (StringUtils.isNotEmpty(resJson)) {
                RestfulResponse<String> restfulResponse = JSON.parseObject(resJson, new TypeReference<RestfulResponse<String>>() {
                });
                if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                    requestId = restfulResponse.getData();
                }
            }

            logger.info("wms推送TMS发运结果 -->{}", resJson);
            integrationService.insertExportLogNew(String.valueOf(oorId),
                    paramDTO,
                    requestId,
                    "已发运回传OTM/BMS",
                    InterfaceEventEnum.BS_OP_DELIVERY.getCode(), "admin",pushUrl);
        } catch (Exception e) {
            logger.error("wms推送TMS发运失败-->{}", e);
        }
    }

    /**
     * 发运  更新车辆为已出库
     * @param releaseGid
     */
    private void updateVinOutbound (String releaseGid) {
        ArrayList<String> status = Lists.newArrayList();
        status.add(TableStatusEnum.STATUS_10.getCode());
        status.add(TableStatusEnum.STATUS_20.getCode());
        EntityWrapper<OutboundNoticeLine> nlEW = new EntityWrapper<>();
        nlEW.eq("line_source_key", releaseGid)
                .in("status", status)
                .orderBy("id", false);
        OutboundNoticeLine noticeLine = outboundNoticeLineService.selectOne(nlEW);
        if (noticeLine != null) {
            try {
                //调用方法出库
                outboundShipLineService.shipByNoticeLineId(noticeLine.getId(), PutAwayType.NOTICE_PUTAWAY, SourceSystem.AUTO);
                //调用方法修改备料状态
                prepareHeaderService.updatePrepareFinishByLineId(noticeLine.getId(), "统一数据调整发运");
            } catch (Exception e) {
                logger.error("自动调整出库失败 车架号:{}...运单号:{}", noticeLine.getLotNo1(), noticeLine.getLineSourceKey());
            }
        }
    }

    /**
     * 是否标记了异常不可发运
     * @param vin 车架号
     */
    private void exceptionNoShip (String vin) {
        Wrapper<ExceptionRegister> registerWrapper = new EntityWrapper<>();
        registerWrapper.eq("vin", vin).eq("otm_status", TableStatusEnum.STATUS_N.getCode());
        ExceptionRegister exceptionRegister = exceptionRegisterService.selectOne(registerWrapper);
        if (Objects.nonNull(exceptionRegister)) {
            throw new BaseException("车架号:" + vin + "异常不发运,请联系调度!");
        }
    }

    /**
     * 发运日志
     * @param id
     * @param code
     */
    private void insertStatusLog (Long id, String code) {
        new Thread(() -> {
            StatusLog sl = new StatusLog();
            sl.setTableType(code);
            sl.setTableId(String.valueOf(id));
            sl.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            sl.setStatusName(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
            sl.setUserCreate("统一调整发运");
            statusLogService.insert(sl);
        }).start();
    }

    private List<String> getShipmentGid () {
        List<String> shipmentGids = new ArrayList<>();
        shipmentGids.add("S19013000683");
        return shipmentGids;
    }

    private List<String> getVins () {
        List<String> vins = new ArrayList<>();
        vins.add("LB378Y4W8KA024652");
        return vins;
    }

    /**
     * 组装推送参数
     * @param paramDTO
     * @param shipDTO
     * @param oor
     * @param otmEvent
     */
    private void setParamDTO (ShipParamDTO paramDTO, ShipmentForShipDTO shipDTO, OtmOrderRelease oor, OTMEvent otmEvent) {
        paramDTO.setPlateNo(shipDTO.getPlateNo());
        paramDTO.setTrailerNo(shipDTO.getTrailerNo());
        paramDTO.setVehicleName(oor.getStanVehicleType());
        paramDTO.setVin(oor.getVin());
        paramDTO.setOriginProvince(oor.getOriginLocationProvince());
        paramDTO.setOriginCity(oor.getOriginLocationCity());
        paramDTO.setOriginCounty(oor.getOriginLocationCounty());
        paramDTO.setOriginAddr(oor.getOriginLocationAddress());
        paramDTO.setDestProvince(oor.getDestLocationProvince());
        paramDTO.setDestCity(oor.getDestLocationCity());
        paramDTO.setDestCounty(oor.getDestLocationCounty());
        paramDTO.setDestAddr(oor.getDestLocationAddress());
        paramDTO.setDriverGid(shipDTO.getDriverGid());
        paramDTO.setProviderGid(shipDTO.getServiceProviderGid());
        paramDTO.setShipTime(String.valueOf(oor.getShipDate().getTime()));
        // paramDTO.setShipTime(String.valueOf(new Date().getTime()));
        paramDTO.setOriginCode(oor.getOriginLocationGid());
        paramDTO.setDestCode(oor.getDestLocationGid());
        paramDTO.setExportKey(otmEvent.getExportKey());
        paramDTO.setCallBackUrl(otmEvent.getCallBackUrl());
        paramDTO.setEventType(otmEvent.getEventType());
        paramDTO.setOccurDate(otmEvent.getOccurDate());
        paramDTO.setRecdDate(otmEvent.getRecdDate());
        paramDTO.setSort(otmEvent.getSort());
        paramDTO.setDescribe(otmEvent.getDescribe());
        paramDTO.setOrderReleaseId(otmEvent.getOrderReleaseId());
        paramDTO.setShipmentId(otmEvent.getShipmentId());
        paramDTO.setQrCode(otmEvent.getQrCode());
        paramDTO.setCusOrderNo(oor.getCusOrderNo());
        paramDTO.setCusWaybill(oor.getCusWaybillNo());
        //String res = nodeExport.exportEventToOTMNew(paramDTO);
    }



    /**
     * 手动推送入库数据
     */
    @Test
    public void pushInboundToOTM(){
        EntityWrapper<ShipmentForShipDTO> ew = new EntityWrapper<>();
        List<String> vins = this.getVins();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode());
        ew.in("re_shipment_gid", this.getShipmentGid());
        //ew.eq("id","151478842744107008");
        ew.in("re_vin", vins);
        ew.ne("releaseStatus", TableStatusEnum.STATUS_50.getCode());
        ew.eq("releaseStatus", TableStatusEnum.STATUS_BS_INBOUND.getCode());
        ew.eq("is_ship", TableStatusEnum.STATUS_1.getCode());
        ew.orderBy("gmt_create", false);
        ew.orderBy("id", false);
        ew.orderBy("releaseId", false);
        List<ShipmentForShipDTO> list = shipmentService.getShipDetail(ew);
        if (CollectionUtils.isNotEmpty(list)) {
            for (ShipmentForShipDTO shipDTO : list) {
                new Thread(() -> {
                    StatusLog statusLog = new StatusLog();
                    statusLog.setTableId(String.valueOf(shipDTO.getId()));
                    statusLog.setTableType(TableStatusEnum.STATUS_20.getCode());
                    statusLog.setStatus(TableStatusEnum.STATUS_BS_INBOUND.getCode());
                    statusLog.setStatusName(TableStatusEnum.STATUS_BS_INBOUND.getCode());
                    statusLog.setUserCreate("统一调整入库");
                    statusLogService.insert(statusLog);
                }).start();

                List<OtmOrderRelease> releaseList = shipDTO.getOtmOrderReleaseList();
                ArrayList<StatusLog> insertLogs = Lists.newArrayList();
                for (OtmOrderRelease oor : releaseList) {
                    StatusLog sl = new StatusLog();
                    sl.setTableType(TableStatusEnum.STATUS_10.getCode());
                    sl.setTableId(String.valueOf(oor.getId()));
                    sl.setStatus(TableStatusEnum.STATUS_BS_INBOUND.getCode());
                    sl.setStatusName(TableStatusEnum.STATUS_BS_INBOUND.getCode());
                    sl.setUserCreate("数据调整统一入库");
                    insertLogs.add(sl);

                    OTMEvent otmEvent = integrationService.getOtmEvent(String.valueOf(oor.getId()),
                            oor.getReleaseGid(),
                            InterfaceEventEnum.BS_WMS_IN.getCode(),
                            oor.getShipmentGid(),
                            "已入库回传OTM/BMS");
                    otmEvent.setRecdDate(CommonMethod.datetoString(shipDTO.getGmtModified()));
                    otmEvent.setOccurDate(CommonMethod.datetoString(shipDTO.getGmtModified()));

                    //*****调整参数--传递更多信息
                    ShipParamDTO paramDTO = new ShipParamDTO();
                    this.setParamDTO(paramDTO,shipDTO,oor,otmEvent);

//                    String pushUrl = "http://10.20.30.154:8080/lisa-integration/event/export";
                    String pushUrl = "http://10.20.30.155:8280/lisa-tms/shipTask/shipDataForBMS";
                    try {
                        String paramJSON = JSONObject.toJSONString(paramDTO);
                        logger.info("wms推送TMS入库  releaseGid:{},shipmentGid:{}", oor.getReleaseGid(), oor.getShipmentGid());
                        String resJson = HttpClientUtil.postJson(pushUrl, null, paramJSON, 60000);
                        String requestId = "";
                        if (StringUtils.isNotEmpty(resJson)) {
                            RestfulResponse<String> restfulResponse = JSON.parseObject(resJson, new TypeReference<RestfulResponse<String>>() {
                            });
                            if (Objects.nonNull(restfulResponse) && restfulResponse.getCode() == 0) {
                                requestId = restfulResponse.getData();
                            }
                        }
                        logger.info("wms推送TMS入库结果 -->{}", resJson);
                        integrationService.insertExportLogNew(String.valueOf(oor.getId()),
                                paramDTO,
                                requestId,
                                "已入库回传OTM/BMS",
                                InterfaceEventEnum.BS_OP_DELIVERY.getCode(), "admin",pushUrl);
                    } catch (Exception e) {
                        logger.error("wms推送TMS入库失败-->{}", e);
                    }
                }
                if (CollectionUtils.isNotEmpty(insertLogs)) {
                    statusLogService.insertBatch(insertLogs);
                }
            }
        }
    }
}
