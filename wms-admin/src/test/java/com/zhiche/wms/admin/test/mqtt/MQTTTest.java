package com.zhiche.wms.admin.test.mqtt;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.WebApplication;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.service.mqtt.PublishSubscribe;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:31 2019/8/28
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MQTTTest {
    @Autowired
    private OtmOrderReleaseMapper otmOrderReleaseMapper;
    @Test
    public void test(){
        Page<OtmOrderRelease> page = new Page<>();
        page.setSize(800);
        EntityWrapper<OtmOrderRelease> ew = new EntityWrapper<>();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode());
        List<OtmOrderRelease> list = otmOrderReleaseMapper.selectPage(page,ew);
        String json = JSONObject.toJSONString(list);
        PublishSubscribe.publish(json);
        PublishSubscribe.subscribe();
    }
}
