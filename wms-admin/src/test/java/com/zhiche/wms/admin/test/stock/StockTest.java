package com.zhiche.wms.admin.test.stock;

import com.zhiche.wms.WebApplication;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.stock.Sku;
import com.zhiche.wms.domain.model.stock.SkuProperty;
import com.zhiche.wms.domain.model.stock.Stock;
import com.zhiche.wms.domain.model.stock.StockProperty;
import com.zhiche.wms.service.outbound.impl.OutboundPrepareHeaderServiceImpl;
import com.zhiche.wms.service.stock.impl.SkuServiceImpl;
import com.zhiche.wms.service.stock.impl.StockServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

/**
 * Created by zhaoguixin on 2018/5/30.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class StockTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(StockTest.class);

    @Autowired
    private SnowFlakeId snowFlakeId;

    @Autowired
    private SkuServiceImpl skuService;

    @Autowired
    private StockServiceImpl stockService;

    @Test
    public void insert() {
        Sku sku = new Sku();
        long id = snowFlakeId.nextId();
        sku.setId(snowFlakeId.nextId());
//        sku.setId("39050717826256896");
        sku.setOwnerId("2");
        sku.setMaterielId("1");
        sku.setUom("2");
        sku.setLotNo0("111");
        sku.setLotNo1("111");
        sku.setLotNo2("111");
        sku.setLotNo3("111");
        sku.setLotNo4("121212121");
        sku.setLotNo5("111");
        sku.setLotNo6("1212312");
        sku.setLotNo7("111");
        sku.setLotNo8("111");
        sku.setLotNo9("111");
        Sku sku1 = skuService.selectById(1);

        LOGGER.info("测试结果：" + Boolean.toString(skuService.insertOrUpdate(sku)));
    }

    @Test
    public void getSku() {
        SkuProperty skuProperty = new SkuProperty();

        skuProperty.setOwnerId("2");
        skuProperty.setMaterielId("1");
        skuProperty.setUom("2");
        skuProperty.setLotNo0("111");
        skuProperty.setLotNo1("111");
        skuProperty.setLotNo2("111");
        skuProperty.setLotNo3("111");
        skuProperty.setLotNo4("121212121");
        skuProperty.setLotNo5("111");
        skuProperty.setLotNo6("1212312");
        skuProperty.setLotNo7("111");
        skuProperty.setLotNo8("111");
        skuProperty.setLotNo9("111");

        Sku sku = skuService.getUniqueSku(skuProperty);

        LOGGER.info("测试结果：" + sku.getId());
    }

    @Test
    public void getStock() {
        StockProperty stockProperty = new StockProperty();

        stockProperty.setOwnerId("2");
        stockProperty.setMaterielId("1");
        stockProperty.setUom("2");
        stockProperty.setLotNo0("111");
        stockProperty.setLotNo1("111");
        stockProperty.setLotNo2("111");
        stockProperty.setLotNo3("111");
        stockProperty.setLotNo4("121212121");
        stockProperty.setLotNo5("111");
        stockProperty.setLotNo6("1212312");
        stockProperty.setLotNo7("111");
        stockProperty.setLotNo8("111");
        stockProperty.setLotNo9("111");
        stockProperty.setStoreHouseId(new Long("1102"));
        stockProperty.setLocationId(new Long("110201"));

        Stock stock = stockService.getUniqueStock(stockProperty);

        LOGGER.info("测试结果：" + stock.toString());
    }

    @Test
    public void updateStock() {
        StockProperty stockProperty = new StockProperty();

        stockProperty.setOwnerId("5");
        stockProperty.setMaterielId("1");
        stockProperty.setUom("2");
        stockProperty.setLotNo0("111");
        stockProperty.setLotNo1("111");
        stockProperty.setLotNo2("111");
        stockProperty.setLotNo3("111");
        stockProperty.setLotNo4("121212121");
        stockProperty.setLotNo5("111");
        stockProperty.setLotNo6("1212312");
        stockProperty.setLotNo7("111");
        stockProperty.setLotNo8("111");
        stockProperty.setLotNo9("111");
        stockProperty.setStoreHouseId(new Long("1102"));
        stockProperty.setLocationId(new Long("110201"));
        stockProperty.setQty(BigDecimal.TEN);
        stockProperty.setNetWeight(BigDecimal.TEN);
        stockProperty.setGrossWeight(BigDecimal.TEN);
        stockProperty.setGrossCubage(BigDecimal.TEN);
        stockProperty.setPackedCount(BigDecimal.ONE);
        Stock stock = null;
        stock = stockService.addStock(stockProperty, "10", new Long(1212));
        LOGGER.info("测试结果：" + stock.toString());

        stock = stockService.addStock(stockProperty, "10", new Long(1213));
        LOGGER.info("测试结果：" + stock.toString());

        stock = stockService.addStock(stockProperty, "10", new Long(1214));
        LOGGER.info("测试结果：" + stock.toString());

        stock = stockService.minusStock(stockProperty, "20", new Long(1215));
        LOGGER.info("测试结果：" + stock.toString());

//        stock = stockService.minusStock(stockProperty,"20","1216");
//        LOGGER.info("测试结果："+stock.toString());

    }


}
