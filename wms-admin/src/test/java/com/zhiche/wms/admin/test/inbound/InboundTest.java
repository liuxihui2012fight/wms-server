package com.zhiche.wms.admin.test.inbound;

import com.zhiche.wms.WebApplication;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.inbound.InboundNoticeHeader;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;
import com.zhiche.wms.service.base.IBusinessDocNumberService;
import com.zhiche.wms.service.constant.PutAwayType;
import com.zhiche.wms.service.constant.SourceSystem;
import com.zhiche.wms.service.constant.Status;
import com.zhiche.wms.service.inbound.IInboundNoticeHeaderService;
import com.zhiche.wms.service.inbound.IInboundNoticeLineService;
import com.zhiche.wms.service.inbound.IInboundPutawayHeaderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zhaoguixin on 2018/6/12.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= WebApplication.class)
public class InboundTest {

    @Autowired
    private IInboundNoticeHeaderService inboundNoticeHeaderService;

    @Autowired
    private IInboundNoticeLineService inboundNoticeLineService;

    @Autowired
    private IInboundPutawayHeaderService inboundPutawayHeaderService;

    @Autowired
    private IBusinessDocNumberService businessDocNumberService;

    @Autowired
    private SnowFlakeId snowFlakeId;

    @Test
    public void creatInboundNotice(){
        InboundNoticeHeader inboundNoticeHeader = new InboundNoticeHeader();
        inboundNoticeHeader.setStoreHouseId(new Long(1));
        inboundNoticeHeader.setNoticeNo(businessDocNumberService.getInboundNoticeNo());
        inboundNoticeHeader.setOrderDate(new Date());
        inboundNoticeHeader.setExpectSumQty(new BigDecimal("2"));
        inboundNoticeHeader.setLineCount(2);
        inboundNoticeHeader.setStatus(Status.Inbound.NOT);
        inboundNoticeHeader.setId(snowFlakeId.nextId());

        InboundNoticeLine inboundNoticeLine = new InboundNoticeLine();
        inboundNoticeLine.setHeaderId(inboundNoticeHeader.getId());
        inboundNoticeLine.setOwnerId("1");
        inboundNoticeLine.setExpectQty(BigDecimal.ONE);
        inboundNoticeLine.setMaterielId("10");
        inboundNoticeLine.setLotNo0("1001");
        inboundNoticeLine.setLotNo1("1002");
        inboundNoticeLine.setStatus(Status.Inbound.NOT);
        inboundNoticeLine.setId(snowFlakeId.nextId());

        InboundNoticeLine inboundNoticeLine1 = new InboundNoticeLine();
        inboundNoticeLine1.setHeaderId(inboundNoticeHeader.getId());
        inboundNoticeLine1.setOwnerId("1");
        inboundNoticeLine1.setExpectQty(BigDecimal.ONE);
        inboundNoticeLine1.setMaterielId("20");
        inboundNoticeLine1.setLotNo0("1001");
        inboundNoticeLine1.setLotNo1("1002");
        inboundNoticeLine1.setStatus(Status.Inbound.NOT);
        inboundNoticeLine1.setId(snowFlakeId.nextId());

        inboundNoticeHeaderService.insert(inboundNoticeHeader);
        inboundNoticeLineService.insert(inboundNoticeLine);
        inboundNoticeLineService.insert(inboundNoticeLine1);
    }

    @Test
    public void inbound(){
        inboundPutawayHeaderService.updateByNoticeLineId(new Long("46047251692253184"), PutAwayType.NOTICE_PUTAWAY,
                SourceSystem.BARRIER, null);

    }

    @Test
    public void updateStatus(){
        inboundNoticeHeaderService.updateStatus(new Long("43921675720323072"),null);
    }


}
