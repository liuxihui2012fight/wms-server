package com.zhiche.wms.admin.test.stockadjust;

import com.zhiche.wms.WebApplication;
import com.zhiche.wms.domain.model.stockadjust.StockAdjustHeader;
import com.zhiche.wms.domain.model.stockadjust.StockAdjustLine;
import com.zhiche.wms.service.stockadjust.IStockAdjustHeaderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zhaoguixin on 2018/6/5.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= WebApplication.class)
public class StockAdjustTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(StockAdjustTest.class);

    @Autowired
    private IStockAdjustHeaderService stockAdjustHeaderService;


    @Test
    public void creatStockAdjust(){
        StockAdjustHeader stockAdjustHeader = new StockAdjustHeader();
        stockAdjustHeader.setAdjustNo("100020");
        stockAdjustHeader.setStoreHouseId(new Long(1111));
        stockAdjustHeader.setOwnerId("2100");
        stockAdjustHeader.setOrderDate(new Date());
        stockAdjustHeader.setAdjustType("10");

        StockAdjustLine stockAdjustLine = new StockAdjustLine();
        stockAdjustLine.setOwnerId("2100");
        stockAdjustLine.setMaterielId("3100");
        stockAdjustLine.setMaterielCode("3100");
        stockAdjustLine.setMaterielName("3100");
        stockAdjustLine.setLocationId(new Long(4100));
        stockAdjustLine.setUom("10");
        stockAdjustLine.setQty(new BigDecimal("15"));
        stockAdjustLine.setLotNo0("111");
        stockAdjustLine.setLotNo1("111");
        stockAdjustLine.setLotNo2("111");
        stockAdjustLine.setLotNo3("111");
        stockAdjustLine.setLotNo4("111");
        stockAdjustLine.setLotNo5("111");
        stockAdjustLine.setLotNo6("111");
        stockAdjustLine.setLotNo7("111");
        stockAdjustLine.setLotNo8("111");
        stockAdjustLine.setLotNo9("111");

        stockAdjustHeader.addStockAdjustLine(stockAdjustLine);

        StockAdjustLine stockAdjustLine1 = new StockAdjustLine();

        stockAdjustLine1.setOwnerId("2100");
        stockAdjustLine1.setMaterielId("3200");
        stockAdjustLine1.setMaterielCode("3200");
        stockAdjustLine1.setMaterielName("3200");
        stockAdjustLine1.setLocationId(new Long(4200));
        stockAdjustLine1.setUom("15");
        stockAdjustLine1.setQty(new BigDecimal("20"));
        stockAdjustLine1.setLotNo0("111");
        stockAdjustLine1.setLotNo1("111");
        stockAdjustLine1.setLotNo2("111");
        stockAdjustLine1.setLotNo3("111");
        stockAdjustLine1.setLotNo4("121212121");
        stockAdjustLine1.setLotNo5("111");
        stockAdjustLine1.setLotNo6("1212312");
        stockAdjustLine1.setLotNo7("111");
        stockAdjustLine1.setLotNo8("111");
        stockAdjustLine1.setLotNo9("111");

        stockAdjustHeader.addStockAdjustLine(stockAdjustLine1);

        LOGGER.info("测试结果："+Boolean.toString(stockAdjustHeaderService.creatStockAdjust(stockAdjustHeader)));
    }

    @Test
    public void auditStockAdjust(){
        LOGGER.info("测试结果："+Boolean.toString(stockAdjustHeaderService.auditStockAdjust(new Long("41960170628190208"))));

    }

    @Test
    public void cancelAuditStockAdjust(){
        LOGGER.info("测试结果："+Boolean.toString(stockAdjustHeaderService.cancelAuditStockAdjust(new Long("41960170628190208"))));

    }

    @Test
    public void creatAndAuditStockAdjust(){

        StockAdjustHeader stockAdjustHeader = new StockAdjustHeader();
        stockAdjustHeader.setAdjustNo("100010");
        stockAdjustHeader.setStoreHouseId(new Long(11111));
        stockAdjustHeader.setOwnerId("2100");
        stockAdjustHeader.setOrderDate(new Date());
        stockAdjustHeader.setAdjustType("10");

        StockAdjustLine stockAdjustLine = new StockAdjustLine();
        stockAdjustLine.setOwnerId("2100");
        stockAdjustLine.setMaterielId("3100");
        stockAdjustLine.setMaterielCode("3100");
        stockAdjustLine.setMaterielName("3100");
        stockAdjustLine.setLocationId(new Long(4100));
        stockAdjustLine.setUom("10");
        stockAdjustLine.setQty(new BigDecimal("15"));
        stockAdjustLine.setLotNo0("111");
        stockAdjustLine.setLotNo1("111");
        stockAdjustLine.setLotNo2("111");
        stockAdjustLine.setLotNo3("111");
        stockAdjustLine.setLotNo4("111");
        stockAdjustLine.setLotNo5("111");
        stockAdjustLine.setLotNo6("111");
        stockAdjustLine.setLotNo7("111");
        stockAdjustLine.setLotNo8("111");
        stockAdjustLine.setLotNo9("111");

        stockAdjustHeader.addStockAdjustLine(stockAdjustLine);

        StockAdjustLine stockAdjustLine1 = new StockAdjustLine();

        stockAdjustLine1.setOwnerId("2100");
        stockAdjustLine1.setMaterielId("3200");
        stockAdjustLine1.setMaterielCode("3200");
        stockAdjustLine1.setMaterielName("3200");
        stockAdjustLine1.setLocationId(new Long(4200));
        stockAdjustLine1.setUom("15");
        stockAdjustLine1.setQty(new BigDecimal("20"));
        stockAdjustLine1.setLotNo0("111");
        stockAdjustLine1.setLotNo1("111");
        stockAdjustLine1.setLotNo2("111");
        stockAdjustLine1.setLotNo3("111");
        stockAdjustLine1.setLotNo4("121212121");
        stockAdjustLine1.setLotNo5("111");
        stockAdjustLine1.setLotNo6("1212312");
        stockAdjustLine1.setLotNo7("111");
        stockAdjustLine1.setLotNo8("111");
        stockAdjustLine1.setLotNo9("111");

        stockAdjustHeader.addStockAdjustLine(stockAdjustLine1);

        LOGGER.info("测试结果："+Boolean.toString(stockAdjustHeaderService.creatAndAuditStockAdjust(stockAdjustHeader)));
    }


}
