package com.zhiche.wms.admin.test.movement;

import com.zhiche.wms.WebApplication;
import com.zhiche.wms.domain.model.movement.MovementHeader;
import com.zhiche.wms.domain.model.movement.MovementLine;
import com.zhiche.wms.service.movement.IMovementHeaderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zhaoguixin on 2018/5/30.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= WebApplication.class)
public class MovementTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovementTest.class);

    @Autowired
    private IMovementHeaderService movementHeaderService;

    @Test
    public void creatMovement(){

        MovementHeader movementHeader = new MovementHeader();
        movementHeader.setMovementNo("100010");
        movementHeader.setStoreHouseId(new Long(11));
        movementHeader.setOwnerId("21");
        movementHeader.setOrderDate(new Date());
        movementHeader.setBusinessType("10");
        movementHeader.setBusinessDocId(new Long(11));

        MovementLine movementLine = new MovementLine();
        movementLine.setType("10");
        movementLine.setRelationLineId(new Long(11));
        movementLine.setOwnerId("2");
        movementLine.setMaterielId("1");
        movementLine.setMaterielCode("2121212");
        movementLine.setMaterielName("11111");
        movementLine.setDestinationLocationId(new Long(11));
        movementLine.setUom("2");
        movementLine.setQty(new BigDecimal("12"));
        movementLine.setLotNo0("111");
        movementLine.setLotNo1("111");
        movementLine.setLotNo2("111");
        movementLine.setLotNo3("111");
        movementLine.setLotNo4("121212121");
        movementLine.setLotNo5("111");
        movementLine.setLotNo6("1212312");
        movementLine.setLotNo7("111");
        movementLine.setLotNo8("111");
        movementLine.setLotNo9("111");

        movementHeader.addMovementLine(movementLine);

        MovementLine movementLine1 = new MovementLine();
        movementLine1.setType("10");
        movementLine1.setRelationLineId(new Long(11));
        movementLine1.setOwnerId("2");
        movementLine1.setMaterielId("2");
        movementLine1.setMaterielCode("2121212");
        movementLine1.setMaterielName("111111");
        movementLine1.setDestinationLocationId(new Long(11));
        movementLine1.setUom("2");
        movementLine1.setQty(new BigDecimal("21"));
        movementLine1.setLotNo0("111");
        movementLine1.setLotNo1("111");
        movementLine1.setLotNo2("111");
        movementLine1.setLotNo3("111");
        movementLine1.setLotNo4("121212121");
        movementLine1.setLotNo5("111");
        movementLine1.setLotNo6("1212312");
        movementLine1.setLotNo7("111");
        movementLine1.setLotNo8("111");
        movementLine1.setLotNo9("111");

        movementHeader.addMovementLine(movementLine1);

        LOGGER.info("测试结果："+Boolean.toString(movementHeaderService.creatMovement(movementHeader)));
    }

    @Test
    public void auditMovement(){
        LOGGER.info("测试结果："+Boolean.toString(movementHeaderService.auditMovement(new Long("41958740517326848"))));

    }

    @Test
    public void cancelAuditMovement(){
        LOGGER.info("测试结果："+Boolean.toString(movementHeaderService.cancelAuditMovement(new Long("41958740517326848"))));

    }

    @Test
    public void creatAndAuditMovement(){
        MovementHeader movementHeader = new MovementHeader();
        movementHeader.setMovementNo("100010");
        movementHeader.setStoreHouseId(new Long(11));
        movementHeader.setOwnerId("21");
        movementHeader.setOrderDate(new Date());
        movementHeader.setBusinessType("10");
        movementHeader.setBusinessDocId(new Long(11));

        MovementLine movementLine = new MovementLine();
        movementLine.setType("10");
        movementLine.setRelationLineId(new Long(11));
        movementLine.setOwnerId("2");
        movementLine.setMaterielId("1");
        movementLine.setMaterielCode("2121212");
        movementLine.setMaterielName("11111");
        movementLine.setDestinationLocationId(new Long(11));
        movementLine.setUom("2");
        movementLine.setQty(new BigDecimal("12"));
        movementLine.setLotNo0("111");
        movementLine.setLotNo1("111");
        movementLine.setLotNo2("111");
        movementLine.setLotNo3("111");
        movementLine.setLotNo4("121212121");
        movementLine.setLotNo5("111");
        movementLine.setLotNo6("1212312");
        movementLine.setLotNo7("111");
        movementLine.setLotNo8("111");
        movementLine.setLotNo9("111");

        movementHeader.addMovementLine(movementLine);

        MovementLine movementLine1 = new MovementLine();
        movementLine1.setType("10");
        movementLine1.setRelationLineId(new Long(11));
        movementLine1.setOwnerId("2");
        movementLine1.setMaterielId("2");
        movementLine1.setMaterielCode("2121212");
        movementLine1.setMaterielName("111111");
        movementLine1.setDestinationLocationId(new Long(11));
        movementLine1.setUom("2");
        movementLine1.setQty(new BigDecimal("21"));
        movementLine1.setLotNo0("111");
        movementLine1.setLotNo1("111");
        movementLine1.setLotNo2("111");
        movementLine1.setLotNo3("111");
        movementLine1.setLotNo4("121212121");
        movementLine1.setLotNo5("111");
        movementLine1.setLotNo6("1212312");
        movementLine1.setLotNo7("111");
        movementLine1.setLotNo8("111");
        movementLine1.setLotNo9("111");

        movementHeader.addMovementLine(movementLine1);

        LOGGER.info("测试结果："+Boolean.toString(movementHeaderService.createAndAuditMovenment(movementHeader)));
    }

}
