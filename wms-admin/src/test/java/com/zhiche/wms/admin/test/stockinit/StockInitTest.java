package com.zhiche.wms.admin.test.stockinit;

import com.zhiche.wms.WebApplication;
import com.zhiche.wms.admin.test.stockadjust.StockAdjustTest;
import com.zhiche.wms.domain.model.stockinit.StockInitHeader;
import com.zhiche.wms.domain.model.stockinit.StockInitLine;
import com.zhiche.wms.service.stockinit.IStockInitHeaderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zhaoguixin on 2018/6/5.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= WebApplication.class)
public class StockInitTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(StockInitTest.class);

    @Autowired
    private IStockInitHeaderService stockInitHeaderService;


    @Test
    public void creatStockInit(){
        StockInitHeader stockInitHeader = new StockInitHeader();
        stockInitHeader.setInitNo("100020");
        stockInitHeader.setStoreHouseId(new Long(1111));
        stockInitHeader.setOwnerId("2100");
        stockInitHeader.setOrderDate(new Date());

        StockInitLine stockInitLine = new StockInitLine();
        stockInitLine.setOwnerId("2100");
        stockInitLine.setMaterielId("4100");
        stockInitLine.setMaterielCode("4100");
        stockInitLine.setMaterielName("4100");
        stockInitLine.setLocationId(new Long(4200));
        stockInitLine.setUom("10");
        stockInitLine.setQty(new BigDecimal("15"));
        stockInitLine.setLotNo0("111");
        stockInitLine.setLotNo1("111");
        stockInitLine.setLotNo2("111");
        stockInitLine.setLotNo3("111");
        stockInitLine.setLotNo4("111");
        stockInitLine.setLotNo5("111");
        stockInitLine.setLotNo6("111");
        stockInitLine.setLotNo7("111");
        stockInitLine.setLotNo8("111");
        stockInitLine.setLotNo9("111");

        stockInitHeader.addStockInitLine(stockInitLine);

        StockInitLine stockInitLine1 = new StockInitLine();

        stockInitLine1.setOwnerId("2100");
        stockInitLine1.setMaterielId("4200");
        stockInitLine1.setMaterielCode("4200");
        stockInitLine1.setMaterielName("4200");
        stockInitLine1.setLocationId(new Long(4200));
        stockInitLine1.setUom("15");
        stockInitLine1.setQty(new BigDecimal("20"));
        stockInitLine1.setLotNo0("111");
        stockInitLine1.setLotNo1("111");
        stockInitLine1.setLotNo2("111");
        stockInitLine1.setLotNo3("111");
        stockInitLine1.setLotNo4("121212121");
        stockInitLine1.setLotNo5("111");
        stockInitLine1.setLotNo6("1212312");
        stockInitLine1.setLotNo7("111");
        stockInitLine1.setLotNo8("111");
        stockInitLine1.setLotNo9("111");

        stockInitHeader.addStockInitLine(stockInitLine1);

        LOGGER.info("测试结果："+Boolean.toString(stockInitHeaderService.createStockInit(stockInitHeader)));
    }

    @Test
    public void auditStockInit(){
        LOGGER.info("测试结果："+Boolean.toString(stockInitHeaderService.auditStockInit(new Long("41962496571084800"))));

    }

    @Test
    public void cancelAuditStockInit(){
        LOGGER.info("测试结果："+Boolean.toString(stockInitHeaderService.cancelAuditStockInit(new Long("41962496571084800"))));

    }

    @Test
    public void createAndAuditStockInit(){

        StockInitHeader stockInitHeader = new StockInitHeader();
        stockInitHeader.setInitNo("100010");
        stockInitHeader.setStoreHouseId(new Long(11111));
        stockInitHeader.setOwnerId("2100");
        stockInitHeader.setOrderDate(new Date());

        StockInitLine stockInitLine = new StockInitLine();
        stockInitLine.setOwnerId("2100");
        stockInitLine.setMaterielId("3100");
        stockInitLine.setMaterielCode("3100");
        stockInitLine.setMaterielName("3100");
        stockInitLine.setLocationId(new Long(4100));
        stockInitLine.setUom("10");
        stockInitLine.setQty(new BigDecimal("15"));
        stockInitLine.setLotNo0("111");
        stockInitLine.setLotNo1("111");
        stockInitLine.setLotNo2("111");
        stockInitLine.setLotNo3("111");
        stockInitLine.setLotNo4("111");
        stockInitLine.setLotNo5("111");
        stockInitLine.setLotNo6("111");
        stockInitLine.setLotNo7("111");
        stockInitLine.setLotNo8("111");
        stockInitLine.setLotNo9("111");

        stockInitHeader.addStockInitLine(stockInitLine);

        StockInitLine stockInitLine1 = new StockInitLine();

        stockInitLine1.setOwnerId("2100");
        stockInitLine1.setMaterielId("3200");
        stockInitLine1.setMaterielCode("3200");
        stockInitLine1.setMaterielName("3200");
        stockInitLine1.setLocationId(new Long(4200));
        stockInitLine1.setUom("15");
        stockInitLine1.setQty(new BigDecimal("20"));
        stockInitLine1.setLotNo0("111");
        stockInitLine1.setLotNo1("111");
        stockInitLine1.setLotNo2("111");
        stockInitLine1.setLotNo3("111");
        stockInitLine1.setLotNo4("121212121");
        stockInitLine1.setLotNo5("111");
        stockInitLine1.setLotNo6("1212312");
        stockInitLine1.setLotNo7("111");
        stockInitLine1.setLotNo8("111");
        stockInitLine1.setLotNo9("111");

        stockInitHeader.addStockInitLine(stockInitLine1);

        LOGGER.info("测试结果："+Boolean.toString(stockInitHeaderService.createAndAuditStockInit(stockInitHeader)));
    }
}
