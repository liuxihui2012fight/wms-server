package com.zhiche.wms.admin.test.base;

import com.zhiche.wms.WebApplication;
import com.zhiche.wms.admin.aop.AspetAop;
import com.zhiche.wms.service.base.IBusinessDocNumberService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by zhaoguixin on 2018/6/7.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= WebApplication.class)
public class BusinessDocNumber {
    @Autowired
    private IBusinessDocNumberService businessDocNumberService;

    @Test
    public void getNextValue() {

        System.out.println(businessDocNumberService.getInboundNoticeNo());
        System.out.println(businessDocNumberService.getInboundInspectNo());
        System.out.println(businessDocNumberService.getInboundPutAwayNo());
        System.out.println(businessDocNumberService.getOutboundNoticeNo());
        System.out.println(businessDocNumberService.getOutboundPrepareNo());
        System.out.println(businessDocNumberService.getOutboundShipNo());
        System.out.println(businessDocNumberService.getMovementNo());
        System.out.println(businessDocNumberService.getStockInitNo());
        System.out.println(businessDocNumberService.getStockAdjustNo());
    }
}
