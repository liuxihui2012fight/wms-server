package com.zhiche.wms.service.log.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.wms.WebApplication;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.service.base.IStorehouseService;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class ImportShipmentImplTest {

    @Autowired
    private SnowFlakeId snowFlakeId;
    @Autowired
    private IStorehouseService storehouseService;
    @Autowired
    private IOtmOrderReleaseService otmOrderReleaseService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportShipmentImplTest.class);
    ImportShipmentImpl importShipment = new ImportShipmentImpl();
   //目的地wms本地仓库存在时，不做新增处理，返回false
    @Test
    public void insertWmsStore() {
        OtmOrderRelease otmOrderRelease = new OtmOrderRelease();
        otmOrderRelease.setId(snowFlakeId.nextId());
        otmOrderRelease.setDestLocationGid("杨浦铁运库");
        otmOrderRelease.setDestLocationAddress("目的地测试地址");
        otmOrderRelease.setDestLocationProvince("江西");
        otmOrderRelease.setDestLocationName("目的地测试库");
        otmOrderRelease.setDestLocationCounty("南昌");
        otmOrderRelease.setOriginLocationGid("上海中转库");
        otmOrderRelease.setOriginLocationProvince("江西");
        otmOrderRelease.setOriginLocationCity("南昌");
        otmOrderRelease.setOriginLocationAddress("发运地测试库");
        otmOrderRelease.setOriginLocationName("发运地测试库");
        Boolean result = sertWmsStore(otmOrderRelease);
        LOGGER.info("目的地，发运地都存在wms测试结果====="+result);
    }

    //目的地不存存在wms仓库中，新增目的地仓库，返回结果为true
    @Test
    public void DestNotWmsStore() {
        OtmOrderRelease otmOrderRelease = new OtmOrderRelease();
        otmOrderRelease.setId(snowFlakeId.nextId());
        otmOrderRelease.setDestLocationGid("目的地不存在测试3");
        otmOrderRelease.setDestLocationAddress("目的地测试地址");
        otmOrderRelease.setDestLocationProvince("江西");
        otmOrderRelease.setDestLocationName("目的地测试库");
        otmOrderRelease.setDestLocationCounty("南昌");
        otmOrderRelease.setOriginLocationGid("杨浦铁运库");
        otmOrderRelease.setOriginLocationProvince("江西");
        otmOrderRelease.setOriginLocationCity("南昌");
        otmOrderRelease.setOriginLocationAddress("发运地测试库");
        otmOrderRelease.setOriginLocationName("发运地测试库");
        Boolean result = sertWmsStore(otmOrderRelease);
        LOGGER.info("目的地不存在，发运地存在wms测试结果====="+result);
    }

    private Boolean sertWmsStore(OtmOrderRelease otmOrderRelease) {
        EntityWrapper<Storehouse> em = new EntityWrapper<>();
        em.eq("code", otmOrderRelease.getDestLocationGid());
        //i1表示wms是否存在订单目的地地址
        int i1 = storehouseService.selectCount(em);
        //如果订单中目的地不存在wms，wms本地做目的地仓库新增处理
        if (i1 < 1) {
            Storehouse storehouse = new Storehouse();
            storehouse.setId(snowFlakeId.nextId());
            storehouse.setCode(otmOrderRelease.getDestLocationGid());
            storehouse.setName(otmOrderRelease.getDestLocationName());
            storehouse.setProvince(otmOrderRelease.getDestLocationProvince());
            storehouse.setCity(otmOrderRelease.getDestLocationCity());
            storehouse.setAddress(otmOrderRelease.getDestLocationAddress());
            storehouse.setProperty(TableStatusEnum.STATUS_10.getCode());
            storehouse.setStatus(TableStatusEnum.STATUS_10.getCode());
            storehouse.setGmtCreate(new Date());
            storehouse.setUserCreate("admin");
            storehouse.setRemark("目的库wms本地新增");
            storehouseService.insert(storehouse);
            return true;
        } else {//否则则不做新增处理
            return false;
        }
    }
}