package com.zhiche.wms.service.opbaas.impl;

import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import org.codehaus.jackson.map.Serializers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import com.zhiche.wms.core.supports.BaseException;

import static org.junit.jupiter.api.Assertions.*;

class OrderReleaseServiceImplTest {
    OrderReleaseServiceImpl orderReleaseService = new OrderReleaseServiceImpl();

    @Test
    public void shouldSuccessIn24Hours() {
        // 发运时间比当前时间不超过24小时，符合条件时
        String inputTime = "2020-01-17 08:00:00";
        // 发运时间判断方法返回boolean值
        Boolean result = orderReleaseService.releaseDespatchTime(inputTime);
        assertTrue(result);
    }

    @Test
    public void shouldRaiseExceptionWhenAfter24Hours() {
        String inputTime = "2020-01-19 19:00:00";
        BaseException e = assertThrows(BaseException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                orderReleaseService.releaseDespatchTime(inputTime);
            }
        });
        assertTrue("发运时间不能超过当前时间24小时".equalsIgnoreCase(e.getMessage()));
    }




    @Test
    public void shouldRaiseExceptionWhenInputNull() {
        BaseException e = assertThrows(BaseException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                orderReleaseService.releaseDespatchTime(null);
            }
        });
        assertTrue("发运时间不能为空".equalsIgnoreCase(e.getMessage()));
    }


    @Test
    public void shouldRaiseExceptionWhenInputEmpty() {
        BaseException e = assertThrows(BaseException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                orderReleaseService.releaseDespatchTime("");
            }
        });
        assertTrue("发运时间不能为空".equalsIgnoreCase(e.getMessage()));
    }

    @Test
    public void whenShipStatusInbound(){
        //指令信息表示已入库状态
        String shipStatus = "BS_INBOUND" ;
        //返回boolean结果用于判断是否进行指令状态更改操作
        Boolean result = orderReleaseService.updateShipStatus(shipStatus);
        assertFalse(result);
    }

    @Test
    public void whenShipStatusNotInbound(){
        //指令状态为需发运时
         String shipStatus = "is_ship";
        Boolean result = orderReleaseService.updateShipStatus(shipStatus);
        assertTrue(result);
    }

    @Test
    public void whenShipStatusIsNull(){
        //指令状态为null时
        String shipStatus = null;
        Boolean result = orderReleaseService.updateShipStatus(shipStatus);
        assertTrue(result);
    }

    @Test
    public void whenShipStatusIsEmpty(){
        //指令状态为null时
        String shipStatus = " ";
        Boolean result = orderReleaseService.updateShipStatus(shipStatus);
        assertTrue(result);
    }

}
